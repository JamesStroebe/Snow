#pragma once

#include <Snow.h>

struct RenderComponent : public Snow::Entity::Component<RenderComponent> {
	bool Visible;
};

struct ColorComponent : public Snow::Entity::Component<ColorComponent> {
	Snow::Math::Vector4f Color;
};

struct SpriteComponent : public Snow::Entity::Component<SpriteComponent> {
	Snow::Core::Ref<Snow::Render::API::Texture2D> Texture;
};

struct TransformComponent : public Snow::Entity::Component<TransformComponent> {
	Snow::Math::Vector2f Position;
	Snow::Math::Vector2f Scale;
};

struct CameraComponent : public Snow::Entity::Component<CameraComponent> {
	Snow::Math::float32_t fovAngle;
	Snow::Math::Vector2f CameraSize;
};

struct MovementComponent : public Snow::Entity::Component<MovementComponent> {
	Snow::Math::Vector2f delta;
	Snow::Core::InputManager inputManager;
};