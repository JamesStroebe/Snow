#pragma once

#include <Snow.h>

#include "Components.h"
#include "Systems.h"

using ECS = Snow::Entity::EntityComponentSystem;
class CoreLayer : public Snow::Core::Layer {
public:
	CoreLayer();
	~CoreLayer();

	virtual void OnAttach() override;
	virtual void OnDetach() override;
	virtual void OnEvent(Snow::Core::Event::Event& e) override;
	virtual void OnUpdate(Snow::Core::Time::Timestep ts) override;
	virtual void OnRender() override;

	virtual void OnImGuiRender() override;

private:
	Snow::Entity::EntityHandle m_PlayerHandle;

	RenderSystem m_RenderSystem;
	Snow::Entity::SystemList m_RenderSystemList;
	MovementSystem m_MovementSystem;
	Snow::Entity::SystemList m_MovementSystemList;

	Snow::Scene::Scene2D m_Scene;
	Snow::Render::Camera::OrthographicCamera* m_OrthoCamera = nullptr;

	Snow::Core::Ref<Snow::Render::API::Texture2D> m_PlayerTexture;

	Snow::Core::Ref<Snow::Render::API::RenderPass> m_RenderPass;
	Snow::Core::Ref<Snow::Render::API::Framebuffer> m_Framebuffer;
	Snow::Core::Ref<Snow::Render::API::Pipeline> m_InvertPipeline;
	Snow::Core::Ref<Snow::Render::API::VertexBuffer> m_InvertVBO;
	Snow::Core::Ref<Snow::Render::API::IndexBuffer> m_InvertIBO;

};