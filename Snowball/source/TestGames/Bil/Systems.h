#pragma once

#include <Snow.h>

class RenderSystem : public Snow::Entity::BaseSystem {
public:
	RenderSystem();
	virtual void UpdateComponents(Snow::Core::Time::Timestep ts, Snow::Entity::BaseComponent** comps) override;
};

class MovementSystem : public Snow::Entity::BaseSystem {
public:
	MovementSystem();
	virtual void UpdateComponents(Snow::Core::Time::Timestep ts, Snow::Entity::BaseComponent** comps) override;
};