#pragma once


enum class GameState {
	Menu = 0,
	Running,
	Dead,
	Stopped
};