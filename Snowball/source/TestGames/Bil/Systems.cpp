#include "Systems.h"

#include "Components.h"

RenderSystem::RenderSystem() {
	AddComponentType(RenderComponent::ID);
	AddComponentType(TransformComponent::ID);
	AddComponentType(ColorComponent::ID);
	AddComponentType(SpriteComponent::ID, Snow::Entity::ComponentFlags::Optional);
}

void RenderSystem::UpdateComponents(Snow::Core::Time::Timestep ts, Snow::Entity::BaseComponent** comps) {
	RenderComponent* renderComp = (RenderComponent*)comps[0];
	TransformComponent* transformComp = (TransformComponent*)comps[1];
	ColorComponent* colorComp = (ColorComponent*)comps[2];
	SpriteComponent* spriteComp = (SpriteComponent*)comps[3];

	if (renderComp->Visible) {
		if (spriteComp != nullptr) 
			Snow::Render::Renderer2D::SubmitTexturedQuad(transformComp->Position, transformComp->Scale, spriteComp->Texture);
		else
			Snow::Render::Renderer2D::SubmitColoredQuad(transformComp->Position, transformComp->Scale, colorComp->Color);
	}
}

MovementSystem::MovementSystem() {
	AddComponentType(MovementComponent::ID);
}

void MovementSystem::UpdateComponents(Snow::Core::Time::Timestep ts, Snow::Entity::BaseComponent** comps) {
	MovementComponent* movementComp = (MovementComponent*)comps[0];

	if (movementComp->inputManager.IsKeyPressed(SNOW_KEY_W))
		movementComp->delta = Snow::Math::Vector2f(0.0f, 1.0f) * ts.GetSeconds();
	else if (movementComp->inputManager.IsKeyPressed(SNOW_KEY_S))
		movementComp->delta = Snow::Math::Vector2f(-1.0f, 0.0f) * ts.GetSeconds();

	if (movementComp->inputManager.IsKeyPressed(SNOW_KEY_D))
		movementComp->delta = Snow::Math::Vector2f(1.0f, 0.0f) * ts.GetSeconds();
	else if (movementComp->inputManager.IsKeyPressed(SNOW_KEY_A))
		movementComp->delta = Snow::Math::Vector2f(-1.0f, 0.0f) * ts.GetSeconds();
}