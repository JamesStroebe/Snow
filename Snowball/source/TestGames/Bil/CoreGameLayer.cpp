#include "CoreGameLayer.h"

CoreLayer::CoreLayer() :
	Snow::Core::Layer("CoreGame"){
	CameraComponent cameraComp;
	cameraComp.fovAngle = 45.0f;
	cameraComp.CameraSize = Snow::Math::Vector2f(30, 20);
	m_OrthoCamera = new Snow::Render::Camera::OrthographicCamera(-cameraComp.CameraSize.x / 2, cameraComp.CameraSize.x / 2, -cameraComp.CameraSize.y / 2, cameraComp.CameraSize.y / 2);
	
	m_Scene.SetCamera(*m_OrthoCamera);

	m_PlayerTexture = Snow::Render::API::Texture2D::Create("assets/textures/Wood.png");

	RenderComponent renderComp;
	renderComp.Visible = true;

	TransformComponent transform;
	transform.Position = { 5, 5 };
	transform.Scale = { 1, 1 };

	MovementComponent movement;
	movement.inputManager = *Snow::Core::Input::GetInputManager();

	SpriteComponent sprite;
	sprite.Texture = m_PlayerTexture;

	m_PlayerHandle = ECS::CreateEntity(renderComp, transform, movement, sprite);

	m_RenderSystem = RenderSystem();
	m_RenderSystemList.AddSystem(m_RenderSystem);

	m_MovementSystem = MovementSystem();
	m_MovementSystemList.AddSystem(m_MovementSystem);
}

CoreLayer::~CoreLayer() {

}

void CoreLayer::OnAttach() {

	

	Snow::Render::API::PipelineSpecification invertPipelineSpec;
	invertPipelineSpec.Shaders = { Snow::Render::Shader::Shader::Create("assets/shaders/glsl/InvertVert.glsl", Snow::Render::Shader::ShaderType::VertexShader), Snow::Render::Shader::Shader::Create("assets/shaders/glsl/InvertFrag.glsl", Snow::Render::Shader::ShaderType::PixelShader) };
	invertPipelineSpec.VertexBufferLayout = {
		{Snow::Render::API::VertexAttribType::Float3, "POSITION"},
		{Snow::Render::API::VertexAttribType::Float2, "TEXCOORD"}
	};

	m_InvertPipeline = Snow::Render::API::Pipeline::Create(invertPipelineSpec);

	Snow::Render::Renderer::GetCommandBuffer()->Execute();

	float positions[20] = {
		-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
		 1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
		 1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
		-1.0f,  1.0f, 0.0f, 0.0f, 1.0f
	};

	uint32_t indices[6] = {
		0, 1, 2, 2, 3, 0
	};

	m_InvertVBO = Snow::Render::API::VertexBuffer::Create(positions, 20 * sizeof(float));
	m_InvertIBO = Snow::Render::API::IndexBuffer::Create(indices, 6 * sizeof(uint32_t));

	Snow::Render::API::FramebufferSpecification framebufferSpec;
	framebufferSpec.Format = Snow::Render::API::FramebufferFormat::RGBA8;
	framebufferSpec.Color = Snow::Math::Vector4f(1.0, 1.0, 1.0, 1.0);
	framebufferSpec.Size = Snow::Math::Vector2f(640, 480);
	framebufferSpec.Samples = 1;
	framebufferSpec.AttachmentTypes = (Snow::Render::API::FramebufferType)(Snow::Render::API::FramebufferType::RENDER_BUFFER_COLOR | Snow::Render::API::FramebufferType::RENDER_BUFFER_DEPTH);
	
	m_Framebuffer = Snow::Render::API::Framebuffer::Create(framebufferSpec);

	Snow::Render::API::RenderPassSpecification renderpassSpec;
	renderpassSpec.TargetFramebuffer = m_Framebuffer;
	m_RenderPass = Snow::Render::API::RenderPass::Create(renderpassSpec);
}

void CoreLayer::OnDetach() {

}

void CoreLayer::OnEvent(Snow::Core::Event::Event& e) {
	m_OrthoCamera->OnEvent(e);
}

void CoreLayer::OnUpdate(Snow::Core::Time::Timestep ts) {
	ECS::UpdateSystems(m_MovementSystemList, ts);
}

void CoreLayer::OnRender() {

	Snow::Render::Renderer::GetCommandBuffer()->BeginRenderPass(m_RenderPass);
	Snow::Render::Renderer2D::BeginScene(m_Scene);
	ECS::UpdateSystems(m_RenderSystemList, 0);
	Snow::Render::Renderer2D::EndScene();
	Snow::Render::Renderer2D::Present();
	Snow::Render::Renderer::GetCommandBuffer()->EndRenderPass();


	m_Framebuffer->BindColorAttachment();
	Snow::Render::Renderer::SetDepthTesting(false);
	Snow::Render::Renderer::GetCommandBuffer()->SubmitElements(m_InvertPipeline, m_InvertVBO, m_InvertIBO);
	Snow::Render::Renderer::SetDepthTesting(true);
}

void CoreLayer::OnImGuiRender() {

}