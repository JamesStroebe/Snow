#pragma once

#include <Snow.h>
#include <Snow/EntryPoint.h>

#include "CoreGameLayer.h"

class App : public Snow::Core::Application {
public:
	App(const Snow::Core::WindowProps& props) :
		Snow::Core::Application(props) {
		PushLayer(new CoreLayer());
	}

	~App() {

	}

	virtual void OnShutdown() override {
		m_Running = false;
		Snow::Render::Renderer::Shutdown();
	}

	virtual void OnRender() override {}
};

Snow::Core::Application* Snow::Core::CreateApplication() {
	Snow::Render::Renderer::SetRenderAPI(Snow::Render::RenderAPI::OPENGL);
	return new App(Snow::Core::WindowProps(640, 480, "GameTest"));
}