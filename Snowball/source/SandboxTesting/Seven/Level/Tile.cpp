#include "Tile.h"

Tile::Tile(int x, int y, Snow::Render::Sprite::Sprite sprite) :
	m_Sprite(sprite) {
	m_Pos = { x,y };
}

Tile::~Tile() {

}

void Tile::OnRender() {

	Snow::Render::Renderer2D::SubmitSprite({ m_Pos.x, m_Pos.y }, 1.0f, m_Sprite);
}