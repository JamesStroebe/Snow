#pragma once

#include <Snow.h>


class Tile {
public:
	Tile() = default;
	Tile(int x, int y, Snow::Render::Sprite::Sprite sprite);
	~Tile();

	void OnRender();

	inline Snow::Math::Vector2f GetPos() { return m_Pos; }

	inline Snow::Render::Sprite::Sprite GetSprite() { return m_Sprite; }
private:

	Snow::Math::Vector2f m_Pos;
	Snow::Render::Sprite::Sprite m_Sprite;
};