#include "Level.h"

Level::Level(int width, int height) {
	m_Width = width;
	m_Height = height;

	Snow::Render::API::TextureSpecification textureSpec;
	textureSpec.Filter = Snow::Render::API::TextureFilter::Nearest;

	m_TileSet = Snow::Render::Sprite::SpriteSheet("assets/textures/TileSet.png", textureSpec);
	Snow::Render::Sprite::Sprite dirtSprite = Snow::Render::Sprite::Sprite(0, 960, 32, 32, m_TileSet);
	Snow::Render::Sprite::Sprite grassSprite = Snow::Render::Sprite::Sprite(0, 992, 32, 32, m_TileSet);
	Snow::Render::Sprite::Sprite sandSprite = Snow::Render::Sprite::Sprite(0, 928, 32, 32, m_TileSet);
	//dirtSprite.GetTexture()->Bind();
	//Snow::Renderer2D::AddTexture(dirtSprite.GetTexture());
	//grassSprite.GetTexture()->Bind();
	//Snow::Renderer2D::AddTexture(grassSprite.GetTexture());

	//m_Tiles.resize(m_Width * m_Height);
	for (int j = 0; j <= m_Height; j++) {
		for (int i = 0; i <= m_Width; i++) {
			if (j==m_Height)
				m_Tiles.push_back(Tile(i * 1, j * 1, grassSprite));
			else if(j==m_Height-1)
				m_Tiles.push_back(Tile(i * 1, j * 1, sandSprite));
			else
				m_Tiles.push_back(Tile(i * 1, j * 1, dirtSprite));
		}
	}
}

Level::~Level() {
	m_Tiles.empty();
}

void Level::OnRender() {
	
	for (auto& tile : m_Tiles)
		tile.OnRender();
}

void Level::GetMouseCoord(uint32_t mouseX, uint32_t mouseY) {
	SNOW_CORE_TRACE("{0}, {1}", mouseX, mouseY);
}



void Level::AddTile(int x, int y) {
	//Snow::Sprite grassSprite = Snow::Sprite(0, 992, 32, 32, m_TileSet);
	//m_Tiles.push_back(Tile(x, y, grassSprite));
}