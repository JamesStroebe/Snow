#pragma once

#include <Snow.h>
#include "Tile.h"

class Level {
public:
	Level() = default;
	Level(int width, int height);
	~Level();


	//std::optional<Tile> GetTile(int x, int y) { return m_Tiles[x + y * m_Width]; }
	void OnRender();

	void GetMouseCoord(uint32_t mouseX, uint32_t mouseY);

	void AddTile(int x, int y);
private:

	int m_Width, m_Height;
	Snow::Render::Sprite::SpriteSheet m_TileSet;
	std::vector<Tile> m_Tiles;
};