#include <Snow.h>
#include <Snow/EntryPoint.h>

#include "Snowball2D.h"
#include "DirectXTest.h"
#include "VulkanTest.h"

class Snowball : public Snow::Core::Application {
public:
	Snowball(const Snow::Core::WindowProps& props) :
		Snow::Core::Application(props) {
#if 0
		PushLayer(new VulkanLayer());
#endif
#if 1
		PushLayer(new DirectXTest());
#endif
#if 0
		PushLayer(new Snowball2D());
#endif
#if 0
		PushLayer(new GameLayer());
#endif
	}

	virtual void OnShutdown() override {
		m_Running = false;
		Snow::Render::Renderer::Shutdown();
		//Snow::Entity::EntityComponentSystem::Shutdown();
	}

	virtual void OnRender() override {

	}
};

Snow::Core::Application* Snow::Core::CreateApplication() {
	Snow::Render::Renderer::SetRenderAPI(Snow::Render::RenderAPI::OPENGL);
	return new Snowball(Snow::Core::WindowProps(1280, 720, "Snow"));
}