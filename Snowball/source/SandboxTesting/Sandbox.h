#pragma once

#include <Snow.h>

class SandboxLayer : public Snow::Core::Layer {

	public:
		SandboxLayer();
		~SandboxLayer() = default;

		virtual void OnAttach() override;
		virtual void OnDetach() override;
		virtual void OnUpdate(Snow::Core::Time::Timestep ts) override;
		virtual void OnRender() override;
		virtual void OnEvent(Snow::Core::Event::Event& e) override;

	private:
		Snow::Render::Camera::OrthographicCamera m_Camera;
		Snow::Core::Ref<Snow::Render::API::Texture2D> m_MarbleTexture;
		Snow::Core::Scope<Snow::Render::API::Framebuffer> m_FrameBuffer, m_FinalFrameBuffer;
		float width = 12.0f;
		float height = 3.0f;
		float increment = 0.111f;
		float increment2 = 0.01f;
		float incrementR = 0.001f, incrementG = 0.001f;
		float r = 0.5f;
		float g = 0.1f;

		Snow::Core::Buffer buffer;
};		