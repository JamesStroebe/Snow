#include "Sandbox.h"

SandboxLayer::SandboxLayer() :
	Layer("SandboxLayer"), m_Camera(-1.6f, 1.6f, -0.9f, 0.9f) {}



void SandboxLayer::OnAttach() {
	//m_FrameBuffer.reset(Snow::Framebuffer::Create(1280, 720, Snow::FramebufferFormat::RGBA16F));
	//m_FinalFrameBuffer.reset(Snow::Framebuffer::Create(1280, 720, Snow::FramebufferFormat::RGBA8));

	//m_MarbleTexture = Snow::Texture2D::Create("assets/textures/Marble.jpg");
	//m_MarbleTexture->Bind();
}


void SandboxLayer::OnDetach() {

}

void SandboxLayer::OnUpdate(Snow::Core::Time::Timestep ts) {
	//m_Camera.OnUpdate(ts);
	//
	//Snow::Renderer::Clear(Snow::RenderBufferType::RENDER_BUFFER_COLOR | Snow::RenderBufferType::RENDER_BUFFER_DEPTH);
	//
	//Snow::Application& app = Snow::Application::Get();
	//m_FrameBuffer->Resize(app.GetWindow().GetWidth(), app.GetWindow().GetHeight());
	//m_FinalFrameBuffer->Resize(app.GetWindow().GetWidth(), app.GetWindow().GetHeight());
	//m_FrameBuffer->Bind();
	//
	//Snow::Renderer2D::BeginScene(m_Camera);
	//
	//for (int i = 0; i < 4; i++) {
	//	for (int j = 0; j < 10; j++) {
	//		Snow::Renderer2D::Submit({ i/10.0f, j/10.0f }, { 0.2, 0.2 }, { r, g, width / 50.0f, 1.0f });
	//	}
	//}
	//
	//Snow::Renderer2D::Submit({ -0.5f, 0.0f }, { 0.2, 0.2 }, {r, g, width / 50.0f, 1.0f});
	//Snow::Renderer2D::Submit({ -0.1f, -0.2f }, { 0.2, 0.2 }, {r, g, width / 100.0f, 1.0f});
	////Snow::Renderer2D::Submit({ -0.5f, 0.4f }, { 0.3, 0.3 }, m_MarbleTexture);
	////Snow::Renderer2D::Submit({ 0.0f, 0.0f }, { 0.1f, 0.1f }, m_MarbleTexture);
	//Snow::Renderer2D::EndScene();
	////Snow::Renderer2D::Present();
	////Snow::Renderer2D::BeginScene(m_Camera);
	////Snow::Renderer2D::EndScene();
	////m_FrameBuffer->BindTexture();
	////Snow::Renderer2D::Present();
	////Snow::Renderer2D::BeginScene(m_Camera);
	////Snow::Renderer2D::EndScene();
	//
	//m_FrameBuffer->Unbind();
	//m_FinalFrameBuffer->Bind();
	//m_FrameBuffer->BindTexture();
	//Snow::Renderer2D::Present();
	//
	//Snow::Renderer::Present();
	//
	////m_FinalFrameBuffer->Unbind();
	
	if (Snow::Core::Input::IsKeyPressed(SNOW_KEY_F4, SNOW_MOD_LEFT_ALT)) {
		auto& app = Snow::Core::Application::Get();
		app.OnShutdown();
	}

	width += increment;
	height += increment2;
	r += incrementR; g += incrementG;
	
	if (width > 50)
		increment *= -1;
	else if (width < 10)
		increment *= -1;
	
	if (height > 5)
		increment2 *= -1;
	else if (height < 1)
		increment2 *= -1;
	
	if (r > 1)
		incrementR *= -1;
	else if (r < 0)
		incrementR *= -1;
	
	if (g > 1)
		incrementG *= -1;
	else if (g < 0)
		incrementG *= -1;
}

void SandboxLayer::OnRender() {

}

void SandboxLayer::OnEvent(Snow::Core::Event::Event& e) {
	m_Camera.OnEvent(e);
}