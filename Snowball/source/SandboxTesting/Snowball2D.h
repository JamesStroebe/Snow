#pragma once

#include <Snow.h>

#include "Seven/Level/Level.h"

class Snowball2D : public Snow::Core::Layer {
public:
	Snowball2D();
	virtual ~Snowball2D() = default;

	virtual void OnAttach() override;
	virtual void OnDetach() override;

	void OnUpdate(Snow::Core::Time::Timestep ts) override;
	void OnRender() override;
	void OnEvent(Snow::Core::Event::Event& e) override;
private:
	Snow::Core::Ref<Snow::Render::API::RenderPass> m_RenderPass;
	Snow::Core::Ref<Snow::Render::API::Framebuffer> m_Framebuffer;
	Snow::Core::Ref<Snow::Render::API::Pipeline> m_Pipeline;

	//Snow::Ref<Snow::Material> m_Material;


	//Snow::Render::Camera::PerspectiveCamera m_Camera;
	Snow::Render::Camera::OrthographicCamera m_OrthoCamera;
	Snow::Core::Ref<Snow::Render::API::Texture2D> m_Texture, m_Snow, m_Blank;

	Snow::Core::Ref<Snow::Render::Shader::Shader> m_QuadShader, m_HDRShader;

	Snow::Render::Sprite::SpriteSheet m_Sheet;
	Snow::Render::Sprite::Sprite m_Sprite, m_Sprite2;

	Level m_Level;



	float r = 1.0f / (float)size, g = 1.0f / (float)size;
	float increment = 0.01f;

	int size = 200;

	Snow::Core::Ref<Snow::Render::Mesh> m_Purp, m_Plane;
	Snow::Core::Ref<Snow::Render::API::Texture2D> m_BRDFLUT;

	struct AlbedoInput {
		Snow::Math::Vector3f Color;
		Snow::Core::Ref<Snow::Render::API::Texture2D> TextureMap;

	};
	AlbedoInput m_AlbedoInput;

	struct NormalInput {
		Snow::Core::Ref<Snow::Render::API::Texture2D> TextureMap;
		bool UseTexture = true;
	};
	NormalInput m_NormalInput;

	struct MetalnessInput {
		float Value = 0.5f;
		Snow::Core::Ref<Snow::Render::API::Texture2D> TextureMap;

	};
	MetalnessInput m_MetalnessInput;

	struct RoughnessInput {
		float Value = 0.5f;
		Snow::Core::Ref<Snow::Render::API::Texture2D> TextureMap;
	};
	RoughnessInput m_RoughnessInput;

	Snow::Core::Ref<Snow::Render::API::TextureCube> m_EnvCubeMap, m_EnvIrradiance;

	struct Light {
		Snow::Math::Vector3f Direction;
		Snow::Math::Vector3f Radiance;
	};
	Light m_Light;

	bool m_Prefilter = false;
	float m_EnvRotation = 0.0f;

	std::vector<Snow::Core::Ref<Snow::Render::MaterialInstance>> m_MaterialInstances;
};