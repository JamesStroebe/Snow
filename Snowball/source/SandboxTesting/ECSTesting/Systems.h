#pragma once

#include <Snow.h>

class RenderSystem : public Snow::Entity::BaseSystem {
public:
	RenderSystem();
	virtual void UpdateComponents(Snow::Core::Time::Timestep ts, Snow::Entity::BaseComponent** comps) override;
};

class CameraSystem : public Snow::Entity::BaseSystem {
public:
	CameraSystem();
	virtual void UpdateComponents(Snow::Core::Time::Timestep ts, Snow::Entity::BaseComponent** comps) override;
};

class ImGuiSystem : public Snow::Entity::BaseSystem {
public:
	ImGuiSystem();
	virtual void UpdateComponents(Snow::Core::Time::Timestep ts, Snow::Entity::BaseComponent** comps) override;
};