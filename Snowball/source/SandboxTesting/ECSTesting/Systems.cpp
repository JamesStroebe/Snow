#include "Systems.h"

#include "Components.h"

#include <imgui.h>

using namespace Snow;

RenderSystem::RenderSystem() {
	AddComponentType(RenderableComponent::ID);
	AddComponentType(ColorComponent::ID);
	AddComponentType(TransformComponent::ID);
	AddComponentType(SpriteComponent::ID, Entity::ComponentFlags::Optional);
}

void RenderSystem::UpdateComponents(Snow::Core::Time::Timestep ts, Snow::Entity::BaseComponent** comps) {
	RenderableComponent* renderable = (RenderableComponent*)comps[0];
	ColorComponent* color = (ColorComponent*)comps[1];
	TransformComponent* transform = (TransformComponent*)comps[2];
	SpriteComponent* sprite = (SpriteComponent*)comps[3];

	//if (comps[2]->)
	//	SpriteComponent* sprite = (SpriteComponent*)comps[2];
	
	if (renderable->Visible) {

		if (sprite) {
			if (transform->Rotation == 0)
				Snow::Render::Renderer2D::SubmitTexturedQuad(transform->Position, transform->Scale, sprite->SpriteSheet);
			else
				Snow::Render::Renderer2D::SubmitRotatedTexturedQuad(transform->Position, transform->Scale, transform->Rotation, sprite->SpriteSheet, color->Color);
		}
		else {
			if (transform->Rotation <= 5)
				Snow::Render::Renderer2D::SubmitColoredQuad(transform->Position, transform->Scale, color->Color);
			else
				Snow::Render::Renderer2D::SubmitRotatedColoredQuad(transform->Position, transform->Scale, Snow::Math::radians(transform->Rotation), color->Color);
		}
		
	}
}

CameraSystem::CameraSystem() {
	AddComponentType(CameraComponent::ID);
	AddComponentType(MovementComponent::ID);
}

void CameraSystem::UpdateComponents(Snow::Core::Time::Timestep ts, Snow::Entity::BaseComponent** comps) {
	CameraComponent* cameraComp = (CameraComponent*)comps[0];
	MovementComponent* movementComp = (MovementComponent*)comps[1];
	
	if (movementComp->InputManager->IsKeyPressed(SNOW_KEY_W)) {
		movementComp->Movement = Math::Vector3f(0.0f, 1.0f, 0.0f) * ts.GetSeconds();
		cameraComp->Position += movementComp->Movement;
	}
	else if (movementComp->InputManager->IsKeyPressed(SNOW_KEY_S)) {
		movementComp->Movement = Math::Vector3f(0.0f, -1.0f, 0.0f) * ts.GetSeconds();
		cameraComp->Position += movementComp->Movement;
	}

	if (movementComp->InputManager->IsKeyPressed(SNOW_KEY_D)) {
		movementComp->Movement = Math::Vector3f(1.0f, 0.0f, 0.0f) * ts.GetSeconds();
		cameraComp->Position += movementComp->Movement;
	}
	else if (movementComp->InputManager->IsKeyPressed(SNOW_KEY_A)) {
		movementComp->Movement = Math::Vector3f(-1.0f, 0.0f, 0.0f) * ts.GetSeconds();
		cameraComp->Position += movementComp->Movement;
	}

	//if (cameraComp->Position.y != 0) {
	//	int y = 4;
	//	return;
	//}
		
}

ImGuiSystem::ImGuiSystem() {
	AddComponentType(TransformComponent::ID);
	AddComponentType(ColorComponent::ID);
	AddComponentType(SpriteComponent::ID);
}

void ImGuiSystem::UpdateComponents(Snow::Core::Time::Timestep ts, Snow::Entity::BaseComponent** comps) {
	TransformComponent* transformComp = (TransformComponent*)comps[0];
	ColorComponent* colorComp = (ColorComponent*)comps[1];

	ImGui::Begin("ComponentData");
	ImGui::SliderFloat2("Position", Math::valuePtr(transformComp->Position), -6.0, 6.0);
	ImGui::SliderFloat("Rotation", &(transformComp->Rotation), 0.0, 2 * Math::pi<float>());
	ImGui::ColorEdit4("Color", Math::valuePtr(colorComp->Color));
	ImGui::End();

}