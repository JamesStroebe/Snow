#pragma once

#include "Snow.h"

struct RenderableComponent : public Snow::Entity::Component<RenderableComponent> {
	bool Visible;
};

struct ColorComponent : public Snow::Entity::Component<ColorComponent> {
	Snow::Math::Vector4f Color;
};

struct SpriteComponent : public Snow::Entity::Component<SpriteComponent> {
	Snow::Core::Ref<Snow::Render::API::Texture2D> SpriteSheet;
	Snow::Math::Vector2f Position, Scale;
};

struct TransformComponent : public Snow::Entity::Component<TransformComponent> {
	Snow::Math::Vector3f Position;
	Snow::Math::float32_t Rotation;
	Snow::Math::Vector2f Scale;
};

struct CameraComponent : public Snow::Entity::Component<CameraComponent> {
	Snow::Math::Vector3f Position;
};

struct MovementComponent : public Snow::Entity::Component<MovementComponent> {
	Snow::Math::Vector3f Movement;
	Snow::Core::InputManager* InputManager;
};
