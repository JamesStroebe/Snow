#pragma once

#include <Snow.h>

class VulkanLayer : public Snow::Core::Layer {
public:
	VulkanLayer();
	virtual ~VulkanLayer() = default;

	virtual void OnAttach() override;
	virtual void OnDetach() override;

	virtual void OnUpdate(Snow::Core::Time::Timestep ts) override;
	virtual void OnRender() override;
	virtual void OnEvent(Snow::Core::Event::Event& e) override;
private:
	Snow::Core::Ref<Snow::Render::API::RenderPass> m_RenderPass;
	Snow::Core::Ref<Snow::Render::API::Framebuffer> m_Framebuffer;

	Snow::Core::Ref<Snow::Render::API::Pipeline> m_Pipeline;
};
