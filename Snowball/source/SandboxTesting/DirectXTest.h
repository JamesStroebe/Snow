#pragma once

#include <Snow.h>

#include "Seven/Level/Level.h"

#include "ECSTesting/Components.h"
#include "ECSTesting/Systems.h"

using namespace Snow;

using namespace Snow::Core::Serialize;
using ECS = Snow::Entity::EntityComponentSystem;

class DirectXTest : public Core::Layer {
public:
	DirectXTest();
	virtual ~DirectXTest() = default;

	virtual void OnAttach() override;
	virtual void OnDetach() override;

	virtual void OnUpdate(Core::Time::Timestep ts) override;
	virtual void OnRender() override;
	virtual void OnImGuiRender() override;
	virtual void OnEvent(Core::Event::Event& e) override;
private:
	Core::Ref<Snow::Render::API::RenderPass> m_RenderPass;
	Core::Ref<Snow::Render::API::Framebuffer> m_Framebuffer, m_InvertFramebuffer;

	Scene::Scene3D m_Scene;
	Scene::Scene2D m_Scene2D;
	Render::Camera::PerspectiveCamera m_PerspectiveCamera;
	Render::Camera::OrthographicCamera* m_OrthoCamera;
	Core::Ref<Render::API::Texture2D> m_Texture;

	Core::Ref<Render::API::Pipeline> m_InvertPipeline;

	Core::Ref<Render::API::VertexBuffer> m_InvertVBO;
	Core::Ref<Render::API::IndexBuffer> m_InvertIBO;

	Core::Ref<Render::Mesh> m_Mesh;

	SerializeDatabase* m_Database;

	Snow::Entity::SystemList m_RenderSystemList;
	RenderSystem m_RenderSystem;
	Snow::Entity::SystemList m_ImGuiSystemList;
	ImGuiSystem m_ImGuiSystem;
	Snow::Entity::SystemList m_CameraSystemList;
	CameraSystem m_CameraSystem;

	Snow::Entity::EntityHandle m_CameraEntity;
	Snow::Entity::EntityHandle m_SnowEntity;
	std::vector<Snow::Entity::EntityHandle> m_Entities;
	//Snow::Render::Font::Font* m_Arial;

	Level m_Level;
	float angle = 1.0f;

	Math::Vector3f m_TextureTint = { 1.0f, 0.5f, 1.0f };
	Math::Vector2f m_TexturePosition = { 0.0f, 0.0f };
	Math::float32_t m_TextureSize = 1.0f;
	Math::float32_t m_TextureRotation = 0.0f;
};