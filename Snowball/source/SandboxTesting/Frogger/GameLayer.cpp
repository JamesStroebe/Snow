#include "GameLayer.h"



GameLayer::GameLayer() :
	Layer("GameLayer") {
	Snow::Render::Camera::OrthographicCamera camera(-64.0f, 64.0f, -36.0f, 36.0f);
	m_Scene.SetCamera(camera);
	
}

void GameLayer::OnAttach() {
	player = new Player(20.0, 30.0, 2.0, 2.0);
}

void GameLayer::OnDetach() {

}

void GameLayer::OnUpdate(Snow::Core::Time::Timestep ts) {
	
	player->Move();
	Snow::Render::Renderer2D::BeginScene(m_Scene);
	//m_Scene.GetCamera()->RecalculateViewMatrix();
	Snow::Render::Renderer2D::SubmitColoredQuad({ player->GetX(), player->GetY() }, { player->GetWidth(), player->GetHeight() }, { 1.0f, 0.0f, 0.4f, 1.0f });
	Snow::Render::Renderer2D::SubmitColoredQuad({ 30.0f, 1.0f }, { 60.0f, 2.0f }, { 1.0f, 0.95f, 0.9f, 1.0f });
	Snow::Render::Renderer2D::SubmitColoredQuad({ 39.5f, 11.0f }, { 7.0f, 2.0f }, { 0.6f, 1.0f, 0.9f, 1.0f });
	Snow::Render::Renderer2D::EndScene();
	Snow::Render::Renderer2D::Present();
	//Snow::Render::Renderer::Present();
}

void GameLayer::OnRender() {

}

void GameLayer::OnEvent(Snow::Core::Event::Event& e) {
	//m_Scene.GetCamera()->OnEvent(e);
}