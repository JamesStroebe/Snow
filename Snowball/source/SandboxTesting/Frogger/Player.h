#pragma once

#include <Snow.h>

class Player {

public:
	Player(double x, double y, double width, double height);

	bool Colliding(double x, double y, double width, double height);

	bool CollideTop(double x, double y, double width, double height);
	bool CollideBottom(double x, double y, double width, double height);
	bool CollideLeft(double x, double y, double width, double height);
	bool CollideRight(double x, double y, double width, double height);

	void Move();

	inline double GetX() { return m_X; }
	inline double GetY() { return m_Y; }

	inline double GetWidth() { return m_Width; }
	inline double GetHeight() { return m_Height; }
private:
	double m_X, m_Y;
	double m_Width, m_Height;
	double m_VelY = 0.0;

	double m_Gravity = -0.01;

};
