#pragma once

#include <Snow.h>

#include "Player.h"

class GameLayer : public Snow::Core::Layer {
public:
	GameLayer();
	~GameLayer() = default;

	virtual void OnAttach() override;
	virtual void OnDetach() override;
	virtual void OnUpdate(Snow::Core::Time::Timestep ts) override;
	virtual void OnRender() override;
	virtual void OnEvent(Snow::Core::Event::Event& e) override;

private:
	Snow::Scene::Scene2D m_Scene;

	Player* player;

};
