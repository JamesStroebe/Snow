
#include "Player.h"

Player::Player(double x, double y, double width, double height) :
	m_X(x), m_Y(y), m_Width(width), m_Height(height) {

}

bool Player::Colliding(double x, double y, double width, double height) { 

	double X = m_X - m_Width / 2.0;
	double Y = m_Y - m_Height / 2.0;

	bool BL = X >= x && X <= x + width && Y >= y&& Y <= y + height;
	bool BR = X + m_Width >= x && X + m_Width <= x + width && Y >= y && Y <= y + height;
	bool UL = X >= x&& X <= x + width && Y + m_Height >= y && Y + m_Height <= y + height;
	bool UR = X + m_Width >= x && X + m_Width <= x + width && Y + m_Height >= y && Y + m_Height <= y + height;

	bool colliding = BL || BR || UL || UR;

	return colliding;
}

bool Player::CollideTop(double x, double y, double width, double height) {
	return Colliding(x, y, width, height) && m_Y > (y + height / 2);
}

bool Player::CollideBottom(double x, double y, double width, double height) {
	return Colliding(x, y, width, height) && m_Y < (y + height / 2);
}

bool Player::CollideLeft(double x, double y, double width, double height) {
	return Colliding(x, y, width, height) && m_X > (x + width / 2);
}

bool Player::CollideRight(double x, double y, double width, double height) {
	return Colliding(x, y, width, height) && m_X < (x + width / 2);
}

void Player::Move() {
	if (Snow::Core::Input::IsKeyPressed(SNOW_KEY_SPACE) && (Colliding(0, 0, 60, 2) || Colliding(36, 10, 7, 2)))
		m_VelY += 0.5;
	
	m_Y += m_VelY;

	
	
	if (!CollideLeft(0, 0, 60, 2) && !CollideRight(0, 0, 60, 2) && !CollideLeft(36, 10, 7, 2) && !CollideRight(36, 10, 7, 2)) {
		if (Snow::Core::Input::IsKeyPressed(SNOW_KEY_A))
			m_X -= 0.15;
		else if (Snow::Core::Input::IsKeyPressed(SNOW_KEY_D))
			m_X += 0.15;
	}
	
	if (!CollideTop(0, 0, 60, 2) && !CollideTop(36, 10, 7, 2)) {
		m_VelY += m_Gravity;
	}else {
		m_VelY = 0;
	}

	//if(CollideBottom(0, 0, 60, 2) ||CollideBottom(36, 10, 7, 2))
	
	
	
	//SNOW_CORE_TRACE("{0}", Colliding(0.0, 0.0, 60.0, 2.0));
	SNOW_CORE_TRACE("{0}, {1}, {2}", m_X, m_Y, m_VelY);

}
