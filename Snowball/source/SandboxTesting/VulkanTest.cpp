#include "VulkanTest.h"

VulkanLayer::VulkanLayer() {
	//Snow::Render::API::RenderPassSpecification renderpassSpec;
	//renderpassSpec.BufferType = Snow::Render::API::RenderBufferType::RENDER_BUFFER_COLOR;
}

void VulkanLayer::OnAttach() {
	Snow::Render::API::RenderPassSpecification renderPassSpec;
	//renderPassSpec.BufferType = Snow::Render::API::RenderBufferType::RENDER_BUFFER_COLOR | Snow::Render::API::RenderBufferType::RENDER_BUFFER_DEPTH;
	m_RenderPass = Snow::Render::API::RenderPass::Create(renderPassSpec);

	Snow::Render::API::FramebufferSpecification framebufferSpec;
	framebufferSpec.Color = Snow::Math::Vector4f(0.9f, 0.75f, 0.5f, 1.0f);
	//framebufferSpec.RenderPass = m_RenderPass;

	m_Framebuffer = Snow::Render::API::Framebuffer::Create(framebufferSpec);






	Snow::Render::API::PipelineSpecification pipelineSpec;


	//switch (Snow::Render::Renderer::GetRenderAPI()) {
	//case Snow::Render::RenderAPI::OPENGL:		pipelineSpec.Shaders = {Snow::Render::Shader::Shader::Create("assets/shaders/BatchRender.glsl"); break;
	//case Snow::Render::RenderAPI::DIRECTX:	pipelineSpec.Shader = Snow::Render::Shader::Shader::Create("assets/shaders/BatchRender.hlsl"); break;
	//case Snow::Render::RenderAPI::VULKAN:		pipelineSpec.Shader = Snow::Render::Shader::Shader::Create("assets/shaders/basic.vert.spv", "assets/shaders/basic.frag.spv"); break;
	//}


	pipelineSpec.VertexBufferLayout = {
		{ Snow::Render::API::VertexAttribType::Float3, "POSITION" },
		{ Snow::Render::API::VertexAttribType::Float2, "TEXCOORD"},
		{ Snow::Render::API::VertexAttribType::Float, "ID"},
		{ Snow::Render::API::VertexAttribType::Float4, "COLOR" },

	};

	m_Pipeline = Snow::Render::API::Pipeline::Create(pipelineSpec);
}

void VulkanLayer::OnDetach() {

}

void VulkanLayer::OnUpdate(Snow::Core::Time::Timestep ts) {

}

void VulkanLayer::OnRender() {
	//Snow::Render::Renderer::BeginRenderPass(m_RenderPass);
	//Snow::Render::Renderer::Submit(m_Pipeline, nullptr, nullptr, 0);
	//Snow::Render::Renderer::EndRenderPass();
	//Snow::Render::Renderer::Flush();
	//Snow::Render::Renderer::Present();
}

void VulkanLayer::OnEvent(Snow::Core::Event::Event& e) {

}