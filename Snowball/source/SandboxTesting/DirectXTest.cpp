#include "DirectXTest.h"

#include "imgui.h"

DirectXTest::DirectXTest() :
	Layer("DirectX"), m_PerspectiveCamera(Math::Vector3f(4, 500, 6.0), 1280.0f, 720.0f) {
	Snow::Math::float32_t aspectRatio = 1280.0f / 720.0f;
	Snow::Math::float32_t width = 25.0f;

	m_OrthoCamera = new Snow::Render::Camera::OrthographicCamera(-width, width, -width / aspectRatio, width / aspectRatio);
	m_Level = Level(50, 50);

	Snow::Core::Ref<Snow::Render::API::Texture2D> bg = Snow::Render::API::Texture2D::Create("assets/textures/Sunset.jpg");
	m_Texture = Snow::Render::API::Texture2D::Create("assets/textures/SnowLogo.png");

	m_Scene.SetCamera(m_PerspectiveCamera);

	m_Scene2D.SetCamera(*m_OrthoCamera);
	m_Scene2D.SetBackground(bg);

	// ECS
	m_RenderSystem = RenderSystem();
	m_CameraSystem = CameraSystem();
	m_ImGuiSystem = ImGuiSystem();

	m_RenderSystemList.AddSystem(m_RenderSystem);
	m_ImGuiSystemList.AddSystem(m_ImGuiSystem);
	m_CameraSystemList.AddSystem(m_CameraSystem);

#if 0 // serialization testing
	m_Database = new SerializeDatabase("Snow");
#endif

	//m_Arial = new Snow::Render::Font::Font("assets/fonts/arial.ttf");
}



void DirectXTest::OnAttach() {
#if 1
	Snow::Render::API::FramebufferSpecification framebufferSpec;
	framebufferSpec.Format = Snow::Render::API::FramebufferFormat::RGBA8;
	framebufferSpec.Color = { 0.75f, 0.6f, 0.5f, 1.0f };
    framebufferSpec.Size = { 1280.0f, 720.0f };
	framebufferSpec.Samples = 1;
    framebufferSpec.AttachmentTypes = (Snow::Render::API::FramebufferType)(Snow::Render::API::FramebufferType::RENDER_BUFFER_COLOR | Snow::Render::API::FramebufferType::RENDER_BUFFER_DEPTH);

	m_Framebuffer = Snow::Render::API::Framebuffer::Create(framebufferSpec);

	Snow::Render::API::FramebufferSpecification invertFramebufferSpec;
	invertFramebufferSpec.Format = Snow::Render::API::FramebufferFormat::RGBA8;
	invertFramebufferSpec.Color = { 0.5f, 0.5f, 0.5f, 1.0f };
	invertFramebufferSpec.Size = { 1280.0f, 720.0f };
	invertFramebufferSpec.Samples = 1;
	invertFramebufferSpec.AttachmentTypes = (Snow::Render::API::FramebufferType)(Snow::Render::API::FramebufferType::RENDER_BUFFER_COLOR | Snow::Render::API::FramebufferType::RENDER_BUFFER_DEPTH);

	m_InvertFramebuffer = Snow::Render::API::Framebuffer::Create(invertFramebufferSpec);

	Snow::Render::API::PipelineSpecification invertPipelineSpec;
	invertPipelineSpec.Shaders = { Snow::Render::Shader::Shader::Create("assets/shaders/glsl/InvertVert.glsl", Snow::Render::Shader::ShaderType::VertexShader), Snow::Render::Shader::Shader::Create("assets/shaders/glsl/InvertFrag.glsl", Snow::Render::Shader::ShaderType::PixelShader) };
	invertPipelineSpec.VertexBufferLayout = {
		{Snow::Render::API::VertexAttribType::Float3, "POSITION"},
		{Snow::Render::API::VertexAttribType::Float2, "TEXCOORD"}
	};

	m_InvertPipeline = Snow::Render::API::Pipeline::Create(invertPipelineSpec);

	Snow::Render::Renderer::GetCommandBuffer()->Execute();

	float positions[20] = {
		-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
		 1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
		 1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
		-1.0f,  1.0f, 0.0f, 0.0f, 1.0f
	};

	uint32_t indices[6] = {
		0, 1, 2, 2, 3, 0
	};

	m_InvertVBO = Snow::Render::API::VertexBuffer::Create(positions, 20 * sizeof(float));
	m_InvertIBO = Snow::Render::API::IndexBuffer::Create(indices, 6 * sizeof(uint32_t));

    Snow::Render::API::RenderPassSpecification renderpassSpec;
    renderpassSpec.TargetFramebuffer = m_Framebuffer;

    m_RenderPass = Snow::Render::API::RenderPass::Create(renderpassSpec);
#endif
#if 0 // testing mesh creation | needed for 3d rendering
	m_Mesh = Snow::Core::CreateRef<Snow::Render::Mesh>("assets/models/Sphere.obj");
#endif

#if 0 // testing serialization
	SerializeObject o = SerializeObject("OBJ");

	o.AddField(SerializeField::Int("T", 17));
	o.AddField(SerializeField::Int("R", 27));
	o.AddString(SerializeString::Create("name", "James"));
	m_Database->AddObject(o);

	SerializeObject o1 = SerializeObject("OBk");
	o1.AddField(SerializeField::Double("m_Y", 7.6));
	o1.AddString(SerializeString::Create("m_X", "George"));
	m_Database->AddObject(o1);


	m_Database->SerializeToFile("Database.snow");

	SerializeDatabase readData = SerializeDatabase::DeserializeFromFile("Database.snow");
#endif

	// Components

	RenderableComponent renderComp;
	TransformComponent transformComp;
	ColorComponent colorComp;
	SpriteComponent spriteComp;

	// Component Initialization
	renderComp.Visible = true;

	transformComp.Position = { 0, 0, 0 };
	transformComp.Scale = { 1,1 };
	transformComp.Rotation = 0;

	colorComp.Color = { 0, 1, 0, 1 };

	spriteComp.SpriteSheet = m_Texture;
	spriteComp.Position = { 0.0f, 0.0f };
	spriteComp.Scale = { 1.0f, 1.0f };

	m_SnowEntity = ECS::CreateEntity(renderComp, transformComp, colorComp, spriteComp);

	// Testing textured entity
	m_Entities.push_back(m_SnowEntity);
	

	for (Math::uint32_t i = 0; i < 5000; i++) {
		transformComp.Position = { Math::Random::RandInt(-20, 20), Math::Random::RandInt(-16, 16), Snow::Math::Random::RandFloat(0.1, 0.5) };
		transformComp.Rotation = Snow::Math::Random::RandFloat(0, 360);
		float x = Snow::Math::Random::RandFloat();
		transformComp.Scale = { x, x };

		colorComp.Color = { Snow::Math::Random::RandFloat(), Snow::Math::Random::RandFloat(), Snow::Math::Random::RandFloat(), 1.0f };

		//m_Entities.push_back(ECS::CreateEntity(renderComp, transformComp, colorComp));
	}

	// Creating the camera entity
	CameraComponent cameraComp;
	cameraComp.Position = { 0.0f, 0.0f, 0.0f };

	MovementComponent movementComp;
	movementComp.Movement = Math::Vector3f(0);
	movementComp.InputManager = Snow::Core::Input::GetInputManager();

	m_CameraEntity = ECS::CreateEntity(cameraComp, movementComp);
	m_Entities.push_back(m_CameraEntity);
}

void DirectXTest::OnDetach() {

}

void DirectXTest::OnUpdate(Snow::Core::Time::Timestep ts) {
	m_PerspectiveCamera.OnUpdate(ts);

	ECS::UpdateSystems(m_CameraSystemList, ts);
	
}

void DirectXTest::OnRender() {

	Snow::Render::Renderer::GetCommandBuffer()->BeginRenderPass(m_RenderPass);

#define FRAMEBUFFERS 0
#if RENDER3D
	m_Framebuffer->Bind();
	Snow::Render::Renderer3D::BeginScene(m_Scene);
	Snow::Render::Renderer3D::SubmitMesh(m_Mesh);
	Snow::Render::Renderer3D::SubmitMesh(m_Mesh, Math::rotate(Math::Matrix4x4f(1.0f), Math::radians(90.0f), Math::Vector3f(0.0f, 0.0f, 1.0f)));
	Snow::Render::Renderer3D::EndScene();
	//Snow::Render::Renderer::GetCommandBuffer()->Flush();
	m_Framebuffer->Unbind();
#endif

	//m_Framebuffer->Bind();
	m_Scene2D.GetCamera().SetPosition(ECS::GetComponent<CameraComponent>(m_CameraEntity)->Position);
	Snow::Render::Renderer2D::BeginScene(m_Scene2D);
	//m_Level.OnRender();
	ECS::UpdateSystems(m_RenderSystemList, 0);
	Snow::Render::Renderer2D::EndScene();
	Snow::Render::Renderer2D::Present();

	//m_Framebuffer->Unbind();
	Snow::Render::Renderer::GetCommandBuffer()->EndRenderPass();
	m_InvertFramebuffer->Bind();
	m_Framebuffer->BindColorAttachment();
	Snow::Render::Renderer::SetDepthTesting(false);
	Snow::Render::Renderer::GetCommandBuffer()->SubmitElements(m_InvertPipeline, m_InvertVBO, m_InvertIBO);
	Snow::Render::Renderer::SetDepthTesting(true);
	m_InvertFramebuffer->Unbind();

	
}

void DirectXTest::OnImGuiRender() {
	ImGui::Begin("Settings");
	auto stats = Snow::Render::Renderer2D::GetStats();
	ImGui::Text("Renderer2D Stats:");
	ImGui::Text("\tDraw Calls: %d", stats.DrawCalls);
	ImGui::Text("\tQuad Count: %d", stats.QuadCount);
	ImGui::Text("\tIndex Count: %d", (stats.VertexCount * 3 / 2));
	ImGui::Text("\tVertex Count: %d", stats.VertexCount);
	Snow::Render::Renderer2D::ResetStats();
	ImGui::ColorEdit3("Texture Tint", Math::valuePtr(m_TextureTint));
	ImGui::DragFloat2("Texture Position", Math::valuePtr(m_TexturePosition), 0.1, -10.0f, 10.0f);
	ImGui::DragFloat("Texture Size", &m_TextureSize, 0.1f, 0.1f, 30.0f);
	ImGui::DragFloat("Texture Rotation", &m_TextureRotation, 1.0f, 0.0f, 360.0f);
	ImGui::End();

	ECS::UpdateSystems(m_ImGuiSystemList, 0);
}

void DirectXTest::OnEvent(Snow::Core::Event::Event& e) {

	if (Snow::Core::Input::IsKeyPressed(SNOW_KEY_F11)) {
		Snow::Core::Event::WindowFullscreenEvent event;

		auto& app = Snow::Core::Application::Get();
		auto& window = app.GetWindow();
		window->m_EventCallback(event);
	}

	if (Snow::Core::Input::IsKeyPressed(SNOW_KEY_G)) {
		auto& app = Snow::Core::Application::Get();
		app.OnShutdown();
	}


	m_PerspectiveCamera.OnEvent(e);
	m_OrthoCamera->OnEvent(e);
}