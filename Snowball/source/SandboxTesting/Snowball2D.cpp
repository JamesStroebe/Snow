#include <Snowball2D.h>




Snowball2D::Snowball2D() :
	Layer("Sandbox2D"), /*m_Camera(45.0f, 1280.0f, 720.0f),*/  m_OrthoCamera(-32.0f, 32.0f, -18.0f, 18.0f) {
	m_Level = Level(50, 50);

	//m_Purp = Snow::Core::CreateRef<Snow::Render::Mesh>("assets/models/Sphere.obj");

	m_BRDFLUT = Snow::Render::API::Texture2D::Create("assets/textures/BRDF_LUT.tga");

	m_EnvCubeMap = Snow::Render::API::TextureCube::Create("assets/textures/enviroments/Arches_E_PineTree_Radiance.tga");
	m_EnvIrradiance = Snow::Render::API::TextureCube::Create("assets/textures/enviroments/Arches_E_PineTree_Irradiance.tga");

	m_AlbedoInput.TextureMap = Snow::Render::API::Texture2D::Create("assets/textures/Wood/WoodMaterial_diffuse.png");
	m_NormalInput.TextureMap = Snow::Render::API::Texture2D::Create("assets/textures/Wood/WoodMaterial_normal.png");
	m_RoughnessInput.TextureMap = Snow::Render::API::Texture2D::Create("assets/textures/Wood/WoodMaterial_smoothness.png");
	m_MetalnessInput.TextureMap = Snow::Render::API::Texture2D::Create("assets/textures/Wood/WoodMaterial_metallic.png");


	//m_HDRShader = Snow::Render::Shader::Shader::Create("assets/shaders/hdr.glsl");
	//m_QuadShader = Snow::Render::Shader::Shader::Create("assets/shaders/quad.glsl");

	//m_Plane = Snow::CreateRef<Snow::Mesh>("assets/models/Plane1m.obj");

	//Snow::TextureSpecification ts;
	//ts.Format = Snow::TextureFormat::RGBA;
	//ts.Wrap = Snow::TextureWrap::MirroredRepeat;

	//m_Sheet = Snow::SpriteSheet("assets/textures/SnowLogo.png", ts);
	//m_Sprite = Snow::Sprite(0, 0, 512, 512, m_Sheet);
	//m_Sprite2 = Snow::Sprite(256, 256, 100, 100, m_Sheet);
}

void Snowball2D::OnAttach() {

	Snow::Render::API::RenderPassSpecification renderSpec;
	//renderSpec.BufferType = Snow::Render::API::RenderBufferType::RENDER_BUFFER_COLOR | Snow::Render::API::RenderBufferType::RENDER_BUFFER_DEPTH;
	m_RenderPass = Snow::Render::API::RenderPass::Create(renderSpec);

	Snow::Render::API::FramebufferSpecification framebufferSpec;
	framebufferSpec.Color = { 0.0, 0.0, 0.0, 0.0 };
	//framebufferSpec.RenderPass = m_RenderPass;

	m_Framebuffer = Snow::Render::API::Framebuffer::Create(framebufferSpec);

	

	for (uint32_t i = 0; i < 3; i++) {
		Snow::Core::Ref<Snow::Render::MaterialInstance> mi(new Snow::Render::MaterialInstance(m_Purp->GetMaterial()));
		mi->Set("r_Transform", Snow::Math::Matrix4x4f(1.0f));//glm::inverse(glm::translate(glm::mat4(1.0f), glm::vec3(i * 1.1, 0.0f, 0.0f))));
		m_MaterialInstances.push_back(mi);
	}

	//Snow::PipelineSpecification pipelineSpec;
	//pipelineSpec.Shader = Snow::Shader::Create("assets/shaders/basicVulkan.glsl");
	//pipelineSpec.VertexBufferLayout = { {} };

	//m_Pipeline = Snow::Pipeline::Create(pipelineSpec);


	//m_Sprite.GetTexture()->Bind();
	//SNOW_CORE_TRACE("WIDTH: {0}", m_Sprite.GetTexture()->GetWidth());
	
	//m_Texture = Snow::Texture2D::Create("assets/textures/T.png");
	//m_Texture->Bind();
}

void Snowball2D::OnDetach() {
}

void Snowball2D::OnUpdate(Snow::Core::Time::Timestep ts) {

	//m_Camera.OnUpdate(ts);

	
	SNOW_TRACE(Snow::Math::Random::RandFloat(5,17));

	if (Snow::Core::Input::IsKeyPressed(SNOW_KEY_F4, SNOW_MOD_LEFT_ALT)) {
		auto& app = Snow::Core::Application::Get();
		app.OnShutdown();
	}
	
}

void Snowball2D::OnRender() {
	//Snow::Render::Renderer::BeginRenderPass(m_Framebuffer->GetSpecification().RenderPass);



	//Snow::Render::Shader::ShaderBufferDeclaration<sizeof(glm::mat4) * 1 + sizeof(glm::vec3) * 3 + sizeof(float) * 6, 10> material;

	//material.Push("u_ViewProj", m_Camera.GetViewProjectionMatrix());
	//material.Push("u_Model", glm::mat4(1.0f));
	//m_Purp->GetMaterial()->Set("light", m_Light);

	m_Purp->GetMaterial()->Set("r_Transform", Snow::Math::Matrix4x4f(1.0f));
	m_Purp->GetMaterial()->Set("u_RadiancePrefilter", 0.0f);
	m_Purp->GetMaterial()->Set("u_NormalTexToggle", 1.0f);
	m_Purp->GetMaterial()->Set("u_EnvMapRotation", m_EnvRotation);
	m_Purp->GetMaterial()->Set("Normal", m_NormalInput.TextureMap);
	m_Purp->GetMaterial()->Set("Albedo", m_AlbedoInput.TextureMap);
	m_Purp->GetMaterial()->Set("Metalness", m_MetalnessInput.TextureMap);
	m_Purp->GetMaterial()->Set("Roughness", m_RoughnessInput.TextureMap);
	m_Purp->GetMaterial()->Set("u_EnvRadianceTex", m_EnvCubeMap);
	m_Purp->GetMaterial()->Set("u_EnvIrradianceTex", m_EnvIrradiance);
	m_Purp->GetMaterial()->Set("u_BRDFLUTTexture", m_BRDFLUT);

	//m_Purp->GetMaterial()->Bind(m_Pipeline);



	//m_Purp->GetShader()->Bind();
	//m_Purp->GetShader()->UploadShaderBuffer(m_Purp->GetMaterial()->GetBuffer());


	//Snow::Renderer2D::BeginScene(m_OrthoCamera);
	//m_Level.OnRender();
	//Snow::Renderer2D::EndScene();
	//Snow::Renderer2D::Present();


	//Snow::Render::Renderer3D::BeginScene(m_Camera);
	Snow::Render::Renderer3D::SubmitMesh(m_Purp);
	//Snow::Renderer3D::SubmitMesh(m_Purp, glm::mat4(1.0f), m_MaterialInstances[1]);
	//Snow::Renderer3D::SubmitMesh(m_Purp, glm::mat4(1.0f), m_MaterialInstances[2]);
	//Snow::Renderer3D::SubmitMesh(m_Plane);
	Snow::Render::Renderer3D::EndScene();
	Snow::Render::Renderer3D::Present();


	//Snow::Render::Renderer::Flush();
	//Snow::Render::Renderer::Present();
}

void Snowball2D::OnEvent(Snow::Core::Event::Event& e) {
	m_OrthoCamera.OnEvent(e);
	

}