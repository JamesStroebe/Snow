#version 430

out vec4 Color;

in vec2 v_TexCoord;

uniform sampler2D u_Texture;


const float contrast = 0.0;

void main() {
	Color = texture(u_Texture, v_TexCoord);

	float distFromScreenCenter = distance(v_TexCoord, vec2(.5, .5)); //garbage vignette function, idk
	Color.rgb = (Color.rgb - 0.5) * (1.0 + contrast) + 0.5;
	Color.rgb = Color.rgb - vec3(pow(distFromScreenCenter, 2));
}
