#type vertex
#version 330

layout (location = 0) in vec3 a_Position;
layout (location = 1) in vec2 a_TexCoord;
layout (location = 2) in float a_TexID;
layout (location = 3) in vec4 a_Color;

uniform buffer RCamera {
	mat4 r_Proj;
	mat4 r_View;
	mat4 r_Transform;
} camera;

out vec2 v_TexCoord;
out float v_TexID;
out vec4 v_Color;

void main() {
	v_TexCoord = a_TexCoord;
	v_TexID = a_TexID;
	v_Color = a_Color;
	gl_Position = camera.r_Proj * camera.r_View * camera.r_Transform * vec4(a_Position, 1.0);
}

#type fragment
#version 330

layout(location = 0) out vec4 Color;

in vec2 v_TexCoord;
in float v_TexID;
in vec4 v_Color;

uniform vec3 u_Color = {1.0, 1.0, 1.0};

uniform sampler2D u_Texture[32];

void main() {
	vec4 texColor = v_Color * vec4(u_Color, 1.0);

	int tID;
	if(v_TexID >= 0.0) {
		tID = int(v_TexID);
		texColor = texture(u_Texture[tID], v_TexCoord);
	}

	
	Color = texColor; 
}