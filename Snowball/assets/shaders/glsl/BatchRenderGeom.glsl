#version 430

layout(points) in;

layout(triangle_strip, max_vertices = 4) out;

uniform buffer render RCamera {
    mat4 r_MVP;
} camera;

in VertexData {
    float v_TexID;
    vec4 v_Color;
} VS_IN[];

out FragmentData {
    vec2 v_TexCoords;
    float v_TexID;
    vec4 v_Color;
} FS_OUT;

void main() {

    vec4 pos = vec4(-0.5, -0.5, 0.0, 0.0);
    vec4 vertexPos = vec4(gl_in[0].gl_Position+pos);
    gl_Position = camera.r_MVP * vertexPos;
    FS_OUT.v_TexCoords = vec2(0.0, 0.0);
    FS_OUT.v_TexID = VS_IN[0].v_TexID;
    FS_OUT.v_Color = VS_IN[0].v_Color;
    EmitVertex();

    pos = vec4(0.5, -0.5, 0.0, 0.0);
    vertexPos = vec4(gl_in[0].gl_Position+pos);
    gl_Position = camera.r_MVP * vertexPos;
    FS_OUT.v_TexCoords = vec2(1.0, 0.0);
    FS_OUT.v_TexID = VS_IN[0].v_TexID;
    FS_OUT.v_Color = VS_IN[0].v_Color;
    EmitVertex();

    pos = vec4(0.5, 0.5, 0.0, 0.0);
    vertexPos = vec4(gl_in[0].gl_Position+pos);
    gl_Position = camera.r_MVP * vertexPos;
    FS_OUT.v_TexCoords = vec2(1.0, 1.0);
    FS_OUT.v_TexID = VS_IN[0].v_TexID;
    FS_OUT.v_Color = VS_IN[0].v_Color;
    EmitVertex();

    pos = vec4(-0.5, 0.5, 0.0, 0.0);
    vertexPos = vec4(gl_in[0].gl_Position+pos);
    gl_Position = camera.r_MVP * vertexPos;
    FS_OUT.v_TexCoords = vec2(0.0, 1.0);
    FS_OUT.v_TexID = VS_IN[0].v_TexID;
    FS_OUT.v_Color = VS_IN[0].v_Color;
    EmitVertex();

    EndPrimitive();
}