#type vertex
#version 430

layout (location=0) in vec3 a_Position;
layout (location=1) in vec3 a_Normal;
layout (location=2) in vec3 a_Tangent;
layout (location=3) in vec3 a_Binormal;
layout (location=4) in vec2 a_TexCoord;

uniform buffer RCamera {
	mat4 r_ViewProjection;
	vec3 r_CameraPos;
} camera;

uniform buffer Model {
	mat4 r_Transform;
} model;

out VertexOutput {
	vec3 CameraPosition; 
	vec3 WorldPosition;
	vec3 Normal;
	vec2 TexCoord;
	mat3 WorldNormals;
	vec3 Binormal;
} vs_output;



void main() {
	gl_Position = camera.r_ViewProjection * model.r_Transform * vec4(a_Position, 1.0);

	vs_output.CameraPosition = camera.r_CameraPos;
	vs_output.WorldPosition = vec3(model.r_Transform * vec4(a_Position, 1.0));
	vs_output.Normal = a_Normal;
	vs_output.TexCoord = a_TexCoord;
	vs_output.WorldNormals = mat3(model.r_Transform) * mat3(a_Tangent, a_Binormal, a_Normal);
	vs_output.Binormal = a_Binormal;

}

#type pixel
#version 430

layout (location = 0) out vec4 finalColor;

const float PI = 3.141592653;
const float Epsilon = 0.00001;

const int LightCount = 1;

const vec3 Fdielectric = vec3(0.04);

in VertexOutput {
	vec3 CameraPosition;
	vec3 WorldPosition;
	vec3 Normal;
	vec2 TexCoord;
	mat3 WorldNormals;
	vec3 Binormal;
} vs_input;

struct Light {
	vec3 Direction;
	vec3 Radiance;
} light;


uniform sampler2D Normal;
uniform sampler2D Albedo;
uniform sampler2D Metalness;
uniform sampler2D Roughness;

uniform samplerCube u_EnvRadianceTex;
uniform samplerCube u_EnvIrradianceTex;

uniform sampler2D u_BRDFLUTTexture;

uniform buffer Material {
	float u_EnvMapRotation;
	float u_NormalTexToggle;
	float u_RadiancePrefilter;
} material;



struct PBRParameters {
	vec3 AlbedoColor;
	float RoughnessValue;
	float MetalnessValue;

	vec3 NormalVector;
	vec3 View;
	float NdotV;
} params;

float ndfGGX(float cosLh, float roughness) {
	float alpha = roughness*roughness;
	float alphaSq = alpha*alpha;

	float denom = (cosLh * cosLh) * (alphaSq-1.0)+1.0;

	return alphaSq/(denom*denom*PI);
}

float gSchlickG1(float cosTheta, float k) {
	return cosTheta/(cosTheta* (1.0 - k) + k);
}

float gaSchlickGGX(float cosLi, float NdotV, float roughness) {
	float r = roughness + 1.0;
	float k = (r*r)/8.0;
	return gSchlickG1(cosLi, k) * gSchlickG1(NdotV, k);
}

float GeometrySchlickGGX(float NdotV, float roughness) {
	float r = roughness + 1.0;
	float k = (r*r)/8.0;

	float nom = NdotV;
	float denom = NdotV * (1.0-k)+k;

	return nom/denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {
	float NdotV = max(dot(N,V), 0.0);
	float NdotL = max(dot(N,L), 0.0);

	float ggx1 = GeometrySchlickGGX(NdotV, roughness);
	float ggx2 = GeometrySchlickGGX(NdotL, roughness);

	return ggx1*ggx2;
}

vec3 fresnelSchlick(vec3 F0, float cosTheta){
	return F0 + (1.0-F0) * pow(1.0-cosTheta, 5.0);
}

vec3 fresnelSchlickRoughness(vec3 F0, float cosTheta, float roughness) {
	return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0-cosTheta, 5.0);
}

float RadicalInverse_VdC(uint bits) {
	bits = (bits<<16u) | (bits>>16u);
	bits = ((bits&0x55555555u)<<1u) | ((bits&0xAAAAAAAAu)>>1u);
	bits = ((bits&0x33333333u)<<2u) | ((bits&0xCCCCCCCCu)>>2u);
	bits = ((bits&0x0F0F0F0Fu)<<4u) | ((bits&0xF0F0F0F0u)>>4u);
	bits = ((bits&0x00FF00FFu)<<8u) | ((bits&0xFF00FF00u)>>8u);
	return float(bits) * 2.3283064365386963e-10; // 0x10000000
}

vec2 Hammersley(uint i, uint N) {
	return vec2(float(i)/float(N), RadicalInverse_VdC(i));
}

vec3 ImportanceSampleGGX(vec2 Xi, float roughness, vec3 N) {
	float a = roughness * roughness;
	float Phi = 2.0 * PI * Xi.x;
	float CosTheta = sqrt((1-Xi.y)/(1+(a*a - 1) * Xi.y));
	float SinTheta = sqrt(1 - CosTheta * CosTheta);
	vec3 H;
	H.x = SinTheta * cos(Phi);
	H.y = SinTheta * sin(Phi);
	H.z = CosTheta;
	vec3 UpVector = abs(N.z) < 0.999 ? vec3(0,0,1) : vec3(1,0,0);
	vec3 TangentX = normalize(cross(UpVector, N));
	vec3 TangentY = cross(N, TangentX);
	return TangentX * H.x + TangentY * H.y + N * H.z;
}

float TotalWeight = 0.0;

vec3 PrefilterEnvMap(float roughness, vec3 R) {
	vec3 N = R;
	vec3 V = R;
	vec3 PrefilteredColor = vec3(0.0);
	int numSamples = 1024;
	for(int i=0; i<numSamples; i++) {
		vec2 Xi = Hammersley(uint(i), numSamples);
		vec3 H = ImportanceSampleGGX(Xi, roughness, N);
		vec3 L = 2.0 * dot(V, H) * H - V;
		float NoL = clamp(dot(N, L), 0.0, 1.0);
		if(NoL > 0) {
			PrefilteredColor += texture(u_EnvRadianceTex, L).rgb * NoL;
			TotalWeight += NoL;
		}
	}
	return PrefilteredColor / TotalWeight;

}

vec3 RotateVectorY(float angle, vec3 vec) {
	angle = radians(angle);
	mat3x3 rotationMatrix ={vec3(cos(angle),0.0,sin(angle)),
                            vec3(0.0,1.0,0.0),
                            vec3(-sin(angle),0.0,cos(angle))};
	return rotationMatrix * vec;
}

vec3 Lighting(vec3 F0) {
	vec3 result = vec3(0.0);
	for(int i = 0; i < LightCount; i++) {
		vec3 Li = -light.Direction;
		vec3 Lradiance = light.Radiance;
		vec3 Lh = normalize(Li + params.View);

		float cosLi = max(0.0, dot(params.NormalVector, Li));
		float cosLh = max(0.0, dot(params.NormalVector, Lh));

		vec3 F = fresnelSchlick(F0, max(0.0, dot(Lh, params.View)));
		float D = ndfGGX(cosLh, params.RoughnessValue);
		float G = gaSchlickGGX(cosLi, params.NdotV, params.RoughnessValue);

		vec3 kd = (1.0 - F) * (1.0 - params.MetalnessValue);
		vec3 diffuseBRDF = kd * params.AlbedoColor;

		vec3 specularBRDF = (F * D * G) / max(Epsilon, 4.0 * cosLi * params.NdotV);

		result += (diffuseBRDF + specularBRDF) * Lradiance * cosLi;
	}
	return result;
}

vec3 IBL (vec3 F0, vec3 Lr) {
	vec3 irradiance = texture(u_EnvIrradianceTex, params.NormalVector).rgb;
	vec3 F = fresnelSchlickRoughness(F0, params.NdotV, params.RoughnessValue);
	vec3 kd = (1.0 - F) * (1.0 - params.MetalnessValue);
	vec3 diffuseIBL = params.AlbedoColor * irradiance;

	int u_EnvRadianceTexLevels = textureQueryLevels(u_EnvRadianceTex);
	float NoV = clamp(params.NdotV, 0.0, 1.0);
	vec3 R = 2.0 * dot(params.View, params.NormalVector) * params.NormalVector - params.View;
	vec3 specularIrradiance = vec3(0.0);

	if(material.u_RadiancePrefilter>0.5)
		specularIrradiance = PrefilterEnvMap(params.RoughnessValue * params.RoughnessValue, R) * material.u_RadiancePrefilter;
	else
		specularIrradiance = textureLod(u_EnvRadianceTex, RotateVectorY(material.u_EnvMapRotation, Lr), sqrt(params.RoughnessValue)* u_EnvRadianceTexLevels).rgb * (1.0 - material.u_RadiancePrefilter);

	vec2 specularBRDF = texture(u_BRDFLUTTexture, vec2(params.NdotV, 1.0 - params.RoughnessValue)).rg;
	vec3 specularIBL = specularIrradiance * (F * specularBRDF.x + specularBRDF.y);

	return kd * diffuseIBL + specularIBL;
}

void main() {
	
	params.AlbedoColor = texture(Albedo, vs_input.TexCoord).rgb;
	params.MetalnessValue = texture(Metalness, vs_input.TexCoord).r;
	params.RoughnessValue = texture(Roughness, vs_input.TexCoord).r;
	params.RoughnessValue = max(params.RoughnessValue, 0.05);

	params.NormalVector = normalize(vs_input.Normal);

	if(material.u_NormalTexToggle > 0.5) {
		params.NormalVector = normalize(2.0 * texture(Normal, vs_input.TexCoord).rgb - 1.0);
		params.NormalVector = normalize(vs_input.WorldNormals * params.NormalVector);
	}

	params.View = normalize(vs_input.CameraPosition - vs_input.WorldPosition);
	params.NdotV = max(dot(params.NormalVector, params.View), 0.0);

	vec3 Lr = 2.0 * params.NdotV * params.NormalVector - params.View;

	vec3 F0 = mix(Fdielectric, params.AlbedoColor, params.MetalnessValue);

	vec3 lightContribution = Lighting(F0);
	vec3 iblContribution = IBL(F0, Lr);

	//finalColor = vec4(lightContribution + iblContribution, 1.0);


	finalColor = vec4((vs_input.CameraPosition), 1.0);
}