#type vertex
#version 430 

layout (location = 0) in vec3 a_Position;
layout (location = 1) in vec3 a_Normal;
layout (location = 2) in vec3 a_Tangent;
layout (location = 3) in vec3 a_Binormal;
layout (location = 4) in vec2 a_TexCoord;

uniform buffer RCamera {
	mat4 r_ViewProjection;
} camera;

uniform buffer Model {
	mat4 r_Transform;
} model;

out VertexOutput {
	vec3 Normal;
	vec2 TexCoord;
} vs_output;

void main() {

	vs_output.Normal = a_Normal;
	vs_output.TexCoord = a_TexCoord;
	
	gl_Position = camera.r_ViewProjection * model.r_Transform * vec4(a_Position, 1.0);

}

#type pixel
#version 430

layout (location = 0) out vec4 Color;

uniform buffer Material {
	float y;
};

in VertexOutput {
	vec3 Normal;
	vec2 TexCoord;
} vs_output;

void main() {
	Color = vec4(vs_output.Normal, 1.0);
}