#version 330

layout (location = 0) in vec3 a_Position;
layout (location = 1) in vec2 a_TexCoord;
layout (location = 2) in float a_TexID;
layout (location = 3) in vec4 a_Color;

uniform buffer render RCamera {
	mat4 r_MVP;
} camera;

out FragmentData {
	vec2 v_TexCoord;
    float v_TexID;
    vec4 v_Color;
} data;

void main() {
	data.v_TexCoord = a_TexCoord;
	data.v_TexID = a_TexID;
	data.v_Color = a_Color;
	gl_Position = camera.r_MVP * vec4(a_Position, 1.0);
}