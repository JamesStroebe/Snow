#version 430

layout (location = 0) out vec4 Color;

in Data {
    vec3 position;
    vec3 normal;
    vec3 tangent;
    vec2 texCoord;
} ps_in;

void main() {
    vec3 nor = ps_in.normal + vec3(1.0, 1.0, 1.0); 

    vec4 nor1 = vec4(ps_in.texCoord, 0.3, 1.0);
    Color = nor1;
}