#version 430

layout (location = 0) in vec3 a_Position;
layout (location = 1) in vec3 a_Normal;
layout (location = 2) in vec3 a_Bitangent;
layout (location = 3) in vec3 a_Tangent;
layout (location = 4) in vec2 a_TexCoord;

uniform buffer render Camera {
    mat4 r_MVP;
};

out Data {
    vec3 position;
    vec3 normal;
    vec3 tangent;
    vec2 texCoord;
} vs_out;

void main() {
    vec4 pos = vec4(a_Position, 1.0);

    
    vs_out.position = a_Position;
    vs_out.normal = a_Normal;
    vs_out.tangent = a_Tangent;
    vs_out.texCoord = a_TexCoord;

    gl_Position = r_MVP * pos;
}