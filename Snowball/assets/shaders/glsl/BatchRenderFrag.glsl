#version 330

layout(location = 0) out vec4 Color;

in FragmentData {
	vec2 v_TexCoord;
	float v_TexID;
	vec4 v_Color;
} data;

uniform vec3 u_Color = {1.0, 1.0, 1.0};

uniform sampler2D u_Texture[32];

void main() {
	vec4 texColor = data.v_Color * vec4(u_Color, 1.0);

	int tID;
	if(data.v_TexID >= 0.0) {
		tID = int(data.v_TexID);
		texColor = texture(u_Texture[tID], data.v_TexCoord);
	}

	
	Color = texColor * data.v_Color; 
}