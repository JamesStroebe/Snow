
struct PS_INPUT {
	float4 positionCS : SV_POSITION;
	float2 texcoord : TEXCOORD;
	float texID : ID;
	float4 color : COLOR;
};

Texture2D tex[32] : register(t0);
SamplerState samp : register(s0);

float4 main(PS_INPUT input) : SV_TARGET {
    float4 texColor = input.color;

    switch(input.texID){
    case 0:
        texColor *= tex[0].Sample(samp, input.texcoord); break;
    case 1:
        texColor *= tex[1].Sample(samp, input.texcoord); break;
    case 2:
        texColor *= tex[2].Sample(samp, input.texcoord); break;
    case 3:
        texColor *= tex[3].Sample(samp, input.texcoord); break;
    case 4:
        texColor *= tex[4].Sample(samp, input.texcoord); break;
    case 5:
        texColor *= tex[5].Sample(samp, input.texcoord); break;
    case 6:
        texColor *= tex[6].Sample(samp, input.texcoord); break;
    case 7:
        texColor *= tex[7].Sample(samp, input.texcoord); break;
    case 8:
        texColor *= tex[8].Sample(samp, input.texcoord); break;
    }
    return texColor;
}