#type vertex

struct VS_Input {
	float3 position : POSITION;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
	float2 texcoord : TEXCOORD;
};

struct PS_Input {
	float4 PositionCS : SV_POSITION;
	float3 CameraPosition : CAMERA_POSITION;
	float4 WorldPosition : POSITION;
	float3 Normal : NORMAL;
	float2 TexCoord : TEXCOORD;
	float3x3 WorldNormals : WORLD_NORMALS;
	float3 Binormal : BINORMAL;
};

cbuffer RCamera : register(b0) {
	float4x4 r_Proj;
	float4x4 r_View;
	float3 r_CameraPosition;
}

cbuffer RCamera : register(b1) {
	float4x4 r_Transform;
}

PS_Input main(VS_Input input) {
	float3x3 wTransform = (float3x3)r_Transform;

	PS_Input output;
	float4 pos = float4(input.position, 1.0);
	output.WorldPosition = mul(pos, r_Transform);
	output.CameraPosition = r_CameraPosition;
	output.PositionCS = mul(output.WorldPosition, mul(r_View, r_Proj));
	output.TexCoord = input.texcoord;
	output.Normal = input.normal;

	float3x3 wn = { input.tangent,
					input.binormal,
					input.normal };

	output.WorldNormals = mul(wn, wTransform);
	output.Binormal = input.binormal;

	return output;
}

#type pixel

struct PS_Input {
	float4 PositionCS : SV_POSITION;
	float3 CameraPosition : CAMERA_POSITION;
	float4 WorldPosition : POSITION;
	float3 Normal : NORMAL;
	float2 TexCoord : TEXCOORD;
	float3x3 WorldNormals : WORLD_NORMALS;
	float3 Binormal : BINORMAL;
};

cbuffer Material : register(b0) {
	float EnvMapRotation;
	float u_NormalTexToggle;
	float u_RadiancePrefilter;
};

struct Light {
	float3 Direction;
	float3 Radiance;
};

float LightCount = 1;

const Light light;

struct PBRParameters {
	float3 AlbedoColor;
	float RoughnessValue;
	float MetalnessValue;
	float3 NormalVector;
	float3 View;
	float NdotV;
};

PBRParameters params;

Texture2D Albedo : register(t0);
SamplerState AlbedoState : register(s0);
Texture2D Normal : register(t1);
SamplerState NormalState : register(s1);
Texture2D Metalness : register(t2);
SamplerState MetalnessState : register(s2);
Texture2D Roughness : register(t3);
SamplerState RoughnessState : register(s3);

TextureCube EnvRadianceTexture : register(t10);
SamplerState EnvRadianceState : register(s10);
TextureCube EnvIrradianceTexture : register(t11);
SamplerState EnvIrradianceState : register(s11);

Texture2D BRDFLUTTexture : register(t15);
SamplerState BRDFLUTState : register(s15);

static const float PI = 3.14159265f;
static const float Epsilon = 0.00001f;


float ndfGGX(float cosLh, float roughness) {
	float alpha = roughness * roughness;
	float alphaSq = alpha * alpha;

	float denom = (cosLh * cosLh) * (alphaSq - 1.0) + 1.0;

	return alphaSq / (denom * denom * PI);
}

float gSchlickG1(float cosTheta, float k) {
	return cosTheta / (cosTheta * (1.0 - k) + k);
}

float gaSchlickGGX(float cosLi, float NdotV, float roughness) {
	float r = roughness + 1.0;
	float k = (r * r) / 8.0;
	return gSchlickG1(cosLi, k) * gSchlickG1(NdotV, k);
}

float GeometrySchlickGGX(float NdotV, float roughness) {
	float r = roughness + 1.0;
	float k = (r * r) / 8.0;

	float nom = NdotV;
	float denom = NdotV * (1.0 - k) + k;

	return nom / denom;
}

float GeometrySmith(float3 N, float3 V, float3 L, float roughness) {
	float NdotV = max(dot(N, V), 0.0);
	float NdotL = max(dot(N, L), 0.0);

	float ggx1 = GeometrySchlickGGX(NdotV, roughness);
	float ggx2 = GeometrySchlickGGX(NdotL, roughness);

	return ggx1 * ggx2;
}

float3 fresnelSchlick(float3 F0, float cosTheta) {
	return F0 + mul((1.0 - F0), pow(1.0 - cosTheta, 5.0));
}

float3 fresnelSchlickRoughness(float3 F0, float cosTheta, float roughness) {
	//return float3(0.0);
	float roughnessInverse = 1.0 - roughness;
	return F0 + (max(float3(roughnessInverse, roughnessInverse, roughnessInverse), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

float RadicalInverse_VdC(uint bits) {
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // 0x10000000
}

float2 Hammersley(uint i, uint N) {
	return float2(float(i) / float(N), RadicalInverse_VdC(i));
}

float3 ImportanceSampleGGX(float2 Xi, float roughness, float3 N) {
	float a = roughness * roughness;
	float Phi = 2.0 * PI * Xi.x;
	float CosTheta = sqrt((1 - Xi.y) / (1 + (a * a - 1) * Xi.y));
	float SinTheta = sqrt(1 - CosTheta * CosTheta);
	float3 H;
	H.x = SinTheta * cos(Phi);
	H.y = SinTheta * sin(Phi);
	H.z = CosTheta;
	float3 UpVector = abs(N.z) < 0.999 ? float3(0, 0, 1) : float3(1, 0, 0);
	float3 TangentX = normalize(cross(UpVector, N));
	float3 TangentY = cross(N, TangentX);
	return TangentX * H.x + TangentY * H.y + N * H.z;
}

static float TotalWeight = 0.0;

float3 PrefilterEnvMap(float roughness, float3 R) {
	float3 N = R;
	float3 V = R;
	float3 PrefilteredColor = float3(0.0, 0.0, 0.0);
	int numSamples = 1024;
	for (int i = 0; i < numSamples; i++) {
		float2 Xi = Hammersley(uint(i), numSamples);
		float3 H = ImportanceSampleGGX(Xi, roughness, N);
		float3 L = 2.0 * dot(V, H) * H - V;
		float NoL = clamp(dot(N, L), 0.0, 1.0);
		if (NoL > 0.0) {
			PrefilteredColor += EnvRadianceTexture.Sample(EnvRadianceState, L).rgb * NoL;
			TotalWeight += NoL;
		}
	}
	return PrefilteredColor / TotalWeight;

}

float3 RotateVectorY(float angle, float3 vec) {
	angle = radians(angle);
	float3x3 rotationMatrix = { float3(cos(angle),0.0,sin(angle)),
							float3(0.0,1.0,0.0),
							float3(-sin(angle),0.0,cos(angle)) };
	return mul(rotationMatrix, vec);
}

float3 Lighting(float3 F0) {
	float3 result = float3(0.0, 0.0, 0.0);
	for (int i = 0; i < LightCount; i++) {
		float3 Li = -light.Direction;
		float3 Lradiance = light.Radiance;
		float3 Lh = normalize(Li + params.View);

		float cosLi = max(0.0, dot(params.NormalVector, Li));
		float cosLh = max(0.0, dot(params.NormalVector, Lh));

		float3 F = fresnelSchlick(F0, max(0.0, dot(Lh, params.View)));
		float D = ndfGGX(cosLh, params.RoughnessValue);
		float G = gaSchlickGGX(cosLi, params.NdotV, params.RoughnessValue);

		float3 kd = (1.0 - F) * (1.0 - params.MetalnessValue);
		float3 diffuseBRDF = kd * params.AlbedoColor;

		float3 specularBRDF = (F * D * G) / max(Epsilon, 4.0 * cosLi * params.NdotV);

		result += (diffuseBRDF + specularBRDF) * Lradiance * cosLi;
	}
	return result;
}

float3 IBL(float3 F0, float3 Lr) {
	float3 irradiance = EnvIrradianceTexture.Sample(EnvIrradianceState, params.NormalVector).rgb;
	float3 F = fresnelSchlickRoughness(F0, params.NdotV, params.RoughnessValue);
	float3 kd = (1.0 - F) * (1.0 - params.MetalnessValue);
	float3 diffuseIBL = params.AlbedoColor * irradiance;

	int u_EnvRadianceTexLevels = 1;
	//int u_EnvRadianceTexLevels = textureQueryLevels(EnvRadianceTexture);
	float NoV = clamp(params.NdotV, 0.0, 1.0);
	float3 R = 2.0 * dot(params.View, params.NormalVector) * params.NormalVector - params.View;
	float3 specularIrradiance = float3(0.0, 0.0, 0.0);

	if (u_RadiancePrefilter > 0.5)
		specularIrradiance = PrefilterEnvMap(params.RoughnessValue * params.RoughnessValue, R) * u_RadiancePrefilter;
	else
		specularIrradiance = EnvRadianceTexture.tex3dlod(EnvRadianceState, float4(RotateVectorY(EnvMapRotation, Lr), sqrt(params.RoughnessValue) * u_EnvRadianceTexLevels)).rgb * (1.0 - material.u_RadiancePrefilter);

	float2 specularBRDF = texture(u_BRDFLUTTexture, float2(params.NdotV, 1.0 - params.RoughnessValue)).rg;
	float3 specularIBL = specularIrradiance * (F * specularBRDF.x + specularBRDF.y);

	return kd * diffuseIBL + specularIBL;
}

float4 main(PS_Input input) : SV_TARGET {
	params.AlbedoColor = AlbedoTexture.Sample(AlbedoState, input.TexCoord).rgb; // texture(AlbedoTexture, vs_input.TexCoord).rgb;
	params.MetalnessValue = MetalnessTexture.Sample(MetalnessState, input.TexCoord).r;
	params.RoughnessValue = RoughnessTexture.Sample(RoughnessState, input.TexCoord).r;
	params.RoughnessValue = max(params.RoughnessValue, 0.05);

	if (u_NormalTexToggle > 0.5) {
		params.NormalVector = norm(2.0 * NormalTexture.Sample(NormalState, input.TexCoord).rgb - 1.0);
		params.NormalVector = norm(mul(vs_input.WorldNormals, params.NormalVector));
	}

	return float4((vs_input.CameraPosition), 1.0);
}