
struct VS_INPUT {
    float3 position : POSITION;
    float2 texcoord : TEXCOORD;
    float texID : ID;
    float4 color : COLOR;
};

cbuffer render Camera : register(b0) {
    float4x4 r_MVP;
};

struct PS_INPUT {
    float4 positionCS : SV_POSITION;
    float2 texcoord : TEXCOORD;
    float texID : ID;
    float4 color : COLOR;
};

PS_INPUT main(VS_INPUT input) {
    PS_INPUT output;

    float4 position = float4(input.position, 1.0f);
    position = mul(position, r_MVP);

    output.positionCS = position;
    output.texcoord = input.texcoord;
    output.texID = input.texID;
    output.color = input.color;

    return output;
}