#type vertex

struct VS_INPUT {
	float3 position : POSITION;
	float2 texcoord : TEXCOORD;
	float texID : ID;
	float4 color : COLOR;
};

cbuffer RCamera : register(b0) {
	float4x4 r_Proj;
	float4x4 r_View;
	float4x4 r_Transform;
};

struct PS_INPUT {
	float4 positionCS : SV_POSITION;
	float2 texcoord : TEXCOORD;
	float texID : ID;
	float4 color : COLOR;
};

PS_INPUT main(VS_INPUT input) {
	PS_INPUT output;

	float4 position = float4(input.position, 1.0f);
	position = mul(position, r_Transform);
	position = mul(position, r_View);
	position = mul(position, r_Proj);

	output.positionCS = position;
	output.texcoord = input.texcoord;
	output.texID = input.texID;
	output.color = input.color;

	return output;
};



#type pixel

struct PS_INPUT {
	float4 positionCS : SV_POSITION;
	float2 texcoord : TEXCOORD;
	float texID : ID;
	float4 color : COLOR;
};

cbuffer PSConstantBuffer : register(b0) {
	float4 u_Color;
};


Texture2D tex[32] : register(t0);
SamplerState samp : register(s0);

float4 main(PS_INPUT input) : SV_TARGET{
	float4 texColor = { 1.0, 1.0, 1.0, 1.0 };
	
	switch (input.texID) {
	case 0:
		texColor *= tex[0].Sample(samp, input.texcoord); break;
	case 1:
		texColor *= tex[1].Sample(samp, input.texcoord); break;
	case 2:
		texColor *= tex[2].Sample(samp, input.texcoord); break;
	case 3:
		texColor *= tex[3].Sample(samp, input.texcoord); break;
	case 4:
		texColor *= tex[4].Sample(samp, input.texcoord); break;
	case 5:
		texColor *= tex[5].Sample(samp, input.texcoord); break;
	case 6:
		texColor *= tex[6].Sample(samp, input.texcoord); break;
	case 7:
		texColor *= tex[7].Sample(samp, input.texcoord); break;
	}
	return texColor;
};