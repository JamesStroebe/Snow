
struct VS_INPUT {
	float3 position : POSITION;
	float2 texcoord : TEXCOORD;
};

cbuffer render Camera : register(b0) {
    float4x4 r_Projection;
};

struct PS_INPUT {
    float4 positionCS : SV_POSITION;
    float2 texcoord : TEXCOORD;
};

PS_INPUT main(VS_INPUT input){
    PS_INPUT output;

    float4 position = float4(input.position, 1.0f);
    position = mul(position, r_Projection);

    output.positionCS = position;
    output.texcoord = input.texcoord;

    return output;
}