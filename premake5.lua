workspace "Snow"
    architecture "x64"
    targetdir "build"

    configurations {
        "Debug",
        "Release",
        "Dist"
    }

    startproject "Sandbox"

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

IncludeDir = {}

IncludeDir["GLAD"] = "Snow/vendor/glad/include"
IncludeDir["ImGui"] = "Snow/vendor/imgui"
IncludeDir["Vulkan"] = "Snow/vendor/vulkan/Include"
IncludeDir["Assimp"] = "Snow/vendor/assimp/include"
IncludeDir["FreeType"] = "Snow/vendor/freetype/include"
IncludeDir["STBImage"] = "Snow/vendor/stb_image"


include "Snow/vendor/glad"
include "Snow/vendor/vulkan"
include "Snow/vendor/freetype"
include "Snow/vendor/imgui"

project "Snow"
    location "Snow"
    kind "StaticLib"
    language "C++"
    cppdialect "C++17"
    staticruntime "on"

    characterset ("MBCS")

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

    pchheader "spch.h"
    pchsource "Snow/source/spch.cpp"

    files {
        "%{prj.name}/source/**.h",
        "%{prj.name}/source/**.inl",
        "%{prj.name}/source/**.c",
        "%{prj.name}/source/**.hpp",
        "%{prj.name}/source/**.cpp",

        "%{prj.name}/vendor/stb_image/stb_image.*"
    }

    includedirs {
        "%{prj.name}/source",
		"%{prj.name}/vendor/spdlog/include",
		"%{IncludeDir.Assimp}",
		"%{IncludeDir.FreeType}",
		"%{IncludeDir.STBImage}",
        --"%{prj.name}/vendor",
        "%{IncludeDir.GLAD}",
        "%{IncludeDir.Vulkan}",
        "%{IncludeDir.ImGui}"
    }

    links {
        "GLAD",
        "Vulkan",
        "FreeType",
        "ImGui",
        "opengl32.lib",
    }

    filter "system:windows"
        systemversion "latest"

        links {
            "d3d11.lib",
			"dxgi.lib",
			"D3DCompiler.lib"
        }

    filter "configurations:Debug"
        defines "SNOW_DEBUG"
        symbols "On"

    filter "configurations:Release"
        defines "SNOW_RELEASE"
        optimize "On"

    filter "configurations:Dist"
        defines "SNOW_DIST"
        optimize "On"

project "Snowball"
    location "Snowball"
    kind "ConsoleApp"
    language "C++"
    cppdialect "C++17"
    staticruntime "On"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

    links {
        "Snow"
    }

    files {
        "%{prj.name}/**.h",
        "%{prj.name}/**.c",
        "%{prj.name}/**.hpp",
        "%{prj.name}/**.cpp"
    }

    includedirs {
        "%{prj.name}/source",
        "Snow/source",
        --"Snow/vendor",
		--"Snow/vendor/freetype/include",
		"Snow/vendor/spdlog/include",
		"Snow/vendor/imgui",
        "Snow/vendor/assimp/include"

    }

    filter "system:windows"
        systemversion "latest"


    filter "configurations:Debug"
        defines "SNOW_DEBUG"
        symbols "On"
		links {
			"Snow/vendor/assimp/bin/Debug/assimp-vc140-mt.lib"
		}

    filter "configurations:Release"
        defines "SNOW_RELEASE"
        optimize "On"
		links {
			"Snow/vendor/assimp/bin/Release/assimp-vc140-mt.lib"
		}

    filter "configurations:Dist"
        defines "SNOW_DIST"
        optimize "On"
		links {
			"Snow/vendor/assimp/bin/Release/assimp-vc140-mt.lib"
		}

