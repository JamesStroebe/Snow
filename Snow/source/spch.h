///////////////////////////////////////////
// File: spch.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once

#include <iostream>
#include <memory>
#include <vector>
#include <string>
#include <array>
#include <tuple>
#include <map>
#include <unordered_map>
#include <unordered_set>

#include <utility>
#include <functional>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <optional>

#include "Setup.h"

//#include "Snow/System/Memory.h"

#include "Snow/Core/Log.h"
#include "Snow/Core/Base.h"
#include "Snow/Core/Events/Events.h"


