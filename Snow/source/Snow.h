///////////////////////////////////////////
// File: Snow.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once


//----------CORE STUFF----------
#include "Snow/Core/Application.h"
#include "Snow/Core/Base.h"
#include "Snow/Core/Buffer.h"
#include "Snow/Core/Input.h"
#include "Snow/Core/Layer.h"
#include "Snow/Core/LayerStack.h"
#include "Snow/Core/Log.h"
#include "Snow/Core/Thread.h"
#include "Snow/Core/Time/Timestep.h"
#include "Snow/Core/Window.h"

#include "Snow/Core/Events/Events.h"

//-------------ENTITY--------------
#include "Snow/Entity/Component.h"
#include "Snow/Entity/EntityComponentSystem.h"
#include "Snow/Entity/System.h"

//--------------MATH---------------

#include "Snow/Math/Math.h"


//----------RENDERER STUFF----------
#include "Snow/Render/Material.h"
#include "Snow/Render/Mesh.h"
#include "Snow/Render/Renderer.h"
#include "Snow/Render/Renderer2D.h"
#include "Snow/Render/Renderer3D.h"
#include "Snow/Render/RenderThread.h"
#include "Snow/Scene/Scene.h"


#include "Snow/Render/API/Buffer.h"
#include "Snow/Render/API/Context.h"
#include "Snow/Render/API/Device.h"
#include "Snow/Render/API/Framebuffer.h"
#include "Snow/Render/API/Pipeline.h"
#include "Snow/Render/API/RenderPass.h"
#include "Snow/Render/API/SwapChain.h"
#include "Snow/Render/API/Texture.h"

#include "Snow/Render/Camera/Camera.h"
#include "Snow/Render/Camera/OrthographicCamera.h"
#include "Snow/Render/Camera/PerspectiveCamera.h"

//#include "Snow/Render/Font/Font.h"

#include "Snow/Render/Shaders/Shader.h"
#include "Snow/Render/Shaders/ShaderLibrary.h"
#include "Snow/Render/Shaders/ShaderUniform.h"


#include "Snow/Render/Sprite/AnimatedSprite.h"
#include "Snow/Render/Sprite/Sprite.h"
#include "Snow/Render/Sprite/SpriteSheet.h"



//----------UTILITIES STUFF----------
#include "Snow/Core/Serialization/SerializeArray.h"
#include "Snow/Core/Serialization/SerializeBase.h"
#include "Snow/Core/Serialization/SerializeDatabase.h"
#include "Snow/Core/Serialization/SerializeField.h"
#include "Snow/Core/Serialization/SerializeObject.h"
#include "Snow/Core/Serialization/SerializeString.h"
#include "Snow/Core/Serialization/SerializeUtil.h"

#include "Snow/Utils/StringUtils.h"