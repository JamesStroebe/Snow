///////////////////////////////////////////
// File: Scene.h
//
// Original Author: James Stroebe
//
// Creation Date: 06/04/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Core/Base.h"
#include "Snow/Render/Camera/Camera.h"
#include "Snow/Render/Camera/OrthographicCamera.h"
#include "Snow/Render/Camera/PerspectiveCamera.h"
#include "Snow/Render/API/Texture.h"

#include "Snow/Entity/EntityComponentSystem.h"

namespace Snow {
	namespace Scene {
		class Scene {
		public:
			Scene() {}
			~Scene() {}


			virtual void SetCamera(Render::Camera::Camera& camera) = 0;

			//virtual void AddEntity(Entity::EntityHandle entity) = 0;
			
		};

		class Scene2D : public Scene {
		public:

			static inline Render::Camera::Camera& GetCamera() { return *s_Camera; }
			virtual void SetCamera(Render::Camera::Camera& camera) override { s_Camera = &camera; }

			static void AddEntity(Entity::EntityHandle e) { s_Entities.push_back(e); }
			static std::vector<Entity::EntityHandle>& GetEntities() { return s_Entities; }

			static Core::Ref<Render::API::Texture2D>& GetBackground() { return s_Background; }
			void SetBackground(const Core::Ref<Render::API::Texture2D>& backgroundTexture) { s_Background = backgroundTexture; }
		private:

			static std::vector<Entity::EntityHandle> s_Entities;

			static Render::Camera::Camera* s_Camera;
			static Core::Ref<Render::API::Texture2D> s_Background;
		};

		class Scene3D : public Scene {
		public:

			inline Render::Camera::Camera* GetCamera() const { return m_Camera; }
			virtual void SetCamera(Render::Camera::Camera& camera) override { m_Camera = &camera; }

			void SetSkybox(const Core::Ref<Render::API::TextureCube>& skybox) {}
		private:
			Render::Camera::Camera* m_Camera;
			static Core::Ref<Render::API::TextureCube> s_Skybox;
		};
	}
}