#include "spch.h"
#include "Snow/Scene/Scene.h"

namespace Snow {
	namespace Scene {
		Render::Camera::Camera* Scene2D::s_Camera = nullptr;
		std::vector<Entity::EntityHandle> Scene2D::s_Entities = {};
		Core::Ref<Render::API::Texture2D> Scene2D::s_Background = nullptr;

		Core::Ref<Render::API::TextureCube> Scene3D::s_Skybox = nullptr;
	}
}