#include "spch.h"

#include "Snow/Core/Input.h"

#include <Windows.h>
#include <windowsx.h>

#include "Snow/Platform/Windows/WindowsWindow.h"
#include "Snow/Core/Events/Events.h"

#include "Snow/Core/Application.h"

namespace Snow {

	HWND m_HWND;

	void Core::Input::Init() {
		auto window = static_cast<WindowsWindow*>(Application::GetWindow().get());
		m_HWND = window->GetNativeWindowHandle();
	}

	void Core::InputManager::PlatformUpdate() {
		POINT mouse;
		GetCursorPos(&mouse);
		ScreenToClient(m_HWND, &mouse);

		Math::Vector2f mousePos = Math::Vector2f(mouse.x, mouse.y);
		if (mousePos != m_MousePosition) {
			Event::MouseMovedEvent event(mouse.x, mouse.y);
			m_EventCallback(event);
			m_MousePosition = mousePos;
		}
	}

	void Core::InputManager::SetMousePosition(const Math::Vector2f& position) {
		POINT pt = { (LONG)position.x, (LONG)position.y };
		ClientToScreen(m_HWND, &pt);
		SetCursorPos(pt.x, pt.y);
	}

	void Core::InputManager::SetMouseCursor(Math::int32_t cursor) {
		if (cursor == SNOW_NO_CURSOR) {
			SetCursor(SNOW_NO_CURSOR);
			while (ShowCursor(false) >= 0);
		}
		else {
			SetCursor(LoadCursor(NULL, IDC_ARROW));
			ShowCursor(true);
		}
	}

	void Core::KeyCallback(InputManager* inputManager, int flags, int key, unsigned int message) {
		bool pressed = message == WM_KEYDOWN || message == WM_SYSKEYDOWN;
		inputManager->m_KeyState[key] = pressed;
		bool repeat = (flags >> 30) & 1;

		int modifier = 0;
		switch (key) {
		case SNOW_KEY_CONTROL:
			modifier = SNOW_MOD_LEFT_CONTROL;
			break;
		case SNOW_KEY_ALT:
			modifier = SNOW_MOD_LEFT_ALT;
			break;
		case SNOW_KEY_SHIFT:
			modifier = SNOW_MOD_LEFT_SHIFT;
			break;
		}
		if (pressed)
			inputManager->m_KeyModifiers |= modifier;
		else
			inputManager->m_KeyModifiers &= ~(modifier);

		if (pressed) {
			Event::KeyPressedEvent event(key, repeat, inputManager->m_KeyModifiers);
			inputManager->m_EventCallback(event);
		}
		else {
			Event::KeyReleasedEvent event(key);
			inputManager->m_EventCallback(event);
		}	
	}

	void Core::MouseButtonCallback(InputManager* inputManager, int button, int x, int y) {
		bool down = false;
		switch (button) {
		case WM_LBUTTONDOWN:
			SetCapture(m_HWND);
			button = SNOW_MOUSE_LEFT;
			down = true;
			break;
		case WM_LBUTTONUP:
			ReleaseCapture();
			button = SNOW_MOUSE_LEFT;
			down = false;
			break;
		case WM_RBUTTONDOWN:
			SetCapture(m_HWND);
			button = SNOW_MOUSE_RIGHT;
			down = true;
			break;
		case WM_RBUTTONUP:
			ReleaseCapture();
			button = SNOW_MOUSE_RIGHT;
			down = false;
			break;
		case WM_MBUTTONDOWN:
			SetCapture(m_HWND);
			button = SNOW_MOUSE_MIDDLE;
			down = true;
			break;
		case WM_MBUTTONUP:
			ReleaseCapture();
			button = SNOW_MOUSE_MIDDLE;
			down = false;
			break;
		}
		inputManager->m_MouseButtons[button] = down;
		inputManager->m_MousePosition.x = (float)x;
		inputManager->m_MousePosition.y = (float)y;

		SNOW_ASSERT(inputManager->m_EventCallback);

		if (down) {
			Event::MouseButtonPressedEvent event(button);
			inputManager->m_EventCallback(event);
		}
		else {
			Event::MouseButtonReleasedEvent event(button);
			inputManager->m_EventCallback(event);
		}
			
	}

	void Core::MouseScrollCallback(InputManager* inputManager, int XOffset, int YOffset) {
		inputManager->m_MouseScroll.x = XOffset;
		inputManager->m_MouseScroll.y = YOffset;

		Event::MouseScrolledEvent event(XOffset, YOffset);
		inputManager->m_EventCallback(event);
	}
}