#pragma once

#include "spch.h"
#include "Snow/Core/Window.h"

#include "Snow/Math/Math.h"

namespace Snow {
	
	class WindowsWindow : public Core::Window {
	public:
		WindowsWindow(const Core::WindowProps& props);
		~WindowsWindow();
			
		void Init(const Core::WindowProps& props);
		void OnUpdate() override;
		

		inline unsigned int GetWidth() const override { return m_Data.Width; }
		inline unsigned int GetHeight() const override { return m_Data.Height; }

		inline int GetXPos() const override { return m_Data.xPosition; }
		inline int GetYPos() const override { return m_Data.yPosition; }

		virtual void SetTitle(const std::string& title) override;

		void SetVSync(bool enabled) override { m_Data.VSyncEnabled = enabled; }
		bool IsVSync() const override { return m_Data.VSyncEnabled; }

		inline HINSTANCE GetInstance() const { return m_Data.HInstance; }
		inline HWND GetNativeWindowHandle() { return m_Data.HWnd; }
		inline HDC GetDeviceContextHandle() { return m_Data.HDC; }

	private:
		struct WindowsData {
			std::string Title;
			Math::uint32_t Width, Height;
			Math::int32_t xPosition, yPosition;
			bool VSyncEnabled;
			EventCallbackFn EventCallback;

			HINSTANCE HInstance;
			HWND HWnd;
			HDC HDC;
			LONG WindowStyle;
		};
		
		WindowsData m_Data;
	protected:
		bool OnWindowFullscreen(Core::Event::WindowFullscreenEvent& e);
		bool OnWindowMoved(Core::Event::WindowMovedEvent& e);
	};
}
