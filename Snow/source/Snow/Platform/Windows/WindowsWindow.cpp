#include "spch.h"
#include "WindowsWindow.h"

#include "Snow/Core/Application.h"

#include "Snow/Core/Events/ApplicationEvent.h"

#include <windows.h>
#include <windowsx.h>

EXTERN_C IMAGE_DOS_HEADER __ImageBase;

namespace Snow {

	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	extern void Core::MouseButtonCallback(InputManager* inputManager, int button, int x, int y);
	extern void Core::KeyCallback(InputManager* inputManager, int flags, int key, unsigned int message);
	extern void Core::MouseScrollCallback(InputManager* inputManager, int xOffset, int yOffset);

	static PIXELFORMATDESCRIPTOR GetPixelFormat() {
		PIXELFORMATDESCRIPTOR result = {};
		result.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		result.nVersion = 1;
		result.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		result.iPixelType = PFD_TYPE_RGBA;
		result.cColorBits = 32;
		result.cDepthBits = 24;
		result.cStencilBits = 8;
		result.cAuxBuffers = 0;
		result.iLayerType = PFD_MAIN_PLANE;
		return result;
	}

	WindowsWindow::WindowsWindow(const Core::WindowProps& props) {
		Init(props);
	}

	WindowsWindow::~WindowsWindow() {

	}

	std::wstring s2ws(const std::string& s) {
		int len;
		int slength = (int)s.length() + 1;
		len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
		wchar_t* buf = new wchar_t[len];
		MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
		std::wstring r(buf);
		delete[] buf;
		return r;
	}

	void WindowsWindow::Init(const Core::WindowProps& props) {

		m_Data.Width = props.Width;
		m_Data.Height = props.Height;
		m_Data.Title = props.Title;

		Window::m_InputManager = new Core::InputManager();
		Window::m_InputManager->SetMouseGrabbed(false);

		m_Data.HInstance = (HINSTANCE)& __ImageBase;
		WNDCLASSEXA winClass;

		winClass.hInstance = m_Data.HInstance;
		winClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		winClass.lpfnWndProc = (WNDPROC)WndProc;


		winClass.lpszClassName = m_Data.Title.c_str();
		winClass.cbClsExtra = 0;
		winClass.cbWndExtra = 0;
		winClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		winClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		winClass.hIconSm = winClass.hIcon;
		winClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		winClass.lpszMenuName = NULL;
		winClass.cbSize = sizeof(WNDCLASSEX);

		if (!RegisterClassExA(&winClass))
			SNOW_CORE_ERROR("Could not register Win class");

		RECT size = { 0,0,(LONG)m_Data.Width, (LONG)m_Data.Height };

		int width = m_Data.Width;
		int height = m_Data.Height;


		int posX = (GetSystemMetrics(SM_CXSCREEN) - width) / 2;
		int posY = (GetSystemMetrics(SM_CYSCREEN) - height) / 2;

		AdjustWindowRectEx(&size, WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, true, WS_EX_APPWINDOW | WS_EX_WINDOWEDGE);

		m_Data.HWnd = CreateWindowExA(WS_EX_APPWINDOW | WS_EX_WINDOWEDGE,
			winClass.lpszClassName, m_Data.Title.c_str(),
			WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU | WS_MAXIMIZEBOX | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_THICKFRAME | WS_POPUP,
			posX, posY, width, height,
			NULL, NULL, m_Data.HInstance, NULL);
		m_Data.xPosition = posX;
		m_Data.yPosition = posY;

		Window::RegisterWindowClass(m_Data.HWnd, this);
		m_Data.HDC = GetDC(m_Data.HWnd);
		PIXELFORMATDESCRIPTOR pfd = GetPixelFormat();
		int pixelFormat = ChoosePixelFormat(m_Data.HDC, &pfd);
		if (pixelFormat) {
			if (!SetPixelFormat(m_Data.HDC, pixelFormat, &pfd))
				SNOW_CORE_ERROR("Failed setting pixel format");
		}
		else
			SNOW_CORE_ERROR("Failed choosing pixel format");

		ShowWindow(m_Data.HWnd, SW_SHOW);
		SetFocus(m_Data.HWnd);
	}

	void WindowsWindow::OnUpdate() {
		MSG message;
		while (PeekMessage(&message, NULL, NULL, NULL, PM_REMOVE) > 0) {
			if (message.message == WM_QUIT) {
				Core::Application& app = Core::Application::Get();
				app.OnShutdown();
				return;
			}
			TranslateMessage(&message);
			DispatchMessage(&message);
		}
		m_InputManager->PlatformUpdate();
	}

	void WindowsWindow::SetTitle(const std::string& title) {
		SetWindowText(m_Data.HWnd, title.c_str());
	}

	

	void ResizeCallback(Core::Window* window, int width, int height, int sizeMessage) {
		RECT size = { 0, 0, width, height };
		AdjustWindowRectEx(&size, NULL, FALSE, NULL);

		
		if (sizeMessage == 1) {
			Core::Event::WindowMinimizedEvent event;
			window->m_EventCallback(event);
		}
		else if (sizeMessage == 2) {
			Core::Event::WindowMaximizedEvent event;
			Core::Event::WindowResizeEvent event2(width, height);
			window->m_EventCallback(event);
			window->m_EventCallback(event2);
		}
		else {
			Core::Event::WindowResizeEvent event(width, height);
			window->m_EventCallback(event);
		}
	}

	void FocusCallback(Core::Window* window, bool focused) {
		if (!focused) {
			window->m_InputManager->ClearKeys();
			window->m_InputManager->ClearMouseButtons();
		}
		
		//Core::Event::WindowFocusEvent event(focused);
		//window->m_EventCallback(event);
	}

	void MoveCallback(Core::Window* window, int xPos, int yPos) {
		Core::Event::WindowMovedEvent event(xPos, yPos);
		window->m_EventCallback(event);
	}

	bool WindowsWindow::OnWindowFullscreen(Core::Event::WindowFullscreenEvent& e) {
		m_Data.WindowStyle = GetWindowLong(m_Data.HWnd, GWL_STYLE);
		m_Data.WindowStyle &= ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_SYSMENU);
		SetWindowLongA(m_Data.HWnd, GWL_STYLE, m_Data.WindowStyle);
		return true;
	}

	bool WindowsWindow::OnWindowMoved(Core::Event::WindowMovedEvent& e) {
		m_Data.xPosition = e.GetX();
		m_Data.yPosition = e.GetY();
		return false;
	}

	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
		LRESULT result = NULL;
		Core::Window* window = Core::Window::GetWindowClass(hWnd);
		if (window == nullptr)
			return DefWindowProc(hWnd, message, wParam, lParam);

		Core::InputManager* inputManager = window->GetInputManager();
		switch (message) {
		case WM_ACTIVATE: {
			if (!HIWORD(wParam)) {
				//active
			}
			else {
				// inactive
			}

			return 0;
		}
		case WM_SYSCOMMAND:{
			switch (wParam)
			{
			case SC_SCREENSAVE:
			case SC_MONITORPOWER:
				return 0;
			}
			result = DefWindowProc(hWnd, message, wParam, lParam);
		} break;
		case WM_SETFOCUS:
			FocusCallback(window, true);
			break;
		case WM_KILLFOCUS:
			FocusCallback(window, false);
			break;
		case WM_CLOSE:
		case WM_DESTROY:
			DestroyWindow(hWnd);
			PostQuitMessage(0);
			break;
		case WM_KEYDOWN:
		case WM_KEYUP:
		case WM_SYSKEYDOWN:
		case WM_SYSKEYUP:
			KeyCallback(inputManager, lParam, wParam, message);
			break;
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
		case WM_MBUTTONDOWN:
		case WM_MBUTTONUP:
			MouseButtonCallback(inputManager, message, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
			break;
		case WM_MOUSEWHEEL:
			MouseScrollCallback(inputManager, 0, GET_WHEEL_DELTA_WPARAM(wParam));
			break;
		case WM_MOUSEHWHEEL:
			MouseScrollCallback(inputManager, GET_WHEEL_DELTA_WPARAM(wParam), 0);
			break;
		case WM_SIZE:
			ResizeCallback(window, LOWORD(lParam), HIWORD(lParam), wParam);
			break;
		case WM_MOVE:
			MoveCallback(window, LOWORD(lParam), HIWORD(lParam));
		default:
			result = DefWindowProc(hWnd, message, wParam, lParam);
		}
		return result;
	}

}

