#pragma once

#include "Snow/Render/API/Pipeline.h"

#include "Snow/Platform/DirectX/DirectXCommon.h"

namespace Snow {
	class DirectXPipeline : public Render::API::Pipeline {
	private:
		struct DirectXPipelineData {
			ID3D10Blob* vsBlob;
			ID3D11VertexShader* VertexShader;
			ID3D10Blob* psBlob;
			ID3D11PixelShader* PixelShader;
		};
	public:
		DirectXPipeline(const Render::API::PipelineSpecification& spec);
		~DirectXPipeline();

		virtual void Bind() const override;

		virtual Render::API::PrimitiveType GetPrimitiveType() const { return m_Topology; }
		virtual void SetPrimitiveType(Render::API::PrimitiveType type);

		static D3D11_PRIMITIVE_TOPOLOGY SnowToDX11PrimitiveType(Render::API::PrimitiveType type);

		virtual void UploadBuffer(const Core::Ref<Render::API::UniformBuffer>& uniformBuffer) override;

		inline const DirectXPipelineData& GetData() { return m_PipelineData; }
		inline ID3D11InputLayout* GetInputLayout() { return m_InputLayout; }

		virtual const Render::API::PipelineSpecification& GetSpecification() const override { return m_Specification; }
	private:
		void LinkAndCompileShaders();

		

		Math::uint32_t FindSlot(const Core::Ref<Render::API::UniformBuffer>& uniformBuffer);

		Render::API::PipelineSpecification m_Specification;

		Render::API::PrimitiveType m_Topology;

		HRESULT m_Result;
		mutable DirectXPipelineData m_PipelineData;

		ID3D11InputLayout* m_InputLayout;
	};
}
