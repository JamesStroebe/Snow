#pragma once

#include <Windows.h>
#include <Windowsx.h>


#include <d3d11.h>
#include <d3d11_1.h>
#include <d3dcompiler.h>

#include <dxgi.h>
#include <dxgi1_2.h>
#include <dxgi1_3.h>
#include <dxgi1_4.h>
#include <dxgi1_5.h>
#include <dxgi1_6.h>
#include <dxgicommon.h>
#include <dxgidebug.h>
#include <dxgiformat.h>

#include <d3d10.h>



#define ReleaseCOM(x) if(x) {x->Release(); x=nullptr;}
