#include "spch.h"
#include "Snow/Platform/DirectX/DirectXPipeline.h"
#include "Snow/Platform/DirectX/DirectXContext.h"
#include "Snow/Platform/DirectX/DirectXShader.h"
#include "Snow/Platform/Windows/WindowsWindow.h"

#include "Snow/Render/Renderer.h"

namespace Snow {

	D3D11_PRIMITIVE_TOPOLOGY DirectXPipeline::SnowToDX11PrimitiveType(Render::API::PrimitiveType type) {
		switch (type) {
		case Render::API::PrimitiveType::Point:	return D3D11_PRIMITIVE_TOPOLOGY_POINTLIST;
		case Render::API::PrimitiveType::Triangle:	return D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
		case Render::API::PrimitiveType::TriangleStrip:	return D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
		}
	}

	static DXGI_FORMAT ShaderDataTypeToDirectXBaseType(Render::API::VertexAttribType type) {
		switch (type) {
		case Render::API::VertexAttribType::Float:		return DXGI_FORMAT_R32_FLOAT;
		case Render::API::VertexAttribType::Float2:		return DXGI_FORMAT_R32G32_FLOAT;
		case Render::API::VertexAttribType::Float3:		return DXGI_FORMAT_R32G32B32_FLOAT;
		case Render::API::VertexAttribType::Float4:		return DXGI_FORMAT_R32G32B32A32_FLOAT;
		case Render::API::VertexAttribType::Mat3:		return DXGI_FORMAT_R32_FLOAT;
		case Render::API::VertexAttribType::Mat4:		return DXGI_FORMAT_R32_FLOAT;
		case Render::API::VertexAttribType::Int:		return DXGI_FORMAT_R32_SINT;
		case Render::API::VertexAttribType::Int2:		return DXGI_FORMAT_R32G32_SINT;
		case Render::API::VertexAttribType::Int3:		return DXGI_FORMAT_R32G32B32_SINT;
		case Render::API::VertexAttribType::Int4:		return DXGI_FORMAT_R32G32B32A32_SINT;
		case Render::API::VertexAttribType::Bool:		return DXGI_FORMAT_R1_UNORM;
		}
	}

	DirectXPipeline::DirectXPipeline(const Render::API::PipelineSpecification& spec) {
		m_Specification = spec;
		m_Topology = Render::API::PrimitiveType::Triangle;

		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		DirectXDevice* dxDevice = dxContext->GetDevice();
		WindowsWindow* window = static_cast<WindowsWindow*>(dxContext->GetSpecification().Window.get());

		D3D11_VIEWPORT viewport;
		viewport.TopLeftX = 0;
		viewport.TopLeftY = 0;
		viewport.Width = window->GetWidth();
		viewport.Height = window->GetHeight();
		viewport.MinDepth = 0.0f;
		viewport.MaxDepth = 1.0f;

		dxContext->GetDeviceContextHandle()->RSSetViewports(1, &viewport);

		D3D11_RASTERIZER_DESC rasterDesc;
		ZeroMemory(&rasterDesc, sizeof(D3D11_RASTERIZER_DESC));
		rasterDesc.AntialiasedLineEnable = false;
		rasterDesc.CullMode = D3D11_CULL_BACK;
		rasterDesc.DepthBias = 0;
		rasterDesc.DepthBiasClamp = 0.0f;
		rasterDesc.DepthClipEnable = true;
		rasterDesc.FillMode = D3D11_FILL_SOLID;
		rasterDesc.FrontCounterClockwise = true;
		rasterDesc.MultisampleEnable = false;
		rasterDesc.ScissorEnable = false;
		rasterDesc.SlopeScaledDepthBias = 0.0f;

		ID3D11RasterizerState* rasterizerState;
		m_Result = dxDevice->GetLogicalDevice()->CreateRasterizerState(&rasterDesc, &rasterizerState);
		dxContext->GetDeviceContextHandle()->RSSetState(rasterizerState);
		ReleaseCOM(rasterizerState);

		LinkAndCompileShaders();
	}

	DirectXPipeline::~DirectXPipeline() {}


	void DirectXPipeline::Bind() const {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());

		if(m_PipelineData.VertexShader)
			dxContext->GetDeviceContextHandle()->VSSetShader(m_PipelineData.VertexShader, NULL, 0);
		if(m_PipelineData.PixelShader)
			dxContext->GetDeviceContextHandle()->PSSetShader(m_PipelineData.PixelShader, NULL, 0);
		

		const std::vector<Render::API::AttribElement>& l = m_Specification.VertexBufferLayout.GetElements();
		std::vector<D3D11_INPUT_ELEMENT_DESC> descV;
		for (unsigned int i = 0; i < l.size(); i++) {
			const Render::API::AttribElement& element = l[i];
			D3D11_INPUT_ELEMENT_DESC desc = { element.Name.c_str(), 0, ShaderDataTypeToDirectXBaseType(element.Type), 0, element.Offset, D3D11_INPUT_PER_VERTEX_DATA, 0 };

			descV.push_back(desc);
		}

		ID3D11InputLayout* inputLayout;
		dxContext->GetDevice()->GetLogicalDevice()->CreateInputLayout(descV.data(), l.size(), m_PipelineData.vsBlob->GetBufferPointer(), m_PipelineData.vsBlob->GetBufferSize(), &inputLayout);
	}

	void DirectXPipeline::SetPrimitiveType(Render::API::PrimitiveType type) {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		dxContext->GetDeviceContextHandle()->IASetPrimitiveTopology(SnowToDX11PrimitiveType(type));
	}

	void DirectXPipeline::LinkAndCompileShaders() {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		DirectXDevice* dxDevice = dxContext->GetDevice();
		for (Math::uint32_t i = 0; i < m_Specification.Shaders.size(); i++) {
			DirectXShader* dxShader = static_cast<DirectXShader*>(m_Specification.Shaders[i].get());
			if (dxShader->GetType() == Render::Shader::ShaderType::VertexShader) {
				dxDevice->GetLogicalDevice()->CreateVertexShader(dxShader->GetData()->GetBufferPointer(), dxShader->GetData()->GetBufferSize(), NULL, &m_PipelineData.VertexShader);
				m_PipelineData.vsBlob = dxShader->GetData();
			}
			else if (dxShader->GetType() == Render::Shader::ShaderType::PixelShader) {
				dxDevice->GetLogicalDevice()->CreatePixelShader(dxShader->GetData()->GetBufferPointer(), dxShader->GetData()->GetBufferSize(), NULL, &m_PipelineData.PixelShader);
				m_PipelineData.psBlob = dxShader->GetData();
			}
		}
	}

	Math::uint32_t DirectXPipeline::FindSlot(const Core::Ref<Render::API::UniformBuffer>& uniformBuffer) {

		for (uint32_t i = 0; i < m_Specification.Shaders.size(); i++) {
			if (uniformBuffer->GetDomain() == m_Specification.Shaders[i]->GetType()) {
				auto& dxShader = m_Specification.Shaders[i];
				for (uint32_t j = 0; j < dxShader->GetRendererBuffers().size(); j++) {
					if (uniformBuffer->GetName() == dxShader->GetRendererBuffers()[j]->GetName()) {
						return dxShader->GetRendererBuffers()[j]->GetSlot();
					}
				}

				if (uniformBuffer->GetName() == dxShader->GetMaterialBuffer().GetName()) {
					return dxShader->GetMaterialBuffer().GetSlot();
				}
			}
		}
	}

	void DirectXPipeline::UploadBuffer(const Core::Ref<Render::API::UniformBuffer>& uniformBuffer) {
		uniformBuffer->Bind(FindSlot(uniformBuffer));
	}
}