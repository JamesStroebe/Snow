#pragma once

#include "Snow/Render/API/Context.h"

#include "Snow/Platform/DirectX/DirectXCommon.h"
#include "Snow/Platform/DirectX/DirectXDevice.h"
#include "Snow/Platform/DirectX/DirectXSwapChain.h"

namespace Snow {
	class DirectXContext : public Render::API::Context {
	public:
		DirectXContext::DirectXContext(const Render::API::ContextSpecification& spec);
		DirectXContext::~DirectXContext();

		DirectXDevice* GetDevice() { return m_Device; }
		DirectXSwapChain* GetSwapChain() { return m_SwapChain; }

		ID3D11DeviceContext* GetDeviceContextHandle() { return m_DeviceContext; }

		void SetDeviceContext(ID3D11DeviceContext* context) { m_DeviceContext = context; }

		virtual const Render::API::ContextSpecification& GetSpecification() const override { return m_Specification; }
	private:
		Render::API::ContextSpecification m_Specification;

		DirectXDevice* m_Device;
		DirectXSwapChain* m_SwapChain;

		ID3D11DeviceContext* m_DeviceContext;
	};
}