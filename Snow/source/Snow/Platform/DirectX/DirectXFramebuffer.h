#pragma once

#include "Snow/Render/API/Framebuffer.h"

namespace Snow {
	class DirectXFramebuffer : public Render::API::Framebuffer {
	public:
		DirectXFramebuffer(const Render::API::FramebufferSpecification& spec);
		~DirectXFramebuffer();

		virtual void Bind() const {}
		virtual void Unbind() const override {}
		virtual void BindToRead() const override {}
		virtual void BindColorAttachment(uint32_t slot) const override {}

		virtual void SetColor(Math::Vector4f color) override { m_Specification.Color = color; }

		virtual const Render::API::FramebufferSpecification& GetSpecification() const override { return m_Specification; }

	private:
		Render::API::FramebufferSpecification m_Specification;
	};
}
