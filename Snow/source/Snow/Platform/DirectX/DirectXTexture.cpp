#include "spch.h"

#include "Snow/Platform/DirectX/DirectXTexture.h"

#include "Snow/Platform/DirectX/DirectXContext.h"

#include "Snow/Render/RenderAPI.h"

#include <stb_image.h>

namespace Snow {
	static DXGI_FORMAT SnowToDXTextureFormat(Render::API::TextureFormat format) {
		switch (format) {
		case Render::API::TextureFormat::R8G8B8:	return DXGI_FORMAT_R8G8B8A8_UNORM;
		case Render::API::TextureFormat::R8G8B8A8:	return DXGI_FORMAT_R8G8B8A8_UNORM;
		case Render::API::TextureFormat::R8:		return DXGI_FORMAT_R8_UNORM;
		}
		return DXGI_FORMAT_UNKNOWN;
	}

	static D3D11_FILTER SnowToDXTextureFilter(Render::API::TextureFilter filter) {
		switch (filter) {
		case Render::API::TextureFilter::Linear:	return D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		case Render::API::TextureFilter::Nearest:	return D3D11_FILTER_MIN_MAG_MIP_POINT;
		}
		return D3D11_FILTER_MIN_LINEAR_MAG_MIP_POINT;
	}

	static D3D11_TEXTURE_ADDRESS_MODE SnowToDXTextureWrap(Render::API::TextureWrap wrap) {
		switch (wrap) {
		case Render::API::TextureWrap::Repeat:			return D3D11_TEXTURE_ADDRESS_WRAP;
		case Render::API::TextureWrap::ClampEdge:		return D3D11_TEXTURE_ADDRESS_CLAMP;
		case Render::API::TextureWrap::ClampBorder:		return D3D11_TEXTURE_ADDRESS_BORDER;
		case Render::API::TextureWrap::MirroredRepeat:	return D3D11_TEXTURE_ADDRESS_MIRROR;
		}
		return D3D11_TEXTURE_ADDRESS_CLAMP;
	}

	static int CalculateMipMapCount(unsigned int width, unsigned int height) {
		int levels = 1;
		while ((width | height) >> levels) {
			levels++;
		}
		return levels;
	}

	DirectXTexture2D::DirectXTexture2D(unsigned int width, unsigned int height, const Render::API::TextureSpecification& spec) {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		DirectXDevice* dxDevice = static_cast<DirectXDevice*>(dxContext->GetDevice());

		m_Width = width;
		m_Height = height;
		m_Specification = spec;

		int mips = CalculateMipMapCount(m_Width, m_Height);

		D3D11_SUBRESOURCE_DATA initData;
		initData.pSysMem = nullptr;
		initData.SysMemPitch = 4 * m_Width;
		initData.SysMemSlicePitch = 4 * m_Width * m_Height;

		DXGI_FORMAT format = SnowToDXTextureFormat(m_Specification.Format);

		uint32_t formatSupport = 0;

		HRESULT hr = dxDevice->GetLogicalDevice()->CheckFormatSupport(format, &formatSupport);

		ZeroMemory(&m_Desc, sizeof(D3D11_TEXTURE2D_DESC));
		m_Desc.Width = m_Width;
		m_Desc.Height = m_Height;
		m_Desc.MipLevels = 1;
		m_Desc.ArraySize = 1;
		m_Desc.Format = format;

		m_Desc.SampleDesc.Count = 1;
		m_Desc.SampleDesc.Quality = 0;

		m_Desc.Usage = D3D11_USAGE_DYNAMIC;
		m_Desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		m_Desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		m_Desc.MiscFlags = 0;

		m_Texture = NULL;

		hr = dxDevice->GetLogicalDevice()->CreateTexture2D(&m_Desc, nullptr, &m_Texture);

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		ZeroMemory(&srvDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
		srvDesc.Format = m_Desc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels = m_Desc.MipLevels;
		srvDesc.Texture2D.MostDetailedMip = 0;

		hr = dxDevice->GetLogicalDevice()->CreateShaderResourceView(m_Texture, &srvDesc, &m_ResourceView);
		dxContext->GetDeviceContextHandle()->UpdateSubresource(m_Texture, 0, nullptr, initData.pSysMem, initData.SysMemPitch, initData.SysMemSlicePitch);
		dxContext->GetDeviceContextHandle()->GenerateMips(m_ResourceView);

		m_Desc.Usage = D3D11_USAGE_DEFAULT;
		m_Desc.CPUAccessFlags = 0;
		m_Desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

		ZeroMemory(&m_SamplerDesc, sizeof(D3D11_SAMPLER_DESC));
		D3D11_TEXTURE_ADDRESS_MODE wrap = SnowToDXTextureWrap(m_Specification.Wrap);
		m_SamplerDesc.AddressU = wrap;
		m_SamplerDesc.AddressV = wrap;
		m_SamplerDesc.AddressW = wrap;
		m_SamplerDesc.MinLOD = 0;
		m_SamplerDesc.MaxLOD = mips;
		m_SamplerDesc.Filter = SnowToDXTextureFilter(m_Specification.Filter);
		m_SamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;

		hr = dxDevice->GetLogicalDevice()->CreateSamplerState(&m_SamplerDesc, &m_SamplerState);
	}

	DirectXTexture2D::DirectXTexture2D(const std::string& path, const Render::API::TextureSpecification& spec, bool srgb) {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		DirectXDevice* dxDevice = static_cast<DirectXDevice*>(dxContext->GetDevice());

		int width, height, channels;
		stbi_set_flip_vertically_on_load(1);
		m_ImageData.Data = stbi_load(path.c_str(), &width, &height, &channels, STBI_rgb_alpha);

		m_Width = width;
		m_Height = height;
		m_Specification = spec;


		SNOW_CORE_INFO("Loading texture {0}, srgb={1}", path, srgb);

		int mips = CalculateMipMapCount(m_Width, m_Height);

		D3D11_SUBRESOURCE_DATA initData;
		initData.pSysMem = m_ImageData.Data;
		initData.SysMemPitch = 4 * m_Width;
		initData.SysMemSlicePitch = 4 * m_Width * m_Height;

		DXGI_FORMAT format = SnowToDXTextureFormat(m_Specification.Format);

		uint32_t formatSupport = 0;

		HRESULT hr = dxDevice->GetLogicalDevice()->CheckFormatSupport(format, &formatSupport);

		ZeroMemory(&m_Desc, sizeof(D3D11_TEXTURE2D_DESC));
		m_Desc.Width = m_Width;
		m_Desc.Height = m_Height;
		m_Desc.MipLevels = 1;
		m_Desc.ArraySize = 1;
		m_Desc.Format = format;

		m_Desc.SampleDesc.Count = 1;
		m_Desc.SampleDesc.Quality = 0;

		m_Desc.Usage = D3D11_USAGE_DYNAMIC;
		m_Desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		m_Desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		m_Desc.MiscFlags = 0;

		m_Texture = NULL;

		hr = dxDevice->GetLogicalDevice()->CreateTexture2D(&m_Desc, &initData, &m_Texture);

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		ZeroMemory(&srvDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
		srvDesc.Format = m_Desc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels = m_Desc.MipLevels;
		srvDesc.Texture2D.MostDetailedMip = 0;

		hr = dxDevice->GetLogicalDevice()->CreateShaderResourceView(m_Texture, &srvDesc, &m_ResourceView);
		dxContext->GetDeviceContextHandle()->UpdateSubresource(m_Texture, 0, nullptr, initData.pSysMem, initData.SysMemPitch, initData.SysMemSlicePitch);
		dxContext->GetDeviceContextHandle()->GenerateMips(m_ResourceView);

		m_Desc.Usage = D3D11_USAGE_DEFAULT;
		m_Desc.CPUAccessFlags = 0;
		m_Desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

		ZeroMemory(&m_SamplerDesc, sizeof(D3D11_SAMPLER_DESC));
		D3D11_TEXTURE_ADDRESS_MODE wrap = SnowToDXTextureWrap(m_Specification.Wrap);
		m_SamplerDesc.AddressU = wrap;
		m_SamplerDesc.AddressV = wrap;
		m_SamplerDesc.AddressW = wrap;
		m_SamplerDesc.MinLOD = 0;
		m_SamplerDesc.MaxLOD = mips;
		m_SamplerDesc.Filter = SnowToDXTextureFilter(m_Specification.Filter);
		m_SamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;

		hr = dxDevice->GetLogicalDevice()->CreateSamplerState(&m_SamplerDesc, &m_SamplerState);
	}

	DirectXTexture2D::~DirectXTexture2D() {

	}

	void DirectXTexture2D::Bind(uint32_t slot) {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		dxContext->GetDeviceContextHandle()->PSSetShaderResources(slot, 1, &m_ResourceView);
		dxContext->GetDeviceContextHandle()->PSSetSamplers(slot, 1, &m_SamplerState);
	}

	void DirectXTexture2D::SetData(void* data, uint32_t size) {

	}

	void DirectXTexture2D::Lock() {

	}

	void DirectXTexture2D::Unlock() {

	}

	void DirectXTexture2D::Resize(uint32_t width, uint32_t height) {

	}

	Core::Buffer DirectXTexture2D::GetWriteableBuffer() {
		Core::Buffer buffer;
		return buffer;
	}


	/////////////////////
	/////TEXTURE CUBE////
	/////////////////////

	DirectXTextureCube::DirectXTextureCube(const std::string& path, const Render::API::TextureSpecification& spec) :
		m_Path(path) {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());

		int width, height, channels;
		stbi_set_flip_vertically_on_load(1);

		m_ImageData.Data = stbi_load(path.c_str(), &width, &height, &channels, STBI_rgb_alpha);

		m_Width = width;
		m_Height = height;
		m_Specification = spec;


		//SNOW_CORE_INFO("Loading texture {0}, srgb={1}", path, srgb);

		int mips = CalculateMipMapCount(m_Width, m_Height);

		unsigned int faceWidth = m_Width / 4;
		unsigned int faceHeight = m_Height / 3;

		std::array<unsigned char*, 6> faces;
		for (size_t i = 0; i < faces.size(); i++)
			faces[i] = new unsigned char[faceWidth * faceHeight * 3];

		int faceIndex = 0;

		for (size_t i = 0; i < 4; i++) {
			for (size_t y = 0; y < faceHeight; y++) {
				size_t yOffset = y + faceHeight;
				for (size_t x = 0; x < faceWidth; x++) {
					size_t xOffset = x + i * faceWidth;
					faces[faceIndex][(x + y * faceWidth) * 3 + 0] = m_ImageData[(xOffset + yOffset * m_Width) * 3 + 0];
					faces[faceIndex][(x + y * faceWidth) * 3 + 1] = m_ImageData[(xOffset + yOffset * m_Width) * 3 + 1];
					faces[faceIndex][(x + y * faceWidth) * 3 + 2] = m_ImageData[(xOffset + yOffset * m_Width) * 3 + 2];
				}
			}
			faceIndex++;
		}

		for (size_t i = 0; i < 3; i++) {
			if (i == 1)
				continue;

			for (size_t y = 0; y < faceHeight; y++) {
				size_t yOffset = y + i * faceHeight;
				for (size_t x = 0; x < faceWidth; x++) {
					size_t xOffset = x + faceWidth;
					faces[faceIndex][(x + y * faceWidth) * 3 + 0] = m_ImageData[(xOffset + yOffset * m_Width) * 3 + 0];
					faces[faceIndex][(x + y * faceWidth) * 3 + 1] = m_ImageData[(xOffset + yOffset * m_Width) * 3 + 1];
					faces[faceIndex][(x + y * faceWidth) * 3 + 2] = m_ImageData[(xOffset + yOffset * m_Width) * 3 + 2];
				}
			}
			faceIndex++;
		}

		DXGI_FORMAT format = SnowToDXTextureFormat(m_Specification.Format);

		uint32_t formatSupport = 0;

		HRESULT hr = dxContext->GetDevice()->GetLogicalDevice()->CheckFormatSupport(format, &formatSupport);

		ZeroMemory(&m_Desc, sizeof(D3D11_TEXTURE2D_DESC));
		m_Desc.Width = faceWidth;
		m_Desc.Height = faceHeight;
		m_Desc.MipLevels = mips;
		m_Desc.ArraySize = 6;
		m_Desc.Format = format;

		m_Desc.SampleDesc.Count = 1;
		m_Desc.SampleDesc.Quality = 0;

		m_Desc.Usage = D3D11_USAGE_DYNAMIC;
		m_Desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		m_Desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		m_Desc.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;

		D3D11_SUBRESOURCE_DATA* pData = new D3D11_SUBRESOURCE_DATA[6];

		int result = 0;
		uint32_t index = 0;
		uint32_t faceOrder[6] = { 3,1,0,4,2,5 };
		for (int f = 0; f < 6; f++) {
			uint32_t fIndex = faceOrder[f];
			pData[index].pSysMem = faces[fIndex];
			pData[index].SysMemPitch = faceWidth * 3;
			pData[index].SysMemSlicePitch = faceWidth * faceHeight * 3;
			index++;
			
		}

		m_Texture = nullptr;

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		ZeroMemory(&srvDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
		srvDesc.Format = m_Desc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
		srvDesc.Texture2D.MipLevels = m_Desc.MipLevels;
		srvDesc.Texture2D.MostDetailedMip = 0;

		hr = dxContext->GetDevice()->GetLogicalDevice()->CreateTexture2D(&m_Desc, pData, &m_Texture);
		hr = dxContext->GetDevice()->GetLogicalDevice()->CreateShaderResourceView(m_Texture, &srvDesc, &m_ResourceView);

		ZeroMemory(&m_SamplerDesc, sizeof(D3D11_SAMPLER_DESC));
		D3D11_TEXTURE_ADDRESS_MODE wrap = SnowToDXTextureWrap(m_Specification.Wrap);
		m_SamplerDesc.AddressU = wrap;
		m_SamplerDesc.AddressV = wrap;
		m_SamplerDesc.AddressW = wrap;
		m_SamplerDesc.MinLOD = 0;
		m_SamplerDesc.MaxLOD = mips;
		m_SamplerDesc.Filter = SnowToDXTextureFilter(m_Specification.Filter);
		m_SamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;

		hr = dxContext->GetDevice()->GetLogicalDevice()->CreateSamplerState(&m_SamplerDesc, &m_SamplerState);
	}

	DirectXTextureCube::~DirectXTextureCube() {

	}

	void DirectXTextureCube::Bind(uint32_t slot) {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		dxContext->GetDeviceContextHandle()->PSSetShaderResources(slot, 1, &m_ResourceView);
		dxContext->GetDeviceContextHandle()->PSSetSamplers(slot, 1, &m_SamplerState);
	}

	void DirectXTextureCube::SetData(void* data, uint32_t size) {

	}
}