#include "spch.h"

#include "Snow/Platform/DirectX/DirectXSwapChain.h"

#include "Snow/Platform/DirectX/DirectXContext.h"
#include "Snow/Platform/Windows/WindowsWindow.h"

namespace Snow {
	DirectXSwapChain::DirectXSwapChain(const Render::API::SwapChainSpecification& spec) {
		m_Specification = spec;
		DirectXContext* dxContext = static_cast<DirectXContext*>(m_Specification.Context);
		WindowsWindow* window = static_cast<WindowsWindow*>(dxContext->GetSpecification().Window.get());

		DXGI_SWAP_CHAIN_DESC scd;
		scd.BufferDesc.Width = window->GetWidth();
		scd.BufferDesc.Height = window->GetHeight();

		scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		scd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

		scd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;

		scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		scd.BufferCount = 1;

		scd.SampleDesc.Count = 1;
		scd.SampleDesc.Quality = 0;

		scd.OutputWindow = window->GetNativeWindowHandle();
		scd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

		scd.Windowed = !window->m_Props.Fullscreen;
		scd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

		DXGI_SWAP_CHAIN_FULLSCREEN_DESC fscd;

		fscd.Windowed = window->m_Props.Fullscreen;
		fscd.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
		fscd.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		fscd.RefreshRate.Numerator = 60;
		fscd.RefreshRate.Denominator = 1;

		m_Result = dxContext->GetDevice()->GetFactory()->CreateSwapChain(dxContext->GetDevice()->GetLogicalDevice(), &scd, &m_SwapChain);
	}

	DirectXSwapChain::~DirectXSwapChain() {

	}

	void DirectXSwapChain::AddFramebuffer(const DirectXFramebuffer& framebuffer) {
		m_Framebuffers.push_back(framebuffer);
		DirectXContext* dxContext = static_cast<DirectXContext*>(m_Specification.Context);
		DirectXDevice* dxDevice = static_cast<DirectXDevice*>(dxContext->GetDevice());
		WindowsWindow* window = static_cast<WindowsWindow*>(dxContext->GetSpecification().Window.get());

		m_SwapChain->ResizeBuffers(m_Framebuffers.size(), window->GetWidth(), window->GetHeight(), DXGI_FORMAT_R8G8B8A8_UNORM, 0);

		m_SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&m_RenderTargetTexture);
		m_Result = dxDevice->GetLogicalDevice()->CreateRenderTargetView(m_RenderTargetTexture, NULL, &m_RenderTargetView);
		ReleaseCOM(m_RenderTargetTexture);

		D3D11_TEXTURE2D_DESC depthBufferDesc;
		ZeroMemory(&depthBufferDesc, sizeof(D3D11_TEXTURE2D_DESC));
		depthBufferDesc.Width = window->GetWidth();
		depthBufferDesc.Height = window->GetHeight();
		depthBufferDesc.MipLevels = 1;
		depthBufferDesc.ArraySize = 1;
		depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;

		depthBufferDesc.SampleDesc.Count = 1;
		depthBufferDesc.SampleDesc.Quality = 0;
		depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		depthBufferDesc.CPUAccessFlags = 0;
		depthBufferDesc.MiscFlags = 0;

		dxDevice->GetLogicalDevice()->CreateTexture2D(&depthBufferDesc, 0, &m_DepthStencilTexture);

		D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
		ZeroMemory(&depthStencilDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
		depthStencilDesc.DepthEnable = true;
		depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

		depthStencilDesc.StencilEnable = true;
		depthStencilDesc.StencilReadMask = 0xFF;
		depthStencilDesc.StencilWriteMask = 0xFF;

		depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		dxDevice->GetLogicalDevice()->CreateDepthStencilState(&depthStencilDesc, &m_DepthStencilState);
		dxContext->GetDeviceContextHandle()->OMSetDepthStencilState(m_DepthStencilState, 1);

		D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
		ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
		depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		depthStencilViewDesc.Texture2D.MipSlice = 0;

		dxDevice->GetLogicalDevice()->CreateDepthStencilView(m_DepthStencilTexture, &depthStencilViewDesc, &m_DepthStencilView);
		dxContext->GetDeviceContextHandle()->OMSetRenderTargets(1, &m_RenderTargetView, m_DepthStencilView);
	}
}