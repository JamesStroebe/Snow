#include "spch.h"
#include "Snow/Platform/DirectX/DirectXRenderPass.h"

namespace Snow {
	DirectXRenderPass::DirectXRenderPass(const Render::API::RenderPassSpecification& spec) {
		m_Specification = spec;
	}

	DirectXRenderPass::~DirectXRenderPass() {

	}
}