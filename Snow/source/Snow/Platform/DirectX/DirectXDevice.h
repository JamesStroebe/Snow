#pragma once

#include "Snow/Render/API/Device.h"

#include "Snow/Platform/DirectX/DirectXCommon.h"

namespace Snow {

	class DirectXDevice : public Render::API::Device {
	public:
		DirectXDevice(const Render::API::DeviceSpecification& spec);
		~DirectXDevice();

		ID3D11Device* GetLogicalDevice() { return m_Device; }
		IDXGIFactory2* GetFactory() { return m_Factory2; }

		HDC GetPhysicalDevice() { return m_DeviceHandle; }

		virtual const Render::API::DeviceSpecification& GetSpecification() const override { return m_Specification; }

	private:
		Render::API::DeviceSpecification m_Specification;

		IDXGIDevice* m_DeviceInterface;
		IDXGIFactory2* m_Factory2;
		IDXGIAdapter* m_Adapter;
		IDXGIOutput* m_Output;

		ID3D11Device* m_Device;

		HDC m_DeviceHandle;
		HRESULT m_Result;
	};
}
