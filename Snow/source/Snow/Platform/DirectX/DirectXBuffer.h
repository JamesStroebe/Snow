#pragma once

#include "Snow/Render/API/Buffer.h"

#include "Snow/Platform/DirectX/DirectXCommon.h"
//#include "Snow/Platform/DirectX/D"

namespace Snow {

	static D3D11_USAGE GetUsage(Render::API::BufferSpecification spec);
	static int GetCPUAccess(Render::API::BufferSpecification spec);

	class DirectXVertexBuffer : public Render::API::VertexBuffer {
	public:
		DirectXVertexBuffer(Render::API::BufferSpecification spec);
		DirectXVertexBuffer(uint32_t size, Render::API::BufferSpecification spec);
		DirectXVertexBuffer(void* data, uint32_t size, Render::API::BufferSpecification spec);

		~DirectXVertexBuffer();

		virtual void Bind() const override;
		virtual void Unbind() const override;

		virtual void SetData(void* data, uint32_t size) override;
		virtual void SetSubData(void* data, uint32_t size, uint32_t offset) override;

		virtual void* Map() override;
		virtual void* MapRange(uint32_t size, uint32_t offset) override;
		virtual void Unmap() override;

		virtual void Resize(uint32_t size);

		virtual ID3D11Buffer** GetBuffer() { return &m_BufferHandle; }



	private:

		Render::API::BufferSpecification m_Specification;

		HRESULT m_Result;

		uint32_t m_Size;
		ID3D11Buffer* m_BufferHandle;

		D3D11_BUFFER_DESC m_BufferDesc;
		D3D11_MAPPED_SUBRESOURCE m_MappedSubresource;


	};

	class DirectXIndexBuffer : public Render::API::IndexBuffer {
	public:
		DirectXIndexBuffer(Render::API::BufferSpecification spec);
		DirectXIndexBuffer(uint32_t size, Render::API::BufferSpecification spec);
		DirectXIndexBuffer(void* data, uint32_t size, Render::API::BufferSpecification spec);
		~DirectXIndexBuffer();

		virtual void Bind() const override;
		virtual void Unbind() const override;

		virtual void SetData(void* data, uint32_t size) override;
		virtual void SetSubData(void* data, uint32_t size, uint32_t offset) override;

		virtual void* Map() override;
		virtual void* MapRange(uint32_t size, uint32_t offset) override;
		virtual void Unmap() override;

		virtual void Resize(uint32_t size);

		virtual ID3D11Buffer* GetBuffer() { return m_BufferHandle; }

		virtual uint32_t GetCount() override { return m_Count; }

	private:
		Render::API::BufferSpecification m_Specification;

		HRESULT m_Result;

		uint32_t m_Size;
		ID3D11Buffer* m_BufferHandle;

		D3D11_BUFFER_DESC m_BufferDesc;
		D3D11_MAPPED_SUBRESOURCE m_MappedSubresource;

		uint32_t m_Count;
	};

	class DirectXConstantBuffer : public Render::API::UniformBuffer {
	public:
		DirectXConstantBuffer(const std::string& name);
		~DirectXConstantBuffer();

		//TEMP TEMP TEMP
		virtual void* Map() { return nullptr; }
		virtual void* MapRange() { return nullptr; }
		virtual void Unmap() {}

		virtual void Bind(uint32_t slot) const override;
		virtual void Unbind() const override {}

		virtual const std::string& GetName() override { return m_Name; }

		virtual void Resize(uint32_t size);

		virtual void SetData(void* data, uint32_t size) override;

		virtual void SetDomain(Render::Shader::ShaderType domain) override { m_Domain = domain; }
		virtual Render::Shader::ShaderType GetDomain() const override { return m_Domain; }

	private:
		void* GetPointer();
		void ReleasePointer();

		uint32_t m_Size;
		ID3D11Buffer* m_BufferHandle;

		D3D11_BUFFER_DESC m_BufferDesc;
		D3D11_MAPPED_SUBRESOURCE m_MappedSubresource;

		Render::Shader::ShaderType m_Domain;

		std::string m_Name;
	};
}
