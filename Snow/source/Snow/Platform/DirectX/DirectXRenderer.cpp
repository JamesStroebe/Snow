#include "spch.h"
#include "Snow/Platform/DirectX/DirectXRenderer.h"

namespace Snow {
	std::vector<ID3D11BlendState*> DirectXRenderer::s_BlendStates;
	std::vector<ID3D11DepthStencilState*> DirectXRenderer::s_DepthStencilStates;


	DirectXRenderer::DirectXRenderer() {

	}

	DirectXRenderer::~DirectXRenderer() {

	}

	void DirectXRenderer::InitInternal() {
		auto& caps = Renderer::GetCapabilities();

		caps.MaxSamplerCount = D3D11_COMMONSHADER_INPUT_RESOURCE_REGISTER_COUNT;

		CreateBlendStates();
		CreateDepthStencilStates();

		SetDepthTesting(true);
		SetBlending(true);
	}

	void DirectXRenderer::SetViewportInternal(int x, int y, unsigned int width, unsigned int height) {
		D3D11_VIEWPORT viewport;
		viewport.TopLeftX = x;
		viewport.TopLeftY = y;
		viewport.Width = width;
		viewport.Height = height;
		viewport.MinDepth = 0.0;
		viewport.MaxDepth = 1.0;

		DirectXContext* dxContext = static_cast<DirectXContext*>(Renderer::GetContext());
		dxContext->GetDeviceContextHandle()->RSSetViewports(1, &viewport);
	}

	void DirectXRenderer::SetBlendInternal(bool enabled) {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Renderer::GetContext());
		dxContext->GetDeviceContextHandle()->OMSetBlendState(enabled ? s_BlendStates[1] : s_BlendStates[0], NULL, 0xFFFFFFFF);
	}

	void DirectXRenderer::SetDepthTestingInternal(bool enabled) {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Renderer::GetContext());
		dxContext->GetDeviceContextHandle()->OMSetDepthStencilState(enabled ? s_DepthStencilStates[0] : s_DepthStencilStates[1], NULL);
	}

	void DirectXRenderer::CreateBlendStates() {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Renderer::GetContext());
		{
			D3D11_BLEND_DESC desc;
			ZeroMemory(&desc, sizeof(D3D11_BLEND_DESC));
			desc.RenderTarget[0].BlendEnable = false;
			desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

			ID3D11BlendState* state;
			dxContext->GetDevice()->GetLogicalDevice()->CreateBlendState(&desc, &state);
			s_BlendStates.push_back(state);
		}
		{
			D3D11_BLEND_DESC desc;
			ZeroMemory(&desc, sizeof(D3D11_BLEND_DESC));
			desc.AlphaToCoverageEnable = false;
			desc.IndependentBlendEnable = false;

			desc.RenderTarget[0].BlendEnable = true;

			desc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
			desc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
			desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
			desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_SRC_ALPHA;
			desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
			desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
			desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

			ID3D11BlendState* state;
			dxContext->GetDevice()->GetLogicalDevice()->CreateBlendState(&desc, &state);
			s_BlendStates.push_back(state);
		}
	}

	void DirectXRenderer::CreateDepthStencilStates() {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Renderer::GetContext());
		{

			D3D11_DEPTH_STENCIL_DESC desc;
			desc.DepthEnable = true;
			desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
			desc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
			desc.StencilEnable = true;
			desc.StencilReadMask = 0xff;
			desc.StencilWriteMask = 0xff;

			desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
			desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_INCR_SAT;
			desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

			desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
			desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			desc.BackFace.StencilFunc = D3D11_COMPARISON_NEVER;

			ID3D11DepthStencilState* state;
			dxContext->GetDevice()->GetLogicalDevice()->CreateDepthStencilState(&desc, &state);
			s_DepthStencilStates.push_back(state);
		}
		{
			D3D11_DEPTH_STENCIL_DESC desc;
			desc.DepthEnable = false;
			desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
			desc.DepthFunc = D3D11_COMPARISON_ALWAYS;
			desc.StencilEnable = true;
			desc.StencilReadMask = 0xff;
			desc.StencilWriteMask = 0xff;

			desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
			desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_INCR_SAT;
			desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

			desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
			desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			desc.BackFace.StencilFunc = D3D11_COMPARISON_NEVER;

			ID3D11DepthStencilState* state;
			dxContext->GetDevice()->GetLogicalDevice()->CreateDepthStencilState(&desc, &state);
			s_DepthStencilStates.push_back(state);
		}
	}
}