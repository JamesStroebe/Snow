#pragma once

#include "Snow/Render/API/RenderPass.h"

namespace Snow {

	class DirectXRenderPass : public Render::API::RenderPass {
	public:
		DirectXRenderPass(const Render::API::RenderPassSpecification& spec);
		~DirectXRenderPass();

		virtual const Render::API::RenderPassSpecification& GetSpecification() const override { return m_Specification; }
	private:
		Render::API::RenderPassSpecification m_Specification;
	};

}
