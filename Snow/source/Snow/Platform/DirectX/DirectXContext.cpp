#include "spch.h"
#include "Snow/Platform/DirectX/DirectXContext.h"

namespace Snow {
	DirectXContext::DirectXContext(const Render::API::ContextSpecification& spec) {
		m_Specification = spec;

		Render::API::DeviceSpecification deviceSpec;
		deviceSpec.Context = this;
		m_Device = static_cast<DirectXDevice*>(Render::API::Device::Create(deviceSpec));

		Render::API::SwapChainSpecification swapChainSpec;
		swapChainSpec.Context = this;
		m_SwapChain = static_cast<DirectXSwapChain*>(Render::API::SwapChain::Create(swapChainSpec));
	}

	DirectXContext::~DirectXContext() {
		delete m_Device;
		delete m_SwapChain;
	}
}