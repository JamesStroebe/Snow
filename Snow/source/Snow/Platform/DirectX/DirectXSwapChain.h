#pragma once

#include "Snow/Render/API/SwapChain.h"
#include "Snow/Platform/DirectX/DirectXCommon.h"

#include "Snow/Platform/DirectX/DirectXFramebuffer.h"

namespace Snow {
	class DirectXSwapChain : public Render::API::SwapChain {
	public:
		DirectXSwapChain(const Render::API::SwapChainSpecification& spec);
		~DirectXSwapChain();

		IDXGISwapChain* GetSwapChainHandle() { return m_SwapChain; }

		void AddFramebuffer(const DirectXFramebuffer& framebuffer);
		std::vector<DirectXFramebuffer>& GetFramebuffers() { return m_Framebuffers; }

		ID3D11RenderTargetView* GetRenderTargetView() { return m_RenderTargetView; }
		ID3D11DepthStencilView* GetDepthStencilView() { return m_DepthStencilView; }

		virtual const Render::API::SwapChainSpecification& GetSpecification() const override { return m_Specification; }
	private:

		Render::API::SwapChainSpecification m_Specification;

		IDXGISwapChain* m_SwapChain;

		std::vector<DirectXFramebuffer> m_Framebuffers;

		ID3D11RenderTargetView* m_RenderTargetView;
		ID3D11DepthStencilView* m_DepthStencilView;
		ID3D11DepthStencilState* m_DepthStencilState;

		ID3D11Texture2D* m_RenderTargetTexture;
		ID3D11Texture2D* m_DepthStencilTexture;

		HRESULT m_Result;
	};
}
