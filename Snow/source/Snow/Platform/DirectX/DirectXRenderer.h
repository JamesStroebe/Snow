#pragma once

#include "Snow/Render/Renderer.h"

#include "Snow/Platform/DirectX/DirectXCommon.h"
#include "Snow/Platform/DirectX/DirectXContext.h"
#include "Snow/Platform/DirectX/DirectXDevice.h"
#include "Snow/Platform/DirectX/DirectXSwapChain.h"

namespace Snow {
	class DirectXRenderer : public Render::Renderer {
	public:
		DirectXRenderer();
		~DirectXRenderer();

	protected:
		void InitInternal();

		void SetBlendInternal(bool enabled) override;
		void SetDepthTestingInternal(bool enabled) override;

		virtual void SetViewportInternal(int x, int y, unsigned int width, unsigned int height) override;

	private:
		
		void CreateBlendStates();
		void CreateDepthStencilStates();

		static std::vector<ID3D11BlendState*> s_BlendStates;
		static std::vector<ID3D11DepthStencilState*> s_DepthStencilStates;
	};
}