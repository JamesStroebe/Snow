#pragma once

#include "Snow/Render/RenderCommandBuffer.h"

#include "Snow/Platform/DirectX/DirectXCommon.h"

namespace Snow {
	class DirectXRenderCommandBuffer : public Render::RenderCommandBuffer {
	public:
		DirectXRenderCommandBuffer();
		~DirectXRenderCommandBuffer();

		virtual void* Allocate(Render::RenderCommandBuffer::RenderCommandFn fn, uint32_t size) override;
		virtual void Execute() override;

		virtual void BeginRenderPass(const Core::Ref<Render::API::RenderPass>& renderPass) override;
		virtual void EndRenderPass() override;

		virtual void SubmitElements(const Core::Ref<Render::API::Pipeline>& pipeline, const Core::Ref<Render::API::VertexBuffer>& vertexBuffer, const Core::Ref<Render::API::IndexBuffer>& indexBuffer, uint32_t count) override;
		virtual void SubmitInstanced(const Core::Ref<Render::API::Pipeline>& pipeline, const Core::Ref<Render::API::VertexBuffer>& vertexBuffer, const Core::Ref<Render::API::IndexBuffer>& indexBuffer, uint32_t count) override {}
		virtual void Flush();

		virtual void Present() override;

	private:
		HRESULT m_Result;
	};
}