#include "spch.h"
#include "Snow/Platform/DirectX/DirectXFramebuffer.h"

#include "Snow/Platform/DirectX/DirectXContext.h"

#include "Snow/Render/Renderer.h"

namespace Snow {
	DirectXFramebuffer::DirectXFramebuffer(const Render::API::FramebufferSpecification& spec) {
		m_Specification = spec;

		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		DirectXSwapChain* dxSwapChain = dxContext->GetSwapChain();

		dxSwapChain->AddFramebuffer(*this);
	}

	DirectXFramebuffer::~DirectXFramebuffer() {
	}
}