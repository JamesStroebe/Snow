#pragma once

#include "Snow/Render/Renderer.h"
#include "Snow/Render/API/Texture.h"

#include "Snow/Platform/DirectX/DirectXCommon.h"

namespace Snow {
	class DirectXTexture2D : public Render::API::Texture2D {
	public:
		DirectXTexture2D(unsigned int width, unsigned int height, const Render::API::TextureSpecification& spec);
		DirectXTexture2D(const std::string& path, const Render::API::TextureSpecification& spec, bool srgb);
		virtual ~DirectXTexture2D();

		virtual void Bind(uint32_t slot) override;
		virtual inline uint32_t GetSlot() const override { return m_Slot; }

		virtual void SetData(void* data, uint32_t size) override;

		virtual uint32_t GetWidth() const override { return m_Width; }
		virtual uint32_t GetHeight() const override { return m_Height; }

		virtual void Lock() override;
		virtual void Unlock() override;

		virtual void Resize(uint32_t width, uint32_t height) override;
		virtual Core::Buffer GetWriteableBuffer() override;

		virtual const std::string& GetPath() const override { return m_Path; }

		//virtual const RendererID GetRendererID() const override { return 0; }

		virtual const Render::API::TextureSpecification& GetSpecification() const { return m_Specification; }
	private:

		Render::API::TextureSpecification m_Specification;
		uint32_t m_Width, m_Height, m_Slot;

		Core::Buffer m_ImageData;

		bool m_Locked = false;

		std::string m_Path;

		D3D11_TEXTURE2D_DESC m_Desc;
		ID3D11Texture2D* m_Texture;
		ID3D11ShaderResourceView* m_ResourceView;
		ID3D11SamplerState* m_SamplerState;
		D3D11_SAMPLER_DESC m_SamplerDesc;
	};

	class DirectXTextureCube : public Render::API::TextureCube {
	public:
		DirectXTextureCube(const std::string& path, const Render::API::TextureSpecification& spec);
		virtual ~DirectXTextureCube();

		virtual void Bind(uint32_t slot = 0) override;
		virtual inline uint32_t GetSlot() const override { return m_Slot; }

		virtual void SetData(void* data, uint32_t size) override;

		virtual uint32_t GetWidth() const override { return m_Width; }
		virtual uint32_t GetHeight() const override { return m_Height; }

		virtual const std::string& GetPath() const { return m_Path; };

		virtual const Render::API::TextureSpecification& GetSpecification() const override { return m_Specification; }
	private:
		Render::API::TextureSpecification m_Specification;
		uint32_t m_Width, m_Height, m_Slot;

		Core::Buffer m_ImageData;

		bool m_Locked = false;

		std::string m_Path;

		D3D11_TEXTURE2D_DESC m_Desc;
		ID3D11Texture2D* m_Texture;
		ID3D11ShaderResourceView* m_ResourceView;
		ID3D11SamplerState* m_SamplerState;
		D3D11_SAMPLER_DESC m_SamplerDesc;
	};
}
