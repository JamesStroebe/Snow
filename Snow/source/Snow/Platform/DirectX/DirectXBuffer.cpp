#include "spch.h"

#include "Snow/Platform/DirectX/DirectXBuffer.h"
#include "Snow/Platform/DirectX/DirectXContext.h"

#include "Snow/Render/Renderer.h"



namespace Snow {

	D3D11_USAGE GetUsage(Render::API::BufferSpecification spec) {
		switch (spec.GetStorage()) {
		case Render::API::BufferStorage::Dynamic:	return D3D11_USAGE_DYNAMIC;
		case Render::API::BufferStorage::Static:		return D3D11_USAGE_IMMUTABLE;
		case Render::API::BufferStorage::Staging:	return D3D11_USAGE_STAGING;
		}
		return D3D11_USAGE_DEFAULT;
	}

	int GetCPUAccess(Render::API::BufferSpecification spec) {
		int result = 0;
		if (spec.GetMap() == Render::API::BufferMap::Write)
			result = D3D11_CPU_ACCESS_WRITE;
		if (spec.GetMap() == Render::API::BufferMap::Read)
			result = D3D11_CPU_ACCESS_READ;
		if (spec.GetMap() == Render::API::BufferMap::Both)
			result = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;
		return result;
	}

	/////////////////////////
	/////VERTEX BUFFER///////
	/////////////////////////

	DirectXVertexBuffer::DirectXVertexBuffer(Render::API::BufferSpecification spec) :
		m_Size(0), m_Specification(spec) {

		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());

		ZeroMemory(&m_BufferDesc, sizeof(D3D11_BUFFER_DESC));
		m_BufferDesc.Usage = GetUsage(m_Specification);
		m_BufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		m_BufferDesc.CPUAccessFlags = GetCPUAccess(m_Specification);
		m_BufferDesc.MiscFlags = 0;

		m_Result = dxContext->GetDevice()->GetLogicalDevice()->CreateBuffer(&m_BufferDesc, NULL, &m_BufferHandle);
	}

	DirectXVertexBuffer::DirectXVertexBuffer(uint32_t size, Render::API::BufferSpecification spec) :
		m_Size(0), m_Specification(spec) {

		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());

		ZeroMemory(&m_BufferDesc, sizeof(D3D11_BUFFER_DESC));
		m_BufferDesc.ByteWidth = size;
		m_BufferDesc.Usage = GetUsage(m_Specification);
		m_BufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		m_BufferDesc.CPUAccessFlags = GetCPUAccess(m_Specification);
		m_BufferDesc.MiscFlags = 0;

		m_Result = dxContext->GetDevice()->GetLogicalDevice()->CreateBuffer(&m_BufferDesc, NULL, &m_BufferHandle);
	}

	DirectXVertexBuffer::DirectXVertexBuffer(void* data, uint32_t size, Render::API::BufferSpecification spec) :
		m_Size(0), m_Specification(spec) {

		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());

		ZeroMemory(&m_BufferDesc, sizeof(D3D11_BUFFER_DESC));
		m_BufferDesc.ByteWidth = size;
		m_BufferDesc.Usage = GetUsage(m_Specification);
		m_BufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		m_BufferDesc.CPUAccessFlags = GetCPUAccess(m_Specification);
		m_BufferDesc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA initData;
		initData.pSysMem = data;
		initData.SysMemPitch = 0;
		initData.SysMemSlicePitch = 0;

		m_Result = dxContext->GetDevice()->GetLogicalDevice()->CreateBuffer(&m_BufferDesc, &initData, &m_BufferHandle);
	}

	DirectXVertexBuffer::~DirectXVertexBuffer() {
		m_BufferHandle->Release();
	}

	void DirectXVertexBuffer::Resize(uint32_t size) {
		//m_BufferHandle->Release();
		m_Size = size;
		m_BufferDesc.ByteWidth = size;

		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		dxContext->GetDevice()->GetLogicalDevice()->CreateBuffer(&m_BufferDesc, NULL, &m_BufferHandle);
	}

	void DirectXVertexBuffer::Bind() const {

	}

	void DirectXVertexBuffer::Unbind() const {

	}

	void DirectXVertexBuffer::SetData(void* data, uint32_t size) {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		if (m_Size < size)
			Resize(size);

		Map();
		memcpy(m_MappedSubresource.pData, data, size);
		Unmap();
	}

	void DirectXVertexBuffer::SetSubData(void* data, uint32_t size, uint32_t offset) {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		if (m_Size < size)
			Resize(size);

		Map();
		memcpy((unsigned char*)m_MappedSubresource.pData + offset, data, size);
		Unmap();
	}

	void* DirectXVertexBuffer::Map() {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		dxContext->GetDeviceContextHandle()->Map(m_BufferHandle, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &m_MappedSubresource);
		return m_MappedSubresource.pData;
	}

	void* DirectXVertexBuffer::MapRange(uint32_t size, uint32_t offset) {
		return nullptr;
	}

	void DirectXVertexBuffer::Unmap() {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		dxContext->GetDeviceContextHandle()->Unmap(m_BufferHandle, NULL);
	}

	/////////////////////////
	/////INDEX BUFFER////////
	/////////////////////////

	DirectXIndexBuffer::DirectXIndexBuffer(Render::API::BufferSpecification spec) :
		m_Size(0), m_Specification(spec) {

		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());

		ZeroMemory(&m_BufferDesc, sizeof(D3D11_BUFFER_DESC));
		m_BufferDesc.Usage = GetUsage(m_Specification);
		m_BufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		m_BufferDesc.CPUAccessFlags = GetCPUAccess(m_Specification);
		m_BufferDesc.MiscFlags = 0;

		m_Result = dxContext->GetDevice()->GetLogicalDevice()->CreateBuffer(&m_BufferDesc, NULL, &m_BufferHandle);
	}

	DirectXIndexBuffer::DirectXIndexBuffer(uint32_t size, Render::API::BufferSpecification spec) :
		m_Size(0), m_Specification(spec) {

		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());

		ZeroMemory(&m_BufferDesc, sizeof(D3D11_BUFFER_DESC));
		m_BufferDesc.ByteWidth = size;
		m_BufferDesc.Usage = GetUsage(m_Specification);
		m_BufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		m_BufferDesc.CPUAccessFlags = GetCPUAccess(m_Specification);
		m_BufferDesc.MiscFlags = 0;

		m_Result = dxContext->GetDevice()->GetLogicalDevice()->CreateBuffer(&m_BufferDesc, NULL, &m_BufferHandle);
	}

	DirectXIndexBuffer::DirectXIndexBuffer(void* data, uint32_t size, Render::API::BufferSpecification spec) :
		m_Size(0), m_Specification(spec) {

		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());

		ZeroMemory(&m_BufferDesc, sizeof(D3D11_BUFFER_DESC));
		m_BufferDesc.ByteWidth = size;
		m_BufferDesc.Usage = GetUsage(m_Specification);
		m_BufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		m_BufferDesc.CPUAccessFlags = GetCPUAccess(m_Specification);
		m_BufferDesc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA initData;
		initData.pSysMem = data;
		initData.SysMemPitch = 0;
		initData.SysMemSlicePitch = 0;

		m_Result = dxContext->GetDevice()->GetLogicalDevice()->CreateBuffer(&m_BufferDesc, &initData, &m_BufferHandle);
	}

	DirectXIndexBuffer::~DirectXIndexBuffer() {

	}

	void DirectXIndexBuffer::Resize(uint32_t size) {
		//m_BufferHandle->Release();
		m_Size = size;
		m_Count = m_Size / sizeof(float);
		m_BufferDesc.ByteWidth = size;

		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		dxContext->GetDevice()->GetLogicalDevice()->CreateBuffer(&m_BufferDesc, NULL, &m_BufferHandle);
	}

	void DirectXIndexBuffer::Bind() const {

	}

	void DirectXIndexBuffer::Unbind() const {

	}

	void DirectXIndexBuffer::SetData(void* data, uint32_t size) {
		if (m_Size < size)
			Resize(size);

		Map();
		memcpy(m_MappedSubresource.pData, data, size);
		Unmap();
	}

	void DirectXIndexBuffer::SetSubData(void* data, uint32_t size, uint32_t offset) {
		if (m_Size < size)
			Resize(size);

		Map();
		memcpy((unsigned char*)m_MappedSubresource.pData + offset, data, size);
		Unmap();
	}

	void* DirectXIndexBuffer::Map() {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		dxContext->GetDeviceContextHandle()->Map(m_BufferHandle, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &m_MappedSubresource);
		return m_MappedSubresource.pData;
	}

	void* DirectXIndexBuffer::MapRange(uint32_t size, uint32_t offset) {
		return nullptr;
	}

	void DirectXIndexBuffer::Unmap() {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		dxContext->GetDeviceContextHandle()->Unmap(m_BufferHandle, NULL);
	}

	/////////////////////////
	/////UNIFORM BUFFER//////
	/////////////////////////

	DirectXConstantBuffer::DirectXConstantBuffer(const std::string& name) :
		m_Size(0) {
		m_Name = name;
		ZeroMemory(&m_BufferDesc, sizeof(D3D11_BUFFER_DESC));
		m_BufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		m_BufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		m_BufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());

		dxContext->GetDevice()->GetLogicalDevice()->CreateBuffer(&m_BufferDesc, NULL, &m_BufferHandle);
	}

	DirectXConstantBuffer::~DirectXConstantBuffer() {

	}

	void DirectXConstantBuffer::Bind(uint32_t slot) const {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());

		if (m_Domain == Render::Shader::ShaderType::VertexShader)
			dxContext->GetDeviceContextHandle()->VSSetConstantBuffers(slot, 1, &m_BufferHandle);
		else if (m_Domain == Render::Shader::ShaderType::PixelShader)
			dxContext->GetDeviceContextHandle()->PSSetConstantBuffers(slot, 1, &m_BufferHandle);
	}

	void DirectXConstantBuffer::Resize(uint32_t size) {
		m_Size = size;
		m_BufferDesc.ByteWidth = size;
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		dxContext->GetDevice()->GetLogicalDevice()->CreateBuffer(&m_BufferDesc, NULL, &m_BufferHandle);
	}

	void DirectXConstantBuffer::SetData(void* data, uint32_t size) {
		if (m_Size < size)
			Resize(size);

		GetPointer();
		memcpy(m_MappedSubresource.pData, data, size);
		ReleasePointer();
	}

	void* DirectXConstantBuffer::GetPointer() {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		HRESULT hr = dxContext->GetDeviceContextHandle()->Map(m_BufferHandle, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &m_MappedSubresource);
		return m_MappedSubresource.pData;
	}

	void DirectXConstantBuffer::ReleasePointer() {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		dxContext->GetDeviceContextHandle()->Unmap(m_BufferHandle, NULL);
	}
}