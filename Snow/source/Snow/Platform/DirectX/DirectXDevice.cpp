#include "spch.h"

#include "Snow/Platform/DirectX/DirectXDevice.h"

#include "Snow/Platform/DirectX/DirectXContext.h"

#include "Snow/Platform/Windows/WindowsWindow.h"

namespace Snow {

	DirectXDevice::DirectXDevice(const Render::API::DeviceSpecification& spec) {
		m_Specification = spec;

		WindowsWindow* window = static_cast<WindowsWindow*>(m_Specification.Context->GetSpecification().Window.get());
		m_DeviceHandle = window->GetDeviceContextHandle();

		DirectXContext* dxContext = static_cast<DirectXContext*>(m_Specification.Context);
		ID3D11DeviceContext* deviceContext = dxContext->GetDeviceContextHandle();
		m_Result = D3D11CreateDevice(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, D3D11_CREATE_DEVICE_SINGLETHREADED, NULL, NULL, D3D11_SDK_VERSION, &m_Device, NULL, &deviceContext);
		dxContext->SetDeviceContext(deviceContext);

		m_Result = m_Device->QueryInterface(__uuidof(IDXGIDevice), (void**)&m_DeviceInterface);
		m_Result = m_DeviceInterface->GetParent(__uuidof(IDXGIAdapter), (void**)&m_Adapter);
		m_Result = m_Adapter->GetParent(__uuidof(IDXGIFactory2), (void**)&m_Factory2);
	}

	DirectXDevice::~DirectXDevice() {

	}
}