#include "spch.h"

#include "Snow/Platform/DirectX/DirectXRenderCommandBuffer.h"

#include "Snow/Platform/DirectX/DirectXBuffer.h"
#include "Snow/Platform/DirectX/DirectXPipeline.h"
#include "Snow/Platform/DirectX/DirectXShader.h"
#include "Snow/Platform/DirectX/DirectXRenderer.h"

#include "Snow/Platform/DirectX/DirectXContext.h"

#include "Snow/Render/Renderer.h"

namespace Snow {

	

	DirectXRenderCommandBuffer::DirectXRenderCommandBuffer() {

	}

	DirectXRenderCommandBuffer::~DirectXRenderCommandBuffer() {

	}

	void* DirectXRenderCommandBuffer::Allocate(RenderCommandFn fn, uint32_t size) {
		*(RenderCommandFn*)m_CommandBufferPtr = fn;
		m_CommandBufferPtr += sizeof(RenderCommandFn);

		*(uint32_t*)m_CommandBufferPtr = size;
		m_CommandBufferPtr += sizeof(uint32_t);

		void* memory = m_CommandBufferPtr;
		m_CommandBufferPtr += size;

		m_CommandCount++;
		return memory;
	}

	void DirectXRenderCommandBuffer::Execute() {
		Core::byte* buffer = m_CommandBuffer;

		for (uint32_t i = 0; i < m_CommandCount; i++) {
			RenderCommandFn func = *(RenderCommandFn*)buffer;
			buffer += sizeof(RenderCommandFn);

			uint32_t size = *(uint32_t*)buffer;
			buffer += sizeof(uint32_t);
			func(buffer);
			buffer += size;
		}
		m_CommandBufferPtr = m_CommandBuffer;
		m_CommandCount = 0;
	}

	void DirectXRenderCommandBuffer::BeginRenderPass(const Core::Ref<Render::API::RenderPass>& renderPass) {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		DirectXSwapChain* dxSwapChain = dxContext->GetSwapChain();

		Math::Vector4f color = dxSwapChain->GetFramebuffers().at(0).GetSpecification().Color;

		//if (renderPass->GetSpecification().BufferType & Render::API::RenderBufferType::RENDER_BUFFER_COLOR)
		//	dxContext->GetDeviceContextHandle()->ClearRenderTargetView(dxContext->GetSwapChain()->GetRenderTargetView(), &color[0]);
		//if (renderPass->GetSpecification().BufferType & Render::API::RenderBufferType::RENDER_BUFFER_DEPTH)
		//	dxContext->GetDeviceContextHandle()->ClearDepthStencilView(dxContext->GetSwapChain()->GetDepthStencilView(), D3D11_CLEAR_DEPTH, 1.0, 0);
		//if (renderPass->GetSpecification().BufferType & Render::API::RenderBufferType::RENDER_BUFFER_STENCIL)
		//	dxContext->GetDeviceContextHandle()->ClearDepthStencilView(dxContext->GetSwapChain()->GetDepthStencilView(), D3D11_CLEAR_STENCIL, 0.0, 0xFF);
	}

	void DirectXRenderCommandBuffer::EndRenderPass() {

	}

	void DirectXRenderCommandBuffer::SubmitElements(const Core::Ref<Render::API::Pipeline>& pipeline, const Core::Ref<Render::API::VertexBuffer>& vertexBuffer, const Core::Ref<Render::API::IndexBuffer>& indexBuffer, uint32_t indexCount) {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
		DirectXPipeline* dxPipeline = static_cast<DirectXPipeline*>(pipeline.get());
		DirectXVertexBuffer* dxVertexBuffer = static_cast<DirectXVertexBuffer*>(vertexBuffer.get());
		DirectXIndexBuffer* dxIndexBuffer = static_cast<DirectXIndexBuffer*>(indexBuffer.get());

		dxPipeline->Bind();

		
		

		uint32_t offset = 0;
		uint32_t stride = dxPipeline->GetSpecification().VertexBufferLayout.GetStride();
		dxContext->GetDeviceContextHandle()->IASetInputLayout(dxPipeline->GetInputLayout());

		dxContext->GetDeviceContextHandle()->IASetPrimitiveTopology(dxPipeline->SnowToDX11PrimitiveType(dxPipeline->GetPrimitiveType()));

		dxContext->GetDeviceContextHandle()->IASetVertexBuffers(0, 1, dxVertexBuffer->GetBuffer(), &stride, &offset);
		dxContext->GetDeviceContextHandle()->IASetIndexBuffer(dxIndexBuffer->GetBuffer(), DXGI_FORMAT_R32_UINT, 0);

		dxContext->GetDeviceContextHandle()->DrawIndexed(dxIndexBuffer->GetCount(), 0, 0);
	}

	void DirectXRenderCommandBuffer::Flush() {

	}

	void DirectXRenderCommandBuffer::Present() {
		DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());

		dxContext->GetSwapChain()->GetSwapChainHandle()->Present(0, 0);
	}
}