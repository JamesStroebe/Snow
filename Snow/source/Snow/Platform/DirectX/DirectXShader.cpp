#include "spch.h"

#include "Snow/Platform/DirectX/DirectXShader.h"

#include "Snow/Render/Renderer.h"

namespace Snow {

	DirectXShader::DirectXShader(const std::string& path, Render::Shader::ShaderType type) :
		m_Path(path), m_Type(type) {
		m_Name = FindFileName(path);
		auto& source = ReadFileToString(path);
		Load(source);
	}

	DirectXShader::~DirectXShader() {

	}

	

	void DirectXShader::Reload() {
		std::string source = ReadFileToString(m_Path);
		Load(source);
	}

	//uint32_t DirectXShader::FindSlot(const std::string& uboName) {
	//	for (auto& buffer : m_VSRendererConstantBuffers) {
	//		if (uboName == buffer->GetName())
	//			return buffer->GetSlot();
	//	}
	//
	//	for (auto& buffer : m_PSRendererConstantBuffers) {
	//		if (uboName == buffer->GetName())
	//			return buffer->GetSlot();
	//	}
	//
	//
	//	if (uboName == m_VSMaterialConstantBuffer->GetName())
	//		return m_VSMaterialConstantBuffer->GetSlot();
	//
	//
	//	if (uboName == m_PSMaterialConstantBuffer->GetName())
	//		return m_PSMaterialConstantBuffer->GetSlot();
	//
	//	return -1;
	//}

	//void DirectXShader::UploadBuffer(const Core::Ref<Render::API::UniformBuffer>& uniformBuffer) {
	//	int bufferSlot = FindSlot(uniformBuffer->GetName());
	//	uniformBuffer->Bind(bufferSlot);
	//}

	void DirectXShader::Load(const std::string& source) {
		m_Source = source;
		Parse();

		CreateShaderModule();
	}

	//std::unordered_map<Render::Shader::ShaderSourceType, std::string> DirectXShader::PreProcess(const std::string& source) {
		//std::unordered_map<Render::Shader::ShaderSourceType, std::string> shaderSources;
		//
		//const char* typeToken = "#type";
		//size_t typeTokenLength = strlen(typeToken);
		//size_t pos = source.find(typeToken, 0);
		//while (pos != std::string::npos) {
		//	size_t eol = source.find_first_of("\r\n", pos);
		//	SNOW_CORE_ASSERT(eol != std::string::npos, "Syntax Error");
		//	size_t begin = pos + typeTokenLength + 1;
		//	std::string type = source.substr(begin, eol - begin);
		//	SNOW_CORE_ASSERT(ShaderTypeFromString(type), "Invalid Shader type specificed");
		//
		//	size_t nextLinePos = source.find_first_not_of("\r\n", eol);
		//	pos = source.find(typeToken, nextLinePos);
		//	shaderSources[ShaderTypeFromString(type)] = source.substr(nextLinePos, pos - (nextLinePos == std::string::npos ? source.size() - 1 : nextLinePos));
		//}

		//return shaderSources;
	//}

	void DirectXShader::CreateShaderModule() {
		ID3D10Blob* ShaderBlob;
		ID3D10Blob* ErrorBlob;
		DXShaderErrorInfo info;

		HRESULT status;
		std::string profile;
		if (m_Type == Render::Shader::ShaderType::VertexShader) {
			status = D3DCompile(m_Source.c_str(), m_Source.size(), NULL, NULL, NULL, "main", "vs_4_0", D3DCOMPILE_DEBUG, 0, &ShaderBlob, &ErrorBlob);
			profile = "vs_4_0";
		}
		else if (m_Type == Render::Shader::ShaderType::PixelShader) {
			status = D3DCompile(m_Source.c_str(), m_Source.size(), NULL, NULL, NULL, "main", "ps_4_0", D3DCOMPILE_DEBUG, 0, &ShaderBlob, &ErrorBlob);
			profile = "ps_4_0";
		}
		if (status != S_OK)
			info.message = "Unable to compile shader from source\n";
		if (ErrorBlob) {
			info.profile += profile + "\n";
			if (ErrorBlob->GetBufferSize())
				SNOW_CORE_ERROR("Shader compiler errors: {0}, {1}", (const char*)ErrorBlob->GetBufferPointer(), profile);
			ErrorBlob->Release();
		}
		if (status == S_OK)
			m_ShaderData = ShaderBlob;
		else
			m_ShaderData = nullptr;
	}

	void DirectXShader::Parse() {
		const char* token;
		std::string str;
	
		m_Resources.clear();
		m_Structs.clear();
	
		str = m_Source;
	
	
		while (token = FindToken(str, "cbuffer")) {
			const char* s = str.c_str();
			const std::string& block = GetBlock(token, &s);
			ParseConstantBuffer(block);
			RemoveToken(str, block);
		}
	
		while (token = FindToken(str, "struct")) {
			const char* s = str.c_str();
			const std::string& block = GetBlock(token, &s);
			ParseStruct(block);
			RemoveToken(str, block);
		}
	
		while (token = FindToken(str, "Texture2D")) {
			const char* s = str.c_str();
			const std::string& statement = GetStatement(token, &s);
			ParseTexture(statement);
			RemoveToken(str, statement);
		}
	
		while (token = FindToken(str, "TextureCube")) {
			const char* s = str.c_str();
			const std::string& statement = GetStatement(token, &s);
			ParseTexture(statement);
			RemoveToken(str, statement);
		}
	
		while (token = FindToken(str, "SamplerState")) {
			const char* s = str.c_str();
			const std::string& statement = GetStatement(token, &s);
			ParseSamplerState(statement);
			RemoveToken(str, statement);
		}
	
	}

	static Render::Shader::FieldType StringToField(const std::string& type) {
		if (type == "float")	return Render::Shader::FieldType::Float;
		if (type == "float2")	return Render::Shader::FieldType::Float2;
		if (type == "float3")	return Render::Shader::FieldType::Float3;
		if (type == "float4")	return Render::Shader::FieldType::Float4;
		if (type == "float3x3")	return Render::Shader::FieldType::Mat3;
		if (type == "float4x4")	return Render::Shader::FieldType::Mat4;
		if (type == "int")		return Render::Shader::FieldType::Int;
		if (type == "uint")		return Render::Shader::FieldType::UInt;
		return Render::Shader::FieldType::None;
	}

	static Render::Shader::ResourceType StringToResourceType(const std::string& type) {
		if (type == "Texture2D")	return Render::Shader::ResourceType::Texture2D;
		if (type == "TextureCube")	return Render::Shader::ResourceType::TextureCube;
		if (type == "Sampler")		return Render::Shader::ResourceType::Sampler;
		return Render::Shader::ResourceType::None;
	}

	void DirectXShader::ParseConstantBuffer(const std::string& block) {
		std::vector<std::string> tokens = Tokenize(block);
		uint32_t index = 1;
		
		std::string bufferType = tokens[index++];
		std::string bufferName = tokens[index++];
		uint32_t reg = 0;
		if (tokens[index++] == ":") {
			std::string bufferRegister = tokens[index++];
			reg = NextInt(bufferRegister);
		}
		index++;
	
		Render::Shader::ShaderBuffer* buffer;

		buffer = new Render::Shader::ShaderBuffer(bufferName, reg);
		if (bufferType != "material")
			m_RendererConstantBuffers.push_back(buffer);
		else
			m_MaterialConstantBuffer = buffer;

		RemoveToken(m_Source, bufferType);
	
		
		while (index < tokens.size()) {
			if (tokens[index] == "}")
				break;
	
			std::string type = tokens[index++];
			std::string fieldName = tokens[index++];
	
			if (const char* s = strstr(fieldName.c_str(), ";"))
				fieldName = std::string(fieldName.c_str(), s - fieldName.c_str());
	
	
			Render::Shader::FieldType t = StringToField(type);
			Render::Shader::ShaderField* field = nullptr;
			if (t == Render::Shader::FieldType::None) {
				Render::Shader::ShaderStruct* s = FindStruct(type);
				field = new Render::Shader::ShaderField(s, fieldName);
			}
			else {
				field = new Render::Shader::ShaderField(t, fieldName);
			}
	
			if (bufferType != "material") 
				m_RendererConstantBuffers.front()->PushField(field);
			else
				m_MaterialConstantBuffer->PushField(field);
		}
	}

	void DirectXShader::ParseStruct(const std::string& block) {
		std::vector<std::string> tokens = Tokenize(block);
	
		uint32_t index = 0;
		index++;
		std::string name = tokens[index++];
		Render::Shader::ShaderStruct* uniformStruct = new Render::Shader::ShaderStruct(name);
		index++;
		while (index < tokens.size()) {
			if (tokens[index] == "}")
				break;
	
			std::string type = tokens[index++];
			std::string name = tokens[index++];
	
			if (const char* s = strstr(name.c_str(), ";"))
				name = std::string(name.c_str(), s - name.c_str());
	
			uint32_t count = 1;
			const char* namestr = name.c_str();
			if (const char* s = strstr(namestr, "[")) {
				name = std::string(namestr, s - namestr);
	
				const char* end = strstr(namestr, "]");
				std::string c(s + 1, end - s);
				count = atoi(c.c_str());
			}
			Render::Shader::ShaderField* field = new Render::Shader::ShaderField(StringToField(type), name, count);
			uniformStruct->AddField(field);
		}
		m_Structs.push_back(uniformStruct);
	}

	void DirectXShader::ParseTexture(const std::string& statement) {
		std::vector<std::string> tokens = Tokenize(statement);

		uint32_t index = 0;

		uint32_t reg = 0;

		std::string type = tokens[index++];
		std::string name = tokens[index++];

		if (tokens[index++] == ":") {
			std::string TextureRegister = tokens[index++];
			reg = NextInt(TextureRegister);
		}

		Render::Shader::ShaderResource* resource = new Render::Shader::ShaderResource(StringToResourceType(type), name, 1);
		resource->SetSlot(reg);
		m_Resources.push_back(resource);
	}

	void DirectXShader::ParseSamplerState(const std::string& statement) {
		std::vector<std::string> tokens = Tokenize(statement);
		SNOW_ASSERT(tokens.front() == "SamplerState");

		uint32_t index = 1;
		uint32_t reg = 0;
		std::string name = tokens[index++];
		if (tokens[index++] == ":") {
			std::string SamplerRegister = tokens[index++];
			reg = NextInt(SamplerRegister);
		}
	}

	Render::Shader::ShaderStruct* DirectXShader::FindStruct(const std::string& name) {
		for (Render::Shader::ShaderStruct* s : m_Structs) {
			if (s->GetName() == name)
				return s;
		}
		return nullptr;
	}
}