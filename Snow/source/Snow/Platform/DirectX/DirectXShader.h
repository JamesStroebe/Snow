#pragma once

#include "Snow/Render/Shaders/Shader.h"
#include "Snow/Platform/DirectX/DirectXCommon.h"
#include "Snow/Platform/DirectX/DirectXContext.h"
#include "Snow/Platform/DirectX/DirectXSwapChain.h"

#include "Snow/Utils/StringUtils.h"

namespace Snow {
	



	struct DXShaderErrorInfo {
		std::string profile;
		std::string message;
	};

	class DirectXShader : public Render::Shader::Shader {
	public:
		DirectXShader() = default;
		DirectXShader(const std::string& path, Render::Shader::ShaderType type);
		//DirectXShader(const std::string& vertPath, const std::string& fragPath) {}
		~DirectXShader();

		

		virtual void Reload() override;

		

		virtual const Render::Shader::ShaderBufferList& GetRendererBuffers() const override { return m_RendererConstantBuffers; }
		virtual const Render::Shader::ShaderBuffer& GetMaterialBuffer() const override { return *m_MaterialConstantBuffer; }

		virtual const Render::Shader::ShaderFieldList& GetGlobalUniforms() const override { return m_GlobalUniforms; }

		virtual const Render::Shader::ShaderResourceList& GetResources() const override { return m_Resources; }

		virtual const std::string& GetName() const override { return m_Name; }
		virtual const Render::Shader::ShaderType GetType() const override { return m_Type; }

		virtual ID3D10Blob* GetData() { return m_ShaderData; }
	private:
		void Load(const std::string& source);

		void CreateShaderModule();

		//std::string PreProcess(const std::string& source);
		void Parse();
		void ParseConstantBuffer(const std::string& block);
		void ParseTexture(const std::string& statements);
		void ParseSamplerState(const std::string& statement);
		void ParseStruct(const std::string& block);

		Render::Shader::ShaderStruct* FindStruct(const std::string& name);

		//ID3D10Blob* CompileShader(const std::string& source, const std::string& profile, const std::string& main, DXShaderErrorInfo& info);
		//void CompileAndUploadShader();



		std::string m_Path, m_Name;
		std::string m_Source;
		Render::Shader::ShaderType m_Type;
		bool m_Loaded = false;

		Render::Shader::ShaderBufferList m_RendererConstantBuffers;
		Render::Shader::ShaderBuffer* m_MaterialConstantBuffer = nullptr;
		Render::Shader::ShaderFieldList m_GlobalUniforms = {};
		Render::Shader::ShaderResourceList m_Resources;
		Render::Shader::ShaderStructList m_Structs;

		ID3D10Blob* m_ShaderData;
		
	};

}
