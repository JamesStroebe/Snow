#include "spch.h"
#include "Snow/Platform/Vulkan/VulkanDevice.h"

#include "Snow/Platform/Vulkan/VulkanContext.h"

namespace Snow {
	QueueFamilyIndices VulkanDevice::FindQueueFamilies(VkPhysicalDevice device) {
		VulkanContext* vkContext = static_cast<VulkanContext*>(m_Specification.Context);
		uint32_t queueCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueCount, NULL);

		std::vector<VkQueueFamilyProperties> queueFamilies(queueCount);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueCount, queueFamilies.data());

		std::vector<VkBool32> supportsPresent(queueCount);
		VkSurfaceKHR& surface = vkContext->GetSurface();
		for (uint32_t i = 0; i < queueCount; i++) {
			vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &supportsPresent[i]);
		}

		m_Indices.graphicsFamily = UINT32_MAX;
		m_Indices.presentFamily = UINT32_MAX;
		for (uint32_t i = 0; i < queueCount; i++) {
			if ((queueFamilies.at(i).queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0) {
				if (m_Indices.graphicsFamily == UINT32_MAX) {
					m_Indices.graphicsFamily = i;
				}
				if (supportsPresent[i] == VK_TRUE) {
					m_Indices.graphicsFamily = i;
					m_Indices.presentFamily = i;
					break;
				}
			}
		}

		if (m_Indices.presentFamily == UINT32_MAX) {
			for (uint32_t i = 0; i < queueCount; i++) {
				if (supportsPresent[i] == VK_TRUE) {
					m_Indices.presentFamily = i;
					break;
				}
			}
		}
		return m_Indices;
	}

	bool VulkanDevice::CheckDeviceExtensionSupport(VkPhysicalDevice device) {
		uint32_t extensionCount;
		vkEnumerateDeviceExtensionProperties(device, NULL, &extensionCount, NULL);

		std::vector<VkExtensionProperties> availableExtensions(extensionCount);
		vkEnumerateDeviceExtensionProperties(device, NULL, &extensionCount, availableExtensions.data());

		std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

		for (const auto& extension : availableExtensions) {
			requiredExtensions.erase(extension.extensionName);
		}

		return requiredExtensions.empty();
	}

	SwapChainSupportDetails VulkanDevice::QuerySwapChainSupport(VkPhysicalDevice device) {
		SwapChainSupportDetails details;

		VulkanContext* vkContext = static_cast<VulkanContext*>(m_Specification.Context);
		VkSurfaceKHR surface = vkContext->GetSurface();

		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

		uint32_t formatCount;
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);

		if (formatCount != 0) {
			details.formats.resize(formatCount);
			vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data());
		}

		uint32_t presentModeCount;
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

		if (presentModeCount != 0) {
			details.presentModes.resize(presentModeCount);
			vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
		}

		return details;
	}

	bool VulkanDevice::IsPhysicalDeviceSuitable(VkPhysicalDevice device) {

		m_Indices = FindQueueFamilies(device);
		bool extensionSupported = CheckDeviceExtensionSupport(device);

		bool swapChainAdequate = false;
		if (extensionSupported) {
			m_SupportDetails = QuerySwapChainSupport(device);
			swapChainAdequate = !m_SupportDetails.formats.empty() && !m_SupportDetails.presentModes.empty();
		}

		return m_Indices.IsComplete() && extensionSupported && swapChainAdequate;
	}

	void VulkanDevice::PickPhysicalDevice() {
		VulkanContext* vkContext = static_cast<VulkanContext*>(m_Specification.Context);
		VkInstance& instance = vkContext->GetInstance();

		uint32_t deviceCount = 0;
		m_Result = vkEnumeratePhysicalDevices(instance, &deviceCount, NULL);


		if (deviceCount == 0)
			SNOW_CORE_CRITICAL("Failed to find GPU with Vulkan support");

		std::vector<VkPhysicalDevice> devices(deviceCount);
		m_Result = vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

		for (const auto& device : devices) {
			if (IsPhysicalDeviceSuitable(device)) {
				m_PhysicalDevice = device;
				break;
			}
		}

		if (m_PhysicalDevice == VK_NULL_HANDLE)
			SNOW_CORE_CRITICAL("Failed to find suitable GPU");
	}

	void VulkanDevice::CreateLogicalDevice(VkPhysicalDevice physicalDevice) {
		VulkanContext* vkContext = static_cast<VulkanContext*>(m_Specification.Context);
		QueueFamilyIndices indices = FindQueueFamilies(physicalDevice);

		std::vector<VkDeviceQueueCreateInfo> deviceQueueCreateInfo;
		std::set<uint32_t> uniqueQueueFamilies = { indices.graphicsFamily.value(), indices.presentFamily.value() };

		float queuePriority = 1.0f;
		for (uint32_t queueFamily : uniqueQueueFamilies) {
			VkDeviceQueueCreateInfo queueCreateInfo = {};
			queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueCreateInfo.queueFamilyIndex = queueFamily;
			queueCreateInfo.queueCount = 1;
			queueCreateInfo.pQueuePriorities = &queuePriority;
			deviceQueueCreateInfo.push_back(queueCreateInfo);
		}

		VkPhysicalDeviceFeatures physicalDeviceFeatures = {};

		VkDeviceCreateInfo deviceCreateInfo = {};
		deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		deviceCreateInfo.pQueueCreateInfos = deviceQueueCreateInfo.data();
		deviceCreateInfo.queueCreateInfoCount = deviceQueueCreateInfo.size();

		deviceCreateInfo.pEnabledFeatures = &physicalDeviceFeatures;
		deviceCreateInfo.enabledExtensionCount = deviceExtensions.size();
		deviceCreateInfo.ppEnabledExtensionNames = deviceExtensions.data();

#ifdef SNOW_DEBUG
		auto& validationLayer = vkContext->GetValidationLayers();
		deviceCreateInfo.ppEnabledLayerNames = validationLayer.data();
		deviceCreateInfo.enabledLayerCount = validationLayer.size();
#else
		deviceCreateInfo.enabledLayerCount = 0;
#endif

		m_Result = vkCreateDevice(m_PhysicalDevice, &deviceCreateInfo, nullptr, &m_Device);
		vkGetDeviceQueue(m_Device, indices.graphicsFamily.value(), 0, &m_GraphicsQueue);
		vkGetDeviceQueue(m_Device, indices.presentFamily.value(), 0, &m_PresentQueue);
	}

	VulkanDevice::VulkanDevice(const Render::API::DeviceSpecification& spec) {
		m_Specification = spec;

		PickPhysicalDevice();
		CreateLogicalDevice(m_PhysicalDevice);
	}

	VulkanDevice::~VulkanDevice() {
		vkDestroyDevice(m_Device, nullptr);
	}
}