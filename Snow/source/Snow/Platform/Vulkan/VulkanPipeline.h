#pragma once

#include "Snow/Render/API/Pipeline.h"

#include "Snow/Platform/Vulkan/VulkanCommon.h"

namespace Snow {
	class VulkanPipeline : public Render::API::Pipeline {
	public:
		VulkanPipeline(const Render::API::PipelineSpecification& spec);
		~VulkanPipeline();

		virtual void Bind() const override {}

		virtual void UploadBuffer(const Core::Ref<Render::API::UniformBuffer>& uniformBuffer) override {}

		VkPipeline GetPipelineHandle() { return m_Pipeline; }

		virtual const Render::API::PipelineSpecification& GetSpecification() const override { return m_Specification; }
	private:
		Render::API::PipelineSpecification m_Specification;

		VkPipeline m_Pipeline;
		VkPipelineLayout m_PipelineLayout;

		VkResult m_Result;
	};
}