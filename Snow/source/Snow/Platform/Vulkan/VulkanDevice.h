#pragma once

#include "Snow/Render/API/Device.h"

#include "Snow/Platform/Vulkan/VulkanCommon.h"

#include <optional>
#include <set>

namespace Snow {

	struct QueueFamilyIndices {
		std::optional<uint32_t> graphicsFamily;
		std::optional<uint32_t> presentFamily;

		bool IsComplete() {
			return graphicsFamily.has_value() && presentFamily.has_value();
		}
	};

	struct SwapChainSupportDetails {
		VkSurfaceCapabilitiesKHR capabilities;
		std::vector<VkSurfaceFormatKHR> formats;
		std::vector<VkPresentModeKHR> presentModes;
	};

	class VulkanDevice : public Render::API::Device {
	public:
		VulkanDevice(const Render::API::DeviceSpecification& spec);
		~VulkanDevice();

		VkDevice GetLogicalDevice() { return m_Device; }
		VkPhysicalDevice GetPhysicalDeviceHandle() { return m_PhysicalDevice; }

		SwapChainSupportDetails GetSupportDetails() { return m_SupportDetails; }
		QueueFamilyIndices GetQueueIndices() { return m_Indices; }

		VkQueue& GetGraphicsQueue() { return m_GraphicsQueue; }
		VkQueue& GetPresentQueue() { return m_PresentQueue; }

		virtual const Render::API::DeviceSpecification& GetSpecification() const override { return m_Specification; }
	private:
		QueueFamilyIndices FindQueueFamilies(VkPhysicalDevice physicalDevice);
		bool CheckDeviceExtensionSupport(VkPhysicalDevice physicalDevice);
		bool IsPhysicalDeviceSuitable(VkPhysicalDevice physicalDevice);
		SwapChainSupportDetails QuerySwapChainSupport(VkPhysicalDevice device);
		void PickPhysicalDevice();
		void CreateLogicalDevice(VkPhysicalDevice physicalDevice);

		Render::API::DeviceSpecification m_Specification;

		VkDevice m_Device;
		VkPhysicalDevice m_PhysicalDevice;

		QueueFamilyIndices m_Indices;
		VkQueue m_GraphicsQueue;
		VkQueue m_PresentQueue;

		SwapChainSupportDetails m_SupportDetails;

		const std::vector<const char*> deviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

		VkResult m_Result;
	};
}