#include "spch.h"

#include "Snow/Platform/Vulkan/VulkanShader.h"

#include "Snow/Platform/Vulkan/VulkanContext.h"
#include "Snow/Render/Renderer.h"

#include "Snow/Utils/StringUtils.h"

//#include <shaderc/shaderc.hpp>
//#include <vulkan/shaderc/libshaderc/include/shaderc/shaderc.hpp>

namespace Snow {

	//VkShaderModule VulkanShader::GetShaderModule(Render::Shader::ShaderDomain domain) {
	//	switch (domain) {
	//	case Render::Shader::ShaderDomain::Vertex:	return m_Data.vertexShaderModule;
	//	case Render::Shader::ShaderDomain::Pixel:	return m_Data.pixelShaderModule;
	//	}
	//}

	VulkanShader::VulkanShader(const std::string& path, Render::Shader::ShaderType shaderType) :
		m_Path(path), m_Type(shaderType) {
		m_Name = FindFileName(path);

		m_ShaderModule = CompileShader(ReadFileToBuffer(path));


		//Reload();
	}

	VulkanShader::~VulkanShader() {
		VulkanContext* vkContext = static_cast<VulkanContext*>(Render::Renderer::GetContext());
		VulkanDevice* vkDevice = static_cast<VulkanDevice*>(vkContext->GetDevice());

		vkDestroyShaderModule(vkDevice->GetLogicalDevice(), m_ShaderModule, nullptr);
		//vkDestroyShaderModule(vkDevice->GetLogicalDevice(), m_Data.pixelShaderModule, nullptr);
		//delete& m_Data;
	}

	void VulkanShader::Reload() {
		//std::string source = ReadFileToString(m_Path);
		//Load(source);
	}

	void VulkanShader::Load(const std::string& source) {
		//m_ShaderSource = PreProcess(source);
		//Parse();

		//CompileAndUploadShader();
	}

	//std::unordered_map<Render::Shader::ShaderSourceType, std::string> VulkanShader::PreProcess(const std::string& source) {
	//	std::unordered_map<Render::Shader::ShaderSourceType, std::string> shaderSources;
	//
	//	const char* typeToken = "#type";
	//	size_t typeTokenLength = strlen(typeToken);
	//	size_t pos = source.find(typeToken, 0);
	//	while (pos != std::string::npos) {
	//		size_t eol = source.find_first_of("\r\n", pos); 
	//		SNOW_CORE_ASSERT(eol != std::string::npos, "Syntax Error");
	//		size_t begin = pos + typeTokenLength + 1;
	//		std::string type = source.substr(begin, eol - begin);
	//		SNOW_CORE_ASSERT(ShaderTypeFromString(type), "Invalid Shader type specified");
	//
	//		size_t nextLinePos = source.find_first_not_of("\r\n", eol);
	//		pos = source.find(typeToken, nextLinePos);
	//		shaderSources[ShaderTypeFromString(type)] = source.substr(nextLinePos, pos - (nextLinePos == std::string::npos ? source.size() - 1 : nextLinePos));
	//	}
	//	return shaderSources;
	//}

	VkShaderModule VulkanShader::CompileShader(Core::Buffer sourceSPIRV) {
		VulkanContext* vkContext = static_cast<VulkanContext*>(Render::Renderer::GetContext());
		VulkanDevice* vkDevice = static_cast<VulkanDevice*>(vkContext->GetDevice());

		VkShaderModuleCreateInfo shaderModuleCreateInfo = {};
		shaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		shaderModuleCreateInfo.codeSize = sourceSPIRV.GetSize();
		shaderModuleCreateInfo.pCode = reinterpret_cast<const uint32_t*>(sourceSPIRV.Data);

		VkShaderModule shaderModule;
		m_Result = vkCreateShaderModule(vkDevice->GetLogicalDevice(), &shaderModuleCreateInfo, nullptr, &shaderModule);

		return shaderModule;
	}

	//void VulkanShader::CompileAndUploadShader() {
	//
	//}
	//
	//void VulkanShader::Bind() const {
	//
	//}

	//void VulkanShader::UploadUniformBuffer(const Render::API::UniformBuffer& uniformBuffer) {
	//
	//}

	void VulkanShader::Parse() {

	}

	void VulkanShader::ParseUniform(const std::string& statement) {

	}

	void VulkanShader::ParseConstantBuffer(const std::string& block) {

	}

	void VulkanShader::ParseStruct(const std::string& block) {

	}

	void VulkanShader::ParseTexture(const std::string& statement) {

	}

	void VulkanShader::ParseSamplerState(const std::string& statement) {

	}

	//void VulkanShader::SetVSMaterialUniformBuffer(Buffer buffer) {
	//
	//}
	//
	//void VulkanShader::SetPSMaterialUniformBuffer(Buffer buffer) {
	//
	//}

	Render::Shader::ShaderStruct* VulkanShader::FindStruct(const std::string& name) {
		for (Render::Shader::ShaderStruct* s : m_Structs) {
			if (s->GetName() == name)
				return s;
		}
		return nullptr;
	}
}