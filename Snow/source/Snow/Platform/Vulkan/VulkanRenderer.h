#pragma once

#include "Snow/Render/Renderer.h"

namespace Snow {
	class VulkanRenderer : public Render::Renderer {
	public:
		VulkanRenderer();

	protected:
		virtual void SetDepthTestingInternal(bool enabled) override {}
		virtual void SetBlendInternal(bool enabled) override {}

		virtual void InitInternal();

		virtual void SetViewportInternal(int x, int y, unsigned int width, unsigned int height) override {}
	};
}