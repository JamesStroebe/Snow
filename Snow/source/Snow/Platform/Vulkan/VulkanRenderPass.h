#pragma once


#include "Snow/Render/API/RenderPass.h"

#include "Snow/Platform/Vulkan/VulkanCommon.h"

namespace Snow {
	class VulkanRenderPass : public Render::API::RenderPass {
	public:
		VulkanRenderPass(const Render::API::RenderPassSpecification& spec);
		~VulkanRenderPass();

		VkRenderPass GetRenderPassHandle() { return m_RenderPass; }

		virtual const Render::API::RenderPassSpecification& GetSpecification() const override { return m_Specification; }

	private:
		Render::API::RenderPassSpecification m_Specification;

		VkRenderPass m_RenderPass;
		VkResult m_Result;
	};
}