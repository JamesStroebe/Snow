#pragma once

#include "Snow/Render/Shaders/Shader.h"

#include "Snow/Platform/Vulkan/VulkanCommon.h"

#include "Snow/Utils/StringUtils.h"

namespace Snow {

	

	class VulkanShader : public Render::Shader::Shader {
	
	public:
		VulkanShader() = default;
		VulkanShader(const std::string& path, Render::Shader::ShaderType shaderType);
		~VulkanShader();

		virtual void Reload() override;


		inline const Render::Shader::ShaderBufferList& GetRendererBuffers() const override { return m_RendererConstantBuffer; }
		inline const Render::Shader::ShaderBuffer& GetMaterialBuffer() const override { return m_MaterialConstantBuffer; }
		inline const Render::Shader::ShaderFieldList& GetGlobalUniforms() const override { return m_GlobalUniforms; }
		inline const Render::Shader::ShaderResourceList& GetResources() const override { return m_Resources; }


		virtual const std::string& GetName() const override { return m_Name; }
		virtual const Render::Shader::ShaderType GetType() const override { return m_Type; }

		VkShaderModule GetShaderModule() { return m_ShaderModule; }
	private:
		void Load(const std::string& source);

		void Parse();
		void ParseUniform(const std::string& statement);
		void ParseConstantBuffer(const std::string& block);
		void ParseTexture(const std::string& statement);
		void ParseSamplerState(const std::string& statement);
		void ParseStruct(const std::string& block);

		Render::Shader::ShaderStruct* FindStruct(const std::string& name);

		VkShaderModule CompileShader(Core::Buffer sourceSPIRV);

		
		

		std::string m_Path, m_Name;
		Render::Shader::ShaderType m_Type;
		bool m_Loaded = false;

		Render::Shader::ShaderBufferList m_RendererConstantBuffer;
		Render::Shader::ShaderBuffer m_MaterialConstantBuffer;
		Render::Shader::ShaderFieldList m_GlobalUniforms;
		Render::Shader::ShaderResourceList m_Resources;
		Render::Shader::ShaderStructList m_Structs;


		VkShaderModule m_ShaderModule;

		VkResult m_Result;
	};
}