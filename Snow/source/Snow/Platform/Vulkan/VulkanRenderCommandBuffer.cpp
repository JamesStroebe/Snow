#include "spch.h"

#include "Snow/Platform/Vulkan/VulkanRenderCommandBuffer.h"

#include "Snow/Platform/Vulkan/VulkanContext.h"
#include "Snow/Platform/Vulkan/VulkanRenderer.h"
#include "Snow/Platform/Vulkan/VulkanPipeline.h"

namespace Snow {

	void VulkanRenderCommandBuffer::CreateCommandPool() {
		VulkanContext* vkContext = static_cast<VulkanContext*>(Render::Renderer::GetContext());
		VulkanDevice* vkDevice = vkContext->GetDevice();
		QueueFamilyIndices queueFamilies = vkDevice->GetQueueIndices();

		VkCommandPoolCreateInfo commandPoolCreateInfo = {};
		commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		commandPoolCreateInfo.pNext = nullptr;
		commandPoolCreateInfo.queueFamilyIndex = queueFamilies.graphicsFamily.value();

		m_Result = vkCreateCommandPool(vkDevice->GetLogicalDevice(), &commandPoolCreateInfo, nullptr, &m_CommandPool);
	}

	void VulkanRenderCommandBuffer::CreateCommandBuffers() {

		VulkanContext* vkContext = static_cast<VulkanContext*>(Render::Renderer::GetContext());
		VulkanDevice* vkDevice = vkContext->GetDevice();
		VulkanSwapChain* vkSwapChain = vkContext->GetSwapChain();

		m_CommandBuffers.resize(vkSwapChain->GetFramebuffers().size());

		VkCommandBufferAllocateInfo allocateInfo = {};
		allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocateInfo.pNext = nullptr;
		allocateInfo.commandPool = m_CommandPool;
		allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocateInfo.commandBufferCount = m_CommandBuffers.size();

		m_Result = vkAllocateCommandBuffers(vkDevice->GetLogicalDevice(), &allocateInfo, m_CommandBuffers.data());
	}

	void VulkanRenderCommandBuffer::UpdateCommandBuffers() {
		VulkanContext* vkContext = static_cast<VulkanContext*>(Render::Renderer::GetContext());
		VulkanDevice* vkDevice = vkContext->GetDevice();
		VulkanSwapChain* vkSwapChain = vkContext->GetSwapChain();

		m_CommandBuffers.resize(vkSwapChain->GetFramebuffers().size());

		VkCommandBufferAllocateInfo allocateInfo = {};
		allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocateInfo.pNext = nullptr;
		allocateInfo.commandPool = m_CommandPool;
		allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocateInfo.commandBufferCount = m_CommandBuffers.size();

		m_Result = vkAllocateCommandBuffers(vkDevice->GetLogicalDevice(), &allocateInfo, m_CommandBuffers.data());
	}

	void VulkanRenderCommandBuffer::CreateSemaphores() {
		VulkanContext* vkContext = static_cast<VulkanContext*>(Render::Renderer::GetContext());
		VulkanDevice* vkDevice = vkContext->GetDevice();

		VkSemaphoreCreateInfo semaphoreCreateInfo = {};
		semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

		m_Result = vkCreateSemaphore(vkDevice->GetLogicalDevice(), &semaphoreCreateInfo, nullptr, &m_ImageAvailSemaphore);
		m_Result = vkCreateSemaphore(vkDevice->GetLogicalDevice(), &semaphoreCreateInfo, nullptr, &m_RenderFinishedSemaphore);
	}

	VulkanRenderCommandBuffer::VulkanRenderCommandBuffer() {
		CreateCommandPool();
		CreateCommandBuffers();
		CreateSemaphores();
	}

	VulkanRenderCommandBuffer::~VulkanRenderCommandBuffer() {
		VulkanContext* vkContext = static_cast<VulkanContext*>(Render::Renderer::GetContext());
		VulkanDevice* vkDevice = vkContext->GetDevice();

		vkDestroyCommandPool(vkDevice->GetLogicalDevice(), m_CommandPool, nullptr);
		vkDestroySemaphore(vkDevice->GetLogicalDevice(), m_ImageAvailSemaphore, nullptr);
		vkDestroySemaphore(vkDevice->GetLogicalDevice(), m_RenderFinishedSemaphore, nullptr);

	}

	void VulkanRenderCommandBuffer::BeginRenderPass(const Core::Ref<Render::API::RenderPass>& renderPass) {
		VulkanContext* vkContext = static_cast<VulkanContext*>(Render::Renderer::GetContext());
		VulkanSwapChain* vkSwapChain = static_cast<VulkanSwapChain*>(vkContext->GetSwapChain());
		
		VulkanRenderPass* vkRenderPass = static_cast<VulkanRenderPass*>(renderPass.get());

		for (size_t i = 0; i < m_CommandBuffers.size(); i++) {
			VkCommandBufferBeginInfo commandBufferBeginInfo = {};
			commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			commandBufferBeginInfo.flags = 0;
			commandBufferBeginInfo.pInheritanceInfo = nullptr;

			m_Result = vkBeginCommandBuffer(m_CommandBuffers[i], &commandBufferBeginInfo);


			VkRenderPassBeginInfo renderPassBeginInfo = {};
			renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			renderPassBeginInfo.renderPass = vkRenderPass->GetRenderPassHandle();
			renderPassBeginInfo.framebuffer = vkSwapChain->GetFramebuffers().at(i).GetFramebufferHandle();

			renderPassBeginInfo.renderArea.offset = { 0, 0 };
			renderPassBeginInfo.renderArea.extent = vkSwapChain->GetExtent();

			Math::Vector4f c = vkSwapChain->GetFramebuffers().at(i).GetSpecification().Color;
			VkClearValue clearValue = { c.r, c.g, c.b, c.a };

			renderPassBeginInfo.clearValueCount = 1;
			renderPassBeginInfo.pClearValues = &clearValue;

			vkCmdBeginRenderPass(m_CommandBuffers[i], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
		}

		
	}

	void VulkanRenderCommandBuffer::EndRenderPass() {
		for (size_t i = 0; i < m_CommandBuffers.size(); i++) {
			vkCmdEndRenderPass(m_CommandBuffers[i]);

			vkEndCommandBuffer(m_CommandBuffers[i]);
		}
	}

	void VulkanRenderCommandBuffer::SubmitElements(const Core::Ref<Render::API::Pipeline>& pipeline, const Core::Ref<Render::API::VertexBuffer>& vertexBuffer, const Core::Ref<Render::API::IndexBuffer>& indexBuffer, uint32_t indexCount) {
		VulkanPipeline* vkPipeline = static_cast<VulkanPipeline*>(pipeline.get());
		
		for (size_t i = 0; i < m_CommandBuffers.size(); i++) {
			vkCmdBindPipeline(m_CommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, vkPipeline->GetPipelineHandle());
			vkCmdDraw(m_CommandBuffers[i], 3, 1, 0, 0);
		}
	}

	void VulkanRenderCommandBuffer::Flush() {
		VulkanContext* vkContext = static_cast<VulkanContext*>(Render::Renderer::GetContext());
		VulkanDevice* vkDevice = vkContext->GetDevice();
		VulkanSwapChain* vkSwapChain = vkContext->GetSwapChain();
		
		vkAcquireNextImageKHR(vkDevice->GetLogicalDevice(), vkSwapChain->GetSwapChainHandle(), UINT64_MAX, m_ImageAvailSemaphore, VK_NULL_HANDLE, &m_ImageIndex);
		
		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

		VkSemaphore* waitSemaphores = { &m_ImageAvailSemaphore };
		VkPipelineStageFlags waitStages[1] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = waitSemaphores;
		submitInfo.pWaitDstStageMask = waitStages;

		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &m_CommandBuffers[m_ImageIndex];

		VkSemaphore* signalSemaphores = { &m_RenderFinishedSemaphore };
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = signalSemaphores;

		
		m_Result = vkQueueSubmit(vkDevice->GetGraphicsQueue(), 1, &submitInfo, VK_NULL_HANDLE);
	}

	void VulkanRenderCommandBuffer::Present() {
		VulkanContext* vkContext = static_cast<VulkanContext*>(Render::Renderer::GetContext());
		VulkanDevice* vkDevice = vkContext->GetDevice();
		VulkanSwapChain* vkSwapChain = vkContext->GetSwapChain();

		VkPresentInfoKHR presentInfoKHR = {};
		presentInfoKHR.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfoKHR.waitSemaphoreCount = 1;
		presentInfoKHR.pWaitSemaphores = &m_RenderFinishedSemaphore;
		presentInfoKHR.swapchainCount = 1;
		presentInfoKHR.pSwapchains = &vkSwapChain->GetSwapChainHandle();
		presentInfoKHR.pImageIndices = &m_ImageIndex;
		presentInfoKHR.pResults = nullptr;

		vkQueuePresentKHR(vkDevice->GetPresentQueue(), &presentInfoKHR);
		vkQueueWaitIdle(vkDevice->GetPresentQueue());
	}
}