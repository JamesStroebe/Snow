#pragma once

#include "Snow/Render/RenderCommandBuffer.h"

#include "Snow/Platform/Vulkan/VulkanCommon.h"


namespace Snow {
	class VulkanRenderCommandBuffer : public Render::RenderCommandBuffer {
	public:
		VulkanRenderCommandBuffer();
		~VulkanRenderCommandBuffer();

		virtual void BeginRenderPass(const Core::Ref<Render::API::RenderPass>& renderPass) override;
		virtual void EndRenderPass() override;

		virtual void SubmitElements(const Core::Ref<Render::API::Pipeline>& pipeline, const Core::Ref<Render::API::VertexBuffer>& vertexBuffer, const Core::Ref<Render::API::IndexBuffer>& indexBuffer, uint32_t indexCount) override;

		virtual void Flush() override;

		virtual void Present() override;

		void UpdateCommandBuffers();
	private:
		void CreateCommandPool();
		void CreateCommandBuffers();
		void CreateSemaphores();

		uint32_t m_ImageIndex;

		VkCommandPool m_CommandPool;
		std::vector<VkCommandBuffer> m_CommandBuffers;

		VkSemaphore m_ImageAvailSemaphore;
		VkSemaphore m_RenderFinishedSemaphore;

		VkResult m_Result;
	};
}
