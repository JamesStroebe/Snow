#include "spch.h"
#include "Snow/Platform/Vulkan/VulkanSwapChain.h"

#include "Snow/Platform/Vulkan/VulkanContext.h"

namespace Snow {

	VkSurfaceFormatKHR VulkanSwapChain::ChooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
		for (const auto& format : availableFormats) {
			if (format.format == VK_FORMAT_B8G8R8A8_UNORM && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
				return format;
			}
		}

		return availableFormats[0];
	}

	VkPresentModeKHR VulkanSwapChain::ChooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes) {
		for (const auto& presentMode : availablePresentModes) {
			if (presentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
				return presentMode;
			}
		}

		return  VK_PRESENT_MODE_FIFO_KHR;
	}

	VkExtent2D VulkanSwapChain::ChooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) {
		VulkanContext* vkContext = static_cast<VulkanContext*>(m_Specification.Context);
		Core::Window* window = vkContext->GetSpecification().Window.get();
		if (capabilities.currentExtent.width != UINT32_MAX) {
			return capabilities.currentExtent;
		}
		else {
			VkExtent2D extent = { window->GetWidth(), window->GetHeight() };
			extent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, extent.width));
			extent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, extent.height));
			return extent;
		}
	}

	void VulkanSwapChain::CreateImageViews(std::vector<VkImage> swapChainImages) {
		m_SwapChainImageViews.resize(swapChainImages.size());

		for (uint32_t i = 0; i < swapChainImages.size(); i++) {
			m_SwapChainImageViews[i] = CreateImageView(swapChainImages[i], m_SwapChainImageFormat);
		}
	}

	VkImageView VulkanSwapChain::CreateImageView(VkImage image, VkFormat format) {
		VulkanContext* vkContext = static_cast<VulkanContext*>(m_Specification.Context);
		VulkanDevice* vkDevice = vkContext->GetDevice();

		VkImageViewCreateInfo imageViewCreateInfo = {};
		imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		imageViewCreateInfo.pNext = nullptr;
		imageViewCreateInfo.image = image;
		imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		imageViewCreateInfo.format = format;
		imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
		imageViewCreateInfo.subresourceRange.levelCount = 1;
		imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
		imageViewCreateInfo.subresourceRange.layerCount = 1;

		VkImageView imageView;
		m_Result = vkCreateImageView(vkDevice->GetLogicalDevice(), &imageViewCreateInfo, nullptr, &imageView);
		return imageView;
	}

	void VulkanSwapChain::AddFramebuffer(const VulkanFramebuffer& framebuffer) {
		m_Framebuffers.push_back(framebuffer);
	}


	VulkanSwapChain::VulkanSwapChain(const Render::API::SwapChainSpecification& spec) {
		m_Specification = spec;

		VulkanContext* vkContext = static_cast<VulkanContext*>(m_Specification.Context);
		VulkanDevice* vkDevice = vkContext->GetDevice();

		QueueFamilyIndices indices = vkDevice->GetQueueIndices();
		SwapChainSupportDetails supportDetails = vkDevice->GetSupportDetails();

		m_SwapChainSurfaceFormat = ChooseSwapSurfaceFormat(supportDetails.formats);
		m_SwapChainPresentMode = ChooseSwapPresentMode(supportDetails.presentModes);
		m_SwapChainExtent = ChooseSwapExtent(supportDetails.capabilities);

		uint32_t imageCount = supportDetails.capabilities.minImageCount + 1;
		if (supportDetails.capabilities.maxImageCount > 0 && imageCount > supportDetails.capabilities.maxImageCount)
			imageCount = supportDetails.capabilities.maxImageCount;

		VkSwapchainCreateInfoKHR swapChainCreateInfo = {};
		swapChainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		swapChainCreateInfo.pNext = NULL;
		swapChainCreateInfo.surface = vkContext->GetSurface();
		swapChainCreateInfo.minImageCount = imageCount;
		swapChainCreateInfo.imageFormat = m_SwapChainSurfaceFormat.format;
		swapChainCreateInfo.imageColorSpace = m_SwapChainSurfaceFormat.colorSpace;
		swapChainCreateInfo.imageExtent = m_SwapChainExtent;
		swapChainCreateInfo.imageArrayLayers = 1;
		swapChainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

		uint32_t queueFamilyIndices[] = { indices.graphicsFamily.value(), indices.presentFamily.value() };

		if (indices.graphicsFamily != indices.presentFamily) {
			swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
			swapChainCreateInfo.queueFamilyIndexCount = 2;
			swapChainCreateInfo.pQueueFamilyIndices = queueFamilyIndices;
		}
		else {
			swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
			swapChainCreateInfo.queueFamilyIndexCount = 0;
			swapChainCreateInfo.pQueueFamilyIndices = NULL;
		}

		VkSurfaceTransformFlagsKHR preTransform;
		if (supportDetails.capabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
			preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
		}
		else {
			preTransform = supportDetails.capabilities.currentTransform;
		}

		VkCompositeAlphaFlagBitsKHR compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		std::vector<VkCompositeAlphaFlagBitsKHR> compositeAlphaFlags = {
			VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
			VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
			VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
			VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR,
		};
		for (auto& compositeAlphaFlag : compositeAlphaFlags) {
			if (supportDetails.capabilities.supportedCompositeAlpha & compositeAlphaFlag) {
				compositeAlpha = compositeAlphaFlag;
				break;
			}
		}

		swapChainCreateInfo.preTransform = (VkSurfaceTransformFlagBitsKHR)preTransform;
		swapChainCreateInfo.compositeAlpha = compositeAlpha;
		swapChainCreateInfo.presentMode = m_SwapChainPresentMode;
		swapChainCreateInfo.clipped = VK_TRUE;
		swapChainCreateInfo.oldSwapchain = VK_NULL_HANDLE;

		m_Result = vkCreateSwapchainKHR(vkDevice->GetLogicalDevice(), &swapChainCreateInfo, nullptr, &m_SwapChain);

		m_Result = vkGetSwapchainImagesKHR(vkDevice->GetLogicalDevice(), m_SwapChain, &imageCount, NULL);
		m_SwapChainImages.resize(imageCount);
		m_Result = vkGetSwapchainImagesKHR(vkDevice->GetLogicalDevice(), m_SwapChain, &imageCount, m_SwapChainImages.data());

		m_SwapChainImageFormat = m_SwapChainSurfaceFormat.format;

		CreateImageViews(m_SwapChainImages);
	}

	VulkanSwapChain::~VulkanSwapChain() {
		VulkanContext* vkContext = static_cast<VulkanContext*>(m_Specification.Context);
		VulkanDevice* vkDevice = vkContext->GetDevice();

		for (auto imageView : m_SwapChainImageViews) {
			vkDestroyImageView(vkDevice->GetLogicalDevice(), imageView, nullptr);
		}

		vkDestroySwapchainKHR(vkDevice->GetLogicalDevice(), m_SwapChain, nullptr);
	}
}