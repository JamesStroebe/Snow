#include "spch.h"

#include "Snow/Platform/Vulkan/VulkanFramebuffer.h"

#include "Snow/Platform/Vulkan/VulkanContext.h"
#include "Snow/Platform/Vulkan/VulkanRenderCommandBuffer.h"
#include "Snow/Platform/Vulkan/VulkanRenderPass.h"

#include "Snow/Render/Renderer.h"

namespace Snow {
	VulkanFramebuffer::VulkanFramebuffer(const Render::API::FramebufferSpecification& spec) {
		m_Specification = spec;

		VulkanContext* vkContext = static_cast<VulkanContext*>(Render::Renderer::GetContext());
		VulkanDevice* vkDevice = vkContext->GetDevice();
		VulkanSwapChain* vkSwapChain = vkContext->GetSwapChain();

		//m_RenderPass = static_cast<VulkanRenderPass*>(m_Specification.RenderPass.get());

		VkImageView attachment[] = {
			vkSwapChain->GetImageViews()[0]
		};

		VkFramebufferCreateInfo framebufferInfo = {};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		//framebufferInfo.renderPass = m_RenderPass->GetRenderPassHandle();
		framebufferInfo.attachmentCount = 1;
		framebufferInfo.pAttachments = attachment;
		framebufferInfo.width = vkSwapChain->GetExtent().width;
		framebufferInfo.height = vkSwapChain->GetExtent().height;
		framebufferInfo.layers = 1;

		m_Result = vkCreateFramebuffer(vkDevice->GetLogicalDevice(), &framebufferInfo, nullptr, &m_Framebuffer);

		vkSwapChain->AddFramebuffer(*this);
		
		VulkanRenderCommandBuffer* vkRenderCommandBuffer = static_cast<VulkanRenderCommandBuffer*>(Render::Renderer::GetCommandBuffer());
		vkRenderCommandBuffer->UpdateCommandBuffers();
	}

	VulkanFramebuffer::~VulkanFramebuffer() {
		VulkanContext* vkContext = static_cast<VulkanContext*>(Render::Renderer::GetContext());
		VulkanDevice* vkDevice = static_cast<VulkanDevice*>(vkContext->GetDevice());

		vkDestroyFramebuffer(vkDevice->GetLogicalDevice(), m_Framebuffer, nullptr);
	}
	
}