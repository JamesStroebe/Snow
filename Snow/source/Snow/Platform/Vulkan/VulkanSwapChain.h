#pragma once

#include "Snow/Render/API/SwapChain.h"
#include "Snow/Platform/Vulkan/VulkanCommon.h"

#include "Snow/Platform/Vulkan/VulkanFramebuffer.h"
#include "Snow/Platform/Vulkan/VulkanRenderPass.h"

#include <vector>

namespace Snow {
	class VulkanSwapChain : public Render::API::SwapChain {
	public:
		VulkanSwapChain(const Render::API::SwapChainSpecification& spec);
		~VulkanSwapChain();

		VkSwapchainKHR& GetSwapChainHandle() { return m_SwapChain; }

		VkFormat& GetImageFormat() { return m_SwapChainImageFormat; }
		std::vector<VkImageView> GetImageViews() { return m_SwapChainImageViews; }

		VkExtent2D& GetExtent() { return m_SwapChainExtent; }

		std::vector<VulkanFramebuffer>& GetFramebuffers() { return m_Framebuffers; }
		void AddFramebuffer(const VulkanFramebuffer& framebuffer);

		virtual const Render::API::SwapChainSpecification& GetSpecification() const override { return m_Specification; }

	private:
		VkSurfaceFormatKHR ChooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
		VkPresentModeKHR ChooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);
		VkExtent2D ChooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);

		VkImageView CreateImageView(VkImage image, VkFormat format);
		void CreateImageViews(std::vector<VkImage> swapChainImages);

		Render::API::SwapChainSpecification m_Specification;

		VkSwapchainKHR m_SwapChain, m_OldSwapChain;

		VkSurfaceFormatKHR m_SwapChainSurfaceFormat;
		VkPresentModeKHR m_SwapChainPresentMode;
		VkExtent2D m_SwapChainExtent;
		VkFormat m_SwapChainImageFormat;

		std::vector<VkImage> m_SwapChainImages;
		std::vector<VkImageView> m_SwapChainImageViews;

		std::vector<VulkanFramebuffer> m_Framebuffers;
		
		VkResult m_Result;
	};
}