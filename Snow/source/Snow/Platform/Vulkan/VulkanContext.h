#pragma once

#include "Snow/Render/API/Context.h"

#include "Snow/Platform/Vulkan/VulkanCommon.h"
#include "Snow/Platform/Vulkan/VulkanDevice.h"
#include "Snow/Platform/Vulkan/VulkanSwapChain.h"
#include "Snow/Platform/Vulkan/VulkanRenderPass.h"

namespace Snow {
	class VulkanContext : public Render::API::Context {
	public:
		VulkanContext(const Render::API::ContextSpecification& spec);
		~VulkanContext();

		inline VulkanDevice* GetDevice() { return m_Device; }
		inline VulkanSwapChain* GetSwapChain() { return m_SwapChain; }

		inline VkInstance& GetInstance() { return m_Instance; }
		inline VkSurfaceKHR& GetSurface() { return m_Surface; }

		inline const std::vector<const char*> GetValidationLayers() { return m_ValidationLayerName; }

		virtual const Render::API::ContextSpecification& GetSpecification() const override { return m_Specification; }

	private:
		void CreateInstance();
		void CreateSurface();

		Render::API::ContextSpecification m_Specification;

		VkInstance m_Instance;
		VkSurfaceKHR m_Surface;

		VulkanDevice* m_Device;
		VulkanSwapChain* m_SwapChain;


		VkImageView m_Attachment;

		const std::vector<const char*> m_ValidationLayerName = { "VK_LAYER_KHRONOS_validation", "VK_LAYER_LUNARG_standard_validation", "VK_LAYER_LUNARG_api_dump" };
		std::vector<const char*> enabledInstanceExtensions;

		VkResult m_Result;
	};
}