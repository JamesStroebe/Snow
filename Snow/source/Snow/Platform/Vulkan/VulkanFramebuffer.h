#pragma once

#include "Snow/Render/API/Framebuffer.h"

#include "Snow/Platform/Vulkan/VulkanCommon.h"
#include "Snow/Platform/Vulkan/VulkanRenderPass.h"

namespace Snow {

	class VulkanFramebuffer : public Render::API::Framebuffer {
	public:
		VulkanFramebuffer(const Render::API::FramebufferSpecification& spec);
		~VulkanFramebuffer();

		virtual void Bind() const override {}
		virtual void Unbind() const override {}
		virtual void BindToRead() const override {}
		virtual void BindColorAttachment(uint32_t slot) const override {}

		virtual void SetColor(Math::Vector4f color) override { m_Specification.Color = color; }

		virtual Core::RendererID GetRendererID() { return 0; }

		virtual Core::RendererID GetColorAttachment() { return 0; }
		virtual Core::RendererID GetDepthAttachment() { return 0; }

		VkFramebuffer GetFramebufferHandle() { return m_Framebuffer; }
		VulkanRenderPass& GetRenderPass() { return *m_RenderPass; }

		virtual const Render::API::FramebufferSpecification& GetSpecification() const override { return m_Specification; }
	private:
		Render::API::FramebufferSpecification m_Specification;

		VkFramebuffer m_Framebuffer;
		VkResult m_Result;

		VulkanRenderPass* m_RenderPass;
	};
}