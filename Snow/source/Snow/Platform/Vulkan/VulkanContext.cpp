#include "spch.h"
#include "Snow/Platform/Vulkan/VulkanContext.h"

#include "Snow/Platform/Windows/WindowsWindow.h"

namespace Snow {

	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData) {
		SNOW_CORE_ERROR("Validation Layer: {0}", pCallbackData->pMessage);
		return VK_FALSE;
	}

	VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) {
		auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
		if (func != nullptr) {
			return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
		}
		else {
			return VK_ERROR_EXTENSION_NOT_PRESENT;
		}
	}

	void VulkanContext::CreateInstance() {
		VkApplicationInfo appInfo = {};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = m_Specification.Window->m_Props.Title.c_str();
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.pEngineName = "Snow Engine";
		appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.apiVersion = VK_API_VERSION_1_1;

		std::vector<const char*> instanceExtensions = { VK_KHR_SURFACE_EXTENSION_NAME };

#if SNOW_PLATFORM & SNOW_PLATFORM_WINDOWS32_BIT
		instanceExtensions.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
#elif defined(VK_USE_PLATFORM_ANDROID_KHR)
		instanceExtensions.push_back(VK_KHR_ANDROID_SURFACE_EXTENSION_NAME);
#elif defined(_DIRECT2DISPLAY)
		instanceExtensions.push_back(VK_KHR_DISPLAY_EXTENSION_NAME);
#elif defined(VK_USE_PLATFORM_WAYLAND_KHR)
		instanceExtensions.push_back(VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME);
#elif defined(VK_USE_PLATFORM_XCB_KHR)
		instanceExtensions.push_back(VK_KHR_XCB_SURFACE_EXTENSION_NAME);
#elif defined(VK_USE_PLATFORM_IOS_MVK)
		instanceExtensions.push_back(VK_MVK_IOS_SURFACE_EXTENSION_NAME);
#elif defined(VK_USE_PLATFORM_MACOS_MVK)
		instanceExtensions.push_back(VK_MVK_MACOS_SURFACE_EXTENSION_NAME);
#endif

		if (enabledInstanceExtensions.size() > 0) {
			for (auto enabledExtension : enabledInstanceExtensions)
				instanceExtensions.push_back(enabledExtension);
		}

		VkInstanceCreateInfo instanceCreateInfo = {};
		instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		instanceCreateInfo.pNext = nullptr;
		instanceCreateInfo.pApplicationInfo = &appInfo;

		VkDebugUtilsMessengerCreateInfoEXT debugMessengerCreateInfo;
		if (instanceExtensions.size() > 0) {
			instanceCreateInfo.enabledExtensionCount = (uint32_t)instanceExtensions.size();
			instanceCreateInfo.ppEnabledExtensionNames = instanceExtensions.data();

			debugMessengerCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
			debugMessengerCreateInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
			debugMessengerCreateInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
			debugMessengerCreateInfo.pfnUserCallback = debugCallback;

			instanceCreateInfo.pNext = NULL;



		}

		uint32_t instanceLayerCount;
		m_Result = vkEnumerateInstanceLayerProperties(&instanceLayerCount, nullptr);
		std::vector<VkLayerProperties> availableLayers(instanceLayerCount);
		m_Result = vkEnumerateInstanceLayerProperties(&instanceLayerCount, availableLayers.data());

		bool validationLayerPresent = false;
		for (const char* layerName : m_ValidationLayerName) {
			bool layerFound = false;
			for (const auto& layerProperties : availableLayers) {
				if (strcmp(layerName, layerProperties.layerName) == 0) {
					SNOW_CORE_INFO("Validation Layer {0} found", layerName);
					layerFound = true;
					break;
				}
			}
			if (!layerFound) {
				SNOW_CORE_ERROR("Validation Layer {0} not found", layerName);
				validationLayerPresent = false;
			}
			else {
				validationLayerPresent = true;
			}
		}

		if (validationLayerPresent) {
			instanceCreateInfo.ppEnabledLayerNames = m_ValidationLayerName.data();
			instanceCreateInfo.enabledLayerCount = m_ValidationLayerName.size();
		}
		else {
			SNOW_CORE_CRITICAL("No validation layer is present");
		}
		m_Result = vkCreateInstance(&instanceCreateInfo, NULL, &m_Instance);
	}

	void VulkanContext::CreateSurface() {
#if defined(VK_USE_PLATFORM_WIN32_KHR)
		WindowsWindow* window = static_cast<WindowsWindow*>(m_Specification.Window.get());

		VkWin32SurfaceCreateInfoKHR surfaceCreateInfo = {};
		surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
		surfaceCreateInfo.hinstance = window->GetInstance();
		surfaceCreateInfo.hwnd = window->GetNativeWindowHandle();
		m_Result = vkCreateWin32SurfaceKHR(m_Instance, &surfaceCreateInfo, NULL, &m_Surface);
#endif
	}

	

	


	VulkanContext::VulkanContext(const Render::API::ContextSpecification& spec) {
		m_Specification = spec;

		CreateInstance();
		CreateSurface();

		Render::API::DeviceSpecification deviceSpec;
		deviceSpec.Context = this;
		m_Device = (VulkanDevice*)Render::API::Device::Create(deviceSpec);

		Render::API::SwapChainSpecification swapChainSpec;
		swapChainSpec.Context = this;
		m_SwapChain = (VulkanSwapChain*)Render::API::SwapChain::Create(swapChainSpec);

	}

	VulkanContext::~VulkanContext() {

	}
}