#pragma once

#if SNOW_PLATFORM & SNOW_PLATFORM_WINDOWS64_BIT
#define VK_USE_PLATFORM_WIN32_KHR
#elif defined SNOW_PLATFORM_MAC 
#define VK_USE
#endif
#include <vulkan/vulkan.h>