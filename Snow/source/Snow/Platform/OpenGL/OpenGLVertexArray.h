#pragma once

#include "Snow/Render/API/VertexArray.h"

#include "Snow/Platform/OpenGL/OpenGLCommon.h"

namespace Snow {
	class OpenGLVertexArray : public Render::API::VertexArray {
	public:
		OpenGLVertexArray();
		~OpenGLVertexArray();

		virtual void Bind() const override;
		virtual void Unbind() const override;

	private:
		Core::RendererID m_RendererID;
	};
}