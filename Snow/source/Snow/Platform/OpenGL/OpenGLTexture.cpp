#include "spch.h"
#include "OpenGLTexture.h"

#include "Snow/Render/RenderAPI.h"
#include "Snow/Render/Renderer.h"

#include "Snow/Platform/OpenGL/OpenGLRenderCommandBuffer.h"

#include "OpenGLCommon.h"
#include <stb_image.h>

namespace Snow {

	static GLenum SnowToGLTextureDataFormat(Render::API::TextureFormat format) {
		switch (format) {
		case Render::API::TextureFormat::R8:		return GL_RED;
		case Render::API::TextureFormat::R8G8:		return GL_RG;
		case Render::API::TextureFormat::R8G8B8:	return GL_RGB;
		case Render::API::TextureFormat::R8G8B8A8:	return GL_RGBA;
		}
		return 0;
	}

	static GLenum SnowToGLTextureInternalFormat(Render::API::TextureFormat format) {
		switch (format) {
		case Render::API::TextureFormat::R8:		return GL_R8;
		case Render::API::TextureFormat::R8G8:		return GL_RG8;
		case Render::API::TextureFormat::R8G8B8:	return GL_RGB8;
		case Render::API::TextureFormat::R8G8B8A8:	return GL_RGBA8;
		}
		return 0;
	}

	static GLenum SnowToGLTextureFilter(Render::API::TextureFilter filter) {
		switch (filter) {
		case Render::API::TextureFilter::Linear:	return GL_LINEAR;
		case Render::API::TextureFilter::Nearest:	return GL_NEAREST;
		}
		return 0;
	}

	static GLenum SnowToGLTextureWrap(Render::API::TextureWrap wrap) {
		switch (wrap) {
		case Render::API::TextureWrap::Repeat:			return GL_REPEAT;
		case Render::API::TextureWrap::Clamp:			return GL_CLAMP;
		case Render::API::TextureWrap::ClampEdge:		return GL_CLAMP_TO_EDGE;
		case Render::API::TextureWrap::ClampBorder:	return GL_CLAMP_TO_BORDER;
		case Render::API::TextureWrap::MirroredRepeat:	return GL_MIRRORED_REPEAT;
		}
		return 0;
	}

	static int CalculateMipMapCount(unsigned int width, unsigned int height) {
		int levels = 1;
		while ((width | height) >> levels) {
			levels++;
		}
		return levels;
	}

	OpenGLTexture2D::OpenGLTexture2D(unsigned int width, unsigned int height, const Render::API::TextureSpecification& spec) :
		m_Width(width), m_Height(height), m_Specification(spec) {

		Render::Renderer::Submit([this]() {
			glGenTextures(1, &m_RendererID);
			glBindTexture(GL_TEXTURE_2D, m_RendererID);

			GLenum internalFormat = SnowToGLTextureInternalFormat(m_Specification.Format);
			GLenum dataFormat = SnowToGLTextureDataFormat(m_Specification.Format);
			GLenum filter = SnowToGLTextureFilter(m_Specification.Filter);
			GLenum wrap = SnowToGLTextureWrap(m_Specification.Wrap);

			//SNOW_CORE_TRACE("{0}", m_RendererID);

			glTexStorage2D(GL_TEXTURE_2D, 1, internalFormat, m_Width, m_Height);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap);

			//glTexImage2D(GL_TEXTURE_2D, 1, internalFormat, m_Width, m_Height, 0, dataFormat, GL_UNSIGNED_BYTE, nullptr);
		});
	}

	OpenGLTexture2D::OpenGLTexture2D(const std::string& path, const Render::API::TextureSpecification& spec, bool srgb) :
		m_Path(path), m_Specification(spec) {
		stbi_set_flip_vertically_on_load(1);
		int width, height, channels;
		stbi_uc* data = stbi_load(path.c_str(), &width, &height, &channels, STBI_rgb_alpha);
		m_Width = width;
		m_Height = height;
		m_Channels = channels;
		
		m_ImageData.Allocate(m_Width * m_Height * m_Channels);
		m_ImageData.Write(data, m_Width * m_Height * m_Channels);

		//m_ImageData = Buffer::Copy(data, width * height * channels);

		

		Render::Renderer::Submit([this, data, srgb]() {
			SNOW_CORE_INFO("Loading texture {0}, srgb={1}", m_Path, srgb);
			if (srgb) {
				glGenTextures(1, &m_RendererID);
				glBindTexture(GL_TEXTURE_2D, m_RendererID);
				int levels = CalculateMipMapCount(m_Width, m_Height);

				glTexStorage2D(GL_TEXTURE_2D, levels, GL_SRGB8, m_Width, m_Height);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, levels > 1 ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

				glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_Width, m_Height, GL_RGB, GL_UNSIGNED_BYTE, m_ImageData.Data);
				glGenerateTextureMipmap(m_RendererID);
			}
			else {
				glGenTextures(1, &m_RendererID);
				glBindTexture(GL_TEXTURE_2D, m_RendererID);

				glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, m_Width, m_Height);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_Specification.Filter == Render::API::TextureFilter::Linear ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, SnowToGLTextureFilter(m_Specification.Filter));
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, SnowToGLTextureWrap(m_Specification.Wrap));
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, SnowToGLTextureWrap(m_Specification.Wrap));

				glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_Width, m_Height, GL_RGBA, GL_UNSIGNED_BYTE, data);
				glGenerateMipmap(GL_TEXTURE_2D);

				glBindTexture(GL_TEXTURE_2D, 0);
			}
			stbi_image_free(data);
			
		});
	}

	OpenGLTexture2D::~OpenGLTexture2D() {
		Render::Renderer::Submit([this]() {			
			glDeleteTextures(1, &m_RendererID);
		});
	}

	void OpenGLTexture2D::Bind(uint32_t slot) {
		m_Slot = slot;

		Render::Renderer::Submit([this, slot]() {			
			glActiveTexture(GL_TEXTURE0 + slot);
			glBindTexture(GL_TEXTURE_2D, m_RendererID);
		});
	}

	void OpenGLTexture2D::SetData(void* data, uint32_t size) {

		m_ImageData = Core::Buffer::Copy(data, size);
		
		Render::Renderer::Submit([this]() {			
			SNOW_CORE_ASSERT(m_ImageData.Size == m_Width * m_Height * GetBPP(m_Specification.Format), "Data must be entire texture.");
			glBindTexture(GL_TEXTURE_2D, m_RendererID);
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_Width, m_Height, SnowToGLTextureDataFormat(m_Specification.Format), GL_UNSIGNED_BYTE, m_ImageData.Data);
		});
	}

	void OpenGLTexture2D::Lock() {
		m_Locked = true;
	}

	void OpenGLTexture2D::Unlock() {
		m_Locked = false;

		Render::Renderer::Submit([this]() {			
			glBindTexture(GL_TEXTURE_2D, m_RendererID);
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_Width, m_Height, SnowToGLTextureDataFormat(m_Specification.Format), GL_UNSIGNED_BYTE, m_ImageData.Data);
		});
	}

	void OpenGLTexture2D::Resize(uint32_t width, uint32_t height) {
		SNOW_CORE_ASSERT(m_Locked, "Texture must be locked!");

		m_ImageData.Allocate(width * height * m_Channels);
#if SNOW_DEBUG
		m_ImageData.ZeroInitialize();
#endif
	}

	Core::Buffer OpenGLTexture2D::GetWriteableBuffer() {
		SNOW_CORE_ASSERT(m_Locked, "Texture must be locked!");
		return m_ImageData;
	}

	/////////////////////
	/////TEXTURE CUBE////
	/////////////////////

	OpenGLTextureCube::OpenGLTextureCube(const std::string& path, const Render::API::TextureSpecification& spec) :
		m_Path(path) {
		int width, height, channels;
		stbi_set_flip_vertically_on_load(false);
		m_ImageData = stbi_load(path.c_str(), &width, &height, &channels, STBI_rgb);

		m_Width = width;
		m_Height = height;
		m_Specification.Format = Render::API::TextureFormat::R8G8B8;

		unsigned int faceWidth = m_Width / 4;
		unsigned int faceHeight = m_Height / 3;
		SNOW_CORE_ASSERT(faceWidth == faceHeight, "Non square faces!");

		std::array<unsigned char*, 6> faces;
		for (size_t i = 0; i < faces.size(); i++)
			faces[i] = new unsigned char[(faceWidth * faceHeight * 3)];

		int faceIndex = 0;

		for(size_t i=0; i<4; i++){
			for (size_t y = 0; y < faceHeight; y++) {
				size_t yOffset = y + faceHeight;
				for (size_t x = 0; x < faceWidth; x++) {
					size_t xOffset = x + i * faceWidth;
					faces[faceIndex][(x + y * faceWidth) * 3 + 0] = m_ImageData[(xOffset + yOffset * m_Width) * 3 + 0];
					faces[faceIndex][(x + y * faceWidth) * 3 + 1] = m_ImageData[(xOffset + yOffset * m_Width) * 3 + 1];
					faces[faceIndex][(x + y * faceWidth) * 3 + 2] = m_ImageData[(xOffset + yOffset * m_Width) * 3 + 2];
				}
			}
			faceIndex++;
		}

		for (size_t i = 0; i < 3; i++) {
			if (i == 1)
				continue;

			for (size_t y = 0; y < faceHeight; y++) {
				size_t yOffset = y + i * faceHeight;
				for (size_t x = 0; x < faceWidth; x++) {
					size_t xOffset = x + faceWidth;
					faces[faceIndex][((x + y * faceWidth) * 3 + 0)] = m_ImageData[(xOffset + yOffset * m_Width) * 3 + 0];
					faces[faceIndex][((x + y * faceWidth) * 3 + 1)] = m_ImageData[(xOffset + yOffset * m_Width) * 3 + 1];
					faces[faceIndex][((x + y * faceWidth) * 3 + 2)] = m_ImageData[(xOffset + yOffset * m_Width) * 3 + 2];
				}
			}
			faceIndex++;
		}
		SNOW_CORE_INFO("Loading texture {0}", path);
		Render::Renderer::Submit([=]() {			
			glGenTextures(1, &m_RendererID);
			SNOW_CORE_TRACE("Texture {0}", m_RendererID);
			glBindTexture(GL_TEXTURE_CUBE_MAP, m_RendererID);
			SNOW_CORE_TRACE("Create RenderID: {0}", m_RendererID);

			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTextureParameterf(m_RendererID, GL_TEXTURE_MAX_ANISOTROPY, Render::Renderer::GetCapabilities().MaxAnisotropy);

			auto internalformat = SnowToGLTextureDataFormat(m_Specification.Format);
			auto dataformat = SnowToGLTextureDataFormat(m_Specification.Format);
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, internalformat, faceWidth, faceHeight, 0, dataformat, GL_UNSIGNED_BYTE, faces[2]);
			glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, internalformat, faceWidth, faceHeight, 0, dataformat, GL_UNSIGNED_BYTE, faces[0]);

			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, internalformat, faceWidth, faceHeight, 0, dataformat, GL_UNSIGNED_BYTE, faces[4]);
			glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, internalformat, faceWidth, faceHeight, 0, dataformat, GL_UNSIGNED_BYTE, faces[5]);

			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, internalformat, faceWidth, faceHeight, 0, dataformat, GL_UNSIGNED_BYTE, faces[1]);
			glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, internalformat, faceWidth, faceHeight, 0, dataformat, GL_UNSIGNED_BYTE, faces[3]);

			glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

			glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

			for (size_t i = 0; i < faces.size(); i++)
				delete[] faces[i];

			stbi_image_free(m_ImageData);


		});
	}

	OpenGLTextureCube::~OpenGLTextureCube() {
		Render::Renderer::Submit([this]() {
			glDeleteTextures(1, &m_RendererID);
		});
	}

	void OpenGLTextureCube::Bind(uint32_t slot) {
		Render::Renderer::Submit([this, slot]() {
			glActiveTexture(GL_TEXTURE0 + slot);
			glBindTexture(GL_TEXTURE_CUBE_MAP, m_RendererID);
		});
	}

	void OpenGLTextureCube::Unbind() const {
		Render::Renderer::Submit([this]() {
			glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
		});
	}

	void OpenGLTextureCube::SetData(void* data, uint32_t size) {

	}
}