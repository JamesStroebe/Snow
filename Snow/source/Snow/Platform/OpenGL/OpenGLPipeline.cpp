#include "spch.h"
#include "Snow/Platform/OpenGL/OpenGLPipeline.h"

#include "Snow/Platform/OpenGL/OpenGLShader.h"
#include "Snow/Platform/OpenGL/OpenGLRenderCommandBuffer.h"

#include "Snow/Render/Renderer.h"

namespace Snow {

	GLenum OpenGLPipeline::ShaderDataTypeToOpenGLBaseType(Render::API::VertexAttribType type) {
		switch (type) {
		case Render::API::VertexAttribType::Float:    return GL_FLOAT;
		case Render::API::VertexAttribType::Float2:   return GL_FLOAT;
		case Render::API::VertexAttribType::Float3:   return GL_FLOAT;
		case Render::API::VertexAttribType::Float4:   return GL_FLOAT;
		case Render::API::VertexAttribType::Mat3:     return GL_FLOAT;
		case Render::API::VertexAttribType::Mat4:     return GL_FLOAT;
		case Render::API::VertexAttribType::Int:      return GL_INT;
		case Render::API::VertexAttribType::Int2:     return GL_INT;
		case Render::API::VertexAttribType::Int3:     return GL_INT;
		case Render::API::VertexAttribType::Int4:     return GL_INT;
		case Render::API::VertexAttribType::Bool:     return GL_BOOL;
		}

		SNOW_CORE_ASSERT(false, "Unknown ShaderDataType!");
		return 0;
	}

	OpenGLPipeline::OpenGLPipeline(const Render::API::PipelineSpecification& spec) {
		m_Specification = spec;

		m_PrimitiveType = Render::API::PrimitiveType::Triangle;

		Render::Renderer::Submit([this]() {			
			LinkAndCompileShaders();
			ResolveUniforms();
		});
	}

	OpenGLPipeline::~OpenGLPipeline() {

	}

	void OpenGLPipeline::Bind() const {
		Render::Renderer::Submit([this]() {			
			glUseProgram(m_ShaderProgramRendererID);

			uint32_t layoutIndex = 0;
			//SNOW_CORE_TRACE(m_Specification.VertexBufferLayout.GetStride());
			for (const auto& element : m_Specification.VertexBufferLayout) {
				if (element.Type != Render::API::VertexAttribType::Mat3 && element.Type != Render::API::VertexAttribType::Mat4) {
					glEnableVertexAttribArray(layoutIndex);
					glVertexAttribPointer(layoutIndex,
						element.GetComponentCount(),
						ShaderDataTypeToOpenGLBaseType(element.Type),
						element.Normalized ? GL_TRUE : GL_FALSE,
						m_Specification.VertexBufferLayout.GetStride(),
						(const void*)(intptr_t)element.Offset);
					layoutIndex++;

					
				}
				else {
					if (element.Type == Render::API::VertexAttribType::Mat3) {
						for (uint32_t i = 0; i < 3; i++) {
							glEnableVertexAttribArray(layoutIndex + i);
							glVertexAttribPointer(layoutIndex + i,
								3, ShaderDataTypeToOpenGLBaseType(element.Type),
								element.Normalized ? GL_TRUE : GL_FALSE,
								m_Specification.VertexBufferLayout.GetStride(),
								(const void*)(intptr_t)(element.Offset + sizeof(Math::Vector3f) * i));

							glVertexAttribDivisor(layoutIndex + i, 1);
							layoutIndex++;
						}
					}

					else if (element.Type == Render::API::VertexAttribType::Mat4) {
						for (uint32_t i = 0; i < 4; i++) {
							glEnableVertexAttribArray(layoutIndex + i);
							glVertexAttribPointer(layoutIndex + i,
								4, ShaderDataTypeToOpenGLBaseType(element.Type),
								element.Normalized ? GL_TRUE : GL_FALSE,
								m_Specification.VertexBufferLayout.GetStride(),
								(const void*)(intptr_t)(element.Offset + sizeof(Math::Vector4f) * i));

							glVertexAttribDivisor(layoutIndex + i, 1);
							layoutIndex++;
						}
					}
				}
			}
		});
	}

	void OpenGLPipeline::LinkAndCompileShaders() {
		m_ShaderProgramRendererID = glCreateProgram();

		Math::int32_t glShaderIDIndex = 0;
		for (uint32_t i = 0; i < m_Specification.Shaders.size(); i++) {
			OpenGLShader* glShader = static_cast<OpenGLShader*>(m_Specification.Shaders[i].get());
			glAttachShader(m_ShaderProgramRendererID, glShader->GetShaderID());
		}
		glLinkProgram(m_ShaderProgramRendererID);
		GLint isLinked = 0;
		glGetProgramiv(m_ShaderProgramRendererID, GL_LINK_STATUS, (int*)&isLinked);
		if (isLinked == GL_FALSE) {
			GLint maxLength = 0;
			glGetProgramiv(m_ShaderProgramRendererID, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<GLchar> infoLog(maxLength);
			glGetProgramInfoLog(m_ShaderProgramRendererID, maxLength, &maxLength, &infoLog[0]);
			SNOW_CORE_ERROR("Program linking failed:\n{0}", &infoLog[0]);

			glDeleteProgram(m_ShaderProgramRendererID);

			for (uint32_t i = 0; i < m_Specification.Shaders.size(); i++) {
				OpenGLShader* glShader = static_cast<OpenGLShader*>(m_Specification.Shaders[i].get());
				glDeleteShader(glShader->GetShaderID());
				glDetachShader(m_ShaderProgramRendererID, glShader->GetShaderID());
			}
		}
	}

	// Block Index is the slot that that glsl block is occupying, 
	// there is also a binding point that can be set to anything that links the UBO with the glsl block
	Math::uint32_t OpenGLPipeline::GetUniformBlockLocation(const std::string& name) const {
		Math::uint32_t result = glGetUniformBlockIndex(m_ShaderProgramRendererID, name.c_str());
		if (result == GL_INVALID_INDEX)
			SNOW_CORE_WARN("Could not find uniform block '{0}' in current program", name);

		return result;
	}

	Math::int32_t OpenGLPipeline::GetFieldLocation(const std::string& name) const {
		Math::int32_t result = glGetUniformLocation(m_ShaderProgramRendererID, name.c_str());
		if (result == -1)
			SNOW_CORE_WARN("Could not find uniform '{0}' in current program", name);

		return result;
	}

	void OpenGLPipeline::ResolveUniforms() {
		glUseProgram(m_ShaderProgramRendererID);

		for (uint32_t i = 0; i < m_Specification.Shaders.size(); i++) {
			OpenGLShader* glShader = static_cast<OpenGLShader*>(m_Specification.Shaders[i].get());

			auto& renderBuffers = glShader->GetRendererBuffers();
			Math::uint32_t bindingIndex = 0;
			for (uint32_t buffer = 0; buffer < renderBuffers.size(); buffer++) {
				Math::uint32_t blockIndex = GetUniformBlockLocation(renderBuffers[buffer]->GetName());
				glUniformBlockBinding(m_ShaderProgramRendererID, blockIndex, bindingIndex);
				glShader->SetRenderBufferSlot(buffer, bindingIndex);
				bindingIndex++;
			}

			if (glShader->GetMaterialBuffer().GetFieldList().size()) {
				Math::uint32_t blockIndex = GetUniformBlockLocation(glShader->GetMaterialBuffer().GetName());
				glUniformBlockBinding(m_ShaderProgramRendererID, blockIndex, bindingIndex);
				glShader->SetMaterialBufferSlot(bindingIndex);
			}

			for (uint32_t f = 0; i < glShader->GetGlobalUniforms().size(); i++) {
				auto& field = glShader->GetGlobalUniforms()[i];
				if (field->GetType() == Render::Shader::FieldType::Struct) {
					const auto& fields = field->GetStruct().GetFields();
					for (uint32_t j = 0; j < fields.size(); j++) {
						const auto& f = fields[j];
						f->SetLocation(GetFieldLocation(field->GetName() + "." + f->GetName()));
					}
				}
				else {
					field->SetLocation(GetFieldLocation(field->GetName()));
				}
			}

			Math::uint32_t slot = 0;
			for (Math::uint32_t i = 0; i < glShader->GetResources().size(); i++) {
				const auto& r = glShader->GetResources()[i];
				Math::int32_t location = GetFieldLocation(r->GetName());
				if (r->GetCount() == 1) {
					r->SetSlot(slot);
					if (location != -1)
						UploadUniformInt(location, slot);
					slot++;
				}
				else if (r->GetCount() > 1) {
					r->SetSlot(slot);
					Math::uint32_t count = r->GetCount();
					int* samplers = new int[count];
					for (Math::uint32_t s = 0; s < count; s++)
						samplers[s] = s + slot;
					UploadUniformIntArray(r->GetName(), samplers, count);
					delete[] samplers;
					slot += count;
				}
			}
		}
	}

	Math::uint32_t OpenGLPipeline::FindSlot(const Core::Ref<Render::API::UniformBuffer>& uniformBuffer) {
		
		for (uint32_t i = 0; i < m_Specification.Shaders.size(); i++) {
			if (uniformBuffer->GetDomain() == m_Specification.Shaders[i]->GetType()) {
				auto& glShader = m_Specification.Shaders[i];
				for (uint32_t j = 0; j < glShader->GetRendererBuffers().size(); j++) {
					if (uniformBuffer->GetName() == glShader->GetRendererBuffers()[j]->GetName()) {
						return glShader->GetRendererBuffers()[j]->GetSlot();
					}
				}

				if (uniformBuffer->GetName() == glShader->GetMaterialBuffer().GetName()) {
					return glShader->GetMaterialBuffer().GetSlot();
				}
			}
		}
		SNOW_CORE_TRACE("Could not find uniform buffer object {0} in program", uniformBuffer->GetName());
		
		
	}

	void OpenGLPipeline::UploadBuffer(const Core::Ref<Render::API::UniformBuffer>& uniformBuffer) {
		
		uniformBuffer->Bind(FindSlot(uniformBuffer));
		
	}

	void OpenGLPipeline::UploadUniformInt(Math::uint32_t location, Math::int32_t value) {
		glUniform1i(location, value);
	}

	void OpenGLPipeline::UploadUniformIntArray(Math::uint32_t location, Math::int32_t* values, Math::uint32_t count) {
		glUniform1iv(location, count, values);
	}

	void OpenGLPipeline::UploadUniformFloat(Math::uint32_t location, Math::float32_t value) {
		glUniform1f(location, value);
	}

	void OpenGLPipeline::UploadUniformFloat2(Math::uint32_t location, const Math::Vector2f& value) {
		glUniform2f(location, value.x, value.y);
	}

	void OpenGLPipeline::UploadUniformFloat3(Math::uint32_t location, const Math::Vector3f& value) {
		glUniform3f(location, value.x, value.y, value.z);
	}

	void OpenGLPipeline::UploadUniformFloat4(Math::uint32_t location, const Math::Vector4f& value) {
		glUniform4f(location, value.x, value.y, value.z, value.w);
	}

	void OpenGLPipeline::UploadUniformMat3x3f(Math::uint32_t location, const Math::Matrix3x3f& value) {
		glUniformMatrix3fv(location, 1, GL_FALSE, Math::valuePtr(value));
	}

	void OpenGLPipeline::UploadUniformMat4x4f(Math::uint32_t location, const Math::Matrix4x4f& value) {
		glUniformMatrix4fv(location, 1, GL_FALSE, Math::valuePtr(value));
	}

	void OpenGLPipeline::UploadUniformMat4x4fArray(Math::uint32_t location, const Math::Matrix4x4f* values, Math::uint32_t count) {
		glUniformMatrix4fv(location, count, GL_FALSE, Math::valuePtr(*values));
	}

	void OpenGLPipeline::UploadUniformInt(const std::string& name, int32_t value) {
		int32_t location = GetFieldLocation(name);
		glUniform1i(location, value);
	}

	void OpenGLPipeline::UploadUniformIntArray(const std::string& name, int32_t* values, Math::uint32_t count) {
		int32_t location = GetFieldLocation(name);
		glUniform1iv(location, count, values);
	}

	void OpenGLPipeline::UploadUniformFloat(const std::string& name, Math::float32_t value) {
		int32_t location = GetFieldLocation(name);
		glUniform1f(location, value);
	}

	void OpenGLPipeline::UploadUniformFloat2(const std::string& name, const Math::Vector2f& value) {
		int32_t location = GetFieldLocation(name);
		glUniform2f(location, value.x, value.y);
	}

	void OpenGLPipeline::UploadUniformFloat3(const std::string& name, const Math::Vector3f& value) {
		int32_t location = GetFieldLocation(name);
		glUniform3f(location, value.x, value.y, value.z);
	}

	void OpenGLPipeline::UploadUniformFloat4(const std::string& name, const Math::Vector4f& value) {
		int32_t location = GetFieldLocation(name);
		glUniform4f(location, value.x, value.y, value.z, value.w);
	}

	void OpenGLPipeline::UploadUniformMat3x3f(const std::string& name, const Math::Matrix3x3f& value) {
		int32_t location = GetFieldLocation(name);
		glUniformMatrix3fv(location, 1, GL_FALSE, Math::valuePtr(value));
	}

	void OpenGLPipeline::UploadUniformMat4x4f(const std::string& name, const Math::Matrix4x4f& value) {
		int32_t location = GetFieldLocation(name);
		glUniformMatrix4fv(location, 1, GL_FALSE, Math::valuePtr(value));
	}

	void OpenGLPipeline::UploadUniformMat4x4fArray(const std::string& name, const Math::Matrix4x4f* values, uint32_t count) {
		int32_t location = GetFieldLocation(name);
		glUniformMatrix4fv(location, count, GL_FALSE, Math::valuePtr(*values));
	}
}