#include "spch.h"

#include "Snow/Platform/OpenGL/OpenGLDevice.h"

#include "Snow/Platform/OpenGL/OpenGLContext.h"

namespace Snow {
	OpenGLDevice::OpenGLDevice(const Render::API::DeviceSpecification& spec) {
		m_Specification = spec;
		OpenGLContext* glContext = static_cast<OpenGLContext*>(m_Specification.Context);
		WindowsWindow* window = static_cast<WindowsWindow*>(glContext->GetSpecification().Window.get());
		m_Device = window->GetDeviceContextHandle();
        m_Size = { window->GetWidth(), window->GetHeight()};
    }

	OpenGLDevice::~OpenGLDevice() {
		
	}
}