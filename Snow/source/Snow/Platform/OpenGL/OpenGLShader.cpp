#include "spch.h"
#include "Snow/Platform/OpenGL/OpenGLShader.h"

#include "Snow/Platform/OpenGL/OpenGLRenderCommandBuffer.h"
#include "Snow/Platform/OpenGL/OpenGLRenderer.h"

#include "Snow/Utils/StringUtils.h"


namespace Snow {
	
	static GLenum GetShaderType(Render::Shader::ShaderType type) {
		switch (type) {
		case Render::Shader::ShaderType::None:			return GL_NONE;
		case Render::Shader::ShaderType::VertexShader:	return GL_VERTEX_SHADER;
		case Render::Shader::ShaderType::GeometryShader:return GL_GEOMETRY_SHADER;
		case Render::Shader::ShaderType::ComputeShader:	return GL_COMPUTE_SHADER;
		case Render::Shader::ShaderType::PixelShader:	return GL_FRAGMENT_SHADER;
		}
		SNOW_ASSERT("Unknown ShaderType");
		return GL_NONE;
	}

	OpenGLShader::OpenGLShader(const std::string& path, Render::Shader::ShaderType shaderType) :
		m_Type(shaderType) {
		m_Path = path;
		m_Name = FindFileName(path);
		m_Source = ReadFileToString(path);
		Load(m_Source);
	}

	OpenGLShader::~OpenGLShader() {

	}

	void OpenGLShader::Reload() {

	}

	void OpenGLShader::Load(const std::string& source) {
		Parse();
		CreateShaderModule();
	}

	void OpenGLShader::CreateShaderModule() {

		Render::Renderer::Submit([this]() {
			m_RendererID = glCreateShader(GetShaderType(m_Type));
			const GLchar* sourcec = (const GLchar*)m_Source.c_str();
			glShaderSource(m_RendererID, 1, &sourcec, 0);

			glCompileShader(m_RendererID);

			GLint isCompiled = 0;
			glGetShaderiv(m_RendererID, GL_COMPILE_STATUS, &isCompiled);
			if (isCompiled == GL_FALSE) {
				GLint maxLength = 0;
				glGetShaderiv(m_RendererID, GL_INFO_LOG_LENGTH, &maxLength);

				std::vector<GLchar> infoLog(maxLength);
				glGetShaderInfoLog(m_RendererID, maxLength, &maxLength, &infoLog[0]);
				SNOW_CORE_ERROR("Shader compilation failed:\n{0}", &infoLog[0]);
				glDeleteShader(m_RendererID);
				SNOW_CORE_ASSERT(false, "Shader compilation failure!");
			}
		});
	}

	

	std::string OpenGLShader::PreProcess(const std::string& source) {
		
		return source;
	}

	void OpenGLShader::Parse() {
		const char* token;
		std::string str;

		m_Resources.clear();
		m_Structs.clear();

		str = m_Source;

		while (token = FindToken(str, "uniform buffer")) {
			const char* s = str.c_str();
			const std::string& block = GetBlock(token, &s);
			ParseUniformBlock(block);
			RemoveToken(m_Source, "buffer");
			RemoveToken(str, block);
		}

		while (token = FindToken(str, "struct")) {
			const char* s = str.c_str();
			const std::string& block = GetBlock(token, &s);
			ParseStruct(block);
			RemoveToken(str, block);
		}
			
		while (token = FindToken(str, "uniform")) {
			const char* s = str.c_str();
			const std::string& statement = GetStatement(token, &s);
			ParseGlobalField(statement);
			RemoveToken(str, statement);
		}
	}

	static bool IsTypeStringResource(const std::string& type) {
		if (type == "sampler2D")			return true;
		if (type == "samplerCube")			return true;
		if (type == "sampler2DShadow")		return true;
		return false;
	}

	static Render::Shader::ResourceType StringToResource(const std::string& type) {
		if (type == "sampler2D")	return Render::Shader::ResourceType::Texture2D;
		if (type == "sampler3D")	return Render::Shader::ResourceType::TextureCube;
	}

	static Render::Shader::FieldType StringToField(const std::string& type) {
		if (type == "float")	return Render::Shader::FieldType::Float;
		if (type == "vec2")		return Render::Shader::FieldType::Float2;
		if (type == "vec3")		return Render::Shader::FieldType::Float3;
		if (type == "vec4")		return Render::Shader::FieldType::Float4;
		if (type == "mat3")		return Render::Shader::FieldType::Mat3;
		if (type == "mat4")		return Render::Shader::FieldType::Mat4;
		if (type == "int")		return Render::Shader::FieldType::Int;
		if (type == "uint")		return Render::Shader::FieldType::UInt;
		return Render::Shader::FieldType::None;
	}

	Render::Shader::ShaderStruct* OpenGLShader::FindStruct(const std::string& name) {
		for (Render::Shader::ShaderStruct* s : m_Structs) {
			if (s->GetName() == name)
				return s;
		}
		return nullptr;
	}

	void OpenGLShader::ParseUniformBlock(const std::string& block) {
		std::vector<std::string> tokens = Tokenize(block);

		uint32_t index = 2;

		std::string bufferType = tokens[index++];
		std::string bufferName = tokens[index++];
		index++; // omit the { from the block declaration

		Render::Shader::ShaderBuffer* buffer = new Render::Shader::ShaderBuffer(bufferName, m_BindingSlot);

		if (bufferType != "material") 
			m_RendererBuffers.push_back(buffer);
		else 
			m_MaterialBuffer = *buffer;
		
		RemoveToken(m_Source, bufferType);

		m_BindingSlot++;

		
		while (index < tokens.size()) {
			if (tokens[index] == "}")
				break;

			std::string type = tokens[index++];
			std::string fieldName = tokens[index++];

			if (const char* s = strstr(fieldName.c_str(), ";"))
				fieldName = std::string(fieldName.c_str(), s - fieldName.c_str());

			Math::uint32_t count = 1;
			std::string countnum(fieldName);
			if (const char* s = strstr(fieldName.c_str(), "[")) {
				fieldName = std::string(fieldName, s - fieldName.c_str());

				const char* end = strstr(countnum.c_str(), "]");
				std::string c(s + 1, end - s);
				count = atoi(c.c_str());
			}
			
			

			if (IsTypeStringResource(type)) {
				Render::Shader::ShaderResource* resource = new Render::Shader::ShaderResource(StringToResource(type), fieldName, count);
				m_Resources.push_back(resource);
			}
			else {
				Render::Shader::FieldType t = StringToField(type);
				Render::Shader::ShaderField* field = nullptr;
				if (t == Render::Shader::FieldType::None) {
					Render::Shader::ShaderStruct* s = FindStruct(fieldName);
					field = new Render::Shader::ShaderField(s, fieldName, count);
				}
				else {
					field = new Render::Shader::ShaderField(t, fieldName, count);
				}

				if (bufferType != "material")
					m_RendererBuffers.front()->PushField(field);
				else 
					m_MaterialBuffer.PushField(field);
			}
		}
	}

	void OpenGLShader::ParseGlobalField(const std::string& statement) {
		std::vector<std::string> tokens = Tokenize(statement);
		uint32_t index = 0;

		index++; // omit the "uniform"
		std::string type = tokens[index++];
		std::string name = tokens[index++];

		if (const char* s = strstr(name.c_str(), ";"))
			name = std::string(name.c_str(), s - name.c_str());

		std::string n(name);
		uint32_t count = 1;
		const char* namestr = n.c_str();
		if (const char* s = strstr(namestr, "[")) {
			name = std::string(namestr, s - namestr);

			const char* end = strstr(namestr, "]");
			std::string c(s + 1, end - s);
			count = atoi(c.c_str());
		}

		if (IsTypeStringResource(type)) {
			Render::Shader::ShaderResource* resource = new Render::Shader::ShaderResource(StringToResource(type), name, count);
			m_Resources.push_back(resource);
		}
		else {
			Render::Shader::FieldType t = StringToField(type);
			Render::Shader::ShaderField* field = nullptr;

			if (t == Render::Shader::FieldType::None) {
				Render::Shader::ShaderStruct* s = FindStruct(name);
				field = new Render::Shader::ShaderField(s, name, count);
			}
			else {
				field = new Render::Shader::ShaderField(t, name, count);
			}
			m_GlobalUniforms.push_back(field);
			
		}
	}

	void OpenGLShader::ParseStruct(const std::string& block) {
		std::vector<std::string> tokens = Tokenize(block);

		uint32_t index = 0;
		index++; //omit the "struct"
		std::string name = tokens[index++];
		Render::Shader::ShaderStruct* uniformStruct = new Render::Shader::ShaderStruct(name);
		index++;
		while (index < tokens.size()) {
			if (tokens[index] == "}")
				break;

			std::string type = tokens[index++];
			std::string name = tokens[index++];

			if (const char* s = strstr(name.c_str(), ";"))
				name = std::string(name.c_str(), s - name.c_str());

			uint32_t count = 1;
			const char* namestr = name.c_str();
			if (const char* s = strstr(namestr, "[")) {
				name = std::string(namestr, s - namestr);

				const char* end = strstr(namestr, "]");
				std::string c(s + 1, end - s);
				count = atoi(c.c_str());
			}

			Render::Shader::ShaderField* field = new Render::Shader::ShaderField(StringToField(type), name, count);
			uniformStruct->AddField(field);
		}
		m_Structs.push_back(uniformStruct);
	}
}