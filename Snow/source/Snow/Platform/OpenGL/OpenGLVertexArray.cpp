#include "spch.h"

#include "Snow/Platform/OpenGL/OpenGLVertexArray.h"

#include "Snow/Render/Renderer.h"

namespace Snow {
	OpenGLVertexArray::OpenGLVertexArray() {
		Render::Renderer::Submit([this]() {
			glGenVertexArrays(1, &m_RendererID);
			glBindVertexArray(m_RendererID);
		});
	}

	OpenGLVertexArray::~OpenGLVertexArray() {
		Render::Renderer::Submit([this]() {
			glDeleteVertexArrays(1, &m_RendererID);
		});
	}

	void OpenGLVertexArray::Bind() const {
		Render::Renderer::Submit([this]() {
			glBindVertexArray(m_RendererID);
		});
	}

	void OpenGLVertexArray::Unbind() const {
		Render::Renderer::Submit([this]() {
			glBindVertexArray(0);
		});
	}
}