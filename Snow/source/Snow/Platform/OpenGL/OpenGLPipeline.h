#pragma once

#include "Snow/Render/API/Pipeline.h"

#include "Snow/Platform/OpenGL/OpenGLCommon.h"
#include "Snow/Math/Common/Pointer.h"

namespace Snow {
	class OpenGLPipeline : public Render::API::Pipeline {
	public:
		OpenGLPipeline(const Render::API::PipelineSpecification& spec);
		~OpenGLPipeline();

		virtual void Bind() const override;

        virtual inline Render::API::PrimitiveType GetPrimitiveType() const { return m_PrimitiveType; }
        virtual inline void SetPrimitiveType(Render::API::PrimitiveType type) override { m_PrimitiveType = type; }

        virtual void UploadBuffer(const Core::Ref<Render::API::UniformBuffer>& buffer) override;

		virtual inline const Render::API::PipelineSpecification& GetSpecification() const override { return m_Specification; }

		static GLenum ShaderDataTypeToOpenGLBaseType(Render::API::VertexAttribType type);
	private:

		void LinkAndCompileShaders();

		Math::uint32_t GetUniformBlockLocation(const std::string& name) const;
		Math::int32_t GetFieldLocation(const std::string& name) const;

		void ResolveUniforms();

		Math::uint32_t FindSlot(const Core::Ref<Render::API::UniformBuffer>& uniformBuffer);

		void UploadUniformInt(Math::uint32_t location, Math::int32_t value);
		void UploadUniformIntArray(Math::uint32_t location, Math::int32_t* values, Math::uint32_t count);
		void UploadUniformFloat(Math::uint32_t location, Math::float32_t value);
		void UploadUniformFloat2(Math::uint32_t location, const Math::Vector2f& value);
		void UploadUniformFloat3(Math::uint32_t location, const Math::Vector3f& value);
		void UploadUniformFloat4(Math::uint32_t location, const Math::Vector4f& value);
		void UploadUniformMat3x3f(Math::uint32_t location, const Math::Matrix3x3f& value);
		void UploadUniformMat4x4f(Math::uint32_t location, const Math::Matrix4x4f& value);
		void UploadUniformMat4x4fArray(Math::uint32_t location, const Math::Matrix4x4f* values, Math::uint32_t count);

		void UploadUniformInt(const std::string& name, Math::int32_t value);
		void UploadUniformIntArray(const std::string& name, Math::int32_t* values, Math::uint32_t count);
		void UploadUniformFloat(const std::string& name, Math::float32_t value);
		void UploadUniformFloat2(const std::string& name, const Math::Vector2f& value);
		void UploadUniformFloat3(const std::string& name, const Math::Vector3f& value);
		void UploadUniformFloat4(const std::string& name, const Math::Vector4f& value);
		void UploadUniformMat3x3f(const std::string& name, const Math::Matrix3x3f& value);
		void UploadUniformMat4x4f(const std::string& name, const Math::Matrix4x4f& value);
		void UploadUniformMat4x4fArray(const std::string& name, const Math::Matrix4x4f* values, Math::uint32_t count);


		Render::API::PipelineSpecification m_Specification;

		Core::RendererID m_ShaderProgramRendererID;

		bool m_Bound = false;
        Render::API::PrimitiveType m_PrimitiveType;

        //std::vector<Math::uint32_t> m_Uniform
	};
}
