#include "spch.h"
#include "OpenGLRenderer.h"
#include "OpenGLCommon.h"
#include "Snow/Platform/OpenGL/OpenGLContext.h"
#include "Snow/Platform/OpenGL/OpenGLDevice.h"


namespace Snow {

	

	static void GLLogMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
		if (severity == GL_DEBUG_SEVERITY_HIGH) {
			SNOW_CORE_ERROR("\n\tSource: {0} | Type: {1}\n\tID: {2} | Severity: {3} | Length: {4}\n\tMessage: {5}", source, type, id, severity, length, message);
			
			//__debugbreak();
		}
		else if (severity == GL_DEBUG_SEVERITY_MEDIUM){
			SNOW_CORE_WARN("\n\tSource: {0} | Type: {1}\n\tID: {2} | Severity: {3} | Length: {4}\n\tMessage: {5}", source, type, id, severity, length, message);

		}
		else if (severity == GL_DEBUG_SEVERITY_LOW) {
			SNOW_CORE_INFO("\n\tSource: {0} | Type: {1}\n\tID: {2} | Severity: {3} | Length: {4}\n\tMessage: {5}", source, type, id, severity, length, message);

		}
		else {
			//SNOW_CORE_TRACE("{0}", message);
		}
	}

	OpenGLRenderer::OpenGLRenderer() {
		
	}

	void OpenGLRenderer::InitInternal() {
		GatherGPUData();

		glDebugMessageCallback(GLLogMessage, nullptr);


		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

		//glEnable(GL_CULL_FACE);
		glFrontFace(GL_CCW);

		

		//glCullFace(GL_BACK);

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


		//SNOW_CORE_INFO("	Sampler Size: {0}", caps.MaxSamples);

		GLenum error = glGetError();
		while (error != GL_NO_ERROR) {
			SNOW_CORE_ERROR("OpenGL Error {0}", error);
			error = glGetError();
		}

		m_VAO = Render::API::VertexArray::Create();
		m_VAO->Bind();
	}

	void OpenGLRenderer::SetDepthTestingInternal(bool enabled) {
		Render::Renderer::Submit([this, enabled]() {
			if (enabled)
				glEnable(GL_DEPTH_TEST);
			else
				glDisable(GL_DEPTH_TEST);
		});
	}

	void OpenGLRenderer::SetBlendInternal(bool enabled) {
		Render::Renderer::Submit([this, enabled]() {
			if (enabled)
				glEnable(GL_BLEND);
			else
				glDisable(GL_BLEND);
		});
	}

	void OpenGLRenderer::SetViewportInternal(int x, int y, unsigned int width, unsigned int height) {
		Render::Renderer::Submit([=]() {
			glViewport(x, y, width, height);
		});

        OpenGLContext* glContext = static_cast<OpenGLContext*>(GetContext());
        OpenGLDevice* glDevice = glContext->GetDevice();
        glDevice->SetSize({ width, height });
    }

	void OpenGLRenderer::GatherGPUData() {
		auto& caps = Render::Renderer::GetCapabilities();
		caps.Vendor = (char*)glGetString(GL_VENDOR);
		caps.Renderer = (char*)glGetString(GL_RENDERER);
		caps.Version = (char*)glGetString(GL_VERSION);
		caps.ShadingLanguageVersion = (char*)glGetString(GL_SHADING_LANGUAGE_VERSION);
		glGetIntegerv(GL_NUM_EXTENSIONS, &caps.ExtensionCount);

		for (uint32_t i = 0; i < caps.ExtensionCount; i++) {
			caps.Extensions.push_back((char*)glGetStringi(GL_EXTENSIONS, i));
		}

		glGetIntegerv(GL_MAJOR_VERSION, &caps.VersionMajor);
		glGetIntegerv(GL_MINOR_VERSION, &caps.VersionMinor);

		glGetIntegerv(GL_MAX_SAMPLES, &caps.MaxSamplerCount);

		
		if (caps.VersionMajor >= 4 && caps.VersionMinor >= 3) {
			glGetIntegerv(GL_MAX_FRAMEBUFFER_WIDTH, &caps.MaxFramebufferWidth);
			glGetIntegerv(GL_MAX_FRAMEBUFFER_HEIGHT, &caps.MaxFramebufferHeight);
		}

		
		if (caps.VersionMajor >= 4 && caps.VersionMinor >= 1) {
			glGetIntegerv(GL_MAX_VIEWPORTS, &caps.MaxViewports);

			Math::Vector2i ViewportDim;
			
			glGetIntegerv(GL_MAX_VIEWPORT_DIMS, Math::valuePtr(ViewportDim));
			caps.MaxViewportWidth = ViewportDim.x;
			caps.MaxViewportHeight = ViewportDim.y;
		}
		
		glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &caps.MaxVertexAttribCount);
		glGetIntegerv(GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET, &caps.MaxVertexAttribOffset);

		
		glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &caps.MaxUniformBlockSize);
		glGetIntegerv(GL_MAX_COMBINED_UNIFORM_BLOCKS, &caps.MaxUniformBlockCount);
		glGetIntegerv(GL_MAX_VERTEX_UNIFORM_BLOCKS, &caps.MaxVertexStageUniformBlockCount);
		glGetIntegerv(GL_MAX_COMPUTE_UNIFORM_BLOCKS, &caps.MaxComputeStageUniformBlockCount);
		glGetIntegerv(GL_MAX_GEOMETRY_UNIFORM_BLOCKS, &caps.MaxGeometryStageUniformBlockCount);
		glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_BLOCKS, &caps.MaxPixelStageUniformBlockCount);
		
		glGetIntegerv(GL_MAX_UNIFORM_BUFFER_BINDINGS, &caps.MaxUniformbufferBindingPoints);
		glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &caps.UniformbufferOffsetAlignment);

		glGetBooleanv(GL_DOUBLEBUFFER, (Math::uint8_t*)&caps.DoubleBufferSupport);

		glGetIntegerv(GL_MAX_VERTEX_UNIFORM_COMPONENTS, &caps.MaxVertexStageGlobalUniformCount);
		glGetIntegerv(GL_MAX_COMPUTE_UNIFORM_COMPONENTS, &caps.MaxComputeStageGlobalUniformCount);
		glGetIntegerv(GL_MAX_GEOMETRY_UNIFORM_COMPONENTS, &caps.MaxGeometryStageGlobalUniformCount);
		glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS, &caps.MaxPixelStageGlobalUniformCount);
		glGetIntegerv(GL_MAX_UNIFORM_LOCATIONS, &caps.MaxGlobalUniformLocations);

		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY, &caps.MaxAnisotropy);
	}

}