#pragma once

#include "Snow/Render/Renderer.h"
#include "Snow/Render/API/Texture.h"

namespace Snow {
	class OpenGLTexture2D : public Render::API::Texture2D {
	public:
		OpenGLTexture2D(unsigned int width, unsigned int height, const Render::API::TextureSpecification& spec);
		OpenGLTexture2D(const std::string& path, const Render::API::TextureSpecification& spec, bool srgb);
		virtual ~OpenGLTexture2D();

		virtual void Bind(uint32_t slot = 0) override;
		virtual inline uint32_t GetSlot() const override { return m_Slot; }

		virtual void SetData(void* data, uint32_t size) override;

		virtual uint32_t GetWidth() const override { return m_Width; }
		virtual uint32_t GetHeight() const override { return m_Height; }

		virtual void Lock() override;
		virtual void Unlock() override;

		virtual void Resize(uint32_t width, uint32_t height) override;
		virtual Core::Buffer GetWriteableBuffer() override;

		virtual const std::string& GetPath() const override { return m_Path; }

		virtual const Core::RendererID GetRendererID() const { return m_RendererID; }

		virtual const Render::API::TextureSpecification& GetSpecification() const override { return m_Specification; }
	private:
		Core::RendererID m_RendererID = 0;
		Render::API::TextureSpecification m_Specification;
		uint32_t m_Width, m_Height, m_Channels = 0, m_Slot = 0;

		Core::Buffer m_ImageData;

		bool m_Locked = false;

		std::string m_Path;
	};

	class OpenGLTextureCube : public Render::API::TextureCube {
	public:
		OpenGLTextureCube(const std::string& path, const Render::API::TextureSpecification& spec);
		virtual ~OpenGLTextureCube();

		virtual void Bind(uint32_t slot = 0) override;
		virtual void Unbind() const;

		virtual uint32_t GetSlot() const override { return 0; };

		virtual void SetData(void* data, uint32_t size) override;

		virtual uint32_t GetWidth() const { return m_Width; }
		virtual uint32_t GetHeight() const { return m_Height; }

		virtual const std::string& GetPath() const override { return m_Path; }

		virtual const Core::RendererID GetRendererID() const { return m_RendererID; }

		virtual const Render::API::TextureSpecification& GetSpecification() const override { return m_Specification; };
	private:
		Core::RendererID m_RendererID = 0;
		Render::API::TextureSpecification m_Specification;
		uint32_t m_Width, m_Height;

		unsigned char* m_ImageData;

		std::string m_Path;
	};
}