#include "spch.h"
#include "Snow/Platform/OpenGL/OpenGLContext.h"


namespace Snow {
	

	OpenGLContext::OpenGLContext(const Render::API::ContextSpecification& spec) {
		m_Specification = spec;
		Render::API::DeviceSpecification deviceSpec;
		deviceSpec.Context = this;
		m_Device = static_cast<OpenGLDevice*>(Render::API::Device::Create(deviceSpec));

		Render::API::SwapChainSpecification swapChainSpec;
		swapChainSpec.Context = this;
		m_SwapChain = static_cast<OpenGLSwapChain*>(Render::API::SwapChain::Create(swapChainSpec));

		HGLRC tempContext = wglCreateContext(m_Device->GetPhysicalDevice());
		wglMakeCurrent(m_Device->GetPhysicalDevice(), tempContext);

		int status = gladLoadGL();
		SNOW_CORE_TRACE("gladLoadGL: {0}", status);
		
		status = gladLoadWGLLoader((GLADloadproc)wglGetProcAddress, m_Device->GetPhysicalDevice());
		SNOW_CORE_TRACE("GladLoadWGLLoader: {0}", status);
		
		const int attribList[] = {
			WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
			WGL_CONTEXT_MINOR_VERSION_ARB, 6,
			WGL_CONTEXT_FLAGS_ARB, 0,
			0
		};


		m_RenderContext = wglCreateContextAttribsARB(m_Device->GetPhysicalDevice(), 0, attribList);
		
		if (m_RenderContext) {
			wglMakeCurrent(NULL, NULL);
			wglDeleteContext(tempContext);
			if (!wglMakeCurrent(m_Device->GetPhysicalDevice(), m_RenderContext)) {
				SNOW_CORE_ERROR("Failed to set OpenGL context!");
				SNOW_ASSERT(false);
			}
		}
		else {
			SNOW_CORE_ERROR("Failed to create OpenGL context!");
		}
		
		wglSwapIntervalEXT(0);

		SNOW_CORE_INFO("OpenGL Info:");
		SNOW_CORE_INFO("	Vendor: {0}", glGetString(GL_VENDOR));
		SNOW_CORE_INFO("	Renderer: {0}", glGetString(GL_RENDERER));
		SNOW_CORE_INFO("	Version: {0}", glGetString(GL_VERSION));
		
	}

	OpenGLContext::~OpenGLContext() {
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(m_RenderContext);
	}
}