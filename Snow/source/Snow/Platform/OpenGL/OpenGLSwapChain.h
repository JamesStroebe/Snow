#pragma once

#include "Snow/Render/API/Swapchain.h"

#include "Snow/Platform/OpenGL/OpenGLCommon.h"

#include "Snow/Platform/OpenGL/OpenGLFramebuffer.h"

namespace Snow {

	class OpenGLSwapChain : public Render::API::SwapChain {
	public:
		OpenGLSwapChain(const Render::API::SwapChainSpecification& spec);
		~OpenGLSwapChain();

		void Present();

		virtual const Render::API::SwapChainSpecification& GetSpecification() const override { return m_Specification; }

	private:
		Render::API::SwapChainSpecification m_Specification;
	};
}
