#pragma once

#include "Snow/Render/API/Device.h"

namespace Snow {
	class OpenGLDevice : public Render::API::Device {
	public:
		OpenGLDevice(const Render::API::DeviceSpecification& spec);
		~OpenGLDevice();

		HDC GetPhysicalDevice() { return m_Device; }

        inline Math::Vector2ui GetSize() const { return m_Size; }
        inline void SetSize(Math::Vector2ui size) { m_Size = size; }

        virtual const Render::API::DeviceSpecification& GetSpecification() const override { return m_Specification; }

    private:
		Render::API::DeviceSpecification m_Specification;

		HDC m_Device;
        Math::Vector2ui m_Size;
    };
}
