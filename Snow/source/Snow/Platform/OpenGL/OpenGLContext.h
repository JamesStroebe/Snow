#pragma once

#include "Snow/Render/API/Context.h"
#include "Snow/Platform/Windows/WindowsWindow.h"

#include "Snow/Platform/OpenGL/OpenGLCommon.h"
#include "Snow/Platform/OpenGL/OpenGLDevice.h"
#include "Snow/Platform/OpenGL/OpenGLSwapChain.h"

#if SNOW_PLATFORM & SNOW_PLATFORM_WINDOWS32_BIT
	#include <glad/glad_wgl.h>
#endif

namespace Snow {
	class OpenGLContext : public Render::API::Context {
	public:
		OpenGLContext(const Render::API::ContextSpecification& spec);
		~OpenGLContext();

		OpenGLDevice* GetDevice() { return m_Device; }
		OpenGLSwapChain* GetSwapChain() { return m_SwapChain; }

		inline HGLRC GetHGLRC() { return m_RenderContext; }


		virtual const Render::API::ContextSpecification& GetSpecification() const override { return m_Specification; }

	private:
		Render::API::ContextSpecification m_Specification;

		OpenGLDevice* m_Device;
		OpenGLSwapChain* m_SwapChain;
		HGLRC m_RenderContext;
	};
}