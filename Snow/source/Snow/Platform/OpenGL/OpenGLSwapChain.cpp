#include "spch.h"
#include "Snow/Platform/OpenGL/OpenGLSwapchain.h"

#include "Snow/Platform/OpenGL/OpenGLCommon.h"
#include "Snow/Platform/OpenGL/OpenGLContext.h"

#include "Snow/Platform/Windows/WindowsWindow.h"

namespace Snow {
	OpenGLSwapChain::OpenGLSwapChain(const Render::API::SwapChainSpecification& spec) {
		m_Specification = spec;
	}

	OpenGLSwapChain::~OpenGLSwapChain() {

	}

	void OpenGLSwapChain::Present() {
		OpenGLContext* glContext = dynamic_cast<OpenGLContext*>(m_Specification.Context);
		SwapBuffers(glContext->GetDevice()->GetPhysicalDevice());
	}
}