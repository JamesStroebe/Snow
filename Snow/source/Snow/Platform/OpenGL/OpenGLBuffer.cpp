#include "spch.h"
#include "OpenGLBuffer.h"

#include "OpenGLCommon.h"

#include "Snow/Render/Renderer.h"

namespace Snow {

	/////////////////////////
	/////VERTEX BUFFER///////
	/////////////////////////

	GLenum DrawMode(Render::API::BufferSpecification spec) {
		auto usage = spec.GetUsage();
		auto storage = spec.GetStorage();
		if (storage == Render::API::BufferStorage::Static && usage == Render::API::BufferUsage::Draw)
			return GL_STATIC_DRAW;
		else if (storage == Render::API::BufferStorage::Static && usage == Render::API::BufferUsage::View)
			return GL_STATIC_READ;
		else if (storage == Render::API::BufferStorage::Dynamic && usage == Render::API::BufferUsage::Draw)
			return GL_DYNAMIC_DRAW;
		else if (storage == Render::API::BufferStorage::Dynamic && usage == Render::API::BufferUsage::View)
			return GL_DYNAMIC_READ;
		else if (storage == Render::API::BufferStorage::Staging && usage == Render::API::BufferUsage::Draw)
			return GL_STREAM_DRAW;
		else if (storage == Render::API::BufferStorage::Staging && usage == Render::API::BufferUsage::View)
			return GL_STREAM_READ;
		return GL_NONE;
	}

	GLenum MapMode(Render::API::BufferSpecification spec) {
		switch (spec.GetMap()) {
		case Render::API::BufferMap::Write:
			return GL_WRITE_ONLY;
		case Render::API::BufferMap::Read:
			return GL_READ_ONLY;
		}
		return GL_NONE;
	}

	GLenum MapRangeMode(Render::API::BufferSpecification spec) {
		switch (spec.GetMap()) {
		case Render::API::BufferMap::Write:
			return GL_MAP_WRITE_BIT;
		case Render::API::BufferMap::Read:
			return GL_MAP_READ_BIT;
		}
		return GL_NONE;
	}

	OpenGLVertexBuffer::OpenGLVertexBuffer(Render::API::BufferSpecification spec) :
		m_Size(0), m_Specification(spec), m_Mapped(false) {

		Render::Renderer::Submit([this]() {
			glGenBuffers(1, &m_RendererID);
			glBindBuffer(GL_ARRAY_BUFFER, m_RendererID);
		});
	}

	OpenGLVertexBuffer::OpenGLVertexBuffer(uint32_t size, Render::API::BufferSpecification spec) :
		m_Size(size), m_Specification(spec), m_Mapped(false) {

		Render::Renderer::Submit([this]() {			
			glGenBuffers(1, &m_RendererID);
			glBindBuffer(GL_ARRAY_BUFFER, m_RendererID);
			glBufferData(GL_ARRAY_BUFFER, m_Size, nullptr, DrawMode(m_Specification));
		});
	}

	OpenGLVertexBuffer::OpenGLVertexBuffer(void* data, uint32_t size, Render::API::BufferSpecification spec) :
		m_Size(size), m_Specification(spec), m_Mapped(false) {

		m_LocalData = Core::Buffer::Copy(data, size);

		Render::Renderer::Submit([this]() {
			glGenBuffers(1, &m_RendererID);
			glBindBuffer(GL_ARRAY_BUFFER, m_RendererID);
			glBufferData(GL_ARRAY_BUFFER, m_Size, m_LocalData.Data, DrawMode(m_Specification));
			//SNOW_CORE_TRACE(m_Size);
		});
	}

	OpenGLVertexBuffer::~OpenGLVertexBuffer() {

		Render::Renderer::Submit([this]() {
			glDeleteBuffers(1, &m_RendererID);
		});
	}

	void OpenGLVertexBuffer::Bind() const {
		Render::Renderer::Submit([this]() {
			glBindBuffer(GL_ARRAY_BUFFER, m_RendererID);
		});
	}

	void OpenGLVertexBuffer::Unbind() const {
		Render::Renderer::Submit([this]() {
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		});
	}

	void OpenGLVertexBuffer::SetData(void* data, uint32_t size) {
		if (m_Size < size)
			Resize(size);

		
		m_LocalData.Allocate(size);
		m_LocalData.Write(data, size);


		Render::Renderer::Submit([this]() {
			glBindBuffer(GL_ARRAY_BUFFER, m_RendererID);
			glBufferData(GL_ARRAY_BUFFER, m_LocalData.Size, m_LocalData.Data, DrawMode(m_Specification));
		});
	}

	void OpenGLVertexBuffer::SetSubData(void* data, uint32_t size, uint32_t offset) {

		Render::Renderer::Submit([this, offset, size, data]() {
			glBindBuffer(GL_ARRAY_BUFFER, m_RendererID);
			glBufferSubData(GL_ARRAY_BUFFER, offset, size, data);
		});
	}

	void* OpenGLVertexBuffer::Map() {

		void* buffer;
		Render::Renderer::Submit([this, &buffer]() {
			if (!m_Mapped) {
				m_Mapped = true;
				buffer = glMapBuffer(GL_ARRAY_BUFFER, MapMode(m_Specification));
			}
			SNOW_CORE_TRACE("Buffer already mapped");
		});
		return buffer;
	}

	void* OpenGLVertexBuffer::MapRange(uint32_t size, uint32_t offset) {
		if (!m_Mapped) {
			m_Mapped = true;
			return glMapBufferRange(GL_ARRAY_BUFFER, offset, size, MapRangeMode(m_Specification));
		}
		SNOW_CORE_TRACE("Buffer already mapped");
		return nullptr;
		
	}

	void OpenGLVertexBuffer::Unmap() {
		if (m_Mapped) {
			glUnmapBuffer(GL_ARRAY_BUFFER);
			m_Mapped = false;
		}
		
	}

	void OpenGLVertexBuffer::Resize(uint32_t size) {
		m_Size = size;

		Render::Renderer::Submit([this]() {
			glBufferData(GL_ARRAY_BUFFER, m_Size, nullptr, DrawMode(m_Specification));
		});
	}

	/////////////////////////
	/////INDEX BUFFER////////
	/////////////////////////

	

	OpenGLIndexBuffer::OpenGLIndexBuffer(Render::API::BufferSpecification spec) :
		m_Count(0), m_Size(0), m_Specification(spec), m_Mapped(false) {

		Render::Renderer::Submit([this]() {
			glGenBuffers(1, &m_RendererID);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_RendererID);
		});
	}

	OpenGLIndexBuffer::OpenGLIndexBuffer(uint32_t size, Render::API::BufferSpecification spec) :
		m_Count(size / sizeof(float)), m_Size(size), m_Specification(spec), m_Mapped(false) {

		Render::Renderer::Submit([this]() {
			glGenBuffers(1, &m_RendererID);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_RendererID);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_Size, nullptr, DrawMode(m_Specification));
		});
	}

	OpenGLIndexBuffer::OpenGLIndexBuffer(void* data, uint32_t size, Render::API::BufferSpecification spec) :
		m_Count(size / sizeof(float)), m_Size(size), m_Specification(spec), m_Mapped(false) {

		m_LocalData = Core::Buffer::Copy(data, size);

		Render::Renderer::Submit([this]() {			
			glGenBuffers(1, &m_RendererID);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_RendererID);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_Size, m_LocalData.Data, DrawMode(m_Specification));
		});
	}

	OpenGLIndexBuffer::~OpenGLIndexBuffer() {

		Render::Renderer::Submit([this]() {
			glDeleteBuffers(1, &m_RendererID);
		});
	}

	void OpenGLIndexBuffer::Bind() const {
		Render::Renderer::Submit([this]() {
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_RendererID);
		});
	}

	void OpenGLIndexBuffer::Unbind() const {
		Render::Renderer::Submit([this]() {
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		});
	}

	void OpenGLIndexBuffer::SetData(void* data, uint32_t size) {
		if (m_Size < size)
			Resize(size);

		m_LocalData.Allocate(size);
		m_LocalData.Write(data, size);

		Render::Renderer::Submit([this]() {
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_RendererID);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_LocalData.Size, m_LocalData.Data, DrawMode(m_Specification));
		});
	}

	void OpenGLIndexBuffer::SetSubData(void* data, uint32_t size, uint32_t offset) {

		Render::Renderer::Submit([this, offset, size, data]() {
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_RendererID);
			glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, size, data);
		});
	}

	void* OpenGLIndexBuffer::Map() {
		if (!m_Mapped) {
			m_Mapped = true;
			return glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, MapMode(m_Specification));
		}
		SNOW_CORE_TRACE("Buffer already mapped");
		return nullptr;
	}

	void* OpenGLIndexBuffer::MapRange(uint32_t size, uint32_t offset) {
		if (!m_Mapped) {
			m_Mapped = true;
			return glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, offset, size, MapRangeMode(m_Specification));
		}
		SNOW_CORE_TRACE("Buffer already mapped");
		return nullptr;
	}

	void OpenGLIndexBuffer::Unmap() {
		if (m_Mapped) {
			Render::Renderer::Submit([this]() {
				glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
			});
		}
		m_Mapped = false;
	}

	void OpenGLIndexBuffer::Resize(uint32_t size) {
		m_Size = size;
		m_Count = size / sizeof(float);

		Render::Renderer::Submit([this]() {
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_Size, nullptr, DrawMode(m_Specification));
		});
	}

	/////////////////////////
	/////UNIFORM BUFFER//////
	/////////////////////////

	OpenGLUniformBuffer::OpenGLUniformBuffer(const std::string& name, Render::Shader::ShaderType type) :
		m_Name(name), m_Size(0), m_Mapped(false), m_Domain(type), m_Slot(0) {

		Render::Renderer::Submit([this]() {			
			glGenBuffers(1, &m_RendererID);
			glBindBuffer(GL_UNIFORM_BUFFER, m_RendererID);
		});
	}

	OpenGLUniformBuffer::~OpenGLUniformBuffer() {

		Render::Renderer::Submit([this]() {
			glDeleteBuffers(1, &m_RendererID);
		});
	}

	void OpenGLUniformBuffer::Bind(uint32_t slot) const {
		Render::Renderer::Submit([this, slot] {
			glBindBufferRange(GL_UNIFORM_BUFFER, slot, m_RendererID, 0, m_Size);
			glBindBuffer(GL_UNIFORM_BUFFER, m_RendererID);
		});
	}

	void OpenGLUniformBuffer::Unbind() const {

		Render::Renderer::Submit([this]() {
			glBindBuffer(GL_UNIFORM_BUFFER, 0);
		});
	}

	void OpenGLUniformBuffer::SetData(void* data, uint32_t size) {
		
		m_Size = size;
		if (m_LocalData.Size != size) 
			m_LocalData.Allocate(size);
		m_LocalData.Write((Core::byte*)data, size);


		Render::Renderer::Submit([this, size, data]() {
			glBindBuffer(GL_UNIFORM_BUFFER, m_RendererID);
			glBufferData(GL_UNIFORM_BUFFER, size, data, GL_STATIC_DRAW);
		});
		
	}
}