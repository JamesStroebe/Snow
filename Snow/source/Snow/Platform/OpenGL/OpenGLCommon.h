#pragma once

#include <glad/glad.h>

#include "Setup.h"

#if SNOW_PLATFORM & SNOW_PLATFORM_WINDOWS32_BIT
	#include <Windows.h>
#endif