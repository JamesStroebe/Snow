#include "spch.h"

#include "Snow/Platform/OpenGL/OpenGLRenderCommandBuffer.h"

#include "Snow/Platform/OpenGL/OpenGLBuffer.h"
#include "Snow/Platform/OpenGL/OpenGLPipeline.h"
#include "Snow/Platform/OpenGL/OpenGLShader.h"
#include "Snow/Platform/OpenGL/OpenGLRenderPass.h"
#include "Snow/Platform/OpenGL/OpenGLRenderer.h"

#include "Snow/Platform/OpenGL/OpenGLContext.h"

#include "Snow/Render/Renderer.h"

namespace Snow {
	

	static GLenum SnowToOpenGLBufferType(uint32_t type) {
		uint32_t result = 0;
		
		if(type & (int)Render::API::FramebufferType::RENDER_BUFFER_COLOR)
			result |= GL_COLOR_BUFFER_BIT;
		if(type & (int)Render::API::FramebufferType::RENDER_BUFFER_DEPTH)
			result |= GL_DEPTH_BUFFER_BIT;
		if(type & (int)Render::API::FramebufferType::RENDER_BUFFER_STENCIL)
			result |= GL_STENCIL_BUFFER_BIT;
		
		return result;
	}

	static GLenum PrimitiveTypeToOpenGLBaseType(Render::API::PrimitiveType type) {
		switch(type){
		case Render::API::PrimitiveType::None: return GL_NONE;
		case Render::API::PrimitiveType::Point: return GL_POINTS;
		case Render::API::PrimitiveType::Triangle: return GL_TRIANGLES;
		case Render::API::PrimitiveType::TriangleStrip: return GL_TRIANGLE_STRIP;
        }
	}

	OpenGLRenderCommandBuffer::OpenGLRenderCommandBuffer() {
		m_CommandBuffer = new uint8_t[10 * 1024 * 1024];
		m_CommandBufferPtr = m_CommandBuffer;
		memset(m_CommandBuffer, 0, 10 * 1024 * 1024);
	}

	OpenGLRenderCommandBuffer::~OpenGLRenderCommandBuffer() {
		delete[] m_CommandBuffer;
	}

	void* OpenGLRenderCommandBuffer::Allocate(Render::RenderCommandBuffer::RenderCommandFn fn, uint32_t size) {
		*(RenderCommandFn*)m_CommandBufferPtr = fn;
		m_CommandBufferPtr += sizeof(RenderCommandFn);

		*(uint32_t*)m_CommandBufferPtr = size;
		m_CommandBufferPtr += sizeof(uint32_t);

		void* memory = m_CommandBufferPtr;
		m_CommandBufferPtr += size;

		m_CommandCount++;
		return memory;
	}

	void OpenGLRenderCommandBuffer::Execute() {
		Core::byte* buffer = m_CommandBuffer;

		for (uint32_t i = 0; i < m_CommandCount; i++) {
			RenderCommandFn func = *(RenderCommandFn*)buffer;
			buffer += sizeof(RenderCommandFn);

			uint32_t size = *(uint32_t*)buffer;
			buffer += sizeof(uint32_t);
			func(buffer);

			//SNOW_CORE_TRACE("{0}", size);
			buffer += size;
		}
		m_CommandBufferPtr = m_CommandBuffer;
		m_CommandCount = 0;
	}

	void OpenGLRenderCommandBuffer::BeginRenderPass(const Core::Ref<Render::API::RenderPass>& renderPass) {
		OpenGLContext* glContext = static_cast<OpenGLContext*>(Render::Renderer::GetContext());
		OpenGLSwapChain* glSwapChain = glContext->GetSwapChain();
		OpenGLRenderPass* glRenderPass = static_cast<OpenGLRenderPass*>(renderPass.get());

		Math::Vector4f c = glRenderPass->GetSpecification().TargetFramebuffer->GetSpecification().Color;

		Render::Renderer::Submit([=]() {
			glClearColor(c.r, c.g, c.b, c.a);
			glClear(SnowToOpenGLBufferType((int)glRenderPass->GetSpecification().TargetFramebuffer->GetSpecification().AttachmentTypes));
		});

	}

	void OpenGLRenderCommandBuffer::EndRenderPass() {

	}

	void OpenGLRenderCommandBuffer::SubmitElements(const Core::Ref<Render::API::Pipeline>& pipeline, const Core::Ref<Render::API::VertexBuffer>& vertexBuffer, const Core::Ref<Render::API::IndexBuffer>& indexBuffer, uint32_t indexCount) {
		OpenGLPipeline* glPipeline = static_cast<OpenGLPipeline*>(pipeline.get());
		OpenGLVertexBuffer* glVertexBuffer = static_cast<OpenGLVertexBuffer*>(vertexBuffer.get());
		OpenGLIndexBuffer* glIndexBuffer = static_cast<OpenGLIndexBuffer*>(indexBuffer.get());

		glVertexBuffer->Bind();
		glPipeline->Bind();
		glIndexBuffer->Bind();

		Render::Renderer::Submit([glPipeline, glIndexBuffer, indexCount]() {
			glDrawElements(PrimitiveTypeToOpenGLBaseType(glPipeline->GetPrimitiveType()), indexCount == 0 ? glIndexBuffer->GetCount() : indexCount, GL_UNSIGNED_INT, 0);
		});

		//glVertexBuffer->Unbind();
		//glIndexBuffer->Unbind();
		
	}

	void OpenGLRenderCommandBuffer::SubmitInstanced(const Core::Ref<Render::API::Pipeline>& pipeline, const Core::Ref<Render::API::VertexBuffer>& vertexBuffer, const Core::Ref<Render::API::IndexBuffer>& indexBuffer, uint32_t instanceCount) {
		OpenGLPipeline* glPipeline = static_cast<OpenGLPipeline*>(pipeline.get());
		OpenGLVertexBuffer* glVertexBuffer = static_cast<OpenGLVertexBuffer*>(vertexBuffer.get());
		OpenGLIndexBuffer* glIndexBuffer = static_cast<OpenGLIndexBuffer*>(indexBuffer.get());

		glPipeline->Bind();
		m_Queue.push_back([&](){
			

			glBindBuffer(GL_ARRAY_BUFFER, glVertexBuffer->GetRendererID());

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glIndexBuffer->GetRendererID());
			glDrawElementsInstanced(PrimitiveTypeToOpenGLBaseType(glPipeline->GetPrimitiveType()), indexBuffer->GetCount(), GL_UNSIGNED_INT, 0, instanceCount);
			//glDrawElements(GL_TRIANGLES, indexCount == 0 ? indexBuffer->GetCount() : indexCount, GL_UNSIGNED_INT, 0);
		});
	}

	void OpenGLRenderCommandBuffer::Flush() {
		for (auto& func : m_Queue)
			func();

		m_Queue.clear();
		
	}

	void OpenGLRenderCommandBuffer::Present() {
		OpenGLContext* glContext = static_cast<OpenGLContext*>(Render::Renderer::GetContext());
		m_Result = SwapBuffers(glContext->GetDevice()->GetPhysicalDevice());
		//SNOW_CORE_TRACE("SwapBuffers Result: {0}", result);
	}
}