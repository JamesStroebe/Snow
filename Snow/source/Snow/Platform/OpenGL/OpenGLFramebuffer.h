#pragma once

#include "Snow/Render/API/Framebuffer.h"

#include "Snow/Core/Base.h"

namespace Snow {
	class OpenGLFramebuffer : public Render::API::Framebuffer {
	public:
		OpenGLFramebuffer(const Render::API::FramebufferSpecification& spec);
		~OpenGLFramebuffer();

		virtual void Bind() const override;
        virtual void Unbind() const override;
        virtual void BindToRead() const override;
        virtual void BindColorAttachment(uint32_t slot) const override;

		virtual void SetColor(Math::Vector4f color) override { m_Specification.Color = color; }

        virtual inline Core::RendererID GetRendererID() { return m_RendererID; }

		virtual inline Core::RendererID GetColorAttachment() { return m_ColorAttachment; }
		virtual inline Core::RendererID GetDepthAttachment() { return m_DepthAttachment; }

		virtual inline const Render::API::FramebufferSpecification& GetSpecification() const override { return m_Specification; }
	private:
		void Resize(uint32_t width, uint32_t height);

		Render::API::FramebufferSpecification m_Specification;

		Core::RendererID m_RendererID = 0;

		Core::RendererID m_ColorAttachment = 0;
		Core::RendererID m_DepthAttachment = 0;
	};
}