#include "spch.h"
#include "Snow/Platform/OpenGL/OpenGLFramebuffer.h"

#include "Snow/Platform/OpenGL/OpenGLCommon.h"

#include "Snow/Platform/OpenGL/OpenGLContext.h"

#include "Snow/Render/Renderer.h"

namespace Snow {
	static GLenum SnowToOpenGLFramebufferFormat(Render::API::FramebufferFormat format) {
		switch (format) {
		case Render::API::FramebufferFormat::RGBA16F:	return GL_RGBA16F;
		case Render::API::FramebufferFormat::RGBA8:		return GL_RGBA8;
		}
	}

	static GLenum SnowToOpenGLFramebufferDataType(Render::API::FramebufferFormat format) {
		switch (format) {
		case Render::API::FramebufferFormat::RGBA16F:	return GL_FLOAT;
		case Render::API::FramebufferFormat::RGBA8:		return GL_UNSIGNED_BYTE;
		}
	}

	OpenGLFramebuffer::OpenGLFramebuffer(const Render::API::FramebufferSpecification& spec) :
		m_Specification(spec) {

		Render::Renderer::Submit([this]() {
			glGenFramebuffers(1, &m_RendererID);
			glBindFramebuffer(GL_FRAMEBUFFER, m_RendererID);

			bool multisample = m_Specification.Samples > 1;

			if (multisample) {
				glGenTextures(1, &m_ColorAttachment);
				glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, m_ColorAttachment);

				glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, m_Specification.Samples, SnowToOpenGLFramebufferFormat(m_Specification.Format), m_Specification.Size.x, m_Specification.Size.y, GL_FALSE);
				glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
			}
			else {
				glGenTextures(1, &m_ColorAttachment);
				glBindTexture(GL_TEXTURE_2D, m_ColorAttachment);

				glTexImage2D(GL_TEXTURE_2D, 0, SnowToOpenGLFramebufferFormat(m_Specification.Format), m_Specification.Size.x, m_Specification.Size.y, 0, GL_RGBA, SnowToOpenGLFramebufferDataType(m_Specification.Format), nullptr);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_ColorAttachment, 0);
			}

			if (multisample) {
				glGenTextures(1, &m_DepthAttachment);
				glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, m_DepthAttachment);

				glTexStorage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, m_Specification.Samples, GL_DEPTH24_STENCIL8, m_Specification.Size.x, m_Specification.Size.y, GL_FALSE);
				glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
			}
			else {
				glGenTextures(1, &m_DepthAttachment);
				glBindTexture(GL_TEXTURE_2D, m_DepthAttachment);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, 
					m_Specification.Size.x, m_Specification.Size.y, 0, 
					GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, nullptr);
				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, m_DepthAttachment, 0);
			}

			if (multisample) 
				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, m_ColorAttachment, 0);
			else
				glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_ColorAttachment, 0);

			glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, m_DepthAttachment, 0);

			if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
				SNOW_CORE_ERROR("Framebuffer is incomplete!");

			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		});
	}

	OpenGLFramebuffer::~OpenGLFramebuffer() {}

	void OpenGLFramebuffer::Bind() const {
		Render::Renderer::Submit([this]() {
			glBindFramebuffer(GL_FRAMEBUFFER, m_RendererID);
			glViewport(0, 0, m_Specification.Size.x, m_Specification.Size.y);
		});
    }

	void OpenGLFramebuffer::Unbind() const{
        OpenGLContext* glContext = static_cast<OpenGLContext*>(Render::Renderer::GetContext());
        OpenGLDevice* glDevice = glContext->GetDevice();

		Render::Renderer::Submit([this, glDevice]() {
			glBindFramebuffer(GL_FRAMEBUFFER, 0); //Binds the default framebuffer, ie the screen
			//glViewport(0, 0, glDevice->GetSize().x, glDevice->GetSize().y);
		});
	}

	void OpenGLFramebuffer::BindToRead() const {
		Render::Renderer::Submit([this]() {
			glBindTexture(GL_TEXTURE_2D, 0);
			glBindFramebuffer(GL_READ_FRAMEBUFFER, m_RendererID);
			glReadBuffer(GL_COLOR_ATTACHMENT0);
		});
    }

	void OpenGLFramebuffer::BindColorAttachment(uint32_t slot) const {
		Render::Renderer::Submit([this, slot]() {
			glActiveTexture(GL_TEXTURE0 + slot);
			glBindTexture(GL_TEXTURE_2D, m_ColorAttachment);
		});
    }

	void OpenGLFramebuffer::Resize(uint32_t width, uint32_t height) {
        m_Specification.Size = { width, height };
	}
}