#pragma once

#include "Snow/Render/API/RenderPass.h"

namespace Snow {
	class OpenGLRenderPass : public Render::API::RenderPass {
	public:
		OpenGLRenderPass(const Render::API::RenderPassSpecification& spec);
		~OpenGLRenderPass();

		virtual const Render::API::RenderPassSpecification& GetSpecification() const override { return m_Specification; }

	private:
		Render::API::RenderPassSpecification m_Specification;
	};
}