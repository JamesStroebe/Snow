#pragma once

#include "Snow/Render/Renderer.h"
#include "Snow/Platform/OpenGL/OpenGLCommon.h"
#include "Snow/Render/API/VertexArray.h"
#include "Snow/Platform/OpenGL/OpenGLSwapChain.h"

namespace Snow {
	class OpenGLRenderer : public Render::Renderer {
	public:
		OpenGLRenderer();


	protected:
		virtual void SetDepthTestingInternal(bool enabled) override;
		virtual void SetBlendInternal(bool enabled) override;

		virtual void InitInternal();

		virtual void SetViewportInternal(int x, int y, unsigned int width, unsigned int height) override;
	private:

		void GatherGPUData();

		Core::Ref<Render::API::VertexArray> m_VAO;
	};
}
