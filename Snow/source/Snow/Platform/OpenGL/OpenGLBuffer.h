#pragma once

#include "Snow/Render/API/Buffer.h"

#include "Snow/Core/Buffer.h"

#include "OpenGLCommon.h"

namespace Snow {

	static GLenum DrawMode(Render::API::BufferSpecification spec);
	
	static GLenum MapMode(Render::API::BufferSpecification spec);
	static GLenum MapRangeMode(Render::API::BufferSpecification spec);

	class OpenGLVertexBuffer : public Render::API::VertexBuffer {
	public:
		OpenGLVertexBuffer(Render::API::BufferSpecification spec);
		OpenGLVertexBuffer(uint32_t size, Render::API::BufferSpecification spec);
		OpenGLVertexBuffer(void* data, uint32_t size, Render::API::BufferSpecification spec);
		virtual ~OpenGLVertexBuffer();

		virtual void Bind() const override;
		virtual void Unbind() const override;

		virtual void SetData(void* data, uint32_t size) override;
		virtual void SetSubData(void* data, uint32_t size, uint32_t offset) override;

		virtual void* Map() override;
		virtual void* MapRange(uint32_t size, uint32_t offset) override;
		virtual void Unmap() override;

		virtual void Resize(uint32_t size) override;

		inline const Core::RendererID GetRendererID() const { return m_RendererID; }

		inline const uint32_t GetSize() { return m_LocalData.Size; }
	private:
		Render::API::BufferSpecification m_Specification;

		Core::RendererID m_RendererID = 0;
		uint32_t m_Size;
		bool m_Mapped;
		
		Core::Buffer m_LocalData;
	};

	class OpenGLIndexBuffer : public Render::API::IndexBuffer {
	public:
		OpenGLIndexBuffer(Render::API::BufferSpecification spec);
		OpenGLIndexBuffer(uint32_t size, Render::API::BufferSpecification spec);
		OpenGLIndexBuffer(void* data, uint32_t size, Render::API::BufferSpecification spec);
		~OpenGLIndexBuffer();

		virtual void Bind() const override;
		virtual void Unbind() const override;

		virtual void SetData(void* data, uint32_t size) override;
		virtual void SetSubData(void* data, uint32_t size, uint32_t offset) override;

		virtual void* Map() override;
		virtual void* MapRange(uint32_t size, uint32_t offset) override;
		virtual void Unmap() override;

		virtual void Resize(uint32_t size) override;

		virtual const Core::RendererID GetRendererID() { return m_RendererID; }
		virtual uint32_t GetCount() override { return m_Count; }
	private:

		Render::API::BufferSpecification m_Specification;

		Core::RendererID m_RendererID = 0;
		uint32_t m_Count;
		uint32_t m_Size;
		bool m_Mapped;

		Core::Buffer m_LocalData;
	};

	class OpenGLUniformBuffer : public Render::API::UniformBuffer {
	public:
		OpenGLUniformBuffer(const std::string& name, Render::Shader::ShaderType domain);
		~OpenGLUniformBuffer();

		virtual const Core::RendererID GetRendererID() { return m_RendererID; }

		virtual void SetDomain(Render::Shader::ShaderType domain) override { m_Domain = domain; }
		virtual Render::Shader::ShaderType GetDomain() const override { return m_Domain; }


		//TEMP TEMP TEMP
		virtual void* Map() { return nullptr; }
		virtual void* MapRange() { return nullptr; }
		virtual void Unmap() {}

		virtual void Bind(uint32_t slot) const override;
		virtual void Unbind() const override;

		virtual const std::string& GetName() override { return m_Name; }

		virtual void SetData(void* data, uint32_t size) override;
	private:
		Core::RendererID m_RendererID = 0;
		Core::Buffer m_LocalData;
		std::string m_Name;
		Render::Shader::ShaderType m_Domain;
		bool m_Mapped;

		uint32_t m_Size;
		uint32_t m_Slot;
	};
}
