#pragma once

#include "Snow/Render/RenderCommandBuffer.h"

namespace Snow {
	class OpenGLRenderCommandBuffer : public Render::RenderCommandBuffer {
	public:
		OpenGLRenderCommandBuffer();
		~OpenGLRenderCommandBuffer();

		virtual void* Allocate(Render::RenderCommandBuffer::RenderCommandFn fn, uint32_t size) override;
		virtual void Execute() override;

		virtual void BeginRenderPass(const Core::Ref<Render::API::RenderPass>& renderPass) override;
		virtual void EndRenderPass() override;

		virtual void SubmitElements(const Core::Ref<Render::API::Pipeline>& pipeline, const Core::Ref<Render::API::VertexBuffer>& vertexBuffer, const Core::Ref<Render::API::IndexBuffer>& indexBuffer, uint32_t count) override;
		virtual void SubmitInstanced(const Core::Ref<Render::API::Pipeline>& pipeline, const Core::Ref<Render::API::VertexBuffer>& vertexBuffer, const Core::Ref<Render::API::IndexBuffer>& indexBuffer, uint32_t instanceCount) override;
		virtual void Flush();

		virtual void Present() override;

		inline std::vector<std::function<void()>>* GetQueue() { return &m_Queue; }
	private:
		std::vector<std::function<void()>> m_Queue;

		bool m_Result;
	};
}