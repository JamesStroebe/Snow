#pragma once

#include "Snow/Render/Shaders/Shader.h"
#include "OpenGLCommon.h"

//#include "Snow/Platform/OpenGL/OpenGLShaderUniform.h"

#include "Snow/Utils/StringUtils.h"

namespace Snow {
	class OpenGLShader : public Render::Shader::Shader {
	public:
		OpenGLShader() = default;
		OpenGLShader(const std::string& path, Render::Shader::ShaderType shaderType);
		~OpenGLShader();

		virtual void Reload() override;

		virtual const Render::Shader::ShaderBufferList& GetRendererBuffers() const override { return m_RendererBuffers; }
		virtual const Render::Shader::ShaderBuffer& GetMaterialBuffer() const override { return m_MaterialBuffer; }
		virtual const Render::Shader::ShaderFieldList& GetGlobalUniforms() const override { return m_GlobalUniforms; }
		virtual const Render::Shader::ShaderResourceList& GetResources() const override { return m_Resources; }

		virtual void SetRenderBufferSlot(Math::uint32_t index, Math::uint32_t slot) { m_RendererBuffers[index]->SetSlot(slot); }
		virtual void SetMaterialBufferSlot(Math::uint32_t slot) { m_MaterialBuffer.SetSlot(slot); }

		virtual const std::string& GetName() const override { return m_Name; }
		virtual const Render::Shader::ShaderType GetType() const override { return m_Type; }

		Core::RendererID GetShaderID() const { return m_RendererID; }

	private:
		//better names
		void Load(const std::string& source);

		void CreateShaderModule();

		std::string PreProcess(const std::string& source);
		void Parse();
		void ParseUniformBlock(const std::string& block);
		void ParseGlobalField(const std::string& statement);
		void ParseStruct(const std::string& block);
		Render::Shader::ShaderStruct* FindStruct(const std::string& name);

		std::string m_Path, m_Name;
		std::string m_Source;
		Core::RendererID m_RendererID;
		Render::Shader::ShaderType m_Type;
		Math::uint32_t m_BindingSlot;
		
		Render::Shader::ShaderBufferList m_RendererBuffers; //creation of struct that the data needs to be filled in
		Render::Shader::ShaderBuffer m_MaterialBuffer; // material data
		Render::Shader::ShaderFieldList m_GlobalUniforms;
		Render::Shader::ShaderResourceList m_Resources;
		Render::Shader::ShaderStructList m_Structs;
	};
}
