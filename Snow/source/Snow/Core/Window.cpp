#include "spch.h"
#include "Window.h"

#include "Snow/Platform/Windows/WindowsWindow.h"

namespace Snow {
	namespace Core {
		std::map<void*, Window*> Window::s_Handles;

		void Window::SetEventCallback(const EventCallbackFn& callback) {
			m_EventCallback = callback;
			m_InputManager->SetEventCallback(m_EventCallback);
		}

		Ref<Window> Window::Create(const WindowProps& props) {
#if SNOW_PLATFORM & SNOW_PLATFORM_WINDOWS32_BIT
			return Core::CreateRef<WindowsWindow>(props);
#elif SNOW_PLATFORM & SNOW_PLATFORM_LINUX_BIT
			return nullptr;
#else
			return nullptr;
#endif
		}
	}
}