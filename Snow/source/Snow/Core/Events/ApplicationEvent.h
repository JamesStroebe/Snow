///////////////////////////////////////////
// File: ApplicationEvent.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Events/Event.h"

namespace Snow {
	namespace Core {
		namespace Event {
			class WindowCloseEvent : public Event {
			public:
				WindowCloseEvent() = default;

				EVENT_CLASS_TYPE(WindowClose)
					EVENT_CLASS_CATEGORY(EventCategoryApplication)
			};

			class WindowFocusEvent : public Event {
			public:
				WindowFocusEvent(bool focused) :
					m_Focused(focused) {}

				EVENT_CLASS_TYPE(WindowFocus)
				EVENT_CLASS_CATEGORY(EventCategoryApplication)

			private:
				bool m_Focused;
			};

			class WindowFullscreenEvent : public Event {
			public:
				WindowFullscreenEvent() = default;

				EVENT_CLASS_TYPE(WindowFullscreen);
				EVENT_CLASS_CATEGORY(EventCategoryApplication);
			};

			class WindowMaximizedEvent : public Event {
			public:
				WindowMaximizedEvent() = default;

				EVENT_CLASS_TYPE(WindowMaximized)
				EVENT_CLASS_CATEGORY(EventCategoryApplication)

			};

			class WindowMinimizedEvent : public Event {
			public:
				WindowMinimizedEvent() = default;

				EVENT_CLASS_TYPE(WindowMinimized)
				EVENT_CLASS_CATEGORY(EventCategoryApplication)
			};

			class WindowMovedEvent : public Event {
			public:
				WindowMovedEvent(int x, int y) :
					m_XPos(x), m_YPos(y) {}

				inline int GetX() const { return m_XPos; }
				inline int GetY() const { return m_YPos; }

				EVENT_CLASS_TYPE(WindowMoved)
				EVENT_CLASS_CATEGORY(EventCategoryApplication)

			private:
				int m_XPos, m_YPos;
			};

			class WindowResizeEvent : public Event {
			public:
				WindowResizeEvent(unsigned int width, unsigned int height) :
					m_Width(width), m_Height(height) {}

				inline unsigned int GetWidth() const { return m_Width; }
				inline unsigned int GetHeight() const { return m_Height; }

				std::string ToString() const override {
					std::stringstream ss;
					ss << "WindowResizeEvent: " << m_Width << ", " << m_Height;
					return ss.str();
				}

				EVENT_CLASS_TYPE(WindowResize)
					EVENT_CLASS_CATEGORY(EventCategoryApplication)
			private:
				unsigned int m_Width, m_Height;
			};

			class AppTickEvent : public Event {
			public:
				AppTickEvent() = default;

				EVENT_CLASS_TYPE(AppTick)
					EVENT_CLASS_CATEGORY(EventCategoryApplication)
			};

			class AppUpdateEvent : public Event {
			public:
				AppUpdateEvent() = default;

				EVENT_CLASS_TYPE(AppUpdate)
					EVENT_CLASS_CATEGORY(EventCategoryApplication)
			};

			class AppRenderEvent : public Event {
			public:
				AppRenderEvent() = default;

				EVENT_CLASS_TYPE(AppRender)
					EVENT_CLASS_CATEGORY(EventCategoryApplication)
			};


		}
	}
}
