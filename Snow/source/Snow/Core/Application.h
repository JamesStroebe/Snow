///////////////////////////////////////////
// File: Application.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Core/Base.h"
#include "Snow/Core/Window.h"

#include "Snow/Core/Events/Events.h"
#include "Snow/Core/Time/Timestep.h"
#include "Snow/Core/Time/Timer.h"
#include "Snow/Core/LayerStack.h"

#include "Snow/Render/RenderThread.h"

#include "Snow/ImGui/ImGuiLayer.h"

namespace Snow {
	namespace Core {
		class Application {
		public:
			Application(const WindowProps& props = WindowProps());
			virtual ~Application() = default;

			virtual void OnInit() {};
			virtual void OnUpdate() {};
			virtual void OnEvent(Event::Event& e);
			virtual void OnRender() = 0;
			virtual void OnShutdown() = 0;

			void PushLayer(Layer* layer);
			void PushOverlay(Layer* overlay);

			void RenderImGui();

			void Run();

			double GetTime() const;

			static inline Ref<Window>& GetWindow() { return Get().m_Window; }
			static inline Application& Get() { return *s_Instance; }


		private:
			bool OnWindowMinimized(Event::WindowMinimizedEvent& e);
			bool OnWindowClose(Event::WindowCloseEvent& e);
			bool OnWindowResize(Event::WindowResizeEvent& e);
			bool OnWindowFullscreen(Event::WindowFullscreenEvent& e);

			static Application* s_Instance; // All of these could be static as there should only be one instance of the application
			Ref<Window> m_Window;

			Time::Timestep m_Timestep;
			Time::Timer m_Timer;
			bool m_Minimized = false;
			LayerStack m_LayerStack;
			Imgui::ImGuiLayer* m_ImGuiLayer;
			double m_LastFrameTime = 0.0;

		protected:
			bool m_Running = true;
		};

		Application* CreateApplication();
	}
}