#include "spch.h"
#include "Snow/Core/Input.h"

namespace Snow {
	namespace Core {
		InputManager* Input::s_InputManager = nullptr;

		InputManager::InputManager() {
			ClearKeys();
			ClearMouseButtons();

			m_MouseGrabbed = true;

			Input::s_InputManager = this;
		}

		void InputManager::Update() {
			for (int i = 0; i < MAX_BUTTONS; i++)
				m_MouseClicked[i] = m_MouseButtons[i] && !m_MouseState[i];

			memcpy(m_LastKeyState, m_KeyState, MAX_KEYS);
			memcpy(m_MouseState, m_MouseButtons, MAX_BUTTONS);
		}

		void InputManager::ClearKeys() {
			for (int i = 0; i < MAX_KEYS; i++) {
				m_KeyState[i] = false;
				m_LastKeyState[i] = false;
			}
			m_KeyModifiers = 0;
		}

		void InputManager::ClearMouseButtons() {
			for (int i = 0; i < MAX_BUTTONS; i++) {
				m_MouseButtons[i] = false;
				m_MouseState[i] = false;
				m_MouseClicked[i] = false;
			}
		}

		bool InputManager::IsKeyPressed(Math::uint32_t keycode) const {
			if (keycode >= MAX_KEYS)
				return false;

			return m_KeyState[keycode];
		}

		bool InputManager::IsKeyPressed(Math::uint32_t keycode, Math::uint32_t modifier) const {
			if (keycode >= MAX_KEYS)
				return false;

			return m_KeyState[keycode] && (m_KeyModifiers & modifier);
		}

		bool InputManager::IsMouseButtonPressed(Math::uint32_t button) const {
			if (button >= MAX_BUTTONS)
				return false;

			return m_MouseButtons[button];
		}

		bool InputManager::IsMouseButtonClicked(Math::uint32_t button) const {
			if (button >= MAX_BUTTONS)
				return false;

			return m_MouseClicked[button];
		}

		const Math::Vector2f& InputManager::GetMousePosition() const {
			return m_MousePosition;
		}

		const Math::Vector2f& InputManager::GetMouseScroll() const {
			return m_MouseScroll;
		}

		const bool InputManager::IsMouseGrabbed() const {
			return m_MouseGrabbed;
		}

		void InputManager::SetMouseGrabbed(bool grabbed) {
			m_MouseGrabbed = grabbed;
		}
	}
}