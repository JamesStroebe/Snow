#include "spch.h"
#include "Snow/Core/Log.h"

#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/fmt/bundled/color.h>

namespace Snow {
	namespace Core {
		Ref<spdlog::logger> Log::s_CoreLogger;
		Ref<spdlog::logger> Log::s_ClientLogger;

		void Log::Init() {
			s_CoreLogger = spdlog::stdout_color_mt("CORE");
			s_CoreLogger->set_pattern("(%T) %n - %^%v%$ ");
			s_CoreLogger->set_level(spdlog::level::trace);

			auto coreSink = static_cast<spdlog::sinks::stdout_color_sink_mt*>(s_CoreLogger->sinks()[0].get());
			coreSink->set_color(spdlog::level::trace, coreSink->CYAN);

			coreSink->set_color(spdlog::level::critical, coreSink->RED & ~BACKGROUND_RED);

			s_ClientLogger = spdlog::stdout_color_mt("APP");
			s_CoreLogger->set_pattern("(%T) %n - %^%v%$ ");
			s_ClientLogger->set_level(spdlog::level::trace);
		}
	}
}