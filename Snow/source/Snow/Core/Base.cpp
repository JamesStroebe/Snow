#include "spch.h"
#include "Snow/Core/Base.h"

#include "Snow/Core/Log.h"

#define SNOW_BUILD_ID "v0.1.0 4-5-20"

namespace Snow {
	namespace Core {
		void InitializeCore() {
			Snow::Core::Log::Init();
			SNOW_CORE_INFO("Snow Engine {0}", SNOW_BUILD_ID);
		}

		void ShutdownCore() {
			SNOW_CORE_INFO("Shutting down");
		}
	}
}