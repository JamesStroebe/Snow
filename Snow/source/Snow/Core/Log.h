///////////////////////////////////////////
// File: Log.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Base.h"

#include <spdlog/spdlog.h>
#include <spdlog/fmt/ostr.h>

namespace Snow {
	namespace Core {
		class Log {
		public:
			static void Init();

			inline static Ref<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; }
			inline static Ref<spdlog::logger>& GetClientLogger() { return s_ClientLogger; }
		private:
			static Ref<spdlog::logger> s_CoreLogger;
			static Ref<spdlog::logger> s_ClientLogger;

		};
	}
}

#define SNOW_CORE_TRACE(...)	::Snow::Core::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define SNOW_CORE_INFO(...)		::Snow::Core::Log::GetCoreLogger()->info(__VA_ARGS__)
#define SNOW_CORE_WARN(...)		::Snow::Core::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define SNOW_CORE_ERROR(...)	::Snow::Core::Log::GetCoreLogger()->error(__VA_ARGS__)
#define SNOW_CORE_CRITICAL(...)	::Snow::Core::Log::GetCoreLogger()->critical(__VA_ARGS__)

#define SNOW_TRACE(...)			::Snow::Core::Log::GetClientLogger()->trace(__VA_ARGS__)
#define SNOW_INFO(...)			::Snow::Core::Log::GetClientLogger()->info(__VA_ARGS__)
#define SNOW_WARN(...)			::Snow::Core::Log::GetClientLogger()->warn(__VA_ARGS__)
#define SNOW_ERROR(...)			::Snow::Core::Log::GetClientLogger()->error(__VA_ARGS__)
#define SNOW_CRITICAL(...)		::Snow::Core::Log::GetClientLogger()->critical(__VA_ARGS__)