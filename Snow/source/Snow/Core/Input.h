///////////////////////////////////////////
// File: Input.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Base.h"
#include "Snow/Core/Events/Event.h"

#include "Snow/Math/Math.h"

#define MAX_KEYS 1024
#define MAX_BUTTONS 32

namespace Snow {
	namespace Core {
		using WindowEventCallback = std::function<void(Event::Event&)>;

		class InputManager {
		public:
			friend class Window;

			bool m_KeyState[MAX_KEYS];
			bool m_LastKeyState[MAX_KEYS];

			bool m_MouseButtons[MAX_BUTTONS];
			bool m_MouseState[MAX_BUTTONS];
			bool m_MouseClicked[MAX_BUTTONS];
			bool m_MouseGrabbed = false;
			Math::int32_t m_KeyModifiers;

			
			Math::Vector2f m_MousePosition, m_MouseScroll;
			WindowEventCallback m_EventCallback;

			InputManager();

			inline void SetEventCallback(const WindowEventCallback& eventCallback) { m_EventCallback = eventCallback; }

			void Update();
			void PlatformUpdate();

			bool IsKeyPressed(Math::uint32_t keycode) const;
			bool IsKeyPressed(Math::uint32_t keycode, Math::uint32_t modifier) const;
			bool IsMouseButtonPressed(Math::uint32_t button) const;
			bool IsMouseButtonClicked(Math::uint32_t button) const;

			const Math::Vector2f& GetMousePosition() const;
			const Math::Vector2f& GetMouseScroll() const;
			void SetMousePosition(const Math::Vector2f& position);
			const bool IsMouseGrabbed() const;
			void SetMouseGrabbed(bool grabbed);
			void SetMouseCursor(Math::int32_t cursor);

			void ClearKeys();
			void ClearMouseButtons();
		private:
			friend void MouseButtonCallback(InputManager* inputManager, int button, int x, int y);
			friend void KeyCallback(InputManager* inputManager, int flags, int key, unsigned int message);
			friend void MouseScrollCallback(InputManager* imputManager, int xOffset, int yOffset);
		};

		class Input {
		private:
			friend class InputManager;

			static InputManager* s_InputManager;
		public:
			static void Init();

			inline static bool IsKeyPressed(unsigned int keycode) { return s_InputManager->IsKeyPressed(keycode); }
			inline static bool IsKeyPressed(unsigned int keycode, unsigned int modifier) { return s_InputManager->IsKeyPressed(keycode, modifier); }
			inline static bool IsMouseButtonPressed(unsigned int button) { return s_InputManager->IsMouseButtonPressed(button); }
			inline static bool IsMouseButtonClicked(unsigned int button) { return s_InputManager->IsMouseButtonClicked(button); }

			inline static const Math::Vector2f& GetMousePosition() { return s_InputManager->GetMousePosition(); }
			inline static const Math::Vector2f& GetMouseScroll() { return s_InputManager->GetMouseScroll(); }

			inline static InputManager* GetInputManager() { return s_InputManager; }
		};
	}
}

#define SNOW_MOUSE_LEFT			0x00
#define SNOW_MOUSE_MIDDLE		0x01
#define SNOW_MOUSE_RIGHT		0x02
		
#define SNOW_NO_CURSOR			NULL
#define SNOW_CURSOR_1			0x01
		
#define SNOW_MOD_LEFT_CONTROL	BIT(0)
#define SNOW_MOD_LEFT_ALT		BIT(1)
#define SNOW_MOD_LEFT_SHIFT		BIT(2)
#define SNOW_MOD_RIGHT_CONTROL	BIT(3)
#define SNOW_MOD_RIGHT_ALT		BIT(4)
#define SNOW_MOD_RIGHT_SHIFT	BIT(5)
		
#define SNOW_KEY_TAB			0x09
		
#define SNOW_KEY_0				0x30
#define SNOW_KEY_1				0x31
#define SNOW_KEY_2				0x32
#define SNOW_KEY_3				0x33
#define SNOW_KEY_4				0x34
#define SNOW_KEY_5				0x35
#define SNOW_KEY_6				0x36
#define SNOW_KEY_7				0x37
#define SNOW_KEY_8				0x38
#define SNOW_KEY_9				0x39
		
#define SNOW_KEY_A				0x41
#define SNOW_KEY_B				0x42
#define SNOW_KEY_C				0x43
#define SNOW_KEY_D				0x44
#define SNOW_KEY_E				0x45
#define SNOW_KEY_F				0x46
#define SNOW_KEY_G				0x47
#define SNOW_KEY_H				0x48
#define SNOW_KEY_I				0x49
#define SNOW_KEY_J				0x4A
#define SNOW_KEY_K				0x4B
#define SNOW_KEY_L				0x4C
#define SNOW_KEY_M				0x4D
#define SNOW_KEY_N				0x4E
#define SNOW_KEY_O				0x4F
#define SNOW_KEY_P				0x50
#define SNOW_KEY_Q				0x51
#define SNOW_KEY_R				0x52
#define SNOW_KEY_S				0x53
#define SNOW_KEY_T				0x54
#define SNOW_KEY_U				0x55
#define SNOW_KEY_V				0x56
#define SNOW_KEY_W				0x57
#define SNOW_KEY_X				0x58
#define SNOW_KEY_Y				0x59
#define SNOW_KEY_Z				0x5A
		
#define SNOW_KEY_LBUTTON		0x01
#define SNOW_KEY_RBUTTON		0x02
#define SNOW_KEY_CANCEL			0x03
#define SNOW_KEY_MBUTTON		0x04

#define SNOW_KEY_ESCAPE			0x1B
#define SNOW_KEY_SHIFT			0x10
#define SNOW_KEY_CONTROL		0x11
#define SNOW_KEY_MENU			0x12
#define SNOW_KEY_ALT			SNOW_KEY_MENU
#define SNOW_KEY_PAUSE			0x13
#define SNOW_KEY_CAPITAL		0x14
	
#define SNOW_KEY_SPACE			0x20
#define SNOW_KEY_PRIOR			0x21
#define SNOW_KEY_NEXT			0x22
#define SNOW_KEY_END			0x23
#define SNOW_KEY_HOME			0x24
#define SNOW_KEY_LEFT			0x25
#define SNOW_KEY_UP				0x26
#define SNOW_KEY_RIGHT			0x27
#define SNOW_KEY_DOWN			0x28
#define SNOW_KEY_SELECT			0x29
#define SNOW_KEY_PRINT			0x2A
#define SNOW_KEY_EXECUTE		0x2B
#define SNOW_KEY_SNAPSHOT		0x2C
#define SNOW_KEY_INSERT			0x2D
#define SNOW_KEY_DELETE			0x2E
#define SNOW_KEY_HELP			0x2F

#define SNOW_KEY_NUMPAD0		0x60
#define SNOW_KEY_NUMPAD1		0x61
#define SNOW_KEY_NUMPAD2		0x62
#define SNOW_KEY_NUMPAD3		0x63
#define SNOW_KEY_NUMPAD4		0x64
#define SNOW_KEY_NUMPAD5		0x65
#define SNOW_KEY_NUMPAD6		0x66
#define SNOW_KEY_NUMPAD7		0x67
#define SNOW_KEY_NUMPAD8		0x68
#define SNOW_KEY_NUMPAD9		0x69
#define SNOW_KEY_MULTIPLY		0x6A
#define SNOW_KEY_ADD			0x6B
#define SNOW_KEY_SEPARATOR		0x6C
#define SNOW_KEY_SUBTRACT		0x6D
#define SNOW_KEY_DECIMAL		0x6E
#define SNOW_KEY_DIVIDE			0x6F
#define SNOW_KEY_F1				0x70
#define SNOW_KEY_F2				0x71
#define SNOW_KEY_F3				0x72
#define SNOW_KEY_F4				0x73
#define SNOW_KEY_F5				0x74
#define SNOW_KEY_F6				0x75
#define SNOW_KEY_F7				0x76
#define SNOW_KEY_F8				0x77
#define SNOW_KEY_F9				0x78
#define SNOW_KEY_F10			0x79
#define SNOW_KEY_F11			0x7A
#define SNOW_KEY_F12			0x7B
#define SNOW_KEY_F13			0x7C
#define SNOW_KEY_F14			0x7D
#define SNOW_KEY_F15			0x7E
#define SNOW_KEY_F16			0x7F
#define SNOW_KEY_F17			0x80
#define SNOW_KEY_F18			0x81
#define SNOW_KEY_F19			0x82
#define SNOW_KEY_F20			0x83
#define SNOW_KEY_F21			0x84
#define SNOW_KEY_F22			0x85
#define SNOW_KEY_F23			0x86
#define SNOW_KEY_F24			0x87
		
#define SNOW_KEY_NUMLOCK		0x90
#define SNOW_KEY_SCROLL			0x91
		
#define SNOW_KEY_LSHIFT			0xA0
#define SNOW_KEY_RSHIFT			0xA1
#define SNOW_KEY_LCONTROL		0xA2
#define SNOW_KEY_RCONTROL		0xA3
#define SNOW_KEY_LALT			0xA4
#define SNOW_KEY_RALT			0xA5