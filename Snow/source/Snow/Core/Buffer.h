///////////////////////////////////////////
// File: Buffer.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Base.h"

namespace Snow {
	namespace Core {
		struct Buffer {
			byte* Data;
			uint32_t Size;

			Buffer() :
				Data(nullptr), Size(0) {}

			Buffer(byte* data, uint32_t size) :
				Data(data), Size(size) {}

			Buffer(uint32_t size) :
				Size(size), Data(nullptr) {
				Data = new byte[Size];
			}

			//~Buffer() {
			//	delete Data;
			//}

			static Buffer Copy(void* data, uint32_t size) {
				Buffer buffer;

				buffer.Allocate(size);
				memcpy(buffer.Data, data, size);
				return buffer;
			}



			void Allocate(uint32_t size) {
				if (Data) {
					delete[] Data;
					Data = nullptr;
				}

				if (size == 0)
					return;

				Data = new byte[size];
				Size = size;
			}

			void ZeroInitialize() {
				if (Data)
					memset(Data, 0, Size);
			}

			void Write(void* data, uint32_t size, uint32_t offset = 0) {
				SNOW_CORE_ASSERT((offset + size <= Size), "Buffer Overflow");
				memcpy(Data + offset, data, size);
			}

			operator bool() const {
				return Data;
			}

			byte& operator[](int index) {
				return Data[index];
			}

			template<typename T>
			T* As() {
				return (T*)Data;
			}

			inline uint32_t GetSize() const { return Size; }
		};
	}
}