#pragma once

#include <thread>
#include <string>
#include <functional>

namespace Snow {
	namespace Core {
		enum class ThreadState {
			Wait, Stop, Join
		};

		typedef void(*ThreadFunction)();

		class Thread {
		public:
			static void Start(const std::string& name, ThreadFunction func);
			static void Stop();

			static void Join();
			static void Wait() {};



		private:
			std::string m_Name;
			static std::thread m_Thread;
		};
	}
}