///////////////////////////////////////////
// File: Layer.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Base.h"
#include "Snow/Core/Time/Timestep.h"
#include "Snow/Core/Events/Events.h"


namespace Snow {
	namespace Core {
		class Layer {
		public:
			Layer(const std::string& name = "Layer");
			virtual ~Layer() = default;

			virtual void OnAttach() {}
			virtual void OnDetach() {}
			virtual void OnUpdate(Time::Timestep ts) {}
			virtual void OnRender() {}
			virtual void OnImGuiRender() {}
			virtual void OnEvent(Event::Event& event) {}

			inline const std::string& GetName() const { return m_DebugName; }
		protected:
			std::string m_DebugName;
		};
	}
}