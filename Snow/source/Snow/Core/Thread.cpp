#include "spch.h"
#include "Thread.h"

namespace Snow {
	namespace Core {
		std::thread Thread::m_Thread = {};

		void Thread::Start(const std::string& name, ThreadFunction func) {
			m_Thread = std::thread(func);
		}

		void Thread::Stop() {
			m_Thread.detach();
		}

		void Thread::Join() {
			m_Thread.join();
		}
	}
}