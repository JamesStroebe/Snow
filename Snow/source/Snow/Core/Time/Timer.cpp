#include "spch.h"
#include "Snow/Core/Time/Timer.h"

#include <chrono>

namespace Snow {
	namespace Core {
		namespace Time {
			Timer::Timer() {
				Start();
			}

			void Timer::Start() {
				m_StartTime = m_HighResolutionClock.now();
				
			}

			void Timer::End() {
				m_EndTime = m_HighResolutionClock.now();
			}

			void Timer::Reset() {
				m_StartTime = m_HighResolutionClock.now();
			}
		}
	}
}