///////////////////////////////////////////
// File: Timestep.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include <chrono>

namespace Snow {
	namespace Core {
		namespace Time {
			class Timestep {
			public:
				Timestep() = default;
				Timestep(float initTime) :
					m_TimeDelta(0.0f), m_LastTime(initTime) {}

				inline void Update(float currentTime) { m_TimeDelta = currentTime - m_LastTime; m_LastTime = currentTime; }

				operator float() { return m_TimeDelta; }

				inline float GetSeconds() const { return m_TimeDelta / 1000.0f; }
				inline float GetMillis() const { return m_TimeDelta; }
			private:

				float m_TimeDelta; //Times are in milliseconds
				float m_LastTime;
			};
		}
	}
}