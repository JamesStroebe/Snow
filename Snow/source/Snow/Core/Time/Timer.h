///////////////////////////////////////////
// File: Timer.h
//
// Original Author: James Stroebe
//
// Creation Date: 18/02/2020
//
/////////////////////////////////////////// 

#pragma once

#include <chrono>

namespace Snow {
	namespace Core {
		namespace Time {
			class Timer {
			public:
				Timer();
				void Start();
				void End();

				void Reset();

				float ElapsedSeconds() { return  ElapsedMillis() / 1000.0f; }
				float ElapsedMillis() { return ElapsedMicro() / 1000.0f; }
				double ElapsedMicro() { return (double)(std::chrono::duration_cast<std::chrono::microseconds>(m_EndTime - m_StartTime).count()); }

				void GetCurrentTimeFrame() { m_EndTime = std::chrono::steady_clock::now(); }
				double GetTime() { return std::chrono::steady_clock::now().time_since_epoch().count(); }

				std::chrono::time_point<std::chrono::high_resolution_clock> GetEndTimePoint() { return m_EndTime; }
				
			private:
				std::chrono::high_resolution_clock m_HighResolutionClock;
				
				std::chrono::time_point<std::chrono::high_resolution_clock> m_StartTime;
				std::chrono::time_point<std::chrono::high_resolution_clock> m_EndTime;
				
			};
		}
	}
}