///////////////////////////////////////////
// File: SerializeObject.h
//
// Original Author: James Stroebe
//
// Creation Date: 14/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "SerializeArray.h"
#include "SerializeBase.h"
#include "SerializeField.h"
#include "SerializeString.h"

namespace Snow {
	namespace Core {
		namespace Serialize {
			class SerializeObject : public SerializeBase {
			public:
				SerializeObject(std::string name) {
					SetName(name);
					m_Size += 1 + 2 + 2 + 2 + 2; //container_type, fieldCount, arrayCount, stringCount, objectCount;
				}

				void AddField(SerializeField field) {
					m_Fields.push_back(field);
					m_Size += field.GetSize();

					m_FieldCount = m_Fields.size();
				}

				void AddString(SerializeString string) {
					m_Strings.push_back(string);
					m_Size += string.GetSize();

					m_StringCount = m_Strings.size();
				}

				void AddArray(SerializeArray array) {
					m_Arrays.push_back(array);
					m_Size += array.GetSize();

					m_ArrayCount = m_Arrays.size();
				}

				void AddObject(SerializeObject object) {
					m_Objects.push_back(object);
					m_Size += object.GetSize();

					m_ObjectCount = m_Objects.size();
				}

				int GetSize() {
					return m_Size;
				}

				SerializeField FindField(std::string name) {
					for (SerializeField field : m_Fields) {
						if (field.GetName() == name)
							return field;
					}
				}

				SerializeString FindString(std::string name) {
					for (SerializeString string : m_Strings) {
						if (string.GetName() == name)
							return string;
					}
				}

				SerializeArray FindArray(std::string name) {
					for (SerializeArray array : m_Arrays) {
						if (array.GetName() == name)
							return array;
					}
				}

				SerializeObject FindObject(std::string name) {
					for (SerializeObject object : m_Objects) {
						if (object.GetName() == name)
							return object;
					}
				}

				int GetBytes(Buffer dest, int pointer) {
					pointer = writeBytes(dest, pointer, m_ContainerType);
					pointer = writeBytes(dest, pointer, m_NameLength);
					pointer = writeBytes(dest, pointer, m_Name);
					pointer = writeBytes(dest, pointer, m_Size);

					pointer = writeBytes(dest, pointer, m_FieldCount);
					for (SerializeField field : m_Fields)
						pointer = field.GetBytes(dest, pointer);

					pointer = writeBytes(dest, pointer, m_StringCount);
					for (SerializeString string : m_Strings)
						pointer = string.GetBytes(dest, pointer);

					pointer = writeBytes(dest, pointer, m_ArrayCount);
					for (SerializeArray array : m_Arrays)
						pointer = array.GetBytes(dest, pointer);

					pointer = writeBytes(dest, pointer, m_ObjectCount);
					for (SerializeObject object : m_Objects)
						pointer = object.GetBytes(dest, pointer);

					return pointer;
				}

				static SerializeObject Deserialize(Buffer src, int& pointer) {
					byte containerType = src[pointer++];

					SerializeObject result = SerializeObject();
					result.m_NameLength = readShort(src, pointer);
					pointer += 2;
					result.m_Name = readString(src, pointer, result.m_NameLength);
					pointer += result.m_NameLength;

					result.m_Size = readInt(src, pointer);
					pointer += 4;

					result.m_FieldCount = readShort(src, pointer);
					pointer += 2;

					for (int i = 0; i < result.m_FieldCount; i++) {
						SerializeField field = SerializeField::Deserialize(src, pointer);
						result.m_Fields.push_back(field);
					}

					result.m_StringCount = readShort(src, pointer);
					pointer += 2;

					for (int i = 0; i < result.m_StringCount; i++) {
						SerializeString string = SerializeString::Deserialize(src, pointer);
						result.m_Strings.push_back(string);
					}

					result.m_ArrayCount = readShort(src, pointer);
					pointer += 2;

					for (int i = 0; i < result.m_ArrayCount; i++) {
						SerializeArray array = SerializeArray::Deserialize(src, pointer);
						result.m_Arrays.push_back(array);
					}

					result.m_ObjectCount = readShort(src, pointer);
					pointer += 2;

					for (int i = 0; i < result.m_ObjectCount; i++) {
						SerializeObject object = SerializeObject::Deserialize(src, pointer);
						result.m_Objects.push_back(object);
					}

					return result;
				}

			private:
				SerializeObject() {}

				byte m_ContainerType = CONTAINER_TYPE::OBJECT;
				short m_FieldCount = 0;
				short m_StringCount = 0;
				short m_ArrayCount = 0;
				short m_ObjectCount = 0;

				std::vector<SerializeField> m_Fields;
				std::vector<SerializeString> m_Strings;
				std::vector<SerializeArray> m_Arrays;
				std::vector<SerializeObject> m_Objects;
			};
		}
	}
}