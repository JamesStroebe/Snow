///////////////////////////////////////////
// File: SerializeBase.h
//
// Original Author: James Stroebe
//
// Creation Date: 14/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Base.h"
#include <vector>
#include <string>

namespace Snow {
	namespace Core {
		namespace Serialize {
			class SerializeBase {
			protected:
				short m_NameLength;
				std::string m_Name;

				int m_Size = 2 + 4; //short nameLength, int size(self-inclusive)

			public:
				std::string GetName() {
					return m_Name;
				};

				void SetName(std::string name) {
					//Assert the namelength is not more than 66535 
					if (!m_Name.empty())
						m_Size -= m_Name.size();

					m_NameLength = (short)name.size();
					m_Name = name;
					m_Size += m_NameLength; //size is the current size and the size of name in bytes
				}

				virtual int GetSize() = 0;
			};
		}
	}
}