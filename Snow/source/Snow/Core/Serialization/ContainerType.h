///////////////////////////////////////////
// File: ContainerType.h
//
// Original Author: James Stroebe
//
// Creation Date: 14/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Base.h"
#include <string>

namespace Snow {
	namespace Core {
		namespace Serialize {
			enum CONTAINER_TYPE {
				DATABASE = 6,
				OBJECT = 5,
				ARRAY = 4,
				STRING = 3,
				FIELD = 2,
				DATA = 1,
				UNKNOWN = 0
			};

			enum TYPE {
				NILL = 0,
				BYTE,
				CHAR,
				SHORT,
				USHORT,
				INT,
				UINT,
				LONG,
				ULONG,
				FLOAT,
				UFLOAT,
				DOUBLE,
				UDOUBLE,
				BOOL,
			};

			static int GetTypeSize(TYPE type) {
				switch (type) {
				case TYPE::BYTE:	return 1;
				case TYPE::CHAR:	return 1;
				case TYPE::SHORT:	return 2;
				case TYPE::USHORT:	return 2;
				case TYPE::INT:		return 4;
				case TYPE::UINT:	return 4;
				case TYPE::LONG:	return 8;
				case TYPE::ULONG:	return 8;
				case TYPE::FLOAT:	return 4;
				case TYPE::UFLOAT:	return 4;
				case TYPE::DOUBLE:	return 8;
				case TYPE::UDOUBLE:	return 8;
				case TYPE::BOOL:	return 1;
				}
				return 0;
			}

			static std::string GetType(TYPE type) {
				switch (type) {
				case TYPE::BYTE:	return "Byte";
				case TYPE::CHAR:	return "Char";
				case TYPE::SHORT:	return "Short";
				case TYPE::USHORT:	return "Unsigned Short";
				case TYPE::INT:		return "Int";
				case TYPE::UINT:	return "Unsigned Int";
				case TYPE::LONG:	return "Long";
				case TYPE::ULONG:	return "Unsigned Long";
				case TYPE::FLOAT:	return "Float";
				case TYPE::UFLOAT:	return "Unsigned Float";
				case TYPE::DOUBLE:	return "Double";
				case TYPE::UDOUBLE:	return "Unsigned Double";
				case TYPE::BOOL:	return "Bool";

				}
				return "Unknown";
			}
		}
	}
}