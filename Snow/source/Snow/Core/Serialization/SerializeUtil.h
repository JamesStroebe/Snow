///////////////////////////////////////////
// File: SerializeUtil.h
//
// Original Author: James Stroebe
//
// Creation Date: 14/01/2020
//
/////////////////////////////////////////// 

#pragma once
#pragma warning(disable : 6384 4018 4293 4554 4244)

#include "Snow/Core/Base.h"
#include "Snow/Core/Buffer.h"

#include "ContainerType.h"

#include <vector>

namespace Snow {
	namespace Core {
		namespace Serialize {
			//static int FloatToInt(float f) {
			//	int x;
			//	memcpy(&x, &f, 4);
			//	return x;
			//}
			//
			//static long DoubleToLong(double d) {
			//	long x;
			//	memcpy(&x, &d, 8);
			//	return x;
			//}

			static int writeBytes(Buffer dest, int pointer, byte src) {
				dest.Write(&src, 1, pointer);
				pointer++;
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, byte src[]) {
				int length = sizeof(src) / sizeof(src[0]);
				dest.Write(src, length, pointer);
				pointer += sizeof(src);
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, char value) {
				dest.Write(&value, 1, pointer);
				pointer++;
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, char src[]) {
				int length = sizeof(src) / sizeof(src[0]);
				dest.Write(src, length, pointer);
				pointer += sizeof(src);
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, short value) {
				dest.Write(&value, 2, pointer);
				pointer += 2;
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, short src[]) {
				int length = sizeof(src) / sizeof(src[0]);
				dest.Write(src, length, pointer);
				pointer += sizeof(src);
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, int value) {
				dest.Write(&value, 4, pointer);
				pointer += 4;
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, int src[]) {
				int length = sizeof(src) / sizeof(src[0]);
				dest.Write(src, length, pointer);
				pointer += sizeof(src);
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, long value) {
				dest.Write(&value, 8, pointer);
				pointer += 8;
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, long src[]) {
				int length = sizeof(src) / sizeof(src[0]);
				dest.Write(src, length, pointer);
				pointer += sizeof(src);
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, float value) {
				dest.Write(&value, 4, pointer);
				pointer += 4;
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, float src[]) {
				int length = sizeof(src) / sizeof(src[0]);
				dest.Write(src, length, pointer);
				pointer += sizeof(src);
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, double value) {
				dest.Write(&value, 8, pointer);
				pointer += 8;
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, double src[]) {
				int length = sizeof(src) / sizeof(src[0]);
				dest.Write(src, length, pointer);
				pointer += sizeof(src);
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, bool value) {
				dest.Write(&value, 1, pointer);
				pointer += 1;
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, bool src[]) {
				int length = sizeof(src) / sizeof(src[0]);
				dest.Write(src, length, pointer);
				pointer += sizeof(src);
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, Buffer src) {
				dest.Write(src.Data, src.Size, pointer);
				pointer += src.Size;
				return pointer;
			}

			static int writeBytes(Buffer dest, int pointer, std::string string) {
				dest.Write(string.data(), string.size(), pointer);
				pointer += string.size();
				return pointer;
				
			}

			static byte readByte(Buffer src, int pointer) {
				return src[pointer];
			}

			static void readBytes(Buffer src, int pointer, Buffer& dest) {
				dest.Write(src.Data + pointer, dest.Size, 0);
			}

			static char readChar(Buffer src, int pointer) {
				return (char)src.Data[pointer];
			}

			static void readChars(Buffer src, int pointer, char dest[]) {
				uint32_t length = sizeof(dest) / sizeof(char);
				for (uint32_t i = 0; i < length; i++) {
					dest[i] = readChar(src, pointer);
					pointer += Serialize::GetTypeSize(TYPE::CHAR);
				}
			}

			static short readShort(Buffer src, int pointer) {
				short s = (src.Data[pointer]) << 0 | src.Data[1 + pointer] << 8;
				return s;
			}

			static void readShorts(Buffer src, int pointer, short dest[]) {
				uint32_t length = sizeof(dest) / sizeof(short);
				for (uint32_t i = 0; i < length; i++) {
					dest[i] = readShort(src, pointer);
					pointer += GetTypeSize(TYPE::SHORT);
				}
			}

			static int readInt(Buffer src, int pointer) {
				int i = (src.Data[pointer]) << 0 | (src.Data[1 + pointer]) << 8 | 
						(src.Data[2 + pointer]) << 16 | (src.Data[3 + pointer]) << 24;
				return i;
			}

			static void readInts(Buffer src, int pointer, int dest[]) {
				uint32_t length = sizeof(dest) / sizeof(int);
				for (uint32_t i = 0; i < length; i++) {
					dest[i] = readInt(src, pointer);
					pointer += GetTypeSize(TYPE::INT);
				}
			}

			static long readLong(Buffer src, int pointer) {
				long l = (src.Data[pointer]) << 0 | (src.Data[1 + pointer]) << 8 |
						(src.Data[2 + pointer]) << 16 | (src.Data[3 + pointer]) << 24 |
						(src.Data[4 + pointer]) << 32 | (src.Data[5 + pointer]) << 40 |
						(src.Data[6 + pointer]) << 48 | (src.Data[7 + pointer]) << 56;
				return l;
			}

			static void readLongs(Buffer src, int pointer, long dest[]) {
				uint32_t length = sizeof(dest) / sizeof(long);
				for (uint32_t i = 0; i < length; i++) {
					dest[i] = readLong(src, pointer);
					pointer += GetTypeSize(TYPE::LONG);
				}
			}

			static float readFloat(Buffer src, int pointer) {
				float f = (src.Data[pointer]) << 0 | (src.Data[1 + pointer]) << 8 |
					(src.Data[2 + pointer]) << 16 | (src.Data[3 + pointer]) << 24;
				return f;
			}

			static void readFloats(Buffer src, int pointer, float dest[]) {
				uint32_t length = sizeof(dest) / sizeof(float);
				for (uint32_t i = 0; i < length; i++) {
					dest[i] = readFloat(src, pointer);
					pointer += GetTypeSize(TYPE::FLOAT);
				}
			}

			static double readDouble(Buffer src, int pointer) {
				double d = (src.Data[pointer]) << 0 | (src.Data[1 + pointer]) << 8 |
					(src.Data[2 + pointer]) << 16 | (src.Data[3 + pointer]) << 24 |
					(src.Data[4 + pointer]) << 32 | (src.Data[5 + pointer]) << 40 |
					(src.Data[6 + pointer]) << 48 | (src.Data[7 + pointer]) << 56;
				return d;
			}

			static void readDoubles(Buffer src, int pointer, double dest[]) {
				uint32_t length = sizeof(dest) / sizeof(double);
				for (uint32_t i = 0; i < length; i++) {
					dest[i] = readDouble(src, pointer);
					pointer += GetTypeSize(TYPE::DOUBLE);
				}
			}

			static bool readBool(Buffer src, int pointer) {
				return src.Data[pointer] != 0;
			}

			static void readBools(Buffer src, int pointer, Buffer dest) {
				for (uint32_t i = 0; i < dest.Size; i++) {
					dest[i] = readBool(src, pointer);
					pointer += GetTypeSize(TYPE::BOOL);
				}
			}

			static std::string readString(Buffer src, int pointer, int length) {
				std::string s;
				s.resize(length);

				for (uint32_t i = 0; i < length; i++) {
					s[i] = readChar(src, pointer + i);
				}
				return s;
			}

		}
	}
}

