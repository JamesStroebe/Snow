///////////////////////////////////////////
// File: SerializeString.h
//
// Original Author: James Stroebe
//
// Creation Date: 14/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "SerializeBase.h"
#include "SerializeUtil.h"
#include "ContainerType.h"


namespace Snow {
	namespace Core {
		namespace Serialize {
			class SerializeString : public SerializeBase {
			public:
				std::string GetString() {
					return m_String;
				}


				int GetDataSize() {
					return m_String.length();
				}

				int GetBytes(Buffer dest, int pointer) {
					pointer = writeBytes(dest, pointer, m_ContainerType);
					pointer = writeBytes(dest, pointer, m_NameLength);
					pointer = writeBytes(dest, pointer, m_Name);
					pointer = writeBytes(dest, pointer, m_Size);
					pointer = writeBytes(dest, pointer, m_Count);
					pointer = writeBytes(dest, pointer, m_String);
					return pointer;
				}

				int GetSize() {
					return m_Size;
				}

				static SerializeString Create(std::string name, std::string data) {
					SerializeString string = SerializeString();
					string.SetName(name);
					string.m_Count = data.length();
					string.m_String = data;
					string.UpdateSize();
					return string;
				}

				static SerializeString Deserialize(Buffer src, int& pointer) {
					byte containerType = src[pointer++];

					SerializeString result = SerializeString();
					result.m_NameLength = readShort(src, pointer);
					pointer += 2;

					result.m_Name = readString(src, pointer, result.m_NameLength);
					pointer += result.m_NameLength;

					result.m_Size = readInt(src, pointer);
					pointer += 4;

					result.m_Count = readInt(src, pointer);
					pointer += 4;

					result.m_String.resize(result.m_Count);
					readChars(src, pointer, result.m_String.data());

					pointer += result.m_Count * GetTypeSize(TYPE::CHAR);
					return result;
				}

			private:
				SerializeString() {
					m_Size += 1 + 4; // container_type, length-of-string
				}

				void UpdateSize() {
					m_Size += GetDataSize();
				}

				byte m_ContainerType = CONTAINER_TYPE::STRING;
				int m_Count;
				std::string m_String;
			};
		}
	}
}
