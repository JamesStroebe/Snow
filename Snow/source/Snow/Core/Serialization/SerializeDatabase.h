///////////////////////////////////////////
// File: SerializeDatabase.h
//
// Original Author: James Stroebe
//
// Creation Date: 14/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "SerializeObject.h"
#include "Snow/Core/Log.h"
#include "Snow/Utils/StringUtils.h"
#include <fstream>

namespace Snow {
	namespace Core {
		namespace Serialize {
			class SerializeDatabase : public SerializeBase {
			public:
				SerializeDatabase() = default;
				SerializeDatabase(std::string name) {
					//m_Name = "SNOW";
					SetName(name);

					m_Size += 4 + 2 + 1 + 2 + 2; //HEADER, Version, container_type, nameLength;
				}

				void AddObject(SerializeObject object) {
					m_Objects.push_back(object);
					m_Size += object.GetSize();

					m_ObjectCount = m_Objects.size();
				}

				int GetSize() {
					return m_Size;
				}

				int GetBytes(Buffer dest, int pointer) {
					pointer = writeBytes(dest, pointer, std::string(m_Header));
					pointer = writeBytes(dest, pointer, m_Version);
					pointer = writeBytes(dest, pointer, m_ContainerType);
					pointer = writeBytes(dest, pointer, m_NameLength);
					pointer = writeBytes(dest, pointer, m_Name);
					pointer = writeBytes(dest, pointer, m_Size);

					pointer = writeBytes(dest, pointer, m_ObjectCount);
					for (SerializeObject object : m_Objects)
						pointer = object.GetBytes(dest, pointer);

					return pointer;
				}

				static SerializeDatabase Deserialize(Buffer src) {
					int pointer = 0;
					pointer += 4;

					if (readShort(src, pointer) != m_Version) {
						SNOW_CORE_TRACE("INVALID SAVE FILE");
					}
					pointer += 2;

					byte containerType = readByte(src, pointer++);
					assert(containerType == CONTAINER_TYPE::DATABASE);

					SerializeDatabase result = SerializeDatabase();
					result.m_NameLength = readShort(src, pointer);
					pointer += 2;
					result.m_Name = readString(src, pointer, result.m_NameLength);
					pointer += result.m_NameLength;

					result.m_Size = readInt(src, pointer);
					pointer += 4;

					result.m_ObjectCount = readInt(src, pointer);
					pointer += 4;

					for (int i = 0; i < result.m_ObjectCount; i++) {
						SerializeObject object = SerializeObject::Deserialize(src, pointer);
						result.m_Objects.push_back(object);
					}

					return result;
				}

				SerializeObject FindObject(std::string name) {
					for (SerializeObject object : m_Objects) {
						if (object.GetName() == name)
							return object;
					}
				}

				static SerializeDatabase DeserializeFromFile(const std::string& path) {
					Buffer dest;
					dest = ReadFileToBuffer(path);
					return Deserialize(dest);
				}

				void SerializeToFile(const std::string& path) {
					Buffer data;
					data.Allocate(GetSize());
					GetBytes(data, 0);

					std::ofstream file;
					file.open(path);
					file.write((char*)data.Data, data.Size);
					file.close();
				}

			private:
				

				byte m_ContainerType = CONTAINER_TYPE::DATABASE;

				std::vector<SerializeObject> m_Objects;
				int m_ObjectCount;

				const char* m_Header = "SNOW";
				static const short m_Version = 0x0100;
			};
		}
	}
}
