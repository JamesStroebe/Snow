///////////////////////////////////////////
// File: SerializeField.h
//
// Original Author: James Stroebe
//
// Creation Date: 14/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Buffer.h"
#include "SerializeBase.h"
#include "ContainerType.h"
#include "SerializeUtil.h"

namespace Snow {
	namespace Core {
		namespace Serialize {
			class SerializeField : public SerializeBase {
			public:
				byte GetByte() {
					return m_Data[0];
				}

				short GetShort() {
					return readShort(m_Data, 0);
				}

				char GetChar() {
					return readChar(m_Data, 0);
				}

				int GetInt() {
					return readInt(m_Data, 0);
				}

				long GetLong() {
					return readLong(m_Data, 0);
				}

				float GetFloat() {
					return readFloat(m_Data, 0);
				}

				double GetDouble() {
					return readDouble(m_Data, 0);
				}

				short GetBool() {
					return readBool(m_Data, 0);
				}

				int GetBytes(Buffer dest, int pointer) {
					pointer = writeBytes(dest, pointer, m_ContainerType);
					pointer = writeBytes(dest, pointer, m_NameLength);
					pointer = writeBytes(dest, pointer, m_Name);
					pointer = writeBytes(dest, pointer, m_Type);
					pointer = writeBytes(dest, pointer, m_Data);
					return pointer;
				}

				int GetSize() override {
					return 1 + 2 + m_Name.size() + 1 + m_Data.Size; //container_type, namelength, name, datatype, data
				}

				static SerializeField Byte(std::string name, byte value) {
					SerializeField field = SerializeField();
					field.SetName(name);
					field.m_Type = TYPE::BYTE;
					field.m_Data = Buffer();
					field.m_Data.Allocate(GetTypeSize(TYPE::BYTE));
					writeBytes(field.m_Data, 0, value);
					return field;
				}

				static SerializeField Short(std::string name, short value) {
					SerializeField field = SerializeField();
					field.SetName(name);
					field.m_Type = TYPE::SHORT;
					field.m_Data = Buffer();
					field.m_Data.Allocate(GetTypeSize(TYPE::SHORT));
					writeBytes(field.m_Data, 0, value);
					return field;
				}

				static SerializeField Char(std::string name, char value) {
					SerializeField field = SerializeField();
					field.SetName(name);
					field.m_Type = TYPE::CHAR;
					field.m_Data = Buffer();
					field.m_Data.Allocate(GetTypeSize(TYPE::CHAR));
					writeBytes(field.m_Data, 0, value);
					return field;
				}

				static SerializeField Int(std::string name, int value) {
					SerializeField field = SerializeField();
					field.SetName(name);
					field.m_Type = TYPE::INT;
					field.m_Data = Buffer();
					field.m_Data.Allocate(GetTypeSize(TYPE::INT));
					writeBytes(field.m_Data, 0, value);

					SNOW_CORE_TRACE(field.m_Size); //2, 4, 

					return field;
				}

				static SerializeField Long(std::string name, long value) {
					SerializeField field = SerializeField();
					field.SetName(name);
					field.m_Type = TYPE::LONG;
					field.m_Data = Buffer();
					field.m_Data.Allocate(GetTypeSize(TYPE::LONG));
					writeBytes(field.m_Data, 0, value);
					return field;
				}

				static SerializeField Float(std::string name, float value) {
					SerializeField field = SerializeField();
					field.SetName(name);
					field.m_Type = TYPE::FLOAT;
					field.m_Data = Buffer();
					field.m_Data.Allocate(GetTypeSize(TYPE::FLOAT));
					writeBytes(field.m_Data, 0, value);
					return field;
				}

				static SerializeField Double(std::string name, double value) {
					SerializeField field = SerializeField();
					field.SetName(name);
					field.m_Type = TYPE::DOUBLE;
					field.m_Data = Buffer();
					field.m_Data.Allocate(GetTypeSize(TYPE::DOUBLE));
					writeBytes(field.m_Data, 0, value);
					return field;
				}

				static SerializeField Bool(std::string name, bool value) {
					SerializeField field = SerializeField();
					field.SetName(name);
					field.m_Type = TYPE::BOOL;
					field.m_Data = Buffer();
					field.m_Data.Allocate(GetTypeSize(TYPE::BOOL));
					writeBytes(field.m_Data, 0, value);
					return field;
				}

				static SerializeField Deserialize(Buffer src, int& pointer) {
					byte containerType = src[pointer++];

					SerializeField result = SerializeField();
					result.m_NameLength = readShort(src, pointer);
					pointer += 2;
					result.m_Name = readString(src, pointer, result.m_NameLength);
					pointer += result.m_NameLength;

					result.m_Type = src[pointer++];

					result.m_Data = Buffer();
					result.m_Data.Allocate(GetTypeSize((TYPE)result.m_Type));
					readBytes(src, pointer, result.m_Data);
					pointer += GetTypeSize((TYPE)result.m_Type);
					return result;
				}

			private:
				SerializeField() {}

				byte m_ContainerType = CONTAINER_TYPE::FIELD;
				byte m_Type;
				Buffer m_Data;
			};
		}
	}
}