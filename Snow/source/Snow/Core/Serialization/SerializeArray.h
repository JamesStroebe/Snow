///////////////////////////////////////////
// File: SerializeArray.h
//
// Original Author: James Stroebe
//
// Creation Date: 14/01/2020
//
/////////////////////////////////////////// 

#pragma once

#pragma warning(disable:26495 4267)

#include "SerializeBase.h"
#include "SerializeUtil.h"
#include "ContainerType.h"

namespace Snow {
	namespace Core {
		namespace Serialize {
			class SerializeArray : public SerializeBase {
			public:
				int GetDataSize() {
					switch (m_Type) {
					case TYPE::BYTE:	return m_Data.Size * GetTypeSize(TYPE::BYTE);
					case TYPE::SHORT:	return m_ShortData.size() * GetTypeSize(TYPE::SHORT);
					case TYPE::CHAR:	return m_CharData.size() * GetTypeSize(TYPE::CHAR);
					case TYPE::INT:		return m_IntData.size() * GetTypeSize(TYPE::INT);
					case TYPE::LONG:	return m_LongData.size() * GetTypeSize(TYPE::LONG);
					case TYPE::FLOAT:	return m_FloatData.size() * GetTypeSize(TYPE::FLOAT);
					case TYPE::DOUBLE:	return m_DoubleData.size() * GetTypeSize(TYPE::DOUBLE);
					//case TYPE::BOOL:	return m_BoolData.size() * GetTypeSize(TYPE::BOOL);
					}
					return 0;
				}

				int GetBytes(Buffer dest, int pointer) {
					
					pointer = writeBytes(dest, pointer, m_ContainerType);
					pointer = writeBytes(dest, pointer, m_NameLength);
					pointer = writeBytes(dest, pointer, m_Name);
					pointer = writeBytes(dest, pointer, m_Size);
					pointer = writeBytes(dest, pointer, m_Type);
					pointer = writeBytes(dest, pointer, (int)m_Count);

					switch (m_Type) {
					case TYPE::BYTE: pointer = writeBytes(dest, pointer, m_Data); break;
					case TYPE::SHORT: pointer = writeBytes(dest, pointer, m_ShortData.data()); break;
					case TYPE::CHAR: pointer = writeBytes(dest, pointer, m_CharData.data()); break;
					case TYPE::INT: pointer = writeBytes(dest, pointer, m_IntData.data()); break;
					case TYPE::LONG: pointer = writeBytes(dest, pointer, m_LongData.data()); break;
					case TYPE::FLOAT: pointer = writeBytes(dest, pointer, m_FloatData.data()); break;
					case TYPE::DOUBLE: pointer = writeBytes(dest, pointer, m_DoubleData.data()); break;
						//case TYPE::BOOL: pointer = writeBytes(dest, pointer, m_BoolData[0]); break;
					}
					return pointer;
				}

				int GetSize() {
					return m_Size;
				}

				static SerializeArray Byte(std::string name, Buffer data) {
					SerializeArray Array = SerializeArray();
					Array.SetName(name);
					Array.m_Type = TYPE::BYTE;
					Array.m_Count = data.Size;
					Array.m_Data = data;
					Array.UpdateSize();
					return Array;
				}

				static SerializeArray Char(std::string name, char data[]) {
					SerializeArray Array = SerializeArray();
					Array.SetName(name);
					Array.m_Type = TYPE::CHAR;
					Array.m_Count = sizeof(data) / sizeof(char);
					Array.m_CharData = std::vector<char>(data, data + Array.m_Count);
					Array.UpdateSize();
					return Array;
				}

				static SerializeArray Short(std::string name, short data[]) {
					SerializeArray Array = SerializeArray();
					Array.SetName(name);
					Array.m_Type = TYPE::SHORT;
					Array.m_Count = sizeof(data) / sizeof(short);
					Array.m_ShortData = std::vector<short>(data, data + Array.m_Count);
					Array.UpdateSize();
					return Array;
				}

				static SerializeArray Int(std::string name, int data[]) {
					SerializeArray Array = SerializeArray();
					Array.SetName(name);
					Array.m_Type = TYPE::INT;
					Array.m_Count = sizeof(data) / sizeof(int);
					Array.m_IntData = std::vector<int>(data, data + Array.m_Count);
					Array.UpdateSize();
					return Array;
				}

				static SerializeArray Long(std::string name, long data[]) {
					SerializeArray Array = SerializeArray();
					Array.SetName(name);
					Array.m_Type = TYPE::LONG;
					Array.m_Count = sizeof(data) / sizeof(long);
					Array.m_LongData = std::vector<long>(data, data + Array.m_Count);
					Array.UpdateSize();
					return Array;
				}

				static SerializeArray Float(std::string name, float data[]) {
					SerializeArray Array = SerializeArray();
					Array.SetName(name);
					Array.m_Type = TYPE::FLOAT;
					Array.m_Count = sizeof(data) / sizeof(float);
					Array.m_FloatData = std::vector<float>(data, data + Array.m_Count);
					Array.UpdateSize();
					return Array;
				}

				static SerializeArray Double(std::string name, double data[]) {
					SerializeArray Array = SerializeArray();
					Array.SetName(name);
					Array.m_Type = TYPE::DOUBLE;
					Array.m_Count = sizeof(data) / sizeof(double);
					Array.m_DoubleData = std::vector<double>(data, data + Array.m_Count);
					Array.UpdateSize();
					return Array;
				}

				//static SerializeArray Bool(std::string name, bool data[]) {
				//	SerializeArray Array = SerializeArray();
				//	Array.SetName(name);
				//	Array.m_Type = TYPE::BOOL;
				//	Array.m_Size = sizeof(data) / sizeof(short);
				//	memcpy(Array.m_BoolData.data(), data, sizeof(data));
				//	Array.UpdateSize();
				//	return Array;
				//}

				static SerializeArray Deserialize(Buffer src, int pointer) {
					byte containerType = src[pointer++];

					SerializeArray result = SerializeArray();
					result.m_NameLength = readShort(src, pointer);
					pointer += 2;
					result.m_Name = readString(src, pointer, result.m_NameLength);
					pointer += result.m_NameLength;

					result.m_Size = readInt(src, pointer);
					pointer += 4;

					result.m_Type = src[pointer++];
					result.m_Count = readInt(src, pointer);
					pointer += 4;

					switch (result.m_Type) {
					case TYPE::BYTE:	result.m_Data.Allocate(result.m_Count); readBytes(src, pointer, result.m_Data); break;
					case TYPE::SHORT:	result.m_ShortData.resize(result.m_Count);  readShorts(src, pointer, result.m_ShortData.data()); break;
					case TYPE::CHAR:	result.m_CharData.resize(result.m_Count);  readChars(src, pointer, result.m_CharData.data()); break;
					case TYPE::INT:		result.m_IntData.resize(result.m_Count);  readInts(src, pointer, result.m_IntData.data()); break;
					case TYPE::LONG:	result.m_LongData.resize(result.m_Count);  readLongs(src, pointer, result.m_LongData.data()); break;
					case TYPE::FLOAT:	result.m_FloatData.resize(result.m_Count);  readFloats(src, pointer, result.m_FloatData.data()); break;
					case TYPE::DOUBLE:	result.m_DoubleData.resize(result.m_Count);  readDoubles(src, pointer, result.m_DoubleData.data()); break;

					}
					pointer += result.m_Count * GetTypeSize((TYPE)result.m_Type);
					return result;
				}

			private:
				SerializeArray() {
					m_Size += 1 + 1 + 4;
				}

				void UpdateSize() {
					m_Size += GetDataSize();
				}

				byte m_ContainerType = CONTAINER_TYPE::ARRAY;
				byte m_Type;
				uint32_t m_Count;
				Buffer m_Data;

				std::vector<short> m_ShortData;
				std::vector<char> m_CharData;
				std::vector<int> m_IntData;
				std::vector<long> m_LongData;
				std::vector<float> m_FloatData;
				std::vector<double> m_DoubleData;
				//std::vector<bool> m_BoolData;
			};
		}
	}
}
