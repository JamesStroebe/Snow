///////////////////////////////////////////
// File: Base.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include <memory>
#include <stdint.h>

#include "Snow/Math/Common/Types.h"



namespace Snow {
	namespace Core {
		void InitializeCore();
		void ShutdownCore();
	}
}

#ifdef SNOW_DEBUG
	#define SNOW_ENABLE_ASSERTS
#endif

#ifdef SNOW_ENABLE_ASSERTS
	#define SNOW_ASSERT_NO_MESSAGE(condition) { if(!(condition)) { SNOW_ERROR("Assertion Failed!"); __debugbreak(); } }
	#define SNOW_ASSERT_MESSAGE(condition, ...) { if(!(condition)) { SNOW_ERROR("Assertion Failed: {0}", __VA_ARGS__); __debugbreak(); } }
	#define SNOW_ASSERT_RESOLVE(arg1, arg2, macro, ...) macro
		
	#define SNOW_ASSERT(...) SNOW_ASSERT_RESOLVE(__VA_ARGS__, SNOW_ASSERT_MESSAGE, SNOW_ASSERT_NO_MESSAGE)(__VA_ARGS__)
	#define SNOW_CORE_ASSERT(...) SNOW_ASSERT_RESOLVE(__VA_ARGS__, SNOW_ASSERT_MESSAGE, SNOW_ASSERT_NO_MESSAGE)(__VA_ARGS__)
#else	
	#define SNOW_ASSERT(...)
	#define SNOW_CORE_ASSERT(...)
#endif

#define BIT(x) (1<<x)

#define SNOW_BIND_EVENT_FN(fn) std::bind(&fn, this, std::placeholders::_1) 

namespace Snow {
	namespace Core {
		template<typename T> // Might want to work on an asset system pretty soon
		using Scope = std::unique_ptr<T>;

		template<typename T, typename ... Args>
		constexpr Scope<T> CreateScope(Args&& ... args) {
			return std::make_unique<T>(std::forward<Args>(args)...);
		}

		template<typename T>
		using Ref = std::shared_ptr<T>;

		template<typename T, typename ... Args>
		constexpr Ref<T> CreateRef(Args&& ... args) {
			return std::make_shared<T>(std::forward<Args>(args)...);
		}

		using byte = unsigned char;

		using RendererID = Math::uint32_t;
	}
}







