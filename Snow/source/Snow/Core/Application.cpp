///////////////////////////////////////////
// File: Application.cpp
//
// Related Header: Application.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
// Purpose: Main application that the engine runs on.
//
///////////////////////////////////////////

// Include Paths

#include "spch.h"
#include "Snow/Core/Application.h"

#include "Snow/Render/Renderer.h"

#include "Snow/Core/Input.h"

#include "Snow/Math/Random/Random.h"
#include "Snow/Entity/EntityComponentSystem.h"

//VERY TEMP, MOVE TO ENGINE CONFIGS CLASS SOMEWHERE
#define MAX_GAME_LOGIC_DELTA_TIME 16.666f

namespace Snow {
	namespace Core {
		Application* Application::s_Instance = nullptr;

		Application::Application(const WindowProps& props) {
			SNOW_ASSERT(!s_Instance, "Application already exists!"); // We need to figure out assertions pretty soon.
			
			s_Instance = this;
			//Initialize the Window and Renderer to begin with, and maybe start the timers? also set the random seed generator
			m_Window = Window::Create(props);
			m_Window->SetEventCallback(SNOW_BIND_EVENT_FN(Application::OnEvent));

			Input::Init();

			Render::Renderer::Init(m_Window);

			m_ImGuiLayer = new Imgui::ImGuiLayer();
			PushOverlay(m_ImGuiLayer);

			Math::Random::Init(); 

			m_Timer = Time::Timer();

			Entity::EntityComponentSystem::Init();

			//RenderThread::Start(); // Really need to get the rendering on a seperate thread/fixing the render submittion
		}

		void Application::PushLayer(Layer* layer) { // These could be inline, might have future things to do with the layers for preparing?
			m_LayerStack.PushLayer(layer);
		}

		void Application::PushOverlay(Layer* overlay) {
			m_LayerStack.PushOverlay(overlay);
		}

		void Application::OnEvent(Event::Event& e) {
			Event::EventDispatcher dispatcher(e);
			dispatcher.Dispatch<Event::WindowMinimizedEvent>(SNOW_BIND_EVENT_FN(Application::OnWindowMinimized));
			dispatcher.Dispatch<Event::WindowCloseEvent>(SNOW_BIND_EVENT_FN(Application::OnWindowClose));
			dispatcher.Dispatch<Event::WindowResizeEvent>(SNOW_BIND_EVENT_FN(Application::OnWindowResize));
			dispatcher.Dispatch<Event::WindowFullscreenEvent>(SNOW_BIND_EVENT_FN(Application::OnWindowFullscreen));

			for (auto it = m_LayerStack.end(); it != m_LayerStack.begin();) {
				(*--it)->OnEvent(e);
				if (e.Handled)
					break;
			}
		}

		void Application::RenderImGui() {
			m_ImGuiLayer->Begin();
			for (Layer* layer : m_LayerStack)
				layer->OnImGuiRender();
			m_ImGuiLayer->End();

			
		}

		void Application::Run() {
			uint32_t renderFrames = 0;
			uint32_t updateFrames = 0;
			m_Timer.GetCurrentTimeFrame();
			float updateTimer = m_Timer.ElapsedMillis();
			float timer = 0.0f;
			m_Timestep = Time::Timestep(m_Timer.ElapsedMillis());
			while (m_Running) {
				m_Timer.GetCurrentTimeFrame();
				
				//SNOW_CORE_TRACE("CurrentTimeFrame: {0}", m_Timer.ElapsedMillis());

				//Time to make update and renderloops.

				

				if (!m_Minimized) {
					double current = m_Timer.ElapsedMillis();
					if (current - updateTimer > MAX_GAME_LOGIC_DELTA_TIME) {
						m_Timestep.Update(current);
						for (Layer* layer : m_LayerStack)
							layer->OnUpdate(m_Timestep);
						updateFrames++;
						updateTimer += MAX_GAME_LOGIC_DELTA_TIME;
					}
					
					
					for (Layer* layer : m_LayerStack)
						layer->OnRender();
					renderFrames++;

					Application* app = this;
					
					Render::Renderer::Submit([app] { app->RenderImGui(); });
					
					Render::Renderer::WaitAndRender();
					//RenderThread::WaitAndRender();
				}
				if (m_Timer.ElapsedSeconds() - timer > 1.0f) {
					timer += 1.0f;
					std::string title = std::string("Snow " + std::to_string(updateFrames) + "UPS, " + std::to_string(renderFrames) + "FPS");
					m_Window->SetTitle(title);
					updateFrames = 0;
					renderFrames = 0;
				}
				m_Window->OnUpdate();
			}
			//RenderThread::Stop();
		}


		// Application Event Handler Methods
		bool Application::OnWindowClose(Event::WindowCloseEvent& e) {
			m_Running = false;
			return true;
		}

		bool Application::OnWindowMinimized(Event::WindowMinimizedEvent& e) {
			m_Minimized = true;
			return false;
		}

		bool Application::OnWindowResize(Event::WindowResizeEvent& e) {
			int width = e.GetWidth(), height = e.GetHeight();
			if (width == 0 || height == 0) {
				m_Minimized = true;
				return false;
			}
			m_Minimized = false;

			Render::Renderer::SetViewport(0, 0, width, height);

			return false;
		}

		bool Application::OnWindowFullscreen(Event::WindowFullscreenEvent& e) {
			return false;
		}

		double Application::GetTime() const {
			std::chrono::high_resolution_clock::now();

			return std::chrono::system_clock::now().time_since_epoch().count() / (double)10000000.0;
		}
	}
}