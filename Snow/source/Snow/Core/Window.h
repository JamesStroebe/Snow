///////////////////////////////////////////
// File: Window.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "spch.h"

#include "Snow/Core/Input.h"
#include "Snow/Core/Events/Events.h"

namespace Snow {
	namespace Core {
		struct WindowProps {
			int Width, Height;
			std::string Title;
			bool Fullscreen, VSync;

			WindowProps(int width = 1280, int height = 720, std::string title = "Snow Engine Executable") :
				Width(width), Height(height), Title(title), Fullscreen(false), VSync(false) {}

			inline void SetTitle(const std::string& title) { Title = title; }
		};


		class Window {
		private:
			static std::map<void*, Window*> s_Handles;
		public:
			InputManager* m_InputManager;
			
			using EventCallbackFn = std::function<void(Event::Event&)>;
			EventCallbackFn m_EventCallback;

			WindowProps m_Props;

			static Ref<Window> Create(const WindowProps& props = WindowProps());

			virtual ~Window() = default;
			virtual void OnUpdate() = 0;

			virtual unsigned int GetWidth() const = 0;
			virtual unsigned int GetHeight() const = 0;

			virtual int GetXPos() const = 0;
			virtual int GetYPos() const = 0;

			virtual void SetTitle(const std::string& title) = 0;

			void SetEventCallback(const EventCallbackFn& callback);

			virtual void SetVSync(bool enabled) = 0;
			virtual bool IsVSync() const = 0;
			inline InputManager* GetInputManager() const { return m_InputManager; }

			inline WindowProps& GetWindowProps() { return m_Props; }

			void RegisterWindowClass(void* handle, Window* window) {
				s_Handles[handle] = window;
			}

			static Window* GetWindowClass(void* handle) {
				if (handle == nullptr)
					return s_Handles.begin()->second;
				return s_Handles[handle];
			}
		};
	}
}