///////////////////////////////////////////
// File: LayerStack.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Base.h"
#include "Snow/Core/Layer.h"

#include <vector>

namespace Snow {
	namespace Core {
		class LayerStack {
		public:
			LayerStack() = default;
			~LayerStack();

			void PushLayer(Layer* layer);
			void PushOverlay(Layer* overlay);
			void PopLayer(Layer* layer);
			void PopOverlay(Layer* overlay);

			std::vector<Layer*>::iterator begin() { return m_Layers.begin(); }
			std::vector<Layer*>::iterator end() { return m_Layers.end(); }
		private:
			std::vector<Layer*> m_Layers;
			unsigned int m_LayerInsertIndex = 0;
		};
	}
}