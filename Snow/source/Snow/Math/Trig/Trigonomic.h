#pragma once

#include "Snow/Math/Common/Qualifier.h"

namespace Snow {
	namespace Math {
		template<typename T>
		inline T radians(T angleDeg);

		template<typename T>
		inline T degrees(T angleRad);

		template<typename T>
		inline T sin(T angleRad);

		template<typename T>
		inline T cos(T angleRad);

		template<typename T>
		inline T tan(T angleRad);

		template<typename T>
		inline T asin(T angleRad);

		template<typename T>
		inline T acos(T angleRad);

		template<typename T>
		inline T atan(T angleRad);

		template<typename T>
		inline T atan2(T yValue, T xValue);

		template<typename T>
		inline T sinh(T angleRad);

		template<typename T>
		inline T cosh(T angleRad);

		template<typename T>
		inline T tanh(T angleRad);
	}
}

#include "Trigonomic.inl"