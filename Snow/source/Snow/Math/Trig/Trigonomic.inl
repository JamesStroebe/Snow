#pragma once

#include "Snow/Math/Common/Common.h"
#include <cmath>

namespace Snow {
	namespace Math {
		template<typename T>
		inline T radians(T angleDeg) {
			return angleDeg * static_cast<T>(0.01745329251994329576923690768489);
		}

		template<typename T>
		inline T degrees(T angleRad) {
			return angleRad * static_cast<T>(57.295779513082320876798154814105);
		}

		template<typename T>
		inline T sin(T angleRad) {
			return static_cast<T>(std::sin(angleRad));
		}

		template<typename T>
		inline T cos(T angleRad) {
			return static_cast<T>(std::cos(angleRad));
		}

		template<typename T>
		inline T tan(T angleRad) {
			return static_cast<T>(std::tan(angleRad));
		}

		template<typename T>
		T asin(T angleRad) {
			return static_cast<T>(std::asin(angleRad));
		}

		template<typename T>
		T acos(T angleRad) {
			return static_cast<T>(std::acos(angleRad));
		}

		template<typename T>
		T atan(T angleRad) {
			return static_cast<T>(std::atan(angleRad));
		}

		template<typename T>
		T atan2(T angleY, T angleX) {
			return static_cast<T>(std::atan2(angleY, angleX));
		}

		template<typename T>
		T sinh(T angleRad) {
			return static_cast<T>(std::sinh(angleRad));
		}

		template<typename T>
		T cosh(T angleRad) {
			return static_cast<T>(std::cosh(angleRad));
		}

		template<typename T>
		T tanh(T angleRad) {
			return static_cast<T>(std::tanh(angleRad));
		}

		template<typename T>
		T asinh(T angleRad) {
			return static_cast<T>(std::asinh(angleRad));
		}

		template<typename T>
		T acosh(T angleRad) {
			return static_cast<T>(std::acosh(angleRad));
		}

		template<typename T>
		T atanh(T angleRad) {
			return static_cast<T>(std::atanh(angleRad));
		}
	}
}