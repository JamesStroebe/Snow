#pragma once

#include "Quaternion.h"
#include "QuaternionExponential.h"


namespace Snow {
	namespace Math {


		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> quatIdentity();

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> cross(const Quaternion<T, Q>& q, const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		constexpr Vector<4, T, Q> cross(const Quaternion<T, Q>& q, const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		Quaternion<T, Q> squad(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2,
			const Quaternion<T, Q>& s1, const Quaternion<T, Q>& s2, const T& h);

		template<typename T, qualifier Q>
		Quaternion<T, Q> intermediate(const Quaternion<T, Q>& prev, const Quaternion<T, Q>& curr, const Quaternion<T, Q>& next);

		template<typename T, qualifier Q>
		Vector<3, T, Q> rotate(const Quaternion<T, Q>& q, const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<4, T, Q> rotate(const Quaternion<T, Q>& q, const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		Quaternion<T, Q> rotate(const Quaternion<T, Q>& q, const T& angle, const Vector<3, T, Q>& axis);

		template<typename T, qualifier Q>
		T getRealComp(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		inline Matrix<3, 3, T, Q> ToMatrix3x3(const Quaternion<T, Q>& q) { return Matrix3x3Cast(q); }

		template<typename T, qualifier Q>
		inline Matrix<4, 4, T, Q> ToMatrix4x4(const Quaternion<T, Q>& q) { return Matrix4x4Cast(q); }

		template<typename T, qualifier Q>
		Quaternion<T, Q> ToQuaternion(const Matrix<3, 3, T, Q>& mat) { return QuaternionCast(mat); }

		template<typename T, qualifier Q>
		Quaternion<T, Q> ToQuaternion(const Matrix<4, 4, T, Q>& mat) { return QuaternionCast(mat); }

		template<typename T, qualifier Q>
		Quaternion<T, Q> ShortMix(const Quaternion<T, Q>& x, const Quaternion<T, Q>& y, const T& a);

		template<typename T, qualifier Q>
		Quaternion<T, Q> FastMix(const Quaternion<T, Q>& x, const Quaternion<T, Q>& y, const T& a);

		template<typename T, qualifier Q>
		Quaternion<T, Q> rotation(const Vector<3, T, Q>& origin, const Vector<3, T, Q>& dest);

		template<typename T, qualifier Q>
		constexpr T length2(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		Quaternion<T, Q> lerp(const Quaternion<T, Q>& x, const Quaternion<T, Q>& y, T a);

		template<typename T, qualifier Q>
		Quaternion<T, Q> slerp(const Quaternion<T, Q>& x, const Quaternion<T, Q>& y, T a);

		template<typename T, typename S, qualifier Q>
		Quaternion<T, Q> lerp(const Quaternion<T, Q>& x, const Quaternion<T, Q>& y, T a, S k);

		template<typename T, qualifier Q>
		Quaternion<T, Q> conjugate(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		Quaternion<T, Q> inverse(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		Vector<4, T, Q> isNan(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		Vector<4, T, Q> isInf(const Quaternion<T, Q>& q);


		template<typename T, qualifier Q>
		Vector<3, T, Q> eulerAngles(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		T roll(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		T pitch(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		T yaw(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		Matrix<3, 3, T, Q> Matrix3x3Cast(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> Matrix4x4Cast(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		Quaternion<T, Q> QuaternionCast(const Matrix<3, 3, T, Q>& m);

		template<typename T, qualifier Q>
		Quaternion<T, Q> QuaternionCast(const Matrix<4, 4, T, Q>& m);

		template<typename T, qualifier Q>
		Quaternion<T, Q> QuaternionLookAtLH(const Vector<3, T, Q>& direction, const Vector<3, T, Q>& up);

		template<typename T, qualifier Q>
		Quaternion<T, Q> QuaternionLookAtRH(const Vector<3, T, Q>& direction, const Vector<3, T, Q>& up);

		template<typename T, qualifier Q>
		Quaternion<T, Q> QuaternionLookAt(const Vector<3, T, Q>& direction, const Vector<3, T, Q>& up);

		// Getters for Angle and Axis
		template<typename T, qualifier Q>
		T angle(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		Vector<3, T, Q> axis(const Quaternion<T, Q>& q);

		// Create a Quaternion with a given angle and normalized axis
		template<typename T, qualifier Q>
		Quaternion<T, Q> angleAxis(const T& angle, const Vector<3, T, Q>& vec);
	}
}
#include "QuaternionFunction.inl"