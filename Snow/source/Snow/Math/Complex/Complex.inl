#pragma once

#include "Snow/Math/Complex/Complex.h"

#include "Snow/Math/Complex/ComplexFunctions.h"

namespace Snow {
	namespace Math {


		template<typename T, qualifier Q>
		constexpr T& Complex<T, Q>::operator[](typename Complex<T, Q>::length_type i) {
			return (&x)[i];
		}

		template<typename T, qualifier Q>
		constexpr const T& Complex<T, Q>::operator[](typename Complex<T, Q>::length_type i) const {
			return (&x)[i];
		}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q>::Complex() :
			x(0), y(0) {}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q>::Complex(const Complex<T, Q>& c) :
			x(c.x), y(c.y) {}

		template<typename T, qualifier Q>
		template<qualifier P>
		constexpr Complex<T, Q>::Complex(const Complex<T, P>& c) :
			x(c.x), y(c.y) {}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q>::Complex(T real, T imag) :
			x(real), y(imag) {}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q>::Complex(const Vector<2, T, Q>& vec) :
			x(vec.x), y(vec.y) {}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q>& Complex<T, Q>::operator=(const Complex<T, Q>& c) {
			this->x = c.x;
			this->y = c.y;
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Complex<T, Q>& Complex<T, Q>::operator=(const Complex<U, Q>& c) {
			this->x = static_cast<T>(c.x);
			this->y = static_cast<T>(c.y);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Complex<T, Q>& Complex<T, Q>::operator+=(const Complex<U, Q>& c) {
			this->x += static_cast<T>(c.x);
			this->y += static_cast<T>(c.y);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Complex<T, Q>& Complex<T, Q>::operator-=(const Complex<U, Q>& c) {
			this->x -= static_cast<T>(c.x);
			this->y -= static_cast<T>(c.y);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Complex<T, Q>& Complex<T, Q>::operator*=(const Complex<U, Q>& c) {
			const Complex<T, Q> d(*this);
			this->x = (d.x * c.x) - (d.y * c.y);
			this->y = (d.x * c.y) + (d.y * c.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Complex<T, Q>& Complex<T, Q>::operator*=(const U& scalar) {
			this->x *= scalar;
			this->y *= scalar;
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Complex<T, Q>& Complex<T, Q>::operator/=(const Complex<U, Q>& c) {
			const Complex<T, Q> d(*this);
			return (d *= reciprocal(c));
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Complex<T, Q>& Complex<T, Q>::operator/=(const U& scalar) {
			this->x /= scalar;
			this->y /= scalar;
			return *this;
		}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q> operator+(const Complex<T, Q>& c) {
			return Complex<T, Q>(c.x, c.y);
		}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q> operator-(const Complex<T, Q>& c) {
			return Complex<T, Q>(-c.x, -c.y);
		}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q> operator+(const Complex<T, Q>& c, const Complex<T, Q>& d) {
			return Complex<T, Q>(c) += d;
		}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q> operator-(const Complex<T, Q>& c, const Complex<T, Q>& d) {
			return Complex<T, Q>(c) -= d;
		}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q> operator*(const Complex<T, Q>& c, const T& scalar) {
			return Complex<T, Q>(c) *= scalar;
		}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q> operator*(const T& scalar, const Complex<T, Q>& c) {
			return Complex<T, Q>(c) *= scalar;
		}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q> operator*(const Complex<T, Q>& c, const Complex<T, Q>& d) {
			return Complex<T, Q>(c) *= d;
		}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q> operator/(const Complex<T, Q>& c, const T& scalar) {
			return Complex<T, Q>(c) /= scalar;
		}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q> operator/(const T& scalar, const Complex<T, Q>& c) {
			return Complex<T, Q>(c) /= scalar;
		}

		template<typename T, qualifier Q>
		constexpr Complex<T, Q> operator/(const Complex<T, Q>& c, const Complex<T, Q>& d) {
			return Complex<T, Q>(c) /= d;
		}

		template<typename T, qualifier Q>
		constexpr bool operator==(const Complex<T, Q>& c, const Complex<T, Q>& d) {
			return c.x == d.x && c.y == d.y;
		}

		template<typename T, qualifier Q>
		constexpr bool operator!=(const Complex<T, Q>& c, const Complex<T, Q>& d) {
			return c.x != d.x || c.y != d.y;
		}
	}
}