#pragma once

#include "Snow/Math/Common/Qualifier.h"

namespace Snow {
	namespace Math {
		template<typename T, qualifier Q>
		Vector<4, bool, Q> lessThan(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2);

		template<typename T, qualifier Q>
		Vector<4, bool, Q> lessThanEqual(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2);

		template<typename T, qualifier Q>
		Vector<4, bool, Q> greaterThan(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2);

		template<typename T, qualifier Q>
		Vector<4, bool, Q> greaterThanEqual(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2);
	}
}