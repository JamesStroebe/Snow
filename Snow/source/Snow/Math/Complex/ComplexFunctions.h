#pragma once

#include "Snow/Math/Common/Qualifier.h"

namespace Snow {
	namespace Math {

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> conjugate(const Complex<T, Q>& com);

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> reciprocal(const Complex<T, Q>& com);

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> scaleXY(const Complex<T, Q>& comp, const Vector<2, T, Q>& vec);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> Vector2Cast(const Complex<T, Q>& comp);
	}
}

#include "ComplexFunctions.inl"