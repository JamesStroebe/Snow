#pragma once

#include "Snow/Math/Common/Qualifier.h"

namespace Snow {
	namespace Math {
		template<typename T, qualifier Q>
		struct Complex {
			typedef Complex<T, Q> type;
			typedef T value_type;

			typedef size_t length_type;

			inline constexpr static length_type length() { return 2; }
			
			inline constexpr T& operator[](length_type i);
			inline constexpr const T& operator[](length_type i) const;

			inline constexpr Complex();
			inline constexpr Complex(const Complex<T, Q>& c);
			template<qualifier P>
			inline constexpr Complex(const Complex<T, P>& c);

			inline constexpr Complex(T real, T imag);

			inline constexpr Complex(const Vector<2, T, Q>& vec);

			inline constexpr Complex<T, Q>& operator=(const Complex<T, Q>& c);
			template<typename U>
			inline constexpr Complex<T, Q>& operator=(const Complex<U, Q>& c);

			template<typename U>
			inline constexpr Complex<T, Q>& operator+=(const Complex<U, Q>& c);

			template<typename U>
			inline constexpr Complex<T, Q>& operator-=(const Complex<U, Q>& c);

			template<typename U>
			inline constexpr Complex<T, Q>& operator*=(const Complex<U, Q>& c);

			template<typename U>
			inline constexpr Complex<T, Q>& operator*=(const U& scalar);

			template<typename U>
			inline constexpr Complex<T, Q>& operator/=(const Complex<U, Q>& c);

			template<typename U>
			inline constexpr Complex<T, Q>& operator/=(const U& scalar);

			T x, y;
		};

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> operator+(const Complex<T, Q>& c);

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> operator-(const Complex<T, Q>& c);

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> operator+(const Complex<T, Q>& c, const Complex<T, Q>& d);

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> operator-(const Complex<T, Q>& c, const Complex<T, Q>& d);

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> operator*(const Complex<T, Q>& c, const T& scalar);

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> operator*(const T& scalar, const Complex<T, Q>& c);

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> operator*(const Complex<T, Q>& c, const Complex<T, Q>& d);

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> operator/(const Complex<T, Q>& c, const T& scalar);

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> operator/(const T& scalar, const Complex<T, Q>& c);

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> operator/(const Complex<T, Q>& c, const Complex<T, Q>& d);

		template<typename T, qualifier Q>
		inline constexpr bool operator==(const Complex<T, Q>& c, const Complex<T, Q>& d);

		template<typename T, qualifier Q>
		inline constexpr bool operator!=(const Complex<T, Q>& c, const Complex<T, Q>& d);


		typedef Complex<float, defaultp>					Complexf;

		typedef Complex<float, qualifier::highp>			Complexf_highp;
		typedef Complex<float, qualifier::mediump>			Complexf_mediump;
		typedef Complex<float, qualifier::lowp>				Complexf_lowp;

		typedef Complex<bool, defaultp>						Complexb;

		typedef Complex<bool, qualifier::highp>				Complexb_highp;
		typedef Complex<bool, qualifier::mediump>			Complexb_mediump;
		typedef Complex<bool, qualifier::lowp>				Complexb_lowp;

		typedef Complex<double, defaultp>					Complexd;

		typedef Complex<double, qualifier::highp>			Complexd_highp;
		typedef Complex<double, qualifier::mediump>			Complexd_mediump;
		typedef Complex<double, qualifier::lowp>			Complexd_lowp;

		typedef Complex<int, defaultp>						Complexi;

		typedef Complex<int, qualifier::highp>				Complexi_highp;
		typedef Complex<int, qualifier::mediump>			Complexi_mediump;
		typedef Complex<int, qualifier::lowp>				Complexi_lowp;

		typedef Complex<unsigned int, defaultp>				Complexu;

		typedef Complex<unsigned int, qualifier::highp>		Complexu_highp;
		typedef Complex<unsigned int, qualifier::mediump>	Complexu_mediump;
		typedef Complex<unsigned int, qualifier::lowp>		Complexu_lowp;
	}
}

#include "Complex.inl"