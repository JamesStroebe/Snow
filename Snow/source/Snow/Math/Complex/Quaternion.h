#pragma once


#include "Snow/Math/Common/Qualifier.h"
#include "QuaternionRelational.h"

namespace Snow {
	namespace Math {
		template<typename T, qualifier Q>
		struct Quaternion {
			typedef Quaternion<T, Q> type;
			typedef T value_type;

			typedef size_t length_type;

			static constexpr length_type length() { return 4; }

			constexpr T& operator[](length_type i);
			constexpr const T& operator[](length_type i) const;


			constexpr Quaternion();
			constexpr Quaternion(const Quaternion<T, Q>& q);
			template<qualifier P>
			constexpr Quaternion(const Quaternion<T, P>& q);

			constexpr Quaternion(T scalar, const Vector<3, T, Q>& v);
			constexpr Quaternion(T w, T x, T y, T z);

			template<typename U, qualifier P>
			constexpr Quaternion(const Quaternion<U, P>& q);

			explicit operator Matrix<3, 3, T, Q>() const;
			explicit operator Matrix<4, 4, T, Q>() const;

			Quaternion(const Vector<3, T, Q>& u, const Vector<3, T, Q>& v);

			constexpr explicit Quaternion(const Vector<3, T, Q>& eulerAngles);
			explicit Quaternion(const Matrix<3, 3, T, Q>& mat);
			explicit Quaternion(const Matrix<4, 4, T, Q>& mat);

			constexpr Quaternion<T, Q>& operator=(const Quaternion<T, Q>& q);
			template<typename U>
			constexpr Quaternion<T, Q>& operator=(const Quaternion<U, Q>& q);

			template<typename U>
			constexpr Quaternion<T, Q>& operator+=(const Quaternion<U, Q>& q);

			template<typename U>
			constexpr Quaternion<T, Q>& operator-=(const Quaternion<U, Q>& q);

			template<typename U>
			constexpr Quaternion<T, Q>& operator*=(const Quaternion<U, Q>& q);

			template<typename U>
			constexpr Quaternion<T, Q>& operator*=(U scalar);

			template<typename U>
			constexpr Quaternion<T, Q>& operator/=(U scalar);

			T x, y, z, w;
		};

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator+(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator-(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator+(const Quaternion<T, Q>& q, const Quaternion<T, Q>& p);

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator-(const Quaternion<T, Q>& q, const Quaternion<T, Q>& p);

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator*(const Quaternion<T, Q>& q, const T& scalar);

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator*(const T& scalar, const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(const Quaternion<T, Q>& q, const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(const Vector<3, T, Q>& v, const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		constexpr Vector<4, T, Q> operator*(const Quaternion<T, Q>& q, const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(const Vector<4, T, Q>& v, const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator*(const Quaternion<T, Q>& q, const Quaternion<T, Q>& p);

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator/(const Quaternion<T, Q>& q, const T& scalar);

		template<typename T, qualifier Q>
		constexpr bool operator==(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2);

		template<typename T, qualifier Q>
		constexpr bool operator!=(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2);



		typedef Quaternion<float, defaultp>						Quaternionf;

		typedef Quaternion<float, qualifier::highp>				Quaternionf_highp;
		typedef Quaternion<float, qualifier::mediump>			Quaternionf_mediump;
		typedef Quaternion<float, qualifier::lowp>				Quaternionf_lowp;

		typedef Quaternion<bool, defaultp>						Quaternionb;

		typedef Quaternion<bool, qualifier::highp>				Quaternionb_highp;
		typedef Quaternion<bool, qualifier::mediump>			Quaternionb_mediump;
		typedef Quaternion<bool, qualifier::lowp>				Quaternionb_lowp;

		typedef Quaternion<double, defaultp>					Quaterniond;

		typedef Quaternion<double, qualifier::highp>			Quaterniond_highp;
		typedef Quaternion<double, qualifier::mediump>			Quaterniond_mediump;
		typedef Quaternion<double, qualifier::lowp>				Quaterniond_lowp;

		typedef Quaternion<int, defaultp>						Quaternioni;

		typedef Quaternion<int, qualifier::highp>				Quaternioni_highp;
		typedef Quaternion<int, qualifier::mediump>				Quaternioni_mediump;
		typedef Quaternion<int, qualifier::lowp>				Quaternioni_lowp;

		typedef Quaternion<unsigned int, defaultp>				Quaternionu;

		typedef Quaternion<unsigned int, qualifier::highp>		Quaternionu_highp;
		typedef Quaternion<unsigned int, qualifier::mediump>	Quaternionu_mediump;
		typedef Quaternion<unsigned int, qualifier::lowp>		Quaternionu_lowp;
	}
}
#include "Quaternion.inl"