#pragma once

#include "Snow/Math/Common/Constants.h"
#include "Snow/Math/Trig/Trigonomic.h"
#include "Snow/Math/Common/Exponent.h"

#include "Quaternion.h"
#include "QuaternionGeometric.h"

namespace Snow {
	namespace Math {

		template<typename T, qualifier Q>
		Vector<3, T, Q> rotate(const Quaternion<T, Q>& q, const Vector<3, T, Q>& v) {
			return q * v;
		}

		template<typename T, qualifier Q>
		Vector<4, T, Q> rotate(const Quaternion<T, Q>& q, const Vector<4, T, Q>& v) {
			return q * v;
		}

		template<typename T, qualifier Q>
		Quaternion<T, Q> conjugate(const Quaternion<T, Q>& q) {
			return Quaternion<T, Q>(q.w, -q.x, -q.y, -q.z);
		}

		template<typename T, qualifier Q>
		Quaternion<T, Q> inverse(const Quaternion<T, Q>& q) {
			return conjugate(q) / dot(q, q);
		}

		template<typename T, qualifier Q>
		Vector<3, T, Q> eulerAngles(const Quaternion<T, Q>& q) {
			return Vector<3, T, Q>(pitch(q), yaw(q), roll(q));
		}

		template<typename T, qualifier Q>
		T roll(const Quaternion<T, Q>& q) {
			return static_cast<T>(Math::atan2(static_cast<T>(2)* (q.x * q.y + q.w * q.z), q.w* q.w + q.x * q.x - q.y * q.y - q.z * q.z));
		}

		template<typename T, qualifier Q>
		T pitch(const Quaternion<T, Q>& q) {
			const T y = static_cast<T>(2)* (q.y * q.z + q.w * q.x);
			const T x = q.w * q.w - q.x * q.x - q.y * q.y + q.z * q.z;

			if (Math::all(Math::equal(Vector<2, T, Q>(x, y), Vector<2, T, Q>(0), Math::epsilon<T>())))
				return static_cast<T>(static_cast<T>(2)* Math::atan2(q.x, q.w));

			return static_cast<T>(Math::atan2(y, x));
		}

		template<typename T, qualifier Q>
		T yaw(const Quaternion<T, Q>& q) {
			return asin(clamp(static_cast<T>(-2)* (q.x * q.z - q.w * q.y), static_cast<T>(1), static_cast<T>(1)));
		}

		template<typename T, qualifier Q>
		Matrix<3, 3, T, Q> Matrix3x3Cast(const Quaternion<T, Q>& q) {
			Matrix<3, 3, T, Q> result(1);
			T qxx(q.x * q.x);
			T qyy(q.y * q.y);
			T qzz(q.z * q.z);
			T qxz(q.x * q.z);
			T qxy(q.x * q.y);
			T qyz(q.y * q.z);
			T qwx(q.w * q.x);
			T qwy(q.w * q.y);
			T qwz(q.w * q.z);

			result[0][0] = T(1) - T(2) * (qyy + qzz);
			result[0][1] = T(2) * (qxy - qwz);
			result[0][2] = T(2) * (qxz + qwy);

			result[1][0] = T(2) * (qxy + qwz);
			result[1][1] = T(1) - T(2) * (qxx + qzz);
			result[1][2] = T(2) * (qyz - qwx);

			result[2][0] = T(2) * (qxz - qwy);
			result[2][1] = T(2) * (qyz + qwx);
			result[2][2] = T(1) - T(2) * (qxx + qyy);
			return result;
		}

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> Matrix4x4Cast(const Quaternion<T, Q>& q) {
			return Matrix<4, 4, T, Q>(Matrix3x3Cast(q));
		}

		template<typename T, qualifier Q>
		Quaternion<T, Q> QuaternionCast(const Matrix<3, 3, T, Q>& mat) {
			T x = mat[0][0] - mat[1][1] - mat[2][2];
			T y = mat[1][1] - mat[0][0] - mat[2][2];
			T z = mat[2][2] - mat[0][0] - mat[1][1];
			T w = mat[0][0] + mat[1][1] + mat[2][2];

			int i = 0;
			T b = w;
			if (x > b) {
				b = x;
				i = 1;
			}
			if (y > b) {
				b = y;
				i = 2;
			}
			if (z > b) {
				b = z;
				i = 3;
			}

			T val = Math::sqrt(b + static_cast<T>(1))* static_cast<T>(0.5);
			T mul = static_cast<T>(0.25) / val;

			switch (i) {
			case 0:
				return Quaternion<T, Q>(val, (mat[2][1] - mat[1][2]) * mul, (mat[0][2] - mat[2][0]) * mul, (mat[1][0] - mat[0][1]) * mul);
			case 1:
				return Quaternion<T, Q>((mat[2][1] - mat[1][2]) * mul, val, (mat[1][0] + mat[0][1]) * mul, (mat[0][2] + mat[2][0]) * mul);
			case 2:
				return Quaternion<T, Q>((mat[0][2] - mat[2][0]) * mul, (mat[1][0] + mat[0][1]) * mul, val, (mat[2][1] + mat[1][2]) * mul);
			case 3:
				return Quaternion<T, Q>((mat[1][0] - mat[0][1]) * mul, (mat[0][2] - mat[2][0]) * mul, (mat[2][1] + mat[1][2]) * mul, val);
			default:
				//assert false
				return Quaternion<T, Q>(1, 0, 0, 0);
			}

		}

		template<typename T, qualifier Q>
		Quaternion<T, Q> QuaternionCast(const Matrix<4, 4, T, Q>& mat) {
			return QuaternionCast(Matrix<3, 3, T, Q>(mat));
		}
	}
}