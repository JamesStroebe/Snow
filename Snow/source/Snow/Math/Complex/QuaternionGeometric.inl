#pragma once

namespace Snow {
	namespace Math {
		template<typename T, qualifier Q>
		inline T dot(const Quaternion<T, Q>& x, const Quaternion<T, Q>& y) {
			return computeDot<Quaternion<T, Q>, T, is_aligned<Q>::value>::call(x, y);
		}
	}
}