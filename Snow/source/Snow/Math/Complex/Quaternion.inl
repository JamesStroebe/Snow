#pragma once

#include "Snow/Math/Trig/Trigonomic.h"
#include "Snow/Math/Vector/VectorFunctions.h"
#include "Snow/Math/Common/Exponent.h"

#include "Snow/Math/Complex/QuaternionFunction.h"
#include "Snow/Math/Complex/QuaternionGeometric.h"

namespace Snow {
	namespace Math {

		template<typename T>
		struct genTypeTrait<Quaternion<T>> {
			static const genType gentype = QUATERNION;
		};





		template<typename T, qualifier Q>
		constexpr T& Quaternion<T, Q>::operator[](typename Quaternion<T, Q>::length_type i) {
			return (&x)[i];
		}

		template<typename T, qualifier Q>
		constexpr const T& Quaternion<T, Q>::operator[](typename Quaternion<T, Q>::length_type i) const {
			return (&x)[i];
		}

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q>::Quaternion() :
			x(0), y(0), z(0), w(1) {}

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q>::Quaternion(const Quaternion<T, Q>& q) :
			x(q.x), y(q.y), z(q.z), w(q.w) {}

		template<typename T, qualifier Q>
		template<qualifier P>
		constexpr Quaternion<T, Q>::Quaternion(const Quaternion<T, P>& q) :
			x(q.x), y(q.y), z(q.z), w(q.w) {}

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q>::Quaternion(T scalar, const Vector<3, T, Q>& v) :
			x(v.x), y(v.y), z(v.z), w(scalar) {}

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q>::Quaternion(T w, T x, T y, T z) :
			x(x), y(y), z(z), w(w) {}

		template<typename T, qualifier Q>
		template<typename U, qualifier P>
		constexpr Quaternion<T, Q>::Quaternion(const Quaternion<U, P>& q) :
			x(static_cast<T>(q.x)), y(static_cast<T>(q.y)), z(static_cast<T>(q.z)), w(static_cast<T>(q.w)) {}

		template<typename T, qualifier Q>
		Quaternion<T, Q>::Quaternion(const Vector<3, T, Q>& u, const Vector<3, T, Q>& v) {
			T normUnormV = sqrt(dot(u, u) * dot(v, v));
			T real = normUnormV + dot(u, v);
			Vector<3, T, Q> t;

			if (real < static_cast<T>(1.e-6f)* normUnormV) {
				real = static_cast<T>(0);
				t = abs(u.x) > abs(u.z) ? Vector<3, T, Q>(-u.y, u.x, static_cast<T>(0)) : Vector<3, T, Q>(static_cast<T>(0), -u.z, u.y);
			}
			else
				t = cross(u, v);

			*this = normalize(Quaternion<T, Q>(real, t.x, t.y, t.z));
		}

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q>::Quaternion(const Vector<3, T, Q>& eulerAngle) {
			Vector<3, T, Q> c = Math::cos(eulerAngle * T(0.5));
			Vector<3, T, Q> s = Math::sin(eulerAngle * T(0.5));

			this->w = c.x * c.y * c.z + s.x * s.y * s.z;
			this->x = s.x * c.y * c.z - c.x * s.y * s.z;
			this->y = c.x * s.y * c.z + s.x * c.y * s.z;
			this->z = c.x * c.y * s.z - s.x * s.y * c.z;
		}

		template<typename T, qualifier Q>
		Quaternion<T, Q>::Quaternion(const Matrix<3, 3, T, Q>& m) {
			*this = QuaternionCast(m);
		}

		template<typename T, qualifier Q>
		Quaternion<T, Q>::Quaternion(const Matrix<4, 4, T, Q>& m) {
			*this = QuaternionCast(m);
		}

		template<typename T, qualifier Q>
		Quaternion<T, Q>::operator Matrix<3, 3, T, Q>() const {
			return Matrix3x3Cast(*this);
		}

		template<typename T, qualifier Q>
		Quaternion<T, Q>::operator Matrix<4, 4, T, Q>() const {
			return Matrix4x4Cast(*this);
		}

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q>& Quaternion<T, Q>::operator=(const Quaternion<T, Q>& q) {
			this->w = q.w;
			this->x = q.x;
			this->y = q.y;
			this->z = q.z;
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Quaternion<T, Q>& Quaternion<T, Q>::operator=(const Quaternion<U, Q>& q) {
			this->w = static_cast<T>(q.w);
			this->x = static_cast<T>(q.x);
			this->y = static_cast<T>(q.y);
			this->z = static_cast<T>(q.z);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Quaternion<T, Q>& Quaternion<T, Q>::operator+=(const Quaternion<U, Q>& q) {
			this->w += static_cast<T>(q.w);
			this->x += static_cast<T>(q.x);
			this->y += static_cast<T>(q.y);
			this->z += static_cast<T>(q.z);
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Quaternion<T, Q>& Quaternion<T, Q>::operator-=(const Quaternion<U, Q>& q) {
			this->w -= static_cast<T>(q.w);
			this->x -= static_cast<T>(q.x);
			this->y -= static_cast<T>(q.y);
			this->z -= static_cast<T>(q.z);
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Quaternion<T, Q>& Quaternion<T, Q>::operator*=(const Quaternion<U, Q>& q) {
			const Quaternion<T, Q> p(*this);
			this->w = p.w * q.w - p.x * q.x - p.y * q.y - p.z * q.z;
			this->x = p.w * q.x + p.x * q.w + p.y * q.z - p.z * q.y;
			this->y = p.w * q.y + p.y * q.w + p.z * q.x - p.x * q.z;
			this->z = p.w * q.z + p.z * q.w + p.x * q.y - p.y * q.x;
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Quaternion<T, Q>& Quaternion<T, Q>::operator*=(U scalar) {
			this->w *= scalar;
			this->x *= scalar;
			this->y *= scalar;
			this->z *= scalar;
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Quaternion<T, Q>& Quaternion<T, Q>::operator/=(U scalar) {
			this->w /= scalar;
			this->x /= scalar;
			this->y /= scalar;
			this->z /= scalar;
			return *this;
		}

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator+(const Quaternion<T, Q>& q) {
			return q;
		}

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator-(const Quaternion<T, Q>& q) {
			return Quaternion<T, Q>(-q.w, -q.x, -q.y, -q.z);
		}

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator+(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2) {
			return Quaternion<T, Q>(q1) += q2;
		}

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator-(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2) {
			return Quaternion<T, Q>(q1) -= q2;
		}

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator*(const Quaternion<T, Q>& q, const T& scalar) {
			return Quaternion<T, Q>(q) *= scalar;
		}

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator*(const T& scalar, const Quaternion<T, Q>& q) {
			return Quaternion<T, Q>(q) *= scalar;
		}

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator*(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2) {
			return Quaternion<T, Q>(q1) *= q2;
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(const Quaternion<T, Q>& q, const Vector<3, T, Q>& v) {
			const Vector<3, T, Q> qv(q.x, q.y, q.z);
			const Vector<3, T, Q> uv(cross(qv, v));
			const Vector<3, T, Q> uuv(cross(qv, uv));

			return v + ((uv * q.w) + uuv) * static_cast<T>(2);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(const Vector<3, T, Q>& v, const Quaternion<T, Q>& q) {
			return inverse(q) * v;
		}

		template<typename T, qualifier Q>
		constexpr Vector<4, T, Q> operator*(const Quaternion<T, Q>& q, const Vector<4, T, Q>& v) {
			return Vector<4, T, Q>(q * Vector<3, T, Q>(v), v.w);
		}

		template<typename T, qualifier Q>
		constexpr Vector<4, T, Q> operator*(const Vector<4, T, Q>& v, const Quaternion<T, Q>& q) {
			return inverse(q) * v;
		}

		template<typename T, qualifier Q>
		constexpr Quaternion<T, Q> operator/(const Quaternion<T, Q>& q, const T& scalar) {
			return Quaternion<T, Q>(q) /= scalar;
		}

		template<typename T, qualifier Q>
		constexpr bool operator==(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2) {
			return q1.w == q2.w && q1.x == q2.x && q1.y == q2.y && q1.z == q2.z;
		}

		template<typename T, qualifier Q>
		constexpr bool operator!=(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2) {
			return q1.w != q2.w || q1.x != q2.x || q1.y != q2.y || q1.z != q2.z;
		}
	}
}