#pragma once

#include "Snow/Math/Common/Exponent.h"

namespace Snow {
	namespace Math {

		template<typename T, qualifier Q>
		inline T length(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		inline Quaternion<T, Q> normalize(const Quaternion<T, Q>& q);

		template<typename T, qualifier Q>
		inline T dot(const Quaternion<T, Q>& x, const Quaternion<T, Q>& y);

		template<typename T, qualifier Q>
		inline Quaternion<T, Q> cross(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2);
	}
}
#include "QuaternionGeometric.inl"