#pragma once

namespace Snow {
	namespace Math {

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> conjugate(const Complex<T, Q>& comp) {
			return Complex<T, Q>(comp.x, -comp.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> reciprocal(const Complex<T, Q>& comp) {
			return Complex<T, Q>((comp.x) / ((comp.x * comp.x) + (comp.y * comp.y)), (-comp.y) / ((comp.x * comp.x) + (comp.y * comp.y)));
		}

		template<typename T, qualifier Q>
		inline constexpr Complex<T, Q> scaleXY(const Complex<T, Q>& comp, const Vector<2, T, Q>& vec) {
			return Complex<T, Q>(comp.x * vec.x, comp.y * vec.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> Vector2Cast(const Complex<T, Q>& comp) {
			return Vector<2, T, Q>(comp.x, comp.y);
		}
	}
}