#pragma once

#include "Snow/Math/Common/Qualifier.h"

namespace Snow {
	namespace Math {
		template<typename T, qualifier Q>
		T& Vector<4, T, Q>::operator[](typename Vector<4, T, Q>::length_type i) {
			switch (i) {
			default:
			case 0:
				return x;
			case 1:
				return y;
			case 2:
				return z;
			case 3:
				return w;
			}
		}

		template<typename T, qualifier Q>
		const T& Vector<4, T, Q>::operator[](typename Vector<4, T, Q>::length_type i) const {
			//assert i >=0 && i<length_type
			switch (i) {
			default:
			case 0:
				return x;
			case 1:
				return y;
			case 2:
				return z;
			case 3:
				return w;
			}
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q>::Vector() :
			x(0), y(0), z(0), w(0) {}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<4, T, Q>& v) :
			x(v.x), y(v.y), z(v.z), w(v.w) {}

		// explicit constructors
		template<typename T, qualifier Q>
		template<qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<4, T, P>& v) :
			x(v.x), y(v.y), z(v.z), w(v.w) {}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q>::Vector(T scalar) :
			x(scalar), y(scalar), z(scalar), w(scalar) {}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q>::Vector(T x, T y, T z, T w) :
			x(x), y(y), z(z), w(w) {}

		//conversion scalar constructors
		template<typename T, qualifier Q>
		template<typename U, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<1, U, P>& v) :
			x(static_cast<T>(v.x)), y(static_cast<T>(v.x)), z(static_cast<T>(v.x)), w(static_cast<T>(v.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(A x, B y, C z, D w) :
			x(static_cast<T>(x)), y(static_cast<T>(y)), z(static_cast<T>(z)), w(static_cast<T>(w)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(A x, B y, C z, const Vector<1, D, Q>& w):
			x(static_cast<T>(x)), y(static_cast<T>(y)), z(static_cast<T>(z)), w(static_cast<T>(w.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(A x, B y, const Vector<1, C, Q>& z, D w) :
			x(static_cast<T>(x)), y(static_cast<T>(y)), z(static_cast<T>(z.x)), w(static_cast<T>(w)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(A x, B y, const Vector<1, C, Q>& z, const Vector<1, D, Q>& w):
			x(static_cast<T>(x)), y(static_cast<T>(y)), z(static_cast<T>(z.x)), w(static_cast<T>(w.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(A x, const Vector<1, B, Q>& y, C z, D w):
			x(static_cast<T>(x)), y(static_cast<T>(y.x)), z(static_cast<T>(z)), w(static_cast<T>(w)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(A x, const Vector<1, B, Q>& y, C z, const Vector<1, D, Q>& w) :
			x(static_cast<T>(x)), y(static_cast<T>(y.x)), z(static_cast<T>(z)), w(static_cast<T>(w.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(A x, const Vector<1, B, Q>& y, const Vector<1, C, Q>& z, D w) :
			x(static_cast<T>(x)), y(static_cast<T>(y.x)), z(static_cast<T>(z.x)), w(static_cast<T>(w)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(A x, const Vector<1, B, Q>& y, const Vector<1, C, Q>& z, const Vector<1, D, Q>& w):
			x(static_cast<T>(x)), y(static_cast<T>(y.x)), z(static_cast<T>(z.x)), w(static_cast<T>(w.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<1, A, Q>& x, B y, C z, D w) :
			x(static_cast<T>(x.x)), y(static_cast<T>(y)), z(static_cast<T>(z)), w(static_cast<T>(w)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<1, A, Q>& x, B y, C z, const Vector<1, D, Q>& w):
			x(static_cast<T>(x.x)), y(static_cast<T>(y)), z(static_cast<T>(z)), w(static_cast<T>(w.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<1, A, Q>& x, B y, const Vector<1, C, Q>& z, D w) :
			x(static_cast<T>(x.x)), y(static_cast<T>(y)), z(static_cast<T>(z.x)), w(static_cast<T>(w)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<1, A, Q>& x, B y, const Vector<1, C, Q>& z, const Vector<1, D, Q>& w) :
			x(static_cast<T>(x.x)), y(static_cast<T>(y)), z(static_cast<T>(z.x)), w(static_cast<T>(w.x)) {}
		
		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<1, A, Q>& x, const Vector<1, B, Q>& y, C z, D w) :
			x(static_cast<T>(x.x)), y(static_cast<T>(y.x)), z(static_cast<T>(z)), w(static_cast<T>(w)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<1, A, Q>& x, const Vector<1, B, Q>& y, C z, const Vector<1, D, Q>& w) :
			x(static_cast<T>(x.x)), y(static_cast<T>(y.x)), z(static_cast<T>(z)), w(static_cast<T>(w.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<1, A, Q>& x, const Vector<1, B, Q>& y, const Vector<1, C, Q>& z, D w) :
			x(static_cast<T>(x.x)), y(static_cast<T>(y.x)), z(static_cast<T>(z.x)), w(static_cast<T>(w)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, typename D>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<1, A, Q>& x, const Vector<1, B, Q>& y, const Vector<1, C, Q>& z, const Vector<1, D, Q>& w):
			x(static_cast<T>(x.x)), y(static_cast<T>(y.x)), z(static_cast<T>(z.x)), w(static_cast<T>(w.x)) {}

		//conversion constructors

		//explicit
		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(A x, B y, const Vector<2, C, P>& zw) :
			x(static_cast<T>(x)), y(static_cast<T>(y)), z(static_cast<T>(zw.x)), w(static_cast<T>(zw.y)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(A x, const Vector<1, B, P>& y, const Vector<2, C, P>& zw):
			x(static_cast<T>(x)), y(static_cast<T>(y.x)), z(static_cast<T>(zw.x)), w(static_cast<T>(zw.y)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<1, A, P>& x, B y, const Vector<2, C, P>& zw) :
			x(static_cast<T>(x.x)), y(static_cast<T>(y)), z(static_cast<T>(zw.x)), w(static_cast<T>(zw.y)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<1, A, P>& x, const Vector<1, B, P>& y, const Vector<2, C, P>& zw):
			x(static_cast<T>(x.x)), y(static_cast<T>(y.x)), z(static_cast<T>(zw.x)), w(static_cast<T>(zw.y)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(A x, const Vector<2, B, P>& yz, C w) :
			x(static_cast<T>(x)), y(static_cast<T>(yz.x)), z(static_cast<T>(yz.y)), w(static_cast<T>(w)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(A x, const Vector<2, B, P>& yz, const Vector<1, C, P>& w) :
			x(static_cast<T>(x)), y(static_cast<T>(yz.x)), z(static_cast<T>(yz.y)), w(static_cast<T>(w.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<1, A, P>& x, const Vector<2, B, P>& yz, C w) :
			x(static_cast<T>(x.x)), y(static_cast<T>(yz.x)), z(static_cast<T>(yz.y)), w(static_cast<T>(w)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<1, A, P>& x, const Vector<2, B, P>& yz, const Vector<1, C, P>& w) :
			x(static_cast<T>(x.x)), y(static_cast<T>(yz.x)), z(static_cast<T>(yz.y)), w(static_cast<T>(w.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<2, A, P>& xy, B z, C w) :
			x(static_cast<T>(xy.x)), y(static_cast<T>(xy.y)), z(static_cast<T>(z)), w(static_cast<T>(w)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<2, A, P>& xy, B z, const Vector<1, C, P>& w) :
			x(static_cast<T>(xy.x)), y(static_cast<T>(xy.y)), z(static_cast<T>(z)), w(static_cast<T>(w.x)) {}
		
		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<2, A, P>& xy, const Vector<1, B, P>& z, C w) :
			x(static_cast<T>(xy.x)), y(static_cast<T>(xy.y)), z(static_cast<T>(z.x)), w(static_cast<T>(w)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<2, A, P>& xy, const Vector<1, B, P>& z, const Vector<1, C, P>& w) :
			x(static_cast<T>(xy.x)), y(static_cast<T>(xy.y)), z(static_cast<T>(z.x)), w(static_cast<T>(w.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(A x, const Vector<3, B, P>& yzw) :
			x(static_cast<T>(x)), y(static_cast<T>(yzw.x)), z(static_cast<T>(yzw.y)), w(static_cast<T>(yzw.z)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<1, A, P>& x, const Vector<3, B, P>& yzw) :
			x(static_cast<T>(x.x)), y(static_cast<T>(yzw.x)), z(static_cast<T>(yzw.y)), w(static_cast<T>(yzw.z)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<3, A, P>& xyz, B w) :
			x(static_cast<T>(xyz.x)), y(static_cast<T>(xyz.y)), z(static_cast<T>(xyz.z)), w(static_cast<T>(w)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<3, A, P>& xyz, const Vector<1, B, P>& w) :
			x(static_cast<T>(xyz.x)), y(static_cast<T>(xyz.y)), z(static_cast<T>(xyz.z)), w(static_cast<T>(w.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<2, A, P>& xy, const Vector<2, B, P>& zw) :
			x(static_cast<T>(xy.x)), y(static_cast<T>(xy.y)), z(static_cast<T>(zw.x)), w(static_cast<T>(zw.y)) {}

		template<typename T, qualifier Q>
		template<typename U, qualifier P>
		inline constexpr Vector<4, T, Q>::Vector(const Vector<4, U, P>& v) :
			x(static_cast<T>(v.x)), y(static_cast<T>(v.y)), z(static_cast<T>(v.z)), w(static_cast<T>(v.w)) {}


		//unary arithmetic operators

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator=(const Vector<4, T, Q>& v) {
			this->x = v.x;
			this->y = v.y;
			this->z = v.z;
			this->w = v.w;
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator=(const Vector<4, U, Q>& v) {
			this->x = static_cast<T>(v.x);
			this->y = static_cast<T>(v.y);
			this->z = static_cast<T>(v.z);
			this->w = static_cast<T>(v.w);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator+=(U scalar) {
			this->x += static_cast<T>(scalar);
			this->y += static_cast<T>(scalar);
			this->z += static_cast<T>(scalar);
			this->w += static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator+=(const Vector<1, U, Q>& v) {
			this->x += static_cast<T>(v.x);
			this->y += static_cast<T>(v.x);
			this->z += static_cast<T>(v.x);
			this->w += static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator+=(const Vector<4, U, Q>& v) {
			this->x += static_cast<T>(v.x);
			this->y += static_cast<T>(v.y);
			this->z += static_cast<T>(v.z);
			this->w += static_cast<T>(v.w);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator-=(U scalar) {
			this->x -= static_cast<T>(scalar);
			this->y -= static_cast<T>(scalar);
			this->z -= static_cast<T>(scalar);
			this->w -= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator-=(const Vector<1, U, Q>& v) {
			this->x -= static_cast<T>(v.x);
			this->y -= static_cast<T>(v.x);
			this->z -= static_cast<T>(v.x);
			this->w -= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator-=(const Vector<4, U, Q>& v) {
			this->x -= static_cast<T>(v.x);
			this->y -= static_cast<T>(v.y);
			this->z -= static_cast<T>(v.z);
			this->w -= static_cast<T>(v.w);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator*=(U scalar) {
			this->x *= static_cast<T>(scalar);
			this->y *= static_cast<T>(scalar);
			this->z *= static_cast<T>(scalar);
			this->w *= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator*=(const Vector<1, U, Q>& v) {
			this->x *= static_cast<T>(v.x);
			this->y *= static_cast<T>(v.x);
			this->z *= static_cast<T>(v.x);
			this->w *= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator*=(const Vector<4, U, Q>& v) {
			this->x *= static_cast<T>(v.x);
			this->y *= static_cast<T>(v.y);
			this->z *= static_cast<T>(v.z);
			this->w *= static_cast<T>(v.w);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator/=(U scalar) {
			this->x /= static_cast<T>(scalar);
			this->y /= static_cast<T>(scalar);
			this->z /= static_cast<T>(scalar);
			this->w /= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator/=(const Vector<1, U, Q>& v) {
			this->x /= static_cast<T>(v.x);
			this->y /= static_cast<T>(v.x);
			this->z /= static_cast<T>(v.x);
			this->w /= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator/=(const Vector<4, U, Q>& v) {
			this->x /= static_cast<T>(v.x);
			this->y /= static_cast<T>(v.y);
			this->z /= static_cast<T>(v.z);
			this->w /= static_cast<T>(v.w);
			return *this;
		}

		//increment/decrement operators
		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator++() {
			++this->x;
			++this->y;
			++this->z;
			++this->w;
			return *this;
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator--() {
			--this->x;
			--this->y;
			--this->z;
			--this->w;
			return *this;
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> Vector<4, T, Q>::operator++(int) {
			Vector<4, T, Q> result(*this);
			++ *this;
			return result;
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> Vector<4, T, Q>::operator--(int) {
			Vector<4, T, Q> result(*this);
			-- *this;
			return result;
		}

		// unary bit operators
		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator%=(U scalar) {
			this->x %= static_cast<T>(scalar);
			this->y %= static_cast<T>(scalar);
			this->z %= static_cast<T>(scalar);
			this->w %= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator%=(const Vector<1, U, Q>& v) {
			this->x %= static_cast<T>(v.x);
			this->y %= static_cast<T>(v.x);
			this->z %= static_cast<T>(v.x);
			this->w %= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator%=(const Vector<4, U, Q>& v) {
			this->x %= static_cast<T>(v.x);
			this->y %= static_cast<T>(v.y);
			this->z %= static_cast<T>(v.z);
			this->w %= static_cast<T>(v.w);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator&=(U scalar) {
			this->x &= static_cast<T>(scalar);
			this->y &= static_cast<T>(scalar);
			this->z &= static_cast<T>(scalar);
			this->w &= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator&=(const Vector<1, U, Q>& v) {
			this->x &= static_cast<T>(v.x);
			this->y &= static_cast<T>(v.x);
			this->z &= static_cast<T>(v.x);
			this->w &= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator&=(const Vector<4, U, Q>& v) {
			this->x &= static_cast<T>(v.x);
			this->y &= static_cast<T>(v.y);
			this->z &= static_cast<T>(v.z);
			this->w &= static_cast<T>(v.w);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator|=(U scalar) {
			this->x |= static_cast<T>(scalar);
			this->y |= static_cast<T>(scalar);
			this->z |= static_cast<T>(scalar);
			this->w |= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator|=(const Vector<1, U, Q>& v) {
			this->x |= static_cast<T>(v.x);
			this->y |= static_cast<T>(v.x);
			this->z |= static_cast<T>(v.x);
			this->w |= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator|=(const Vector<4, U, Q>& v) {
			this->x |= static_cast<T>(v.x);
			this->y |= static_cast<T>(v.y);
			this->z |= static_cast<T>(v.z);
			this->w |= static_cast<T>(v.w);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator^=(U scalar) {
			this->x ^= static_cast<T>(scalar);
			this->y ^= static_cast<T>(scalar);
			this->z ^= static_cast<T>(scalar);
			this->w ^= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator^=(const Vector<1, U, Q>& v) {
			this->x ^= static_cast<T>(v.x);
			this->y ^= static_cast<T>(v.x);
			this->z ^= static_cast<T>(v.x);
			this->w ^= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator^=(const Vector<4, U, Q>& v) {
			this->x ^= static_cast<T>(v.x);
			this->y ^= static_cast<T>(v.y);
			this->z ^= static_cast<T>(v.z);
			this->w ^= static_cast<T>(v.w);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator<<=(U scalar) {
			this->x <<= static_cast<T>(scalar);
			this->y <<= static_cast<T>(scalar);
			this->z <<= static_cast<T>(scalar);
			this->w <<= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator<<=(const Vector<1, U, Q>& v) {
			this->x <<= static_cast<T>(v.x);
			this->y <<= static_cast<T>(v.x);
			this->z <<= static_cast<T>(v.x);
			this->w <<= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator<<=(const Vector<4, U, Q>& v) {
			this->x <<= static_cast<T>(v.x);
			this->y <<= static_cast<T>(v.y);
			this->z <<= static_cast<T>(v.z);
			this->w <<= static_cast<T>(v.w);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator>>=(U scalar) {
			this->x >>= static_cast<T>(scalar);
			this->y >>= static_cast<T>(scalar);
			this->z >>= static_cast<T>(scalar);
			this->w >>= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator>>=(const Vector<1, U, Q>& v) {
			this->x >>= static_cast<T>(v.x);
			this->y >>= static_cast<T>(v.x);
			this->z >>= static_cast<T>(v.x);
			this->w >>= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<4, T, Q>& Vector<4, T, Q>::operator>>=(const Vector<4, U, Q>& v) {
			this->x >>= static_cast<T>(v.x);
			this->y >>= static_cast<T>(v.y);
			this->z >>= static_cast<T>(v.z);
			this->w >>= static_cast<T>(v.w);
			return *this;
		}

		//unary operators
		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator+(const Vector<4, T, Q>& v) {
			return v;
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator-(const Vector<4, T, Q>& v) {
			return Vector<4, T, Q>(-v.x, -v.y, -v.z, -v.w);
		}

		//binary arithmetic operators
		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator+(const Vector<4, T, Q>& v, T scalar) {
			return Vector<4, T, Q>(v.x + scalar, v.y + scalar, v.z + scalar, v.w + scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator+(const Vector<4, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<4, T, Q>(v1.x + v2.x, v1.y + v2.x, v1.z + v2.x, v1.w + v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator+(T scalar, const Vector<4, T, Q>& v) {
			return Vector<4, T, Q>(v.x + scalar, v.y + scalar, v.z + scalar, v.w + scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator+(const Vector<1, T, Q> v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x + v2.x, v1.x + v2.y, v1.x + v2.z, v1.x + v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator+(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, v1.w + v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator-(const Vector<4, T, Q>& v, T scalar) {
			return Vector<4, T, Q>(v.x - scalar, v.y - scalar, v.z - scalar, v.w - scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator-(const Vector<4, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<4, T, Q>(v1.x - v2.x, v1.y - v2.x, v1.z - v2.x, v1.w - v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator-(T scalar, const Vector<4, T, Q>& v) {
			return Vector<4, T, Q>(scalar - v.x, scalar - v.y, scalar - v.z, scalar - v.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator-(const Vector<1, T, Q> v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x - v2.x, v1.x - v2.y, v1.x - v2.z, v1.x - v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator-(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, v1.w - v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator*(const Vector<4, T, Q>& v, T scalar) {
			return Vector<4, T, Q>(v.x * scalar, v.y * scalar, v.z * scalar, v.w * scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator*(const Vector<4, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<4, T, Q>(v1.x * v2.x, v1.y * v2.x, v1.z * v2.x, v1.w * v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator*(T scalar, const Vector<4, T, Q>& v) {
			return Vector<4, T, Q>(scalar * v.x, scalar * v.y, scalar * v.z, scalar * v.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator*(const Vector<1, T, Q> v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x * v2.x, v1.x * v2.y, v1.x * v2.z, v1.x * v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator*(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z, v1.w * v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator/(const Vector<4, T, Q>& v, T scalar) {
			return Vector<4, T, Q>(v.x / scalar, v.y / scalar, v.z / scalar, v.w / scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator/(const Vector<4, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<4, T, Q>(v1.x / v2.x, v1.y / v2.x, v1.z / v2.x, v1.w / v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator/(T scalar, const Vector<4, T, Q>& v) {
			return Vector<4, T, Q>(scalar / v.x, scalar / v.y, scalar / v.z, scalar / v.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator/(const Vector<1, T, Q> v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x / v2.x, v1.x / v2.y, v1.x / v2.z, v1.x / v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator/(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z, v1.w / v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator%(const Vector<4, T, Q>& v, T scalar) {
			return Vector<4, T, Q>(v.x % scalar, v.y % scalar, v.z % scalar, v.w % scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator%(const Vector<4, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<4, T, Q>(v1.x % v2.x, v1.y % v2.x, v1.z % v2.x, v1.w % v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator%(T scalar, const Vector<4, T, Q>& v) {
			return Vector<4, T, Q>(scalar % v.x, scalar % v.y, scalar % v.z, scalar % v.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator%(const Vector<1, T, Q> v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x % v2.x, v1.x % v2.y, v1.x % v2.z, v1.x % v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator%(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x % v2.x, v1.y % v2.y, v1.z % v2.z, v1.w % v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator&(const Vector<4, T, Q>& v, T scalar) {
			return Vector<4, T, Q>(v.x & scalar, v.y & scalar, v.z & scalar, v.w & scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator&(const Vector<4, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<4, T, Q>(v1.x & v2.x, v1.y & v2.x, v1.z & v2.x, v1.w & v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator&(T scalar, const Vector<4, T, Q>& v) {
			return Vector<4, T, Q>(scalar & v.x, scalar & v.y, scalar & v.z, scalar & v.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator&(const Vector<1, T, Q> v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x & v2.x, v1.x & v2.y, v1.x & v2.z, v1.x & v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator&(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x & v2.x, v1.y & v2.y, v1.z & v2.z, v1.w & v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator|(const Vector<4, T, Q>& v, T scalar) {
			return Vector<4, T, Q>(v.x | scalar, v.y | scalar, v.z | scalar, v.w | scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator|(const Vector<4, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<4, T, Q>(v1.x | v2.x, v1.y | v2.x, v1.z | v2.x, v1.w | v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator|(T scalar, const Vector<4, T, Q>& v) {
			return Vector<4, T, Q>(scalar | v.x, scalar | v.y, scalar | v.z, scalar | v.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator|(const Vector<1, T, Q> v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x | v2.x, v1.x | v2.y, v1.x | v2.z, v1.x | v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator|(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x | v2.x, v1.y | v2.y, v1.z | v2.z, v1.w | v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator^(const Vector<4, T, Q>& v, T scalar) {
			return Vector<4, T, Q>(v.x ^ scalar, v.y ^ scalar, v.z ^ scalar, v.w ^ scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator^(const Vector<4, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<4, T, Q>(v1.x ^ v2.x, v1.y ^ v2.x, v1.z ^ v2.x, v1.w ^ v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator^(T scalar, const Vector<4, T, Q>& v) {
			return Vector<4, T, Q>(scalar ^ v.x, scalar ^ v.y, scalar ^ v.z, scalar ^ v.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator^(const Vector<1, T, Q> v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x ^ v2.x, v1.x ^ v2.y, v1.x ^ v2.z, v1.x ^ v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator^(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x ^ v2.x, v1.y ^ v2.y, v1.z ^ v2.z, v1.w ^ v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator<<(const Vector<4, T, Q>& v, T scalar) {
			return Vector<4, T, Q>(v.x << scalar, v.y << scalar, v.z << scalar, v.w << scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator<<(const Vector<4, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<4, T, Q>(v1.x << v2.x, v1.y << v2.x, v1.z << v2.x, v1.w << v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator<<(T scalar, const Vector<4, T, Q>& v) {
			return Vector<4, T, Q>(scalar << v.x, scalar << v.y, scalar << v.z, scalar << v.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator<<(const Vector<1, T, Q> v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x << v2.x, v1.x << v2.y, v1.x << v2.z, v1.x << v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator<<(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x << v2.x, v1.y << v2.y, v1.z << v2.z, v1.w << v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator>>(const Vector<4, T, Q>& v, T scalar) {
			return Vector<4, T, Q>(v.x >> scalar, v.y >> scalar, v.z >> scalar, v.w >> scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator>>(const Vector<4, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<4, T, Q>(v1.x >> v2.x, v1.y >> v2.x, v1.z >> v2.x, v1.w >> v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator>>(T scalar, const Vector<4, T, Q>& v) {
			return Vector<4, T, Q>(scalar >> v.x, scalar >> v.y, scalar >> v.z, scalar >> v.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator>>(const Vector<1, T, Q> v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x >> v2.x, v1.x >> v2.y, v1.x >> v2.z, v1.x >> v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator>>(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
			return Vector<4, T, Q>(v1.x >> v2.x, v1.y >> v2.y, v1.z >> v2.z, v1.w >> v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator~(const Vector<4, T, Q>& v) {
			return Vector<4, T, Q>(~v.x, ~v.y, ~v.z, ~v.w);
		}

		template<typename T, qualifier Q>
		inline constexpr bool operator==(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
			return v1.x == v2.x && v1.y == v2.y && v1.z == v2.z && v1.w == v2.w;
		}

		template<typename T, qualifier Q>
		inline constexpr bool operator!=(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
			return v1.x != v2.x || v1.y != v2.y || v1.z != v2.z || v1.w != v2.w;
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, bool, Q> operator&&(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
			return Vector<4, bool, Q>(v1.x && v2.x, v1.y && v2.y, v1.z && v2.z, v1.w && v2.w);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<4, bool, Q> operator||(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
			return Vector<4, bool, Q>(v1.x || v2.x, v1.y || v2.y, v1.z || v2.z, v1.w || v2.w);
		}
	}
}