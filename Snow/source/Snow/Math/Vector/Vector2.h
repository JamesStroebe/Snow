#pragma once

#include "Snow/Core/Base.h"

#include "Snow/Math/Common/Qualifier.h"

#include "Snow/Math/Vector/SwizzleFunction.h"
#include <math.h>


namespace Snow {
	namespace Math {
		 //precision, types, 
		template<typename T, qualifier Q>
		struct Vector<2, T, Q> {

			typedef T value_type;
			typedef Vector<2, T, Q> type;
			typedef Vector<2, bool, Q> bool_type;

			//typename storage<2, T, is_aligned<Q>::value>::type data;

			SNOW_SWIZZLE_GEN_VECTOR_FROM_VECTOR2(T, Q)

			typedef size_t length_type;
			inline static constexpr  length_type length() { return 2; }

			inline constexpr T& operator[](length_type i);
			inline constexpr const T& operator[](length_type i) const;
			
			// implicit constructors
			inline constexpr Vector();
			inline constexpr Vector(const Vector& v);
			template<qualifier P>
			inline constexpr Vector(const Vector<2, T, P>& v);

			// Explicit constructors
			inline constexpr explicit Vector(T scalar);
			inline constexpr Vector(T x, T y);

			// conversion constructors
			template<typename U, qualifier P>
			inline constexpr explicit Vector(const Vector<1, U, P>& v);

			// explicit conversion constuctors
			template<typename A, typename B>
			inline constexpr Vector(A x, B y);

			template<typename A, typename B>
			inline constexpr Vector(const Vector<1, A, Q>& x, B y);

			template<typename A, typename B>
			inline constexpr Vector(A x, const Vector<1, B, Q>& y);

			template<typename A, typename B>
			inline constexpr Vector(const Vector<1, A, Q>& x, const Vector<1, B, Q>& y);

			// conversion vector constructors

			//explicit
			template<typename U, qualifier P>
			inline constexpr Vector(const Vector<3, U, P>& v);

			template<typename U, qualifier P>
			inline constexpr Vector(const Vector<4, U, P>& v);

			template<typename U, qualifier P>
			inline constexpr Vector(const Vector<2, U, P>& v);

			

			//unary arithmetic operators

			inline constexpr Vector<2, T, Q>& operator=(const Vector& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator=(const Vector<2, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator+=(U scalar);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator+=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator+=(const Vector<2, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator-=(U scalar);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator-=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator-=(const Vector<2, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator*=(U scalar);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator*=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator*=(const Vector<2, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator/=(U scalar);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator/=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator/=(const Vector<2, U, Q>& v);

			// increment and decrement operators
			inline constexpr Vector<2, T, Q>& operator++();
			inline constexpr Vector<2, T, Q>& operator--();
			inline constexpr Vector<2, T, Q> operator++(int);
			inline constexpr Vector<2, T, Q> operator--(int);

			//unary bit operators
			template<typename U>
			inline constexpr Vector<2, T, Q>& operator%=(U scalar);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator%=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator%=(const Vector<2, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator&=(U scalar);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator&=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator&=(const Vector<2, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator|=(U scalar);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator|=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator|=(const Vector<2, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator^=(U scalar);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator^=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator^=(const Vector<2, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator<<=(U scalar);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator<<=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator<<=(const Vector<2, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator>>=(U scalar);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator>>=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<2, T, Q>& operator>>=(const Vector<2, U, Q>& v);


			union { T x, r, s; };
			union { T y, g, t; };

			
		};

		//unary operators
		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator+(const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator-(const Vector<2, T, Q>& v);

		//binary operators
		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator+(const Vector<2, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator+(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator+(T scalar, const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator+(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator+(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator-(const Vector<2, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator-(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator-(T scalar, const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator-(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator-(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator*(const Vector<2, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator*(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator*(T scalar, const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator*(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator*(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator/(const Vector<2, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator/(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator/(T scalar, const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator/(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator/(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2);

		//bitwise binary operators
		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator%(const Vector<2, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator%(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator%(T scalar, const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator%(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator%(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator&(const Vector<2, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator&(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator&(T scalar, const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator&(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator&(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator|(const Vector<2, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator|(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator|(T scalar, const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator|(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator|(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator^(const Vector<2, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator^(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator^(T scalar, const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator^(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator^(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator<<(const Vector<2, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator<<(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator<<(T scalar, const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator<<(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator<<(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator>>(const Vector<2, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator>>(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator>>(T scalar, const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator>>(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator>>(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator~(const Vector<2, T, Q>& v);


		//boolean operators
		template<typename T, qualifier Q>
		inline constexpr bool operator==(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr bool operator!=(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, bool, Q> operator&&(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<2, bool, Q> operator||(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2);


		typedef Vector<2, float, qualifier::defaultp> Vector2f;

		typedef Vector<2, float, qualifier::highp> Vector2f_highp;
		typedef Vector<2, float, qualifier::mediump> Vector2f_mediump;
		typedef Vector<2, float, qualifier::lowp> Vector2f_lowp;

		typedef Vector<2, bool, qualifier::defaultp> Vector2b;

		typedef Vector<2, bool, qualifier::highp> Vector2b_highp;
		typedef Vector<2, bool, qualifier::mediump> Vector2b_mediump;
		typedef Vector<2, bool, qualifier::lowp> Vector2b_lowp;

		typedef Vector<2, double, defaultp> Vector2d;

		typedef Vector<2, double, qualifier::highp> Vector2d_highp;
		typedef Vector<2, double, qualifier::mediump> Vector2d_mediump;
		typedef Vector<2, double, qualifier::lowp> Vector2d_lowp;

		typedef Vector<2, int, qualifier::defaultp> Vector2i;

		typedef Vector<2, int, qualifier::highp> Vector2i_highp;
		typedef Vector<2, int, qualifier::mediump> Vector2i_mediump;
		typedef Vector<2, int, qualifier::lowp> Vector2i_lowp;

		typedef Vector<2, unsigned int, qualifier::defaultp> Vector2u;

		typedef Vector<2, unsigned int, qualifier::highp> Vector2u_highp;
		typedef Vector<2, unsigned int, qualifier::mediump> Vector2u_mediump;
		typedef Vector<2, unsigned int, qualifier::lowp> Vector2u_lowp;
	}
}
#include "Snow/Math/Vector/Vector2.inl"