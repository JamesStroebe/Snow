#pragma once

#include "Snow/Math/Common/Qualifier.h"

namespace Snow {
	namespace Math {
		template<typename T, qualifier Q>
		struct Vector<4, T, Q> {

			typedef T value_type;
			typedef Vector<4, T, Q> type;
			typedef Vector<4, bool, Q> bool_type;

			//typename storage<4, T, is_aligned<Q>::value>::type data;

			SNOW_SWIZZLE_GEN_VECTOR_FROM_VECTOR4(T, Q)

			typedef size_t length_type;
			static length_type length() { return 4; }

			T& operator[](length_type i);
			const T& operator[](length_type i) const;

			// implicit constructors
			inline constexpr Vector();
			inline constexpr Vector(const Vector& v);
			template<qualifier P>
			inline constexpr Vector(const Vector<4, T, P>& v);

			// Explicit constructors
			inline constexpr explicit Vector(T scalar);
			inline constexpr Vector(T x, T y, T z, T w);

			// conversion constructors
			template<typename U, qualifier P>
			inline constexpr explicit Vector(const Vector<1, U, P>& v);

			// explicit conversion constuctors
			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(A x, B y, C z, D w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(A x, B y, C z, const Vector<1, D, Q>& w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(A x, B y, const Vector<1, C, Q>& z, D w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(A x, B y, const Vector<1, C, Q>& z, const Vector<1, D, Q>& w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(A x, const Vector<1, B, Q>& y, C z, D w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(A x, const Vector<1, B, Q>& y, C z, const Vector<1, D, Q>& w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(A x, const Vector<1, B, Q>& y, const Vector<1, C, Q>& z, D w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(A x, const Vector<1, B, Q>& y, const Vector<1, C, Q>& z, const Vector<1, D, Q>& w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(const Vector<1, A, Q>& x, B y, C z, D w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(const Vector<1, A, Q>& x, B y, C z, const Vector<1, D, Q>& w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(const Vector<1, A, Q>& x, B y, const Vector<1, C, Q>& z, D w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(const Vector<1, A, Q>& x, B y, const Vector<1, C, Q>& z, const Vector<1, D, Q>& w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(const Vector<1, A, Q>& x, const Vector<1, B, Q>& y, C z, D w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(const Vector<1, A, Q>& x, const Vector<1, B, Q>& y, C z, const Vector<1, D, Q>& w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(const Vector<1, A, Q>& x, const Vector<1, B, Q>& y, const Vector<1, C, Q>& z, D w);

			template<typename A, typename B, typename C, typename D>
			inline constexpr Vector(const Vector<1, A, Q>& x, const Vector<1, B, Q>& y, const Vector<1, C, Q>& z, const Vector<1, D, Q>& w);

			//conversion constructors

			//explicit
			template<typename A, typename B, typename C, qualifier P>
			inline constexpr Vector(A x, B y, const Vector<2, C, P>& zw);

			template<typename A, typename B, typename C, qualifier P>
			inline constexpr Vector(A x, const Vector<1, B, P>& y, const Vector<2, C, P>& zw);

			template<typename A, typename B, typename C, qualifier P>
			inline constexpr Vector(const Vector<1, A, P>& x, B y, const Vector<2, C, P>& zw);

			template<typename A, typename B, typename C, qualifier P>
			inline constexpr Vector(const Vector<1, A, P>& x, const Vector<1, B, P>& y, const Vector<2, C, P>& zw);

			template<typename A, typename B, typename C, qualifier P>
			inline constexpr Vector(A x, const Vector<2, B, P>& yz, C w);

			template<typename A, typename B, typename C, qualifier P>
			inline constexpr Vector(A x, const Vector<2, B, P>& yz, const Vector<1, C, P>& w);

			template<typename A, typename B, typename C, qualifier P>
			inline constexpr Vector(const Vector<1, A, P>& x, const Vector<2, B, P>& yz, C w);

			template<typename A, typename B, typename C, qualifier P>
			inline constexpr Vector(const Vector<1, A, P>& x, const Vector<2, B, P>& yz, const Vector<1, C, P>& w);

			template<typename A, typename B, typename C, qualifier P>
			inline constexpr Vector(const Vector<2, A, P>& xy, B z, C w);

			template<typename A, typename B, typename C, qualifier P>
			inline constexpr Vector(const Vector<2, A, P>& xy, B z, const Vector<1, C, P>& w);

			template<typename A, typename B, typename C, qualifier P>
			inline constexpr Vector(const Vector<2, A, P>& xy, const Vector<1, B, P>& z, C w);

			template<typename A, typename B, typename C, qualifier P>
			inline constexpr Vector(const Vector<2, A, P>& xy, const Vector<1, B, P>& z, const Vector<1, C, P>& w);

			template<typename A, typename B, qualifier P>
			inline constexpr Vector(A x, const Vector<3, B, P>& yzw);

			template<typename A, typename B, qualifier P>
			inline constexpr Vector(const Vector<1, A, P>& x, const Vector<3, B, P>& yzw);

			template<typename A, typename B, qualifier P>
			inline constexpr Vector(const Vector<3, A, P>& xyz, B w);

			template<typename A, typename B, qualifier P>
			inline constexpr Vector(const Vector<3, A, P>& xyz, const Vector<1, B, P>& w);

			template<typename A, typename B, qualifier P>
			inline constexpr Vector(const Vector<2, A, P>& xy, const Vector<2, B, P>& zw);

			//explicit constructor
			template<typename U, qualifier P>
			inline constexpr Vector(const Vector<4, U, P>& v);


			//unary arithmetic operators

			inline constexpr Vector<4, T, Q>& operator=(const Vector& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator=(const Vector<4, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator+=(U scalar);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator+=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator+=(const Vector<4, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator-=(U scalar);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator-=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator-=(const Vector<4, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator*=(U scalar);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator*=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator*=(const Vector<4, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator/=(U scalar);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator/=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator/=(const Vector<4, U, Q>& v);

			// increment and decrement operators
			inline constexpr Vector<4, T, Q>& operator++();
			inline constexpr Vector<4, T, Q>& operator--();
			inline constexpr Vector<4, T, Q> operator++(int);
			inline constexpr Vector<4, T, Q> operator--(int);

			//unary bit operators
			template<typename U>
			inline constexpr Vector<4, T, Q>& operator%=(U scalar);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator%=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator%=(const Vector<4, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator&=(U scalar);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator&=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator&=(const Vector<4, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator|=(U scalar);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator|=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator|=(const Vector<4, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator^=(U scalar);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator^=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator^=(const Vector<4, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator<<=(U scalar);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator<<=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator<<=(const Vector<4, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator>>=(U scalar);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator>>=(const Vector<1, U, Q>& v);

			template<typename U>
			inline constexpr Vector<4, T, Q>& operator>>=(const Vector<4, U, Q>& v);

			union { T x, r, s; };
			union { T y, g, t; };
			union { T z, b, p; };
			union { T w, a, q; };
		};

		//unary operators
		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator+(const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator-(const Vector<4, T, Q>& v);

		//binary operators
		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator+(const Vector<4, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator+(const Vector<4, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator+(T scalar, const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator+(const Vector<1, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator+(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator-(const Vector<4, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator-(const Vector<4, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator-(T scalar, const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator-(const Vector<1, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator-(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator*(const Vector<4, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator*(const Vector<4, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator*(T scalar, const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator*(const Vector<1, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator*(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator/(const Vector<4, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator/(const Vector<4, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator/(T scalar, const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator/(const Vector<1, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator/(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2);

		//bitwise binary operators
		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator%(const Vector<4, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator%(const Vector<4, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator%(T scalar, const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator%(const Vector<1, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator%(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator&(const Vector<4, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator&(const Vector<4, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator&(T scalar, const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator&(const Vector<1, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator&(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator|(const Vector<4, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator|(const Vector<4, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator|(T scalar, const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator|(const Vector<1, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator|(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator^(const Vector<4, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator^(const Vector<4, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator^(T scalar, const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator^(const Vector<1, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator^(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator<<(const Vector<4, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator<<(const Vector<4, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator<<(T scalar, const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator<<(const Vector<1, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator<<(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator>>(const Vector<4, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator>>(const Vector<4, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator>>(T scalar, const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator>>(const Vector<1, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator>>(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, T, Q> operator~(const Vector<4, T, Q>& v);

		//boolean operators
		template<typename T, qualifier Q>
		inline constexpr bool operator==(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr bool operator!=(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, bool, Q> operator&&(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2);

		template<typename T, qualifier Q>
		inline constexpr Vector<4, bool, Q> operator||(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2);


		typedef Vector<4, float, qualifier::defaultp> Vector4f;

		typedef Vector<4, float, qualifier::highp> Vector4f_highp;
		typedef Vector<4, float, qualifier::mediump> Vector4f_mediump;
		typedef Vector<4, float, qualifier::lowp> Vector4f_lowp;

		typedef Vector<4, bool, qualifier::defaultp> Vector4b;

		typedef Vector<4, bool, qualifier::highp> Vector4b_highp;
		typedef Vector<4, bool, qualifier::mediump> Vector4b_mediump;
		typedef Vector<4, bool, qualifier::lowp> Vector4b_lowp;

		typedef Vector<4, double, qualifier::defaultp> Vector4d;

		typedef Vector<4, double, qualifier::highp> Vector4d_highp;
		typedef Vector<4, double, qualifier::mediump> Vector4d_mediump;
		typedef Vector<4, double, qualifier::lowp> Vector4d_lowp;

		typedef Vector<4, int, qualifier::defaultp> Vector4i;

		typedef Vector<4, int, qualifier::highp> Vector4i_highp;
		typedef Vector<4, int, qualifier::mediump> Vector4i_mediump;
		typedef Vector<4, int, qualifier::lowp> Vector4i_lowp;

		typedef Vector<4, unsigned int, qualifier::defaultp> Vector4u;

		typedef Vector<4, unsigned int, qualifier::highp> Vector4u_highp;
		typedef Vector<4, unsigned int, qualifier::mediump> Vector4u_mediump;
		typedef Vector<4, unsigned int, qualifier::lowp> Vector4u_lowp;
	}
}
#include "Snow/Math/Vector/Vector4.inl"