#pragma once

#include "Snow/Math/Common/Qualifier.h"

namespace Snow {
	namespace Math {

		// Common Functions

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> abs(const Vector<L, T, Q>& v);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> ceil(const Vector<L, T, Q>& v);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> clamp(const Vector<L, T, Q>& v1, T min, T max);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> clamp(const Vector<L, T, Q>& v1, const Vector<L, T, Q>& min, const Vector<L, T, Q>& max);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> floor(const Vector<L, T, Q>& v);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> fract(const Vector<L, T, Q>& v);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> frexp(const Vector<L, T, Q>& vec, Vector<L, int, Q>& exp);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> isInf(const Vector<L, T, Q>& v);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> isNan(const Vector<L, T, Q>& v);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> ldexp(const Vector<L, T, Q>& v, const Vector<L, int, Q>& exp);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> max(const Vector<L, T, Q>& vec, T value);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> max(T value, const Vector<L, T, Q>& vec);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> max(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> min(const Vector<L, T, Q>& vec, T value);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> min(T value, const Vector<L, T, Q>& vec);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> min(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, typename T, typename U, qualifier Q>
		inline constexpr Vector<L, T, Q> mix(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2, U a);

		template<size_t L, typename T, typename U, qualifier Q>
		inline constexpr Vector<L, T, Q> mix(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2, const Vector<L, U, Q>& a);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> mod(const Vector<L, T, Q>& vec, const T& value);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> mod(const Vector<L, T, Q>& vec, const Vector<L, T, Q>& modVec);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> modf(const Vector<L, T, Q>& vec, const T& value);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> modf(const Vector<L, T, Q>& vec, const Vector<L, T, Q>& modVec);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> round(const Vector<L, T, Q>& vec);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> roundEven(const Vector<L, T, Q>& vec);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> sign(const Vector<L, T, Q>& vec);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> smoothstep(T edge0, T edge1, const Vector<L, T, Q>& vec);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> smoothstep(const Vector<L, T, Q>& edge0, const Vector<L, T, Q>& edge1, const Vector<L, T, Q>& vec);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> step(T edge, const Vector<L, T, Q>& vec);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> step(const Vector<L, T, Q>& edge, const Vector<L, T, Q>& x);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> trunc(const Vector<L, T, Q>& v);

		template<size_t L, qualifier Q>
		Vector<L, int, Q> floatBitsToInt(const Vector<L, float, Q>& vec);

		template<size_t L, qualifier Q>
		Vector<L, unsigned int, Q> floatBitsToUInt(const Vector<L, float, Q>& v);

		template<size_t L, qualifier Q>
		Vector<L, float, Q> intBitsToFloat(const Vector<L, int, Q>& v);

		template<size_t L, qualifier Q>
		Vector<L, float, Q> uintBitsToFloat(const Vector<L, unsigned int, Q>& v);

		// Exponential Functions

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> sqrt(const Vector<L, T, Q>& vec);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> inversesqrt(const Vector<L, T, Q>& vec);

		// Geometric Functions

		template<typename T, qualifier Q>
		inline constexpr Vector<3, T, Q> cross(const Vector<3, T, Q>& vec1, const Vector<3, T, Q>& vec2);

		template<size_t L, typename T, qualifier Q>
		inline constexpr T distance(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, typename T, qualifier Q>
		inline constexpr T dot(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> faceForward(const Vector<L, T, Q>& normal, const Vector<L, T, Q>& incedent, const Vector<L, T, Q>& Nref);

		template<size_t L, typename T, qualifier Q>
		inline constexpr T length(const Vector<L, T, Q>& vec);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> normalize(const Vector<L, T, Q>& vec);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> reflect(const Vector<L, T, Q>& incident, const Vector<L, T, Q>& normal);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> refract(const Vector<L, T, Q>& incident, const Vector<L, T, Q>& normal, T eta);

		// Relational Functions

		template<size_t L, qualifier Q>
		inline constexpr bool any(const Vector<L, bool, Q>& vec);

		template<size_t L, qualifier Q>
		inline constexpr bool all(const Vector<L, bool, Q>& vec);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> operator<(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> lessThan(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> operator<=(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> lessThanEqual(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> operator>=(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> greaterThan(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> operator<=(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> greaterThanEqual(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> operator==(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> equal(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> equal(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2, const T& epsilon);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> equal(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2, const Vector<L, T, Q>& epsilon);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> operator!=(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);
		
		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> notEqual(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2);

		template<size_t L, qualifier Q>
		inline constexpr Vector<L, bool, Q> operator~(const Vector<L, bool, Q>& vec);

		template<size_t L, qualifier Q>
		inline constexpr Vector<L, bool, Q> not(const Vector<L, bool, Q>& vec);

		template<size_t L, qualifier Q>
		inline constexpr bool all(const Vector<L, bool, Q>& vec);

		template<size_t L, qualifier Q>
		inline constexpr bool any(const Vector<L, bool, Q>& vec);


		// Trigonomic Functions

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> radians(const Vector<L, T, Q>& vecDeg);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> degrees(const Vector<L, T, Q>& vecRad);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> sin(const Vector<L, T, Q>& vecRad);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> cos(const Vector<L, T, Q>& vecRad);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> tan(const Vector<L, T, Q>& vecRad);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> asin(const Vector<L, T, Q>& vecRad);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> acos(const Vector<L, T, Q>& vecRad);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> atan(const Vector<L, T, Q>& vecRad);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> atan2(const Vector<L, T, Q>& yVecRad, const Vector<L, T, Q>& xVecRad);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> sinh(const Vector<L, T, Q>& vecRad);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> cosh(const Vector<L, T, Q>& vecRad);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> tanh(const Vector<L, T, Q>& vecRad);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> asinh(const Vector<L, T, Q>& vecRad);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> acosh(const Vector<L, T, Q>& vecRad);

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> atanh(const Vector<L, T, Q>& vecRad);
	}
}

#include "VectorFunctions.inl"