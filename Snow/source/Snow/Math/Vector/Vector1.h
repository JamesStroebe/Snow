#pragma once

#include "Snow/Math/Common/Qualifier.h"

#include "Snow/Math/Vector/SwizzleFunction.h"

namespace Snow {
	namespace Math {
		template<typename T, qualifier Q>
		struct Vector<1, T, Q> {

			typedef T value_type;
			typedef Vector<1, T, Q> type;
			typedef Vector<1, bool, Q> bool_type;

			//typename storage<1, T, is_aligned<Q>::value>::type data;

			typedef size_t length_type;
			static length_type length() { return 2; }

			T& operator[](length_type i);
			const T& operator[](length_type i) const;

			// implicit constructors
			Vector();
			Vector(const Vector& v);
			template<qualifier P>
			Vector(const Vector<1, T, P>& v);

			// Explicit constructors
			explicit Vector(T scalar);

			// conversion vector constructors

			//explicit
			template<typename U, qualifier P>
			Vector(const Vector<2, U, P>& v);

			template<typename U, qualifier P>
			Vector(const Vector<3, U, P>& v);

			template<typename U, qualifier P>
			Vector(const Vector<4, U, P>& v);

			template<typename U, qualifier P>
			Vector(const Vector<1, U, P>& v);

			//unary arithmetic operators

			Vector<1, T, Q>& operator=(const Vector& v);

			template<typename U>
			Vector<1, T, Q>& operator=(const Vector<1, U, Q>& v);

			template<typename U>
			Vector<1, T, Q>& operator+=(U scalar);

			template<typename U>
			Vector<1, T, Q>& operator+=(const Vector<1, U, Q>& v);

			template<typename U>
			Vector<1, T, Q>& operator-=(U scalar);

			template<typename U>
			Vector<1, T, Q>& operator-=(const Vector<1, U, Q>& v);

			template<typename U>
			Vector<1, T, Q>& operator*=(U scalar);

			template<typename U>
			Vector<1, T, Q>& operator*=(const Vector<1, U, Q>& v);

			template<typename U>
			Vector<1, T, Q>& operator/=(U scalar);

			template<typename U>
			Vector<1, T, Q>& operator/=(const Vector<1, U, Q>& v);

			// increment and decrement operators
			Vector<1, T, Q>& operator++();
			Vector<1, T, Q>& operator--();
			Vector<1, T, Q> operator++(int);
			Vector<1, T, Q> operator--(int);

			//unary bit operators
			template<typename U>
			Vector<1, T, Q>& operator%=(U scalar);

			template<typename U>
			Vector<1, T, Q>& operator%=(const Vector<1, U, Q>& v);

			template<typename U>
			Vector<1, T, Q>& operator&=(U scalar);

			template<typename U>
			Vector<1, T, Q>& operator&=(const Vector<1, U, Q>& v);

			template<typename U>
			Vector<1, T, Q>& operator|=(U scalar);

			template<typename U>
			Vector<1, T, Q>& operator|=(const Vector<1, U, Q>& v);

			template<typename U>
			Vector<1, T, Q>& operator^=(U scalar);

			template<typename U>
			Vector<1, T, Q>& operator^=(const Vector<1, U, Q>& v);

			template<typename U>
			Vector<1, T, Q>& operator<<=(U scalar);

			template<typename U>
			Vector<1, T, Q>& operator<<=(const Vector<1, U, Q>& v);

			template<typename U>
			Vector<1, T, Q>& operator>>=(U scalar);

			template<typename U>
			Vector<1, T, Q>& operator>>=(const Vector<1, U, Q>& v);

			union { T x, r, s; };
		};

		//unary operators
		template<typename T, qualifier Q>
		Vector<1, T, Q> operator+(const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator-(const Vector<1, T, Q>& v);

		//binary operators
		template<typename T, qualifier Q>
		Vector<1, T, Q> operator+(const Vector<1, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator+(T scalar, const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator+(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator-(const Vector<1, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator-(T scalar, const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator-(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator*(const Vector<1, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator*(T scalar, const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator*(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator/(const Vector<1, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator/(T scalar, const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator/(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator%(const Vector<1, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator%(T scalar, const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator%(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator&(const Vector<1, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator&(T scalar, const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator&(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator|(const Vector<1, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator|(T scalar, const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator|(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator^(const Vector<1, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator^(T scalar, const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator^(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator<<(const Vector<1, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator<<(T scalar, const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator<<(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator>>(const Vector<1, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator>>(T scalar, const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator>>(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		Vector<1, T, Q> operator~(const Vector<1, T, Q>& v1);

		//boolean operators
		template<typename T, qualifier Q>
		bool operator==(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		bool operator!=(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		Vector<1, bool, Q> operator&&(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		Vector<1, bool, Q> operator||(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2);

		typedef Vector<1, float, qualifier::defaultp>		Vector1f;

		typedef Vector<1, float, qualifier::highp>			Vector1f_highp;
		typedef Vector<1, float, qualifier::mediump>		Vector1f_mediump;
		typedef Vector<1, float, qualifier::lowp>			Vector1f_lowp;

		typedef Vector<1, bool, qualifier::defaultp>		Vector1b;

		typedef Vector<1, bool, qualifier::highp>			Vector1b_highp;
		typedef Vector<1, bool, qualifier::mediump>			Vector1b_mediump;
		typedef Vector<1, bool, qualifier::lowp>			Vector1b_lowp;

		typedef Vector<1, double, qualifier::defaultp>		Vector1d;

		typedef Vector<1, double, qualifier::highp>			Vector1d_highp;
		typedef Vector<1, double, qualifier::mediump>		Vector1d_mediump;
		typedef Vector<1, double, qualifier::lowp>			Vector1d_lowp;

		typedef Vector<1, int, qualifier::defaultp>			Vector1i;

		typedef Vector<1, int, qualifier::highp>			Vector1i_highp;
		typedef Vector<1, int, qualifier::mediump>			Vector1i_mediump;
		typedef Vector<1, int, qualifier::lowp>				Vector1i_lowp;

		typedef Vector<1, unsigned int, qualifier::defaultp> Vector1u;

		typedef Vector<1, unsigned int, qualifier::highp>	Vector1u_highp;
		typedef Vector<1, unsigned int, qualifier::mediump> Vector1u_mediump;
		typedef Vector<1, unsigned int, qualifier::lowp>	Vector1u_lowp;
	}
}