#pragma once

#include "Snow/Math/Common/Qualifier.h"

#include "Snow/Math/Vector/SwizzleFunction.h"

namespace Snow {
	namespace Math {
		template<typename T, qualifier Q>
		struct Vector<3, T, Q> {
			typedef T value_type;
			typedef Vector<3, T, Q> type;
			typedef Vector<3, bool, Q> bool_type;

			//typename storage<3, T, is_aligned<Q>::value>::type data;

			SNOW_SWIZZLE_GEN_VECTOR_FROM_VECTOR3(T, Q)

			typedef size_t length_type;
			static constexpr length_type length() { return 3; }

			constexpr T& operator[](length_type i);
			constexpr const T& operator[](length_type i) const;

			// implicit constructors
			constexpr Vector();
			constexpr Vector(const Vector& v);
			template<qualifier P>
			constexpr Vector(const Vector<3, T, P>& v);

			// Explicit constructors
			constexpr explicit Vector(T scalar);
			constexpr Vector(T x, T y, T z);

			// conversion constructors
			template<typename U, qualifier P>
			constexpr explicit Vector(const Vector<1, U, P>& v);

			// explicit conversion constuctors
			template<typename A, typename B, typename C>
			constexpr Vector(A x, B y, C z);

			template<typename A, typename B, typename C>
			constexpr Vector(A x, B y, const Vector<1, C, Q>& z);

			template<typename A, typename B, typename C>
			constexpr Vector(A x, const Vector<1, B, Q>& y, C z);

			template<typename A, typename B, typename C>
			constexpr Vector(A x, const Vector<1, B, Q>& y, const Vector<1, C, Q>& z);

			template<typename A, typename B, typename C>
			constexpr Vector(const Vector<1, A, Q>& x, B y, C z);

			template<typename A, typename B, typename C>
			constexpr Vector(const Vector<1, A, Q>& x, B y, const Vector<1, C, Q>& z);

			template<typename A, typename B, typename C>
			constexpr Vector(const Vector<1, A, Q>& x, const Vector<1, B, Q>& y, C z);

			template<typename A, typename B, typename C>
			constexpr Vector(const Vector<1, A, Q>& x, const Vector<1, B, Q>& y, const Vector<1, C, Q>& z);

			// conversion vector constructors

			//explicit
			template<typename A, typename B, qualifier P>
			constexpr Vector(const Vector<2, A, P>& xy, B z);

			template<typename A, typename B, qualifier P>
			constexpr Vector(const Vector<2, A, P>& xy, const Vector<1, B, P>& z);

			template<typename A, typename B, qualifier P>
			constexpr Vector(A x, const Vector<2, B, P>& yz);

			template<typename A, typename B, qualifier P>
			constexpr Vector(const Vector<1, A, P>& x, const Vector<2, B, P>& yz);

			template<typename U, qualifier P>
			constexpr explicit Vector(const Vector<4, U, P>& v);

			template<typename U, qualifier P>
			constexpr explicit Vector(const Vector<3, U, P>& v);

			
			//unary arithmetic operators

			constexpr Vector<3, T, Q>& operator=(const Vector& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator=(const Vector<3, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator+=(U scalar);

			template<typename U>
			constexpr Vector<3, T, Q>& operator+=(const Vector<1, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator+=(const Vector<3, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator-=(U scalar);

			template<typename U>
			constexpr Vector<3, T, Q>& operator-=(const Vector<1, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator-=(const Vector<3, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator*=(U scalar);

			template<typename U>
			constexpr Vector<3, T, Q>& operator*=(const Vector<1, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator*=(const Vector<3, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator/=(U scalar);

			template<typename U>
			constexpr Vector<3, T, Q>& operator/=(const Vector<1, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator/=(const Vector<3, U, Q>& v);

			// increment and decrement operators
			constexpr Vector<3, T, Q>& operator++();
			constexpr Vector<3, T, Q>& operator--();
			constexpr Vector<3, T, Q> operator++(int);
			constexpr Vector<3, T, Q> operator--(int);

			//unary bit operators
			template<typename U>
			constexpr Vector<3, T, Q>& operator%=(U scalar);

			template<typename U>
			constexpr Vector<3, T, Q>& operator%=(const Vector<1, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator%=(const Vector<3, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator&=(U scalar);

			template<typename U>
			constexpr Vector<3, T, Q>& operator&=(const Vector<1, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator&=(const Vector<3, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator|=(U scalar);

			template<typename U>
			constexpr Vector<3, T, Q>& operator|=(const Vector<1, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator|=(const Vector<3, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator^=(U scalar);

			template<typename U>
			constexpr Vector<3, T, Q>& operator^=(const Vector<1, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator^=(const Vector<3, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator<<=(U scalar);

			template<typename U>
			constexpr Vector<3, T, Q>& operator<<=(const Vector<1, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator<<=(const Vector<3, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator>>=(U scalar);

			template<typename U>
			constexpr Vector<3, T, Q>& operator>>=(const Vector<1, U, Q>& v);

			template<typename U>
			constexpr Vector<3, T, Q>& operator>>=(const Vector<3, U, Q>& v);


			union { T x, r, s; };
			union { T y, g, t; };
			union { T z, b, p; };
		};

		//unary operators
		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator+(const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator-(const Vector<3, T, Q>& v);

		//binary operators
		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator+(const Vector<3, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator+(const Vector<3, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator+(T scalar, const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator+(const Vector<1, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator+(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator-(const Vector<3, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator-(const Vector<3, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator-(T scalar, const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator-(const Vector<1, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator-(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(const Vector<3, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(const Vector<3, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(T scalar, const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(const Vector<1, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator/(const Vector<3, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator/(const Vector<3, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator/(T scalar, const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator/(const Vector<1, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator/(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2);

		//bitwise binary operators
		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator%(const Vector<3, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator%(const Vector<3, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator%(T scalar, const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator%(const Vector<1, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator%(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator&(const Vector<3, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator&(const Vector<3, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator&(T scalar, const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator&(const Vector<1, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator&(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator|(const Vector<3, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator|(const Vector<3, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator|(T scalar, const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator|(const Vector<1, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator|(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator^(const Vector<3, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator^(const Vector<3, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator^(T scalar, const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator^(const Vector<1, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator^(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator<<(const Vector<3, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator<<(const Vector<3, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator<<(T scalar, const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator<<(const Vector<1, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator<<(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator>>(const Vector<3, T, Q>& v, T scalar);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator>>(const Vector<3, T, Q>& v1, const Vector<1, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator>>(T scalar, const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator>>(const Vector<1, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator>>(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator~(const Vector<3, T, Q>& v);

		//boolean operators
		template<typename T, qualifier Q>
		constexpr bool operator==(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr bool operator!=(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, bool, Q> operator&&(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2);

		template<typename T, qualifier Q>
		constexpr Vector<3, bool, Q> operator||(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2);


		typedef Vector<3, float, defaultp> Vector3f;

		typedef Vector<3, float, highp> Vector3f_highp;
		typedef Vector<3, float, mediump> Vector3f_mediump;
		typedef Vector<3, float, lowp> Vector3f_lowp;

		typedef Vector<3, bool, defaultp> Vector3b;

		typedef Vector<3, bool, highp> Vector3b_highp;
		typedef Vector<3, bool, mediump> Vector3b_mediump;
		typedef Vector<3, bool, lowp> Vector3b_lowp;

		typedef Vector<3, double, defaultp> Vector3d;

		typedef Vector<3, double, highp> Vector3d_highp;
		typedef Vector<3, double, mediump> Vector3d_mediump;
		typedef Vector<3, double, lowp> Vector3d_lowp;

		typedef Vector<3, int, defaultp> Vector3i;
							   
		typedef Vector<3, int, highp> Vector3i_highp;
		typedef Vector<3, int, mediump> Vector3i_mediump;
		typedef Vector<3, int, lowp> Vector3i_lowp;

		typedef Vector<3, unsigned int, defaultp> Vector3u;

		typedef Vector<3, unsigned int, highp> Vector3u_highp;
		typedef Vector<3, unsigned int, mediump> Vector3u_mediump;
		typedef Vector<3, unsigned int, lowp> Vector3u_lowp;
	}
}
#include "Snow/Math/Vector/Vector3.inl"