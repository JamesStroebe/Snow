#pragma once

namespace Snow {
	namespace Math {
		template<typename T, qualifier Q>
		constexpr T& Vector<3, T, Q>::operator[](typename Vector<3, T, Q>::length_type i) {
			switch (i) {
			default:
			case 0:
				return x;
			case 1:
				return y;
			case 2:
				return z;
			}
		}

		template<typename T, qualifier Q>
		constexpr const T& Vector<3, T, Q>::operator[](typename Vector<3, T, Q>::length_type i) const {
			//assert i >=0 && i<length_type
			switch (i) {
			default:
			case 0:
				return x;
			case 1:
				return y;
			case 2:
				return z;
			}
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q>::Vector() :
			x(0), y(0), z(0) {}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q>::Vector(const Vector<3, T, Q>& v) :
			x(v.x), y(v.y), z(v.z) {}

		// explicit constructors
		template<typename T, qualifier Q>
		template<qualifier P>
		constexpr Vector<3, T, Q>::Vector(const Vector<3, T, P>& v) :
			x(v.x), y(v.y), z(v.z) {}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q>::Vector(T scalar) :
			x(scalar), y(scalar), z(scalar) {}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q>::Vector(T x, T y, T z) :
			x(x), y(y), z(z) {}

		//conversion scalar constructors
		template<typename T, qualifier Q>
		template<typename U, qualifier P>
		constexpr Vector<3, T, Q>::Vector(const Vector<1, U, P>& v) :
			x(static_cast<T>(v.x)), y(static_cast<T>(v.x)), z(static_cast<T>(v.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C>
		constexpr Vector<3, T, Q>::Vector(A x, B y, C z) :
			x(static_cast<T>(x)), y(static_cast<T>(y)), z(static_cast<T>(z)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C>
		constexpr Vector<3, T, Q>::Vector(A x, B y, const Vector<1, C, Q>& z) :
			x(static_cast<T>(x)), y(static_cast<T>(y)), z(static_cast<T>(z.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C>
		constexpr Vector<3, T, Q>::Vector(A x, const Vector<1, B, Q>& y, C z) :
			x(static_cast<T>(x)), y(static_cast<T>(y.x)), z(static_cast<T>(z)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C>
		constexpr Vector<3, T, Q>::Vector(A x, const Vector<1, B, Q>& y, const Vector<1, C, Q>& z) :
			x(static_cast<T>(x)), y(static_cast<T>(y.x)), z(static_cast<T>(z.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C>
		constexpr Vector<3, T, Q>::Vector(const Vector<1, A, Q>& x, B y, C z) :
			x(static_cast<T>(x.x)), y(static_cast<T>(y)), z(static_cast<T>(z)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C>
		constexpr Vector<3, T, Q>::Vector(const Vector<1, A, Q>& x, B y, const Vector<1, C, Q>& z) :
			x(static_cast<T>(x.x)), y(static_cast<T>(y)), z(static_cast<T>(z.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C>
		constexpr Vector<3, T, Q>::Vector(const Vector<1, A, Q>& x, const Vector<1, B, Q>& y, C z) :
			x(static_cast<T>(x.x)), y(static_cast<T>(y.x)), z(static_cast<T>(z)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, typename C>
		constexpr Vector<3, T, Q>::Vector(const Vector<1, A, Q>& x, const Vector<1, B, Q>& y, const Vector<1, C, Q>& z) :
			x(static_cast<T>(x.x)), y(static_cast<T>(y.x)), z(static_cast<T>(z.x)) {}

		// conversion vector constructors

		//explicit
		template<typename T, qualifier Q>
		template<typename A, typename B, qualifier P>
		constexpr Vector<3, T, Q>::Vector(const Vector<2, A, P>& xy, B z) :
			x(static_cast<T>(xy.x)), y(static_cast<T>(xy.y)), z(static_cast<T>(z)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, qualifier P>
		constexpr Vector<3, T, Q>::Vector(const Vector<2, A, P>& xy, const Vector<1, B, P>& z) :
			x(static_cast<T>(xy.x)), y(static_cast<T>(xy.y)), z(static_cast<T>(z.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, qualifier P>
		constexpr Vector<3, T, Q>::Vector(A x, const Vector<2, B, P>& yz) :
			x(static_cast<T>(x)), y(static_cast<T>(yz.x)), z(static_cast<T>(yz.y)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B, qualifier P>
		constexpr Vector<3, T, Q>::Vector(const Vector<1, A, P>& x, const Vector<2, B, P>& yz) :
			x(static_cast<T>(x.x)), y(static_cast<T>(yz.x)), z(static_cast<T>(yz.y)) {}

		template<typename T, qualifier Q>
		template<typename U, qualifier P>
		constexpr Vector<3, T, Q>::Vector(const Vector<4, U, P>& v) :
			x(static_cast<T>(v.x)), y(static_cast<T>(v.y)), z(static_cast<T>(v.z)) {}

		template<typename T, qualifier Q>
		template<typename U, qualifier P>
		constexpr Vector<3, T, Q>::Vector(const Vector<3, U, P>& v) :
			x(static_cast<T>(v.x)), y(static_cast<T>(v.y)), z(static_cast<T>(v.z)) {}


		//unary arithmetic operators

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator=(const Vector<3, T, Q>& v) {
			this->x = v.x;
			this->y = v.y;
			this->z = v.z;
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator=(const Vector<3, U, Q>& v) {
			this->x = static_cast<T>(v.x);
			this->y = static_cast<T>(v.y);
			this->z = static_cast<T>(v.z);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator+=(U scalar) {
			this->x += static_cast<T>(scalar);
			this->y += static_cast<T>(scalar);
			this->z += static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator+=(const Vector<1, U, Q>& v) {
			this->x += static_cast<T>(v.x);
			this->y += static_cast<T>(v.x);
			this->z += static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator+=(const Vector<3, U, Q>& v) {
			this->x += static_cast<T>(v.x);
			this->y += static_cast<T>(v.y);
			this->z += static_cast<T>(v.z);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator-=(U scalar) {
			this->x -= static_cast<T>(scalar);
			this->y -= static_cast<T>(scalar);
			this->z -= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator-=(const Vector<1, U, Q>& v) {
			this->x -= static_cast<T>(v.x);
			this->y -= static_cast<T>(v.x);
			this->z -= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator-=(const Vector<3, U, Q>& v) {
			this->x -= static_cast<T>(v.x);
			this->y -= static_cast<T>(v.y);
			this->z -= static_cast<T>(v.z);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator*=(U scalar) {
			this->x *= static_cast<T>(scalar);
			this->y *= static_cast<T>(scalar);
			this->z *= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator*=(const Vector<1, U, Q>& v) {
			this->x *= static_cast<T>(v.x);
			this->y *= static_cast<T>(v.x);
			this->z *= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator*=(const Vector<3, U, Q>& v) {
			this->x *= static_cast<T>(v.x);
			this->y *= static_cast<T>(v.y);
			this->z *= static_cast<T>(v.z);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator/=(U scalar) {
			this->x /= static_cast<T>(scalar);
			this->y /= static_cast<T>(scalar);
			this->z /= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator/=(const Vector<1, U, Q>& v) {
			this->x /= static_cast<T>(v.x);
			this->y /= static_cast<T>(v.x);
			this->z /= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator/=(const Vector<3, U, Q>& v) {
			this->x /= static_cast<T>(v.x);
			this->y /= static_cast<T>(v.y);
			this->z /= static_cast<T>(v.z);
			return *this;
		}

		//increment/decrement operators
		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator++() {
			++this->x;
			++this->y;
			++this->z;
			return *this;
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator--() {
			--this->x;
			--this->y;
			--this->z;
			return *this;
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> Vector<3, T, Q>::operator++(int) {
			Vector<3, T, Q> result(*this);
			++ *this;
			return result;
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> Vector<3, T, Q>::operator--(int) {
			Vector<3, T, Q> result(*this);
			-- *this;
			return result;
		}

		// unary bit operators

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator%=(U scalar) {
			this->x %= static_cast<T>(scalar);
			this->y %= static_cast<T>(scalar);
			this->z %= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator%=(const Vector<1, U, Q>& v) {
			this->x %= static_cast<T>(v.x);
			this->y %= static_cast<T>(v.x);
			this->z %= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator%=(const Vector<3, U, Q>& v) {
			this->x %= static_cast<T>(v.x);
			this->y %= static_cast<T>(v.y);
			this->z %= static_cast<T>(v.z);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator&=(U scalar) {
			this->x &= static_cast<T>(scalar);
			this->y &= static_cast<T>(scalar);
			this->z &= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator&=(const Vector<1, U, Q>& v) {
			this->x &= static_cast<T>(v.x);
			this->y &= static_cast<T>(v.x);
			this->z &= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator&=(const Vector<3, U, Q>& v) {
			this->x &= static_cast<T>(v.x);
			this->y &= static_cast<T>(v.y);
			this->z &= static_cast<T>(v.z);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator|=(U scalar) {
			this->x |= static_cast<T>(scalar);
			this->y |= static_cast<T>(scalar);
			this->z |= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator|=(const Vector<1, U, Q>& v) {
			this->x |= static_cast<T>(v.x);
			this->y |= static_cast<T>(v.x);
			this->z |= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator|=(const Vector<3, U, Q>& v) {
			this->x |= static_cast<T>(v.x);
			this->y |= static_cast<T>(v.y);
			this->z |= static_cast<T>(v.z);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator^=(U scalar) {
			this->x ^= static_cast<T>(scalar);
			this->y ^= static_cast<T>(scalar);
			this->z ^= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator^=(const Vector<1, U, Q>& v) {
			this->x ^= static_cast<T>(v.x);
			this->y ^= static_cast<T>(v.x);
			this->z ^= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator^=(const Vector<3, U, Q>& v) {
			this->x ^= static_cast<T>(v.x);
			this->y ^= static_cast<T>(v.y);
			this->z ^= static_cast<T>(v.z);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator<<=(U scalar) {
			this->x <<= static_cast<T>(scalar);
			this->y <<= static_cast<T>(scalar);
			this->z <<= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator<<=(const Vector<1, U, Q>& v) {
			this->x <<= static_cast<T>(v.x);
			this->y <<= static_cast<T>(v.x);
			this->z <<= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator<<=(const Vector<3, U, Q>& v) {
			this->x <<= static_cast<T>(v.x);
			this->y <<= static_cast<T>(v.y);
			this->z <<= static_cast<T>(v.z);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator>>=(U scalar) {
			this->x >>= static_cast<T>(scalar);
			this->y >>= static_cast<T>(scalar);
			this->z >>= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator>>=(const Vector<1, U, Q>& v) {
			this->x >>= static_cast<T>(v.x);
			this->y >>= static_cast<T>(v.x);
			this->z >>= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		constexpr Vector<3, T, Q>& Vector<3, T, Q>::operator>>=(const Vector<3, U, Q>& v) {
			this->x >>= static_cast<T>(v.x);
			this->y >>= static_cast<T>(v.y);
			this->z >>= static_cast<T>(v.z);
			return *this;
		}

		//unary operators
		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator+(Vector<3, T, Q> const& v) {
			return v;
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator-(const Vector<3, T, Q>& v) {
			return Vector<3, T, Q>(-v.x, -v.y, -v.z);
		}

		//binary arithmetic operators
		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator+(const Vector<3, T, Q>& v, T scalar) {
			return Vector<3, T, Q>(v.x + scalar, v.y + scalar, v.z + scalar);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator+(const Vector<3, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<3, T, Q>(v1.x + v2.x, v1.y + v2.x, v1.z + v2.x);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator+(T scalar, const Vector<3, T, Q>& v) {
			return Vector<3, T, Q>(v.x + scalar, v.y + scalar, v.z + scalar);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator+(const Vector<1, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x + v2.x, v1.x + v2.y, v1.x + v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator+(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator-(const Vector<3, T, Q>& v, T scalar) {
			return Vector<3, T, Q>(v.x - scalar, v.y - scalar, v.z - scalar);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator-(const Vector<3, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<3, T, Q>(v1.x - v2.x, v1.y - v2.x, v1.z - v2.x);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator-(T scalar, const Vector<3, T, Q>& v) {
			return Vector<3, T, Q>(scalar - v.x, scalar - v.y, scalar - v.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator-(const Vector<1, T, Q> v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x - v2.x, v1.x - v2.y, v1.x - v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator-(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(const Vector<3, T, Q>& v, T scalar) {
			return Vector<3, T, Q>(v.x * scalar, v.y * scalar, v.z * scalar);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(const Vector<3, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<3, T, Q>(v1.x * v2.x, v1.y * v2.x, v1.z * v2.x);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(T scalar, const Vector<3, T, Q>& v) {
			return Vector<3, T, Q>(v.x * scalar, v.y * scalar, v.z * scalar);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(const Vector<1, T, Q> v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x * v2.x, v1.x * v2.y, v1.x * v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator*(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator/(const Vector<3, T, Q>& v, T scalar) {
			return Vector<3, T, Q>(v.x / scalar, v.y / scalar, v.z / scalar);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator/(const Vector<3, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<3, T, Q>(v1.x / v2.x, v1.y / v2.x, v1.z / v2.x);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator/(T scalar, const Vector<3, T, Q>& v) {
			return Vector<3, T, Q>(scalar / v.x, scalar / v.y, scalar / v.z );
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator/(const Vector<1, T, Q> v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x / v2.x, v1.x / v2.y, v1.x / v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator/(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator%(const Vector<3, T, Q>& v, T scalar) {
			return Vector<3, T, Q>(v.x % scalar, v.y % scalar, v.z % scalar);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator%(const Vector<3, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<3, T, Q>(v1.x % v2.x, v1.y % v2.x, v1.z % v2.x);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator%(T scalar, const Vector<3, T, Q>& v) {
			return Vector<3, T, Q>(scalar % v.x, scalar % v.y, scalar % v.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator%(const Vector<1, T, Q> v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x % v2.x, v1.x % v2.y, v1.x % v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator%(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x % v2.x, v1.y % v2.y, v1.z % v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator&(const Vector<3, T, Q>& v, T scalar) {
			return Vector<3, T, Q>(v.x & scalar, v.y & scalar, v.z & scalar);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator&(const Vector<3, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<3, T, Q>(v1.x & v2.x, v1.y & v2.x, v1.z & v2.x);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator&(T scalar, const Vector<3, T, Q>& v) {
			return Vector<3, T, Q>(scalar & v.x, scalar & v.y, scalar & v.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator&(const Vector<1, T, Q> v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x & v2.x, v1.x & v2.y, v1.x & v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator&(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x & v2.x, v1.y & v2.y, v1.z & v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator|(const Vector<3, T, Q>& v, T scalar) {
			return Vector<3, T, Q>(v.x | scalar, v.y | scalar, v.z | scalar);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator|(const Vector<3, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<3, T, Q>(v1.x | v2.x, v1.y | v2.x, v1.z | v2.x);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator|(T scalar, const Vector<3, T, Q>& v) {
			return Vector<3, T, Q>(scalar | v.x, scalar | v.y, scalar | v.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator|(const Vector<1, T, Q> v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x | v2.x, v1.x | v2.y, v1.x | v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator|(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x | v2.x, v1.y | v2.y, v1.z | v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator^(const Vector<3, T, Q>& v, T scalar) {
			return Vector<3, T, Q>(v.x ^ scalar, v.y ^ scalar, v.z ^ scalar);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator^(const Vector<3, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<3, T, Q>(v1.x ^ v2.x, v1.y ^ v2.x, v1.z ^ v2.x);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator^(T scalar, const Vector<3, T, Q>& v) {
			return Vector<3, T, Q>(scalar ^ v.x, scalar ^ v.y, scalar ^ v.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator^(const Vector<1, T, Q> v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x ^ v2.x, v1.x ^ v2.y, v1.x ^ v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator^(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x ^ v2.x, v1.y ^ v2.y, v1.z ^ v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator<<(const Vector<3, T, Q>& v, T scalar) {
			return Vector<3, T, Q>(v.x << scalar, v.y << scalar, v.z << scalar);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator<<(const Vector<3, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<3, T, Q>(v1.x << v2.x, v1.y << v2.x, v1.z << v2.x);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator<<(T scalar, const Vector<3, T, Q>& v) {
			return Vector<3, T, Q>(scalar << v.x, scalar << v.y, scalar << v.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator<<(const Vector<1, T, Q> v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x << v2.x, v1.x << v2.y, v1.x << v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator<<(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x << v2.x, v1.y << v2.y, v1.z << v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator>>(const Vector<3, T, Q>& v, T scalar) {
			return Vector<3, T, Q>(v.x >> scalar, v.y >> scalar, v.z >> scalar);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator>>(const Vector<3, T, Q>& v1, const Vector<1, T, Q> v2) {
			return Vector<3, T, Q>(v1.x >> v2.x, v1.y >> v2.x, v1.z >> v2.x);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator>>(T scalar, const Vector<3, T, Q>& v) {
			return Vector<3, T, Q>(scalar >> v.x, scalar >> v.y, scalar >> v.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator>>(const Vector<1, T, Q> v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x >> v2.x, v1.x >> v2.y, v1.x >> v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator>>(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return Vector<3, T, Q>(v1.x >> v2.x, v1.y >> v2.y, v1.z >> v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, T, Q> operator~(const Vector<3, T, Q>& v) {
			return Vector<3, T, Q>(~v.x, ~v.y, ~v.z);
		}

		template<typename T, qualifier Q>
		constexpr bool operator==(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return v1.x == v2.x && v1.y == v2.y && v1.z == v2.z;
		}

		template<typename T, qualifier Q>
		constexpr bool operator!=(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return v1.x != v2.x || v1.y != v2.y || v1.z != v2.z;
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, bool, Q> operator&&(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return Vector<3, bool, Q>(v1.x && v2.x, v1.y && v2.y, v1.z && v2.z);
		}

		template<typename T, qualifier Q>
		constexpr Vector<3, bool, Q> operator||(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return Vector<3, bool, Q>(v1.x || v2.x, v1.y || v2.y, v1.z || v2.z);
		}
	}
}