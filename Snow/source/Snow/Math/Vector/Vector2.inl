#pragma once

namespace Snow {
	namespace Math {

		template<typename T, qualifier Q>
		inline constexpr T& Vector<2, T, Q>::operator[](typename Vector<2, T, Q>::length_type i) {
			//assert i >=0 && i<length_type
			switch (i) {
			default:
			case 0:
				return x;
			case 1:
				return y;
			}
		}

		template<typename T, qualifier Q>
		inline constexpr const T& Vector<2, T, Q>::operator[](typename Vector<2, T, Q>::length_type i) const {
			//assert i >=0 && i<length_type
			switch (i) {
			default:
			case 0:
				return x;
			case 1:
				return y;
			}
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q>::Vector() :
			x(0), y(0) {}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q>::Vector(const Vector<2, T, Q>& v) :
			x(v.x), y(v.y) {}

		// explicit constructors
		template<typename T, qualifier Q>
		template<qualifier P>
		inline constexpr Vector<2, T, Q>::Vector(const Vector<2, T, P>& v) :
			x(v.x), y(v.y) {}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q>::Vector(T scalar) :
			x(scalar), y(scalar) {}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q>::Vector(T x, T y) :
			x(x), y(y) {}

		//conversion scalar constructors
		template<typename T, qualifier Q>
		template<typename U, qualifier P>
		inline constexpr Vector<2, T, Q>::Vector(const Vector<1, U, P>& v) :
			x(static_cast<T>(v.x)), y(static_cast<T>(v.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B>
		inline constexpr Vector<2, T, Q>::Vector(A x, B y) :
			x(static_cast<T>(x)), y(static_cast<T>(y)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B>
		inline constexpr Vector<2, T, Q>::Vector(const Vector<1, A, Q>& x, B y) :
			x(static_cast<T>(x.x)), y(static_cast<T>(y)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B>
		inline constexpr Vector<2, T, Q>::Vector(A x, const Vector<1, B, Q>& y) :
			x(static_cast<T>(x)), y(static_cast<T>(y.x)) {}

		template<typename T, qualifier Q>
		template<typename A, typename B>
		inline constexpr Vector<2, T, Q>::Vector(const Vector<1, A, Q>& x, const Vector<1, B, Q>& y) :
			x(static_cast<T>(x.x)), y(static_cast<T>(y.x)) {}

		//conversion vector constructors
		template<typename T, qualifier Q>
		template<typename U, qualifier P>
		inline constexpr Vector<2, T, Q>::Vector(const Vector<2, U, P>& v) :
			x(static_cast<T>(v.x)), y(static_cast<T>(v.y)) {}

		template<typename T, qualifier Q>
		template<typename U, qualifier P>
		inline constexpr Vector<2, T, Q>::Vector(const Vector<3, U, P>& v) :
			x(static_cast<T>(v.x)), y(static_cast<T>(v.y)) {}

		template<typename T, qualifier Q>
		template<typename U, qualifier P>
		inline constexpr Vector<2, T, Q>::Vector(const Vector<4, U, P>& v) :
			x(static_cast<T>(v.x)), y(static_cast<T>(v.y)) {}

		//unary arithmetic operators
		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator=(const Vector<2, T, Q>& v) {
			this->x = v.x;
			this->y = v.y;
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator=(const Vector<2, U, Q>& v) {
			this->x = static_cast<T>(v.x);
			this->y = static_cast<T>(v.y);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator+=(U scalar) {
			this->x += static_cast<T>(scalar);
			this->y += static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator+=(const Vector<1, U, Q>& v) {
			this->x += static_cast<T>(v.x);
			this->y += static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator+=(const Vector<2, U, Q>& v) {
			this->x += static_cast<T>(v.x);
			this->y += static_cast<T>(v.y);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator-=(U scalar) {
			this->x -= static_cast<T>(scalar);
			this->y -= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator-=(const Vector<1, U, Q>& v) {
			this->x -= static_cast<T>(v.x);
			this->y -= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator-=(const Vector<2, U, Q>& v) {
			this->x -= static_cast<T>(v.x);
			this->y -= static_cast<T>(v.y);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator*=(U scalar) {
			this->x *= static_cast<T>(scalar);
			this->y *= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator*=(const Vector<1, U, Q>& v) {
			this->x *= static_cast<T>(v.x);
			this->y *= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator*=(const Vector<2, U, Q>& v) {
			this->x *= static_cast<T>(v.x);
			this->y *= static_cast<T>(v.y);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator/=(U scalar) {
			this->x /= static_cast<T>(scalar);
			this->y /= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator/=(const Vector<1, U, Q>& v) {
			this->x /= static_cast<T>(v.x);
			this->y /= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator/=(const Vector<2, U, Q>& v) {
			this->x /= static_cast<T>(v.x);
			this->y /= static_cast<T>(v.y);
			return *this;
		}

		//increment/decrement operators
		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator++() {
			++this->x;
			++this->y;
			return *this;
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator--() {
			--this->x;
			--this->y;
			return *this;
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> Vector<2, T, Q>::operator++(int) {
			Vector<2, T, Q> result(*this);
			++ *this;
			return result;
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> Vector<2, T, Q>::operator--(int) {
			Vector<2, T, Q> result(*this);
			-- *this;
			return result;
		}

		// unary bit operators

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator%=(U scalar) {
			this->x %= static_cast<T>(scalar);
			this->y %= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator%=(const Vector<1, U, Q>& v) {
			this->x %= static_cast<T>(v.x);
			this->y %= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator%=(const Vector<2, U, Q>& v) {
			this->x %= static_cast<T>(v.x);
			this->y %= static_cast<T>(v.y);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator&=(U scalar) {
			this->x &= static_cast<T>(scalar);
			this->y &= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator&=(const Vector<1, U, Q>& v) {
			this->x &= static_cast<T>(v.x);
			this->y &= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator&=(const Vector<2, U, Q>& v) {
			this->x &= static_cast<T>(v.x);
			this->y &= static_cast<T>(v.y);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator|=(U scalar) {
			this->x |= static_cast<T>(scalar);
			this->y |= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator|=(const Vector<1, U, Q>& v) {
			this->x |= static_cast<T>(v.x);
			this->y |= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator|=(const Vector<2, U, Q>& v) {
			this->x |= static_cast<T>(v.x);
			this->y |= static_cast<T>(v.y);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator^=(U scalar) {
			this->x ^= static_cast<T>(scalar);
			this->y ^= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator^=(const Vector<1, U, Q>& v) {
			this->x ^= static_cast<T>(v.x);
			this->y ^= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator^=(const Vector<2, U, Q>& v) {
			this->x ^= static_cast<T>(v.x);
			this->y ^= static_cast<T>(v.y);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator<<=(U scalar) {
			this->x <<= static_cast<T>(scalar);
			this->y <<= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator<<=(const Vector<1, U, Q>& v) {
			this->x <<= static_cast<T>(v.x);
			this->y <<= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator<<=(const Vector<2, U, Q>& v) {
			this->x <<= static_cast<T>(v.x);
			this->y <<= static_cast<T>(v.y);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator>>=(U scalar) {
			this->x >>= static_cast<T>(scalar);
			this->y >>= static_cast<T>(scalar);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator>>=(const Vector<1, U, Q>& v) {
			this->x >>= static_cast<T>(v.x);
			this->y >>= static_cast<T>(v.x);
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		inline constexpr Vector<2, T, Q>& Vector<2, T, Q>::operator>>=(const Vector<2, U, Q>& v) {
			this->x >>= static_cast<T>(v.x);
			this->y >>= static_cast<T>(v.y);
			return *this;
		}

		//unary arithmetic operators
		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator+(const Vector<2, T, Q>& v) {
			return v;
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator-(const Vector<2, T, Q>& v) {
			return Vector<2, T, Q>(-v.x, -v.y);
		}

		//binary arithmetic operators
		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator+(const Vector<2, T, Q>& v, T scalar) {
			return Vector<2, T, Q>(v.x + scalar, v.y + scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator+(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x + v2.x, v1.y + v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator+(T scalar, const Vector<2, T, Q>& v) {
			return Vector<2, T, Q>(scalar + v.x, scalar + v.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator+(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x + v2.x, v1.x + v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator+(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x + v2.x, v1.y + v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator-(const Vector<2, T, Q>& v, T scalar) {
			return Vector<2, T, Q>(v.x - scalar, v.y - scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator-(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x - v2.x, v1.y - v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator-(T scalar, const Vector<2, T, Q>& v) {
			return Vector<2, T, Q>(scalar - v.x, scalar - v.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator-(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x - v2.x, v1.x - v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator-(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x - v2.x, v1.y - v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator*(const Vector<2, T, Q>& v, T scalar) {
			return Vector<2, T, Q>(v.x * scalar, v.y * scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator*(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x * v2.x, v1.y * v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator*(T scalar, const Vector<2, T, Q>& v) {
			return Vector<2, T, Q>(scalar * v.x, scalar * v.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator*(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x * v2.x, v1.x * v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator*(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x * v2.x, v1.y * v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator/(const Vector<2, T, Q>& v, T scalar) {
			return Vector<2, T, Q>(v.x / scalar, v.y / scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator/(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x / v2.x, v1.y / v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator/(T scalar, const Vector<2, T, Q>& v) {
			return Vector<2, T, Q>(scalar / v.x, scalar / v.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator/(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x / v2.x, v1.x / v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator/(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x / v2.x, v1.y / v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator%(const Vector<2, T, Q>& v, T scalar) {
			return Vector<2, T, Q>(v.x % scalar, v.y % scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator%(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x % v2.x, v1.y % v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator%(T scalar, const Vector<2, T, Q>& v) {
			return Vector<2, T, Q>(scalar % v.x, scalar % v.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator%(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x % v2.x, v1.x % v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator%(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x % v2.x, v1.y % v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator&(const Vector<2, T, Q>& v, T scalar) {
			return Vector<2, T, Q>(v.x & scalar, v.y & scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator&(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x & v2.x, v1.y & v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator&(T scalar, const Vector<2, T, Q>& v) {
			return Vector<2, T, Q>(scalar & v.x, scalar & v.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator&(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x & v2.x, v1.x & v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator&(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x & v2.x, v1.y & v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator|(const Vector<2, T, Q>& v, T scalar) {
			return Vector<2, T, Q>(v.x | scalar, v.y | scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator|(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x | v2.x, v1.y | v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator|(T scalar, const Vector<2, T, Q>& v) {
			return Vector<2, T, Q>(scalar | v.x, scalar | v.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator|(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x | v2.x, v1.x | v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator|(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x | v2.x, v1.y | v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator^(const Vector<2, T, Q>& v, T scalar) {
			return Vector<2, T, Q>(v.x ^ scalar, v.y ^ scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator^(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x ^ v2.x, v1.y ^ v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator^(T scalar, const Vector<2, T, Q>& v) {
			return Vector<2, T, Q>(scalar ^ v.x, scalar ^ v.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator^(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x ^ v2.x, v1.x ^ v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator^(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x ^ v2.x, v1.y ^ v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator<<(const Vector<2, T, Q>& v, T scalar) {
			return Vector<2, T, Q>(v.x << scalar, v.y << scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator<<(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x << v2.x, v1.y << v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator<<(T scalar, const Vector<2, T, Q>& v) {
			return Vector<2, T, Q>(scalar << v.x, scalar << v.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator<<(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x << v2.x, v1.x << v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator<<(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x << v2.x, v1.y << v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator>>(const Vector<2, T, Q>& v, T scalar) {
			return Vector<2, T, Q>(v.x >> scalar, v.y >> scalar);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator>>(const Vector<2, T, Q>& v1, const Vector<1, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x >> v2.x, v1.y >> v2.x);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator>>(T scalar, const Vector<2, T, Q>& v) {
			return Vector<2, T, Q>(scalar >> v.x, scalar >> v.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator>>(const Vector<1, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x >> v2.x, v1.x >> v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator>>(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, T, Q>(v1.x >> v2.x, v1.y >> v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, T, Q> operator~(const Vector<2, T, Q>& v) {
			return Vector<2, T, Q>(~v.x, ~v.y);
		}

		template<typename T, qualifier Q>
		inline constexpr bool operator==(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return v1.x == v2.x && v1.y == v2.y;
		}

		template<typename T, qualifier Q>
		inline constexpr bool operator!=(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return v1.x != v2.x || v1.y != v2.y;
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, bool, Q> operator&&(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, bool, Q>(v1.x && v2.x, v1.y && v2.y);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<2, bool, Q> operator||(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
			return Vector<2, bool, Q>(v1.x || v2.x, v1.y || v2.y);
		}
	}
}
