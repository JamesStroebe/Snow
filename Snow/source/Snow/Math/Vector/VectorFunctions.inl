#pragma once

#include "Snow/Math/Common/Common.h"
#include "Snow/Math/Trig/Trigonomic.h"

namespace Snow {
	namespace Math {

		template<template<size_t L, typename T, qualifier Q> class Vector, size_t L, typename R, typename T, qualifier Q>
		struct functor1 {};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename R, typename T, qualifier Q>
		struct functor1<Vector, 1, R, T, Q> {
			inline constexpr static Vector<1, R, Q> call(R(*Func) (T x), const Vector<1, T, Q>& v) {
				return Vector<1, R, Q>(func(v.x));
			}
		};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename R, typename T, qualifier Q>
		struct functor1<Vector, 2, R, T, Q> {
			inline constexpr static Vector<2, R, Q> call(R(*func) (T x), const Vector<2, T, Q>& v) {
				return Vector<2, R, Q>(func(v.x), func(v.y));
			}
		};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename R, typename T, qualifier Q>
		struct functor1<Vector, 3, R, T, Q> {
			inline constexpr static Vector<3, R, Q> call(R (*func) (T x), const Vector<3, T, Q>& vec) {
				return Vector<3, R, Q>(func(vec.x), func(vec.y), func(vec.z));
			}
		};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename R, typename T, qualifier Q>
		struct functor1<Vector, 4, R, T, Q> {
			inline constexpr static Vector<4, R, Q> call(R (*func) (T value), const Vector<4, T, Q>& vec) {
				return Vector<4, R, Q>(func(vec.x), func(vec.y), func(value.z), func(value.w));
			}
		};


		template<template<size_t L, typename T, qualifier Q> class Vector, size_t L, typename T, qualifier Q>
		struct functor2 {};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename T, qualifier Q>
		struct functor2<Vector, 1, T, Q> {
			inline static Vector<1, T, Q> call(T(*func) (T x, T y), const Vector<1, T, Q>& vec1, const Vector<1, T, Q>& vec2) {
				return Vector<1, T, Q>(func(vec1.x, vec2.x));
			}
		};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename T, qualifier Q>
		struct functor2<Vector, 2, T, Q> {
			inline static Vector<2, T, Q> call(T(*func) (T x, T y), const Vector<2, T, Q>& vec1, const Vector<2, T, Q>& vec2) {
				return Vector<2, T, Q>(func(vec1.x, vec2.x), func(vec1.y, vec2.y));
			}
		};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename T, qualifier Q>
		struct functor2<Vector, 3, T, Q> {
			inline static Vector<3, T, Q> call(T(*func) (T x, T y), const Vector<3, T, Q>& vec1, const Vector<3, T, Q>& vec2) {
				return Vector<3, T, Q>(func(vec1.x, vec2.x), func(vec1.y, vec2.y), func(vec1.z, vec2.z));
			}
		};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename T, qualifier Q>
		struct functor2<Vector, 4, T, Q> {
			inline static Vector<4, T, Q> call(T(*func) (T x, T y), const Vector<4, T, Q>& vec1, const Vector<4, T, Q>& vec2) {
				return Vector<4, T, Q>(func(vec1.x, vec2.x), func(vec1.y, vec2.y), func(vec1.z, vec2.z), func(vec1.w, vec2.w));
			}
		};

		template<template<size_t L, typename T, qualifier Q> class Vector, size_t L, typename T, qualifier Q>
		struct functor3 {};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename T, qualifier Q>
		struct functor3<Vector, 1, T, Q> {
			inline static Vector<1, T, Q> call(T(*func) (T x, T y, T z), const Vector<1, T, Q>& vec1, const Vector<1, T, Q>& vec2, const Vector<1, T, Q>& vec3) {
				return Vector<1, T, Q>(func(vec1.x, vec2.x, vec3.x));
			}
		};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename T, qualifier Q>
		struct functor3<Vector, 2, T, Q> {
			inline static Vector<2, T, Q> call(T(*func) (T x, T y, T z), const Vector<2, T, Q>& vec1, const Vector<2, T, Q>& vec2, const Vector<2, T, Q>& vec3) {
				return Vector<2, T, Q>(func(vec1.x, vec2.x, vec3.x), func(vec1.y, vec2.y, vec3.y));
			}
		};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename T, qualifier Q>
		struct functor3<Vector, 3, T, Q> {
			inline static Vector<3, T, Q> call(T(*func) (T x, T y, T z), const Vector<3, T, Q>& vec1, const Vector<3, T, Q>& vec2, const Vector<3, T, Q>& vec3) {
				return Vector<3, T, Q>(func(vec1.x, vec2.x, vec3.x), func(vec1.y, vec2.y, vec3.y), func(vec1.z, vec2.z, vec3.z));
			}
		};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename T, qualifier Q>
		struct functor3<Vector, 4, T, Q> {
			inline static Vector<4, T, Q> call(T(*func) (T x, T y, T z), const Vector<4, T, Q>& vec1, const Vector<4, T, Q>& vec2, const Vector<4, T, Q>& vec3) {
				return Vector<4, T, Q>(func(vec1.x, vec2.x, vec3.x), func(vec1.y, vec2.y, vec3.y), func(vec1.z, vec2.z, vec3.z), func(vec1.w, vec2.w, vec3.w));
			}
		};

		template<template<size_t L, typename T, qualifier Q> class Vector, size_t L, typename T, qualifier Q>
		struct functor2scale {};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename T, qualifier Q>
		struct functor2scale<Vector, 1, T, Q> {
			inline static Vector<1, T, Q> call(T(*func) (T x, T y), const Vector<1, T, Q>& vec1, const T& value) {
				return Vector<1, T, Q>(func(vec1.x, value));
			}
		};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename T, qualifier Q>
		struct functor2scale<Vector, 2, T, Q> {
			inline static Vector<2, T, Q> call(T(*func) (T x, T y), const Vector<2, T, Q>& vec1, const T& value) {
				return Vector<2, T, Q>(func(vec1.x, value), func(vec1.y, value));
			}
		};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename T, qualifier Q>
		struct functor2scale<Vector, 3, T, Q> {
			inline static Vector<3, T, Q> call(T(*func) (T x, T y), const Vector<3, T, Q>& vec1, const T& value) {
				return Vector<3, T, Q>(func(vec1.x, value), func(vec1.y, value), func(vec1.z, value));
			}
		};

		template<template<size_t L, typename T, qualifier Q> class Vector, typename T, qualifier Q>
		struct functor2scale<Vector, 4, T, Q> {
			inline static Vector<4, T, Q> call(T(*func) (T x, T y), const Vector<4, T, Q>& vec1, const T& value) {
				return Vector<4, T, Q>(func(vec1.x, value), func(vec1.y, value), func(vec1.z, value), func(vec1.w, value));
			}
		};

		// Common Functions

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> abs(const Vector<L, T, Q>& vec) {
			return functor1<Vector, L, T, T, Q>::call(abs, vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> ceil(const Vector<L, T, Q>& vec) {
			return functor1<Vector, L, T, T, Q>::call(ceil, vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> clamp(const Vector<L, T, Q>& vec, T min1, T max1) {
			return functor3<Vector, L, T, Q>::call(clamp, vec, Vector<L, T, Q>(min1), Vector<L, T, Q>(max1));
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> clamp(const Vector<L, T, Q>& vec, const Vector<L, T, Q>& min1, const Vector<L, T, Q>& max1) {
			return functor3<Vector, L, T, Q>::call(clamp, vec, min1, max1);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> floor(const Vector<L, T, Q>& vec) {
			return functor1<Vector, L, T, T, Q>::call(floor, vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> fract(const Vector<L, T, Q>& vec) {
			return functor1<Vector, L, T, T, Q>::call(fract, vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> frexp(const Vector<L, T, Q>& vec, Vector<L, int, Q>& exp) {
			Vector<L, T, Q> v;
			for (uint32_t i = 0; i < v.length(); i++)
				v[i] = std::frexp(vec[i], &exp[i]);
			return v;
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> isInf(const Vector<L, T, Q>& vec) {
			return functor1<Vector, L, T, T, Q>::call(isInf, vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> isNan(const Vector<L, T, Q>& vec) {
			return functor1<Vector, L, T, T, Q>::call(isNan, vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> ldexp(const Vector<L, T, Q>& vec, const Vector<L, int, Q>& exp) {
			Vector<L, T, Q> v;
			for (uint32_t i = 0; i < v.length(); i++)
				v[i] = std::ldexp(vec[i], &exp[i]);
			return v;
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> max(const Vector<L, T, Q>& vec, T value) {
			return functor2<Vector, L, T, Q>::call(max, vec, Vector<L, T, Q>(value));
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> max(T value, const Vector<L, T, Q>& vec) {
			return functor2<Vector, L, T, Q>::call(max, Vector<L, T, Q>(value), vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> max(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2) {
			return functor2<Vector, L, T, Q>::call(max, vec1, vec2);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> min(const Vector<L, T, Q>& vec, T value) {
			return functor2<Vector, L, T, Q>::call(min, vec, Vector<L, T, Q>(value));
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> min(T value, const Vector<L, T, Q>& vec) {
			return functor2<Vector, L, T, Q>::call(min, Vector<L, T, Q>(value), vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> min(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2) {
			return functor2<Vector, L, T, Q>::call(min, vec1, vec2);
		}

		template<size_t L, typename T, typename U, qualifier Q>
		inline constexpr Vector<L, T, Q> mix(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2, U a) {
			return functor3<Vector, L, T, Q>::call(mix, vec1, vec2, Vector<L, T, Q>(a));
		}

		template<size_t L, typename T, typename U, qualifier Q>
		inline constexpr Vector<L, T, Q> mix(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2, const Vector<L, U, Q>& a) {
			return functor3<Vector, L, T, Q>::call(mix, vec1, vec2, a);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> mod(const Vector<L, T, Q>& vec1, const T& value) {
			return functor2<Vector, L, T, Q>::call(mod, vec1, Vector<L, T, Q>(value));
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> mod(const Vector<L, T, Q>& vec, const Vector<L, T, Q>& modVec) {
			return functor2<Vector, L, T, Q>::call(mod, vec1, modVec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> modf(const Vector<L, T, Q>& vec1, const T& value) {
			return functor2<Vector, L, T, Q>::call(modf, vec1, Vector<L, T, Q>(value));
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> modf(const Vector<L, T, Q>& vec, const Vector<L, T, Q>& modVec) {
			return functor2<Vector, L, T, Q>::call(modf, vec1, modVec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> round(const Vector<L, T, Q>& vec) {
			return functor1<Vector, L, T, T, Q>::call(round, vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> roundEven(const Vector<L, T, Q>& vec) {
			return functor1<Vector, L, T, T, Q>::call(roundEven, vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> sign(const Vector<L, T, Q>& vec) {
			return functor1<Vector, L, T, T, Q>::call(round, vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> smoothstep(T edge0, T edge1, const Vector<L, T, Q>& vec) {
			return functor3<Vector, L, T, Q>(smoothstep, Vector<L, T, Q>(edge0), Vector<L, T, Q>(edge1), vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> smoothstep(const Vector<L, T, Q>& edge0, const Vector<L, T, Q>& edge1, const Vector<L, T, Q>& vec) {
			return functor3<Vector, L, T, Q>(smoothstep, edge0, edge1, vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> step(T edge, const Vector<L, T, Q>& vec) {
			return functor2<Vector, L, T, Q>(step, Vector<L, T, Q>(edge), vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> step(const Vector<L, T, Q>& edge, const Vector<L, T, Q>& vec) {
			return functor2<Vector, L, T, Q>(step, edge, vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> trunc(const Vector<L, T, Q>& vec) {
			return functor1<Vector, L, T, T, Q>::call(trunc, vec);
		}


		// Exponential Functions

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> sqrt(const Vector<L, T, Q>& vec) {
			return functor1<Vector, L, T, T, Q>::call(sqrt, vec);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> inversesqrt(const Vector<L, T, Q>& vec) {
			return functor1<Vector, L, T, T, Q>::call(inversesqrt, vec);
		}


		// Geometric Functions

		template<size_t L, typename T, qualifier Q, bool Aligned>
		struct computeLength {
			static T call(const Vector<L, T, Q>& v) {
				return sqrt(dot(v, v));
			}
		};

		template<size_t L, typename T, qualifier Q, bool Aligned>
		struct computeDistance {
			static T call(const Vector<L, T, Q>& v1, const Vector<L, T, Q>& v2) {
				return length(v1 - v2);
			}
		};

		template<size_t L, typename T, qualifier Q, bool Aligned>
		struct computeDot {};

		template<typename T, qualifier Q, bool Aligned>
		struct computeDot<1, T, Q, Aligned> {
			static T call(const Vector<1, T, Q>& v1, const Vector<1, T, Q>& v2) {
				return v1.x * v2.x;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeDot<2, T, Q, Aligned> {
			static T call(const Vector<2, T, Q>& v1, const Vector<2, T, Q>& v2) {
				Vector<2, T, Q> temp(v1 * v2);
				return temp.x + temp.y;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeDot<3, T, Q, Aligned> {
			static T call(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
				Vector<3, T, Q> temp(v1 * v2);
				return temp.x + temp.y + temp.z;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeDot<4, T, Q, Aligned> {
			static T call(const Vector<4, T, Q>& v1, const Vector<4, T, Q>& v2) {
				Vector<4, T, Q> temp(v1 * v2);
				return temp.x + temp.y + temp.z + temp.w;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeCross {
			static Vector<3, T, Q> call(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
				return Vector<3, T, Q>(v1.y * v2.z - v2.y * v1.z,
					v1.z * v2.x - v2.z * v1.x,
					v1.x * v2.y - v2.x * v1.y);
			}
		};

		template<size_t L, typename T, qualifier Q, bool Aligned>
		struct computeNormalize {
			static Vector<L, T, Q> call(const Vector<L, T, Q>& vec) {
				return vec * inversesqrt(dot(vec, vec));
			}
		};

		template<size_t L, typename T, qualifier Q, bool Aligned>
		struct computeFaceForward {
			static Vector<L, T, Q> call(const Vector<L, T, Q>& N, const Vector<L, T, Q>& I, const Vector<L, T, Q>& Nref) {
				return dot(Nref, I) < static_cast<T>(0) ? N : -N;
			}
		};

		template<size_t L, typename T, qualifier Q, bool Aligned>
		struct computeReflect {
			static Vector<L, T, Q> call(const Vector<L, T, Q>& I, const Vector<L, T, Q>& N) {
				return I - N * dot(N, I) * static_cast<T>(2);
			}
		};

		template<size_t L, typename T, qualifier Q, bool Aligned>
		struct computeRefract {
			static Vector<L, T, Q> call(const Vector<L, T, Q>& I, const Vector<L, T, Q>& N, T eta) {
				const T dotValue(dot(N, I));
				const T k(static_cast<T>(1) - eta * eta * (static_cast<T>(1) - dotValue * dotValue));
				const Vector<L, T, Q> result = (k >= static_cast<T>(0) ? (eta * I - (eta * dotValue + std::sqrt(k)) * N) : vec<L, T, Q>(0));
				return result;
				return I - N * dot(N, I) * static_cast<T>(2);
			}
		};

		template<size_t L, typename T, qualifier Q>
		inline constexpr T length(const Vector<L, T, Q>& vec) {
			return computeLength<L, T, Q, is_aligned<Q>::value>::call(vec);
		}

		template<typename T>
		inline constexpr T distance(const T& a, const T& b) {
			return length(b - a);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr T distance(const Vector<L, T, Q>& v1, const Vector<L, T, Q>& v2) {
			return computeDistance<L, T, Q, is_aligned<Q>::value>::call(v1, v2);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr T dot(const Vector<L, T, Q>& v1, const Vector<L, T, Q>& v2) {
			return computeDot<L, T, Q, is_aligned<Q>::value>::call(v1, v2);
		}

		template<typename T, qualifier Q>
		inline constexpr Vector<3, T, Q> cross(const Vector<3, T, Q>& v1, const Vector<3, T, Q>& v2) {
			return computeCross<T, Q, is_aligned<Q>::value>::call(v1, v2);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> normalize(const Vector<L, T, Q>& vec) {
			return computeNormalize<L, T, Q, is_aligned<Q>::value>::call(vec);
		}

		// Relational Functions

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> operator<(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2) {
			return lessThan(vec1, vec2);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> lessThan(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2) {
			Vector<L, bool, Q> result(true);
			for (size_t i = 0; i < L; i++)
				result[i] = vec1[i] < vec2[i];
			return result;
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> operator<=(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2) {
			return lessThanEqual(vec1, vec2);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> lessThanEqual(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2) {
			Vector<L, bool, Q> result(true);
			for (size_t i = 0; i < L; i++)
				result[i] = vec1[i] <= vec2[i];
			return result;
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> operator>(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2) {
			return greaterThan(vec1, vec2);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> greaterThan(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2) {
			Vector<L, bool, Q> result(true);
			for (size_t i = 0; i < L; i++)
				result[i] = vec1[i] > vec2[i];
			return result;
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> operator>=(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2) {
			return greaterThanEqual(vec1, vec2);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> greaterThanEqual(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2) {
			Vector<L, bool, Q> result(true);
			for (size_t i = 0; i < L; i++)
				result[i] = vec1[i] >= vec2[i];
			return result;
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> operator==(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2) {
			return equal(vec1, vec2);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> equal(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2) {
			Vector<L, bool, Q> result(true);
			for (size_t i = 0; i < L; i++)
				result[i] = vec1[i] == vec2[i];
			return result;
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> equal(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2, const T& epsilon) {
			return equal(vec1, vec2, Vector<L, T, Q>(epsilon));
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> equal(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2, const Vector<L, T, Q>& epsilon) {
			return lessThanEqual(abs(vec1 - vec2), epsilon);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> operator!=(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2) {
			return notEqual(vec1, vec2);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, bool, Q> notEqual(const Vector<L, T, Q>& vec1, const Vector<L, T, Q>& vec2) {
			Vector<L, bool, Q> result(true);
			for (size_t i = 0; i < L; i++)
				result[i] = vec1[i] != vec2[i];
			return result;
		}

		template<size_t L, qualifier Q>
		inline constexpr bool any(const Vector<L, bool, Q>& vec) {
			bool result = false;
			for (size_t i = 0; i < L; i++)
				result = result || vec[i];
			return result;
		}

		template<size_t L, qualifier Q>
		inline constexpr bool all(const Vector<L, bool, Q>& vec) {
			bool result = true;
			for (size_t i = 0; i < L; i++)
				result = result && vec[i];
			return result;
		}

		template<size_t L, qualifier Q>
		inline constexpr Vector<L, bool, Q> operator~(const Vector<L, bool, Q>& vec) {
			return not(vec);
		}

		template<size_t L, qualifier Q>
		inline constexpr Vector<L, bool, Q> not(const Vector<L, bool, Q>& v) {
			Vector<L, bool, Q> result(true);
			for (size_t i = 0; i < L; i++)
				result[i] = !v1[i];
			return result;
		}

		// Trigonomic Functions

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> radians(const Vector<L, T, Q>& vecDeg) {
			return functor1<Vector, L, T, T, Q>::call(radians, vecDeg);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> degrees(const Vector<L, T, Q>& vecRad) {
			return functor1<Vector, L, T, T, Q>::call(degrees, vecRad);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> sin(const Vector<L, T, Q>& vecRad) {
			return functor1<Vector, L, T, T, Q>::call(sin, vecRad);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> cos(const Vector<L, T, Q>& vecRad) {
			return functor1<Vector, L, T, T, Q>::call(cos, vecRad);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> tan(const Vector<L, T, Q>& vecRad) {
			return functor1<Vector, L, T, T, Q>::call(tan, vecRad);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> asin(const Vector<L, T, Q>& vecRad) {
			return functor1<Vector, L, T, T, Q>::call(asin, vecRad);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> acos(const Vector<L, T, Q>& vecRad) {
			return functor1<Vector, L, T, T, Q>::call(acos, vecRad);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> atan(const Vector<L, T, Q>& vecRad) {
			return functor1<Vector, L, T, T, Q>::call(atan, vecRad);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> atan2(const Vector<L, T, Q>& yVecRad, const Vector<L, T, Q>& xVecRad) {
			return functor2<Vector, L, T, Q>::call(atan2, yVecRad, xVecRad);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> sinh(const Vector<L, T, Q>& vecRad) {
			return functor1<Vector, L, T, T, Q>::call(sinh, vecRad);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> cosh(const Vector<L, T, Q>& vecRad) {
			return functor1<Vector, L, T, T, Q>::call(cosh, vecRad);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> tanh(const Vector<L, T, Q>& vecRad) {
			return functor1<Vector, L, T, T, Q>::call(tanh, vecRad);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> asinh(const Vector<L, T, Q>& vecRad) {
			return functor1<Vector, L, T, T, Q>::call(asinh, vecRad);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> acosh(const Vector<L, T, Q>& vecRad) {
			return functor1<Vector, L, T, T, Q>::call(acosh, vecRad);
		}

		template<size_t L, typename T, qualifier Q>
		inline constexpr Vector<L, T, Q> atanh(const Vector<L, T, Q>& vecRad) {
			return functor1<Vector, L, T, T, Q>::call(atanh, vecRad);
		}
	}
}