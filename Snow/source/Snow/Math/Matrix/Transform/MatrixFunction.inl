#pragma once

#include "Snow/Math/Common/Qualifier.h"


namespace Snow {
	namespace Math {
		template<size_t R, size_t C, typename T, qualifier Q, bool Aligned>
		struct computeMatrixCompMult {
			static Matrix<R, C, T, Q> call(const Matrix<R, C, T, Q>& m1, const Matrix<R, C, T, Q>& m2) {
				Matrix<R, C, T, Q> result;
				for (size_t i = 0, i < result.length(); i++)
					result[i] = m1[i] * m2[i];
				return result;
			}
		};

		template<size_t R, size_t C, typename T, qualifier Q, bool Aligned>
		struct computeTranspose {};

		template<typename T, qualifier Q, bool Aligned>
		struct computeTranspose<2, 2, T, Q, Aligned> {
			inline static Matrix<2, 2, T, Q> call(const Matrix<2, 2, T, Q>& mat) {
				Matrix<2, 2, T, Q> result;
				result[0][0] = mat[0][0];
				result[0][1] = mat[1][0];
				result[1][0] = mat[0][1];
				result[1][1] = mat[1][1];
				return result;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeTranspose<2, 3, T, Q, Aligned> {
			inline static Matrix<3, 2, T, Q> call(const Matrix<2, 3, T, Q>& mat) {
				Matrix<3, 2, T, Q> result;
				result[0][0] = mat[0][0];
				result[0][1] = mat[1][0];
				result[1][0] = mat[0][1];
				result[1][1] = mat[1][1];
				result[2][0] = mat[0][2];
				result[2][1] = mat[1][2];
				return result;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeTranspose<2, 4, T, Q, Aligned> {
			inline static Matrix<4, 2, T, Q> call(const Matrix<2, 4, T, Q>& mat) {
				Matrix<4, 2, T, Q> result;
				result[0][0] = mat[0][0];
				result[0][1] = mat[1][0];
				result[1][0] = mat[0][1];
				result[1][1] = mat[1][1];
				result[2][0] = mat[0][2];
				result[2][1] = mat[1][2];
				result[3][0] = mat[0][3];
				result[3][1] = mat[1][3];
				return result;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeTranspose<3, 2, T, Q, Aligned> {
			inline static Matrix<2, 3, T, Q> call(const Matrix<3, 2, T, Q>& mat) {
				Matrix<2, 3, T, Q> result;
				result[0][0] = mat[0][0];
				result[0][1] = mat[1][0];
				result[0][2] = mat[2][0];
				result[1][0] = mat[0][1];
				result[1][1] = mat[1][1];
				result[1][2] = mat[2][1];
				return result;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeTranspose<3, 3, T, Q, Aligned> {
			inline static Matrix<3, 3, T, Q> call(const Matrix<3, 3, T, Q>& mat) {
				Matrix<3, 3, T, Q> result;
				result[0][0] = mat[0][0];
				result[0][1] = mat[1][0];
				result[0][2] = mat[2][0];
				result[1][0] = mat[0][1];
				result[1][1] = mat[1][1];
				result[1][2] = mat[2][1];
				result[2][0] = mat[0][2];
				result[2][1] = mat[1][2];
				result[2][2] = mat[2][2];
				return result;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeTranspose<3, 4, T, Q, Aligned> {
			inline static Matrix<4, 3, T, Q> call(const Matrix<3, 4, T, Q>& mat) {
				Matrix<4, 3, T, Q> result;
				result[0][0] = mat[0][0];
				result[0][1] = mat[1][0];
				result[0][2] = mat[2][0];
				result[1][0] = mat[0][1];
				result[1][1] = mat[1][1];
				result[1][2] = mat[2][1];
				result[2][0] = mat[0][2];
				result[2][1] = mat[1][2];
				result[2][2] = mat[2][2];
				result[3][0] = mat[0][3];
				result[3][1] = mat[1][3];
				result[3][2] = mat[2][3];
				return result;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeTranspose<4, 2, T, Q, Aligned> {
			inline static Matrix<2, 4, T, Q> call(const Matrix<4, 2, T, Q>& mat) {
				Matrix<2, 4, T, Q> result;
				result[0][0] = mat[0][0];
				result[0][1] = mat[1][0];
				result[0][2] = mat[2][0];
				result[0][3] = mat[3][0];
				result[1][0] = mat[0][1];
				result[1][1] = mat[1][1];
				result[1][2] = mat[2][1];
				result[1][3] = mat[3][1];
				return result;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeTranspose<4, 3, T, Q, Aligned> {
			inline static Matrix<3, 4, T, Q> call(const Matrix<4, 3, T, Q>& mat) {
				Matrix<3, 4, T, Q> result;
				result[0][0] = mat[0][0];
				result[0][1] = mat[1][0];
				result[0][2] = mat[2][0];
				result[0][3] = mat[3][0];
				result[1][0] = mat[0][1];
				result[1][1] = mat[1][1];
				result[1][2] = mat[2][1];
				result[1][3] = mat[3][1];
				result[2][0] = mat[0][2];
				result[2][1] = mat[1][2];
				result[2][2] = mat[2][2];
				result[2][3] = mat[3][2];
				return result;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeTranspose<4, 4, T, Q, Aligned> {
			inline static Matrix<4, 4, T, Q> call(const Matrix<4, 4, T, Q>& mat) {
				Matrix<4, 4, T, Q> result;
				result[0][0] = mat[0][0];
				result[0][1] = mat[1][0];
				result[0][2] = mat[2][0];
				result[0][3] = mat[3][0];
				result[1][0] = mat[0][1];
				result[1][1] = mat[1][1];
				result[1][2] = mat[2][1];
				result[1][3] = mat[3][1];
				result[2][0] = mat[0][2];
				result[2][1] = mat[1][2];
				result[2][2] = mat[2][2];
				result[2][3] = mat[3][2];
				result[3][0] = mat[0][3];
				result[3][1] = mat[1][3];
				result[3][2] = mat[2][3];
				result[3][3] = mat[3][3];
				return result;
			}
		};

		template<size_t R, size_t C, typename T, qualifier Q, bool Aligned>
		struct computeDeterminant {};

		template<typename T, qualifier Q, bool Aligned>
		struct computeDeterminant<2, 2, T, Q, Aligned> {
			static T call(const Matrix<2, 2, T, Q>& mat) {
				return mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0];
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeDeterminant<3, 3, T, Q, Aligned> {
			static T call(const Matrix<3, 3, T, Q>& mat) {
				return
					+ mat[0][0] * (mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1])
					- mat[0][1] * (mat[1][0] * mat[2][2] - mat[1][2] * mat[2][0])
					+ mat[0][2] * (mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0]);
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeDeterminant<4, 4, T, Q, Aligned> {
			static T call(const Matrix<4, 4, T, Q>& mat) {
				//TODO
				return 0; //figure out determinant of 4x4 matrix
			}
		};

		template<size_t R, size_t C, typename T, qualifier Q, bool Aligned>
		struct computeCofactor {};

		template<typename T, qualifier Q, bool Aligned>
		struct computeCofactor<2, 2, T, Q, Aligned> {
			static Matrix<2, 2, T, Q> call(const Matrix<2, 2, T, Q>& mat) {
				Matrix<2, 2, T, Q> result;
				result[0][0] = +mat[1][1];
				result[0][1] = -mat[1][0];
				result[1][0] = -mat[0][1];
				result[1][1] = +mat[0][0];
				return result;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeCofactor<3, 3, T, Q, Aligned> {
			static Matrix<3, 3, T, Q> call(const Matrix<3, 3, T, Q>& mat) {
				Matrix<3, 3, T, Q> result;
				result[0][0] = +(mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1]);
				result[0][1] = -(mat[1][0] * mat[2][2] - mat[1][2] * mat[2][0]);
				result[0][2] = +(mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0]);
				result[1][0] = -(mat[0][1] * mat[2][2] - mat[0][2] * mat[2][1]);
				result[1][1] = +(mat[0][0] * mat[2][2] - mat[0][2] * mat[2][0]);
				result[1][2] = -(mat[0][0] * mat[2][1] - mat[0][1] * mat[2][0]);
				result[2][0] = +(mat[0][1] * mat[1][2] - mat[0][2] * mat[1][1]);
				result[2][1] = -(mat[0][0] * mat[1][2] - mat[0][2] * mat[1][0]);
				result[2][2] = +(mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0]);
				return result;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeCofactor<4, 4, T, Q, Aligned> {
			static Matrix<4, 4, T, Q> call(const Matrix<4, 4, T, Q>& mat) {
				Matrix<4, 4, T, Q> result;
				//TODO
				return result;
			}
		};

		template<size_t R, size_t C, typename T, qualifier Q, bool Aligned>
		struct computeAdjugate {};

		template<typename T, qualifier Q, bool Aligned>
		struct computeAdjugate<2, 2, T, Q, Aligned> {
			static Matrix<2, 2, T, Q> call(const Matrix<2, 2, T, Q>& mat) {
				Matrix<2, 2, T, Q> result;
				result[0][0] = mat[1][1];
				result[0][1] = -mat[0][1];
				result[1][0] = -mat[1][0];
				result[1][1] = mat[0][0];
				return result;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeAdjugate<3, 3, T, Q, Aligned> {
			static Matrix<3, 3, T, Q> call(const Matrix<3, 3, T, Q>& mat) {
				Matrix<3, 3, T, Q> result;
				result[0][0] = +(mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1]);
				result[0][1] = -(mat[0][1] * mat[2][2] - mat[0][2] * mat[2][1]);
				result[0][2] = +(mat[0][1] * mat[1][2] - mat[0][2] * mat[1][1]);
				result[1][0] = -(mat[1][0] * mat[2][2] - mat[1][2] * mat[2][0]);
				result[1][1] = +(mat[0][0] * mat[2][2] - mat[0][2] * mat[2][0]);
				result[1][2] = -(mat[0][0] * mat[1][2] - mat[0][2] * mat[1][0]);
				result[2][0] = +(mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0]);
				result[2][1] = -(mat[0][0] * mat[2][1] - mat[0][1] * mat[2][0]);
				result[2][2] = +(mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0]);
				return result;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeAdjugate<4, 4, T, Q, Aligned> {
			static Matrix<4, 4, T, Q> call(const Matrix<4, 4, T, Q>& mat) {
				//TODO
				Matrix<4, 4, T, Q> result(0);
				return result;
			}
			
		};

		template<size_t R, size_t C, typename T, qualifier Q, bool Aligned>
		struct computeInverse{};

		template<typename T, qualifier Q, bool Aligned>
		struct computeInverse<2, 2, T, Q, Aligned> {
			inline static Matrix<2, 2, T, Q> call(const Matrix<2, 2, T, Q>& mat) {
				//assert determinant != 0
				T oneOverDet = static_cast<T>(1) / (
					+ mat[0][0] * mat[1][1]
					- mat[0][1] * mat[1][0]);

				Matrix<2, 2, T, Q> inv( mat[1][1] * oneOverDet, -mat[0][1] * oneOverDet,
									   -mat[1][0] * oneOverDet,  mat[0][0] * oneOverDet);
				return inv;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeInverse<3, 3, T, Q, Aligned> {
			inline static Matrix<3, 3, T, Q> call(const Matrix<3, 3, T, Q>& mat) {
				//assert determinant != 0
				T oneOverDet = static_cast<T>(1) / (
					+mat[0][0] * (mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1])
					- mat[0][1] * (mat[1][0] * mat[2][2] - mat[1][2] * mat[2][0])
					+ mat[0][2] * (mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0])
					);

				Matrix<3, 3, T, Q> inv;
				inv[0][0] = +(mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1]) * oneOverDet;
				inv[0][1] = -(mat[0][1] * mat[2][2] - mat[0][2] * mat[2][1]) * oneOverDet;
				inv[0][2] = +(mat[0][1] * mat[1][2] - mat[0][2] * mat[1][1]) * oneOverDet;
				inv[1][0] = -(mat[1][0] * mat[2][2] - mat[1][2] * mat[2][0]) * oneOverDet;
				inv[1][1] = +(mat[0][0] * mat[2][2] - mat[0][2] * mat[2][0]) * oneOverDet;
				inv[1][2] = -(mat[0][0] * mat[1][2] - mat[0][2] * mat[1][0]) * oneOverDet;
				inv[2][0] = +(mat[1][0] * mat[2][1] - mat[2][0] * mat[1][1]) * oneOverDet;
				inv[2][1] = -(mat[0][0] * mat[2][1] - mat[0][1] * mat[2][0]) * oneOverDet;
				inv[2][2] = +(mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0]) * oneOverDet;
				return inv;
			}
		};

		template<typename T, qualifier Q, bool Aligned>
		struct computeInverse<4, 4, T, Q, Aligned> {
			inline static Matrix<4, 4, T, Q> call(const Matrix<4, 4, T, Q>& mat) {

				T a2323 = mat[2][2] * mat[3][3] - mat[2][3] * mat[3][2];
				T a1323 = mat[2][1] * mat[3][3] - mat[2][3] * mat[3][1];
				T a1223 = mat[2][1] * mat[3][2] - mat[2][2] * mat[3][1];
				T a0323 = mat[2][0] * mat[3][3] - mat[2][3] * mat[3][0];
				T a0223 = mat[2][0] * mat[3][2] - mat[2][2] * mat[3][0];
				T a0123 = mat[2][0] * mat[3][1] - mat[2][1] * mat[3][0];
				T a2313 = mat[1][2] * mat[3][3] - mat[1][3] * mat[3][2];
				T a1313 = mat[1][1] * mat[3][3] - mat[1][3] * mat[3][1];
				T a1213 = mat[1][1] * mat[3][2] - mat[1][2] * mat[3][1];
				T a2312 = mat[1][2] * mat[2][3] - mat[1][3] * mat[2][2];
				T a1312 = mat[1][1] * mat[2][3] - mat[1][3] * mat[2][1];
				T a1212 = mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1];
				T a0313 = mat[1][0] * mat[3][3] - mat[1][3] * mat[3][0];
				T a0213 = mat[1][0] * mat[3][2] - mat[1][2] * mat[3][0];
				T a0312 = mat[1][0] * mat[2][3] - mat[1][3] * mat[2][0];
				T a0212 = mat[1][0] * mat[2][2] - mat[1][2] * mat[2][0];
				T a0113 = mat[1][0] * mat[3][1] - mat[1][1] * mat[3][0];
				T a0112 = mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0];

				T oneOverDet = static_cast<T>(1) /  (mat[0][0] * (mat[1][1] * a2323 - mat[1][2] * a1323 + mat[1][3] * a1223)
											- mat[0][1] * (mat[1][0] * a2323 - mat[1][2] * a0323 + mat[1][3] * a0223)
											+ mat[0][2] * (mat[1][0] * a1323 - mat[1][1] * a0323 + mat[1][3] * a0123)
											- mat[0][3] * (mat[1][0] * a1223 - mat[1][1] * a0223 + mat[1][2] * a0123));

				
				//assert determinant != 0

				Matrix<4, 4, T, Q> inv(
					(oneOverDet *  (mat[1][1] * a2323 - mat[1][2] * a1323 + mat[1][3] * a1223)), (oneOverDet * -(mat[0][1] * a2323 - mat[0][2] * a1323 + mat[0][3] * a1223)), (oneOverDet *  (mat[0][1] * a2313 - mat[0][2] * a1313 + mat[0][3] * a1213)), (oneOverDet * -(mat[0][1] * a2312 - mat[0][2] * a1312 + mat[0][3] * a1212)) ,
					(oneOverDet * -(mat[1][0] * a2323 - mat[1][2] * a0323 + mat[1][3] * a0223)), (oneOverDet *  (mat[0][0] * a2323 - mat[0][2] * a0323 + mat[0][3] * a0223)), (oneOverDet * -(mat[0][0] * a2313 - mat[0][2] * a0313 + mat[0][3] * a0213)), (oneOverDet *  (mat[0][0] * a2312 - mat[0][2] * a0312 + mat[0][3] * a0212)) ,
					(oneOverDet *  (mat[1][0] * a1323 - mat[1][1] * a0323 + mat[1][3] * a0123)), (oneOverDet * -(mat[0][0] * a1323 - mat[0][1] * a0323 + mat[0][3] * a0123)), (oneOverDet *  (mat[0][0] * a1313 - mat[0][1] * a0313 + mat[0][3] * a0113)), (oneOverDet * -(mat[0][0] * a1312 - mat[0][1] * a0312 + mat[0][3] * a0112)) , 
					(oneOverDet * -(mat[1][0] * a1223 - mat[1][1] * a0223 + mat[1][2] * a0123)), (oneOverDet *  (mat[0][0] * a1223 - mat[0][1] * a0223 + mat[0][2] * a0123)), (oneOverDet * -(mat[0][0] * a1213 - mat[0][1] * a0213 + mat[0][2] * a0113)), (oneOverDet *  (mat[0][0] * a1212 - mat[0][1] * a0212 + mat[0][2] * a0112)) 
				);

				//TODO
				
				return inv;
			}
		};

		template<size_t R, size_t C, typename T, qualifier Q>
		Matrix<R, C, T, Q> MatrixCompMult(const Matrix<R, C, T, Q>& x, const Matrix<R, C, T, Q>& y) {
			return computeMatrixCompMult<C, R, T, Q, is_aligned<Q>::value>::call(x, y);
		}

		template<size_t R, size_t C, typename T, qualifier Q>
		typename Matrix<R, C, T, Q>::transpose_type transpose(const Matrix<R, C, T, Q>& m) {
			return computeTranspose<R, C, T, Q, is_aligned<Q>::value>::call(m);
		}

		template<size_t R, size_t C, typename T, qualifier Q>
		T determinant(const Matrix<R, C, T, Q>& m) {
			return computeDeterminant<R, C, T, Q, is_aligned<Q>::value>::call(m);
		}

		template<size_t R, size_t C, typename T, qualifier Q>
		Matrix<R, C, T, Q> cofactor(const Matrix<R, C, T, Q>& m) {
			return computeCofactor<R, C, T, Q, is_aligned<Q>::value>::call(m);
		}

		template<size_t R, size_t C, typename T, qualifier Q>
		Matrix<R, C, T, Q> adjugate(const Matrix<R, C, T, Q>& m) {
			return computeAdjugate<R, C, T, Q, is_aligned<Q>::value>::call(m);
		}

		template<size_t R, size_t C, typename T, qualifier Q>
		inline Matrix<R, C, T, Q> inverse(const Matrix<R, C, T, Q>& m) {
			return computeInverse<R, C, T, Q, is_aligned<Q>::value>::call(m);
		}

		template<size_t R, size_t C, typename T, typename U, qualifier Q>
		Matrix<R, C, T, Q> mix(const Matrix<R, C, T, Q>& x, const Matrix<R, C, T, Q>& y, U a) {
			return Matrix<R, C, T, Q>(x) * (static_cast<U>(1 - a) + mat<R, C, T, Q>(y) * a);
		}

		template<size_t R, size_t C, typename T, typename U, qualifier Q>
		Matrix<R, C, T, Q> mix(const Matrix<R, C, T, Q>& x, const Matrix<R, C, T, Q>& y, const Matrix<R, C, T, Q>& a) {
			return MatrixCompMult(Matrix<R, C, T, Q>(x), static_cast<U>(1) - a + MatrixCompMult(Matrix<R, C, T, Q>(y), a));
		}
	}
}