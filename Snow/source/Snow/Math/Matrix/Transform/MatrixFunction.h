#pragma once

#include "Snow/Math/Common/Common.h"

namespace Snow {
	namespace Math {
		template<size_t R, size_t C, typename T, qualifier Q>
		struct outerProductTrait {};

		template<typename T, qualifier Q>
		struct outerProductTrait<2, 2, T, Q> {
			typedef Matrix<2, 2, T, Q> type;
		};

		template<typename T, qualifier Q>
		struct outerProductTrait<2, 3, T, Q> {
			typedef Matrix<3, 2, T, Q> type;
		};

		template<typename T, qualifier Q>
		struct outerProductTrait<2, 4, T, Q> {
			typedef Matrix<4, 2, T, Q> type;
		};

		template<typename T, qualifier Q>
		struct outerProductTrait<3, 2, T, Q> {
			typedef Matrix<2, 3, T, Q> type;
		};

		template<typename T, qualifier Q>
		struct outerProductTrait<3, 3, T, Q> {
			typedef Matrix<3, 3, T, Q> type;
		};

		template<typename T, qualifier Q>
		struct outerProductTrait<3, 4, T, Q> {
			typedef Matrix<4, 3, T, Q> type;
		};

		template<typename T, qualifier Q>
		struct outerProductTrait<4, 2, T, Q> {
			typedef Matrix<2, 4, T, Q> type;
		};

		template<typename T, qualifier Q>
		struct outerProductTrait<4, 3, T, Q> {
			typedef Matrix<3, 4, T, Q> type;
		};

		template<typename T, qualifier Q>
		struct outerProductTrait<4, 4, T, Q> {
			typedef Matrix<4, 4, T, Q> type;
		};

		template<size_t R, size_t C, typename T, qualifier Q>
		Matrix<R, C, T, Q> MatrixCompMult(const Matrix<R, C, T, Q>& x, const Matrix<R, C, T, Q>& y);

		template<size_t R, size_t C, typename T, qualifier Q>
		typename outerProductTrait<R, C, T, Q>::type outerProduct(const Vector<R, T, Q>& r, const Vector<C, T, Q>& c);

		template<size_t R, size_t C, typename T, qualifier Q>
		typename Matrix<R, C, T, Q>::transpose_type transpose(const Matrix<R, C, T, Q>& m);

		template<size_t R, size_t C, typename T, qualifier Q>
		T determinant(const Matrix<R, C, T, Q>& m);

		template<size_t R, size_t C, typename T, qualifier Q>
		Matrix<R, C, T, Q> cofactor(const Matrix<C, R, T, Q>& m);

		//transpose of cofactor of a matrix
		template<size_t R, size_t C, typename T, qualifier Q>
		Matrix<R, C, T, Q> adjugate(const Matrix<R, C, T, Q>& m);

		template<size_t R, size_t C, typename T, qualifier Q>
		inline Matrix<R, C, T, Q> inverse(const Matrix<R, C, T, Q>& m);

		template<size_t R, size_t C, typename T, typename U, qualifier Q>
		Matrix<R, C, T, Q> mix(const Matrix<R, C, T, Q>& x, const Matrix<R, C, T, Q>& y, const Matrix<R, C, U, Q>& a);

		template<size_t R, size_t C, typename T, typename U, qualifier Q>
		Matrix<R, C, T, Q> mix(const Matrix<R, C, T, Q>& x, const Matrix<R, C, T, Q>& y, U a);
	}
}
#include "MatrixFunction.inl"