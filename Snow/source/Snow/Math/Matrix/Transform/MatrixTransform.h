#pragma once

#include "Snow/Math/Common/Common.h"
#include "Snow/Math/Common/Qualifier.h"

namespace Snow {
	namespace Math {
		template<typename T>
		T identity();

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> translate(const Vector<3, T, Q>& vec);

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> translate(const Matrix<4, 4, T, Q>& mat, const Vector<3, T, Q>& vec);

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> rotate(T angle, const Vector<3, T, Q>& vec);

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> rotate(const Matrix<4, 4, T, Q>& mat, T angle, const Vector<3, T, Q>& axis);

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> scale(const Vector<3, T, Q>& vec);

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> scale(const Matrix<4, 4, T, Q>& mat, const Vector<3, T, Q>& scale);

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> lookAtRH(const Vector<3, T, Q>& eye, const Vector<3, T, Q>& center, const Vector<3, T, Q>& up);

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> lookAtLH(const Vector<3, T, Q>& eye, const Vector<3, T, Q>& center, const Vector<3, T, Q>& up);

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> lookAt(const Vector<3, T, Q>& eye, const Vector<3, T, Q>& center, const Vector<3, T, Q>& up);
	}
}

#include "MatrixTransform.inl"