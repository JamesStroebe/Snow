
#include "Snow/Math/Common/Qualifier.h"

namespace Snow {
	namespace Math {

		/*
		Different coord systems,
		LH (Left-Handed) coord system has x - right, y - up, z - INTO the screen,
		RH (Right-Handed) coord system has x - right, y - up, z - TOWARD the viewer,

		ZO  z coords (near and far planes) are nomalized to 0 to 1 respectively, near goes to 0 and far goes to 1
		NO  z coords (near and far planes) are nomalized to -1 to 1 respectively, near goes to -1 and far goes to 1

		*/

		//ortho projection is written by this
		/*
			[2/(right-left),  	  0           ,		0			,  -(right+left)/(right-left)]
			[  0		   ,  2/(top-bottom)  ,		0			,  -(top+bottom)/(top-bottom)]
			[  0		   ,	  0			  ,  -2/(far-near)  ,  -(far+near)/(far-near)    ]
			[  0		   ,	  0			  ,		0			,			1				 ]
		
		*/

		/*
		LH far plane is positive z,
		RH far plane is negative z,
		*/

		template<typename T>
		Matrix<4, 4, T, defaultp> Ortho(T left, T right, T bottom, T top) {
			Matrix<4, 4, T, defaultp> result(static_cast<T>(1));

			result[0][0] = static_cast<T>(2) / (right - left);
			result[0][3] = -(right + left) / (right - left);
			result[1][1] = static_cast<T>(2) / (top - bottom);
			result[1][3] = -(top + bottom) / (top - bottom);
			result[2][2] = -static_cast<T>(1); //far is 1, near is -1
			result[2][3] = static_cast<T>(0);

			return result;
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoLH_ZO(T left, T right, T bottom, T top, T zNear, T zFar) {
			Matrix<4, 4, T, defaultp> result(static_cast<T>(1));

			result[0][0] = static_cast<T>(2) / (right - left);
			result[0][3] = -(right + left) / (right - left);
			result[1][1] = static_cast<T>(2) / (top - bottom);
			result[1][3] = -(top + bottom) / (top - bottom);
			result[2][2] = static_cast<T>(1) / (zFar - zNear); 
			result[2][3] = -(zNear)/(zFar-zNear); //far goes to 1,  near goes to 0

			return result;
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoLH_NO(T left, T right, T bottom, T top, T zNear, T zFar) {
			Matrix<4, 4, T, defaultp> result(static_cast<T>(1));

			result[0][0] = static_cast<T>(2) / (right - left);
			result[0][3] = -(right + left) / (right - left);
			result[1][1] = static_cast<T>(2) / (top - bottom);
			result[1][3] = -(top + bottom) / (top - bottom);
			result[2][2] = static_cast<T>(2) / (zFar - zNear);
			result[2][3] = -(zFar + zNear) / (zFar - zNear); //far goes to 1,  near goes to 0

			return result;
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoRH_ZO(T left, T right, T bottom, T top, T zNear, T zFar) {
			Matrix<4, 4, T, defaultp> result(static_cast<T>(1));

			result[0][0] = static_cast<T>(2) / (right - left);
			result[0][3] = -(right + left) / (right - left);
			result[1][1] = static_cast<T>(2) / (top - bottom);
			result[1][3] = -(top + bottom) / (top - bottom);
			result[2][2] = - static_cast<T>(1) / (zFar - zNear);
			result[2][3] = -(zNear) / (zFar - zNear); //far goes to 1,  near goes to 0

			return result;
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoRH_NO(T left, T right, T bottom, T top, T zNear, T zFar) {
			Matrix<4, 4, T, defaultp> result(static_cast<T>(1));

			result[0][0] = static_cast<T>(2) / (right - left);
			result[0][3] = -(right + left) / (right - left);
			result[1][1] = static_cast<T>(2) / (top - bottom);
			result[1][3] = -(top + bottom) / (top - bottom);
			result[2][2] = - static_cast<T>(2) / (zFar - zNear);
			result[2][3] = -(zFar + zNear) / (zFar - zNear); //far goes to 1,  near goes to 0

			return result;
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoZO(T left, T right, T bottom, T top, T zNear, T zFar) {
			
			//check for right or left handed system

			return OrthoLH_ZO(left, right, bottom, top, zNear, zFar);

		}

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoNO(T left, T right, T bottom, T top, T zNear, T zFar) {

			//check for right or left handed system

			return OrthoLH_NO(left, right, bottom, top, zNear, zFar);

		}

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoLH(T left, T right, T bottom, T top, T zNear, T zFar) {

			//check for normalized or zero norm

			return OrthoLH_ZO(left, right, bottom, top, zNear, zFar);

		}

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoRH(T left, T right, T bottom, T top, T zNear, T zFar) {

			//check for normalized or zero norm

			return OrthoRH_ZO(left, right, bottom, top, zNear, zFar);

		}

		template<typename T>
		Matrix<4, 4, T, defaultp> Ortho(T left, T right, T bottom, T top, T zNear, T zFar) {

			//check for LH or RH AND normalized or zero norm

			return OrthoLH_ZO(left, right, bottom, top, zNear, zFar);

		}

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveLH_NO(T fovY, T aspect, T zNear, T zFar) {
			const T tanhalfFov = tan(fovY / static_cast<T>(2));

			Matrix<4, 4, T, defaultp> result(static_cast<T>(0));
			result[0][0] = static_cast<T>(1) / (aspect * tanhalfFov);
			result[1][1] = static_cast<T>(1) / (tanhalfFov);
			result[2][2] = (zFar + zNear) / (zFar - zNear);
			result[2][3] = -(static_cast<T>(2)* zFar* zNear) / (zFar - zNear);
			result[3][2] = static_cast<T>(1);
			return result;
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveRH_NO(T fovY, T aspect, T zNear, T zFar) {
			const T tanhalfFov = tan(fovY / static_cast<T>(2));

			Matrix<4, 4, T, defaultp> result(static_cast<T>(0));
			result[0][0] = static_cast<T>(1) / (aspect * tanhalfFov);
			result[1][1] = static_cast<T>(1) / (tanhalfFov);
			result[2][2] = (zFar + zNear) / (zFar - zNear);
			result[2][3] = -(static_cast<T>(2)* zFar* zNear) / (zFar - zNear);
			result[3][2] = static_cast<T>(1);
			return result;
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> Perspective(T fovY, T aspect, T zNear, T zFar) {
			return PerspectiveLH_NO(fovY, aspect, zNear, zFar);
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovLH_ZO(T fovY, T width, T height, T zNear, T zFar) {
			const T rad = fovY;
			const T h = Math::cos(static_cast<T>(0.5)* rad) / Math::sin(static_cast<T>(0.5)* rad);
			const T w = h * height / width;

			Matrix<4, 4, T, defaultp> result(static_cast<T>(0));
			result[0][0] = w;
			result[1][1] = h;
			result[2][2] = zFar / (zFar - zNear);
			result[2][3] = -(zFar * zNear) / (zFar - zNear);
			result[3][2] = static_cast<T>(1);
			return result;
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovLH_NO(T fovY, T width, T height, T zNear, T zFar) {
			const T rad = fovY;
			const T h = Math::cos(static_cast<T>(0.5)* rad) / Math::sin(static_cast<T>(0.5)* rad);
			const T w = h * height / width;

			Matrix<4, 4, T, defaultp> result(static_cast<T>(0));
			result[0][0] = w;
			result[1][1] = h;
			result[2][2] = (zFar + zNear) / (zFar - zNear);
			result[2][3] = -(static_cast<T>(2)* zFar * zNear) / (zFar - zNear);
			result[3][2] = static_cast<T>(1);
			return result;
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovRH_ZO(T fovY, T width, T height, T zNear, T zFar) {
			const T rad = fovY;
			const T h = Math::cos(static_cast<T>(0.5)* rad) / Math::sin(static_cast<T>(0.5)* rad);
			const T w = h * height / width;

			Matrix<4, 4, T, defaultp> result(static_cast<T>(0));
			result[0][0] = w;
			result[1][1] = h;
			result[2][2] = zFar / (zFar - zNear);
			result[2][3] = -(zFar * zNear) / (zFar - zNear);
			result[3][2] = -static_cast<T>(1);
			return result;
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovRH_NO(T fovY, T width, T height, T zNear, T zFar) {
			const T rad = fovY;
			const T h = Math::cos(static_cast<T>(0.5) * rad) / Math::sin(static_cast<T>(0.5) * rad);
			const T w = h * height / width;

			Matrix<4, 4, T, defaultp> result(static_cast<T>(0));
			result[0][0] = w;
			result[1][1] = h;
			result[2][2] = -(zFar + zNear) / (zFar - zNear);
			result[2][3] = -(static_cast<T>(2) * zFar * zNear) / (zFar - zNear);
			result[3][2] = -static_cast<T>(1);
			return result;
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovZO(T fovY, T width, T height, T zNear, T zFar) {
			return PerspectiveFovLH_ZO(fovY, width, height, zNear, zFar);
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovNO(T fovY, T width, T height, T zNear, T zFar) {
			return PerspectiveFovLH_NO(fovY, width, height, zNear, zFar);
		}


		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovLH(T fovY, T width, T height, T zNear, T zFar) {
			return PerspectiveFovLH_ZO(fovY, width, height, zNear, zFar);
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovRH(T fovY, T width, T height, T zNear, T zFar) {
			return PerspectiveFovRH_ZO(fovY, width, height, zNear, zFar);
		}

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFov(T fovY, T width, T height, T zNear, T zFar) {
			return PerspectiveFovRH_NO(fovY, width, height, zNear, zFar);
		}
	}
}