#pragma once

#include "Snow/Math/Common/Qualifier.h"

namespace Snow {
	namespace Math {
		template<typename T>
		T identity() {
			return initGenType<T, genTypeTrait<T>::gentype>::identity();
		}

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> translate(const Vector<3, T, Q>& vec) {
			return translate(Matrix<4, 4, T, Q>(static_cast<T>(1)), vec);
		}

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> translate(const Matrix<4, 4, T, Q>& mat, const Vector<3, T, Q>& vec) {
			Matrix<4, 4, T, Q> result(mat);

			result[0][3] = mat[0][0] * vec.x;
			result[1][3] = mat[1][1] * vec.y;
			result[2][3] = mat[2][2] * vec.z;
			result[3][3] = mat[3][3];
			

			return result;
		}

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> rotate(T angle, const Vector<3, T, Q>& vec) {
			return rotate(Matrix<4, 4, T, Q>(static_cast<T>(1)), angle, vec);
		}

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> rotate(const Matrix<4, 4, T, Q>& mat, T angle, const Vector<3, T, Q>& vec) {
			const T a = angle;
			const T c = cos(a);
			const T s = sin(a);

			Vector<3, T, Q> axis = normalize(vec);

			Matrix<4, 4, T, Q> rot;
			rot[0][0] = c + (static_cast<T>(1) - c) * axis.x * axis.x;
			rot[0][1] = (static_cast<T>(1) - c) * axis.y * axis.x - s * axis.z;
			rot[0][2] = (static_cast<T>(1) - c) * axis.z * axis.x + s * axis.y;
			rot[0][3] = static_cast<T>(0);

			rot[1][0] = (static_cast<T>(1) - c) * axis.x * axis.y + s * axis.z;
			rot[1][1] = c + (static_cast<T>(1) - c) * axis.y * axis.y;
			rot[1][2] = (static_cast<T>(1) - c) * axis.z * axis.y - s * axis.x;
			rot[1][3] = static_cast<T>(0);

			rot[2][0] = (static_cast<T>(1) - c) * axis.x * axis.z - s * axis.y;
			rot[2][1] = (static_cast<T>(1) - c) * axis.y * axis.z + s * axis.x;
			rot[2][2] = c + (static_cast<T>(1) - c) * axis.z * axis.z;
			rot[2][3] = static_cast<T>(0);

			rot[3][0] = static_cast<T>(0);
			rot[3][1] = static_cast<T>(0);
			rot[3][2] = static_cast<T>(0);
			rot[3][3] = static_cast<T>(1);

			return mat * rot;
		}

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> scale(const Vector<3, T, Q>& vec) {
			return scale(Matrix<4, 4, T, Q>(static_cast<T>(1)), vec);
		}

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> scale(const Matrix<4, 4, T, Q>& mat, const Vector<3, T, Q>& vec) {
			
			Matrix<4, 4, T, Q> result(mat);

			result[0][0] *= vec.x;
			result[1][1] *= vec.y;
			result[2][2] *= vec.z;

			return result;
		}

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> lookAtRH(const Vector<3, T, Q>& eye, const Vector<3, T, Q>& center, const Vector<3, T, Q>& up) {
			
			const Vector<3, T, Q> f(normalize(center - eye));
			const Vector<3, T, Q> s(normalize(cross(up, f)));
			const Vector<3, T, Q> u(cross(f, s));

			Matrix<4, 4, T, Q> result(1);
			result[0][0] = s.x;
			result[0][1] = s.y;
			result[0][2] = s.z;
			result[0][3] = -dot(s, eye);
			result[1][0] = u.x;
			result[1][1] = u.y;
			result[1][2] = u.z;
			result[1][3] = -dot(u, eye);
			result[2][0] = -f.x;
			result[2][1] = -f.y;
			result[2][2] = -f.z;
			result[2][3] = dot(f, eye);
			return result;
		}

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> lookAtLH(const Vector<3, T, Q>& eye, const Vector<3, T, Q>& center, const Vector<3, T, Q>& up) {
			
			const Vector<3, T, Q> f(normalize(center - eye));
			const Vector<3, T, Q> s(normalize(cross(up, f)));
			const Vector<3, T, Q> u(cross(f, s));

			Matrix<4, 4, T, Q> result(1);
			result[0][0] = s.x;
			result[0][1] = s.y;
			result[0][2] = s.z;
			result[0][3] = -dot(s, eye);
			result[1][0] = u.x;
			result[1][1] = u.y;
			result[1][2] = u.z;
			result[1][3] = -dot(u, eye);
			result[2][0] = f.x;
			result[2][1] = f.y;
			result[2][2] = f.z;
			result[2][3] = -dot(f, eye);

			return result;
		}

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> lookAt(const Vector<3, T, Q>& eye, const Vector<3, T, Q>& center, const Vector<3, T, Q>& up) {
			


			return lookAtLH(eye, center, up);
		}
	}
}