#pragma once

#include "Snow/Math/Trig/Trigonomic.h"

namespace Snow {
	namespace Math {

		/*
		Different coord systems,
		LH (Left-Handed) coord system has x - right, y - up, z - INTO the screen,
		RH (Right-Handed) coord system has x - right, y - up, z - TOWARD the viewer,

		ZO  z coords (near and far planes) are nomalized to 0 to 1 respectively, near goes to 0 and far goes to 1
		NO  z coords (near and far planes) are nomalized to -1 to 1 respectively, near goes to -1 and far goes to 1
		
		*/

		template<typename T>
		inline Matrix<4, 4, T, defaultp> Ortho(T left, T right, T bottom, T top);

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoLH_ZO(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoLH_NO(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoRH_ZO(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoRH_NO(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoZO(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoNO(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoLH(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> OrthoRH(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> Ortho(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> FrustumLH_ZO(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> FrustumLH_NO(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> FrustumRH_ZO(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> FrustumRH_NO(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> FrustumZO(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> FrustumNO(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> FrustumLH(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> FrustumRH(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> Frustum(T left, T right, T bottom, T top, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveLH_ZO(T fovY, T aspect, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveLH_NO(T fovY, T aspect, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveRH_ZO(T fovY, T aspect, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveRH_NO(T fovY, T aspect, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveZO(T fovY, T aspect, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveNO(T fovY, T aspect, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveLH(T fovY, T aspect, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveRH(T fovY, T aspect, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> Perspective(T fovY, T aspect, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovLH_ZO(T fovY, T width, T height, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovLH_NO(T fovY, T width, T height, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovRH_ZO(T fovY, T width, T height, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovRH_NO(T fovY, T width, T height, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovZO(T fovY, T width, T height, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovNO(T fovY, T width, T height, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovLH(T fovY, T width, T height, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFovRH(T fovY, T width, T height, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> PerspectiveFov(T fovY, T width, T height, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> InfinitePerspectiveLH(T fovY, T width, T height, T zNear, T zFar);

		template<typename T>
		Matrix<4, 4, T, defaultp> InfinitePerspectiveLH(T fovY, T aspect, T zNear);

		template<typename T>
		Matrix<4, 4, T, defaultp> InfinitePerspectiveRH(T fovY, T aspect, T zNear);

		template<typename T>
		Matrix<4, 4, T, defaultp> InfinitePerspective(T fovY, T aspect, T zNear);

		template<typename T>
		Matrix<4, 4, T, defaultp> TweakedInfinitePerspective(T fovY, T aspect, T zNear);

		template<typename T>
		Matrix<4, 4, T, defaultp> TweakedInfinitePerspective(T fovY, T aspect, T zNear, T ep);
	}
}
#include "MatrixClipSpace.inl"