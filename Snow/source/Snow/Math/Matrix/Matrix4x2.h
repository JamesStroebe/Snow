#pragma once

#include "Snow/Math/Common/Qualifier.h"

namespace Snow {
	namespace Math {
		template<typename T, qualifier Q>
		struct Matrix<4, 2, T, Q> {
			typedef T value_type;
			typedef Vector<2, T, Q> row_type;
			typedef Vector<4, T, Q> col_type;
			typedef Matrix<4, 2, T, Q> type;
			typedef Matrix<2, 4, T, Q> transpose_type;

			typedef size_t length_type;
			static length_type length() { return 4; }

			row_type& operator[](length_type i);
			constexpr const row_type& operator[](length_type i) const;

			//implicit constructors
			Matrix();
			template<qualifier P>
			Matrix(const Matrix<4, 2, T, P>& m);

			//explicit constructors
			explicit Matrix(T scalar);
			Matrix(const T& x1, const T& x2,
				   const T& y1, const T& y2,
				   const T& z1, const T& z2, 
				   const T& w1, const T& w2);
			Matrix(const row_type& v1, const row_type& v2, const row_type& v3, const row_type& v4);

			// conversion constructors
			template<typename X1, typename X2,
				     typename Y1, typename Y2,
				     typename Z1, typename Z2,
					 typename W1, typename W2>
			Matrix(const X1& x1, const X2& x2, 
				   const Y1& y1, const Y2& y2,
				   const Z1& z1, const Z2& z2,
				   const W1& w1, const W2& w2);

			template<typename V1, typename V2, typename V3, typename V4>
			Matrix(const Vector<2, V1, Q>& v1, const Vector<2, V2, Q>& v2, const Vector<2, V3, Q>& v3, const Vector<2, V4, Q>& v4);

			//matrix conversions

			template<typename U, qualifier P>
			Matrix(const Matrix<4, 2, U, P>& m);

			Matrix(const Matrix<2, 2, T, Q>& m);
			Matrix(const Matrix<2, 3, T, Q>& m);
			Matrix(const Matrix<2, 4, T, Q>& m);
			Matrix(const Matrix<3, 2, T, Q>& m);
			Matrix(const Matrix<3, 3, T, Q>& m);
			Matrix(const Matrix<3, 4, T, Q>& m);
			Matrix(const Matrix<4, 3, T, Q>& m);
			Matrix(const Matrix<4, 4, T, Q>& m);

			//unary arithmetic operators

			template<typename U>
			Matrix<4, 2, T, Q>& operator=(const Matrix<4, 2, U, Q>& m);

			template<typename U>
			Matrix<4, 2, T, Q>& operator+=(U scalar);

			template<typename U>
			Matrix<4, 2, T, Q>& operator+=(const Matrix<4, 2, U, Q>& m);

			template<typename U>
			Matrix<4, 2, T, Q>& operator-=(U scalar);

			template<typename U>
			Matrix<4, 2, T, Q>& operator-=(const Matrix<4, 2, U, Q>& m);

			template<typename U>
			Matrix<4, 2, T, Q>& operator*=(U scalar);

			template<typename U>
			Matrix<4, 2, T, Q>& operator*=(const Matrix<4, 2, U, Q>& m);

			template<typename U>
			Matrix<4, 2, T, Q>& operator/=(U scalar);

			template<typename U>
			Matrix<4, 2, T, Q>& operator/=(const Matrix<4, 2, U, Q>& m);

			//Increment and decrement operators
			Matrix<4, 2, T, Q>& operator++();
			Matrix<4, 2, T, Q>& operator--();
			Matrix<4, 2, T, Q> operator++(int);
			Matrix<4, 2, T, Q> operator--(int);

		private:
			row_type values[4];
		};

		//unary operators
		template<typename T, qualifier Q>
		Matrix<4, 2, T, Q> operator+(const Matrix<4, 2, T, Q>& m);

		template<typename T, qualifier Q>
		Matrix<4, 2, T, Q> operator-(const Matrix<4, 2, T, Q>& m);

		//binary operators

		template<typename T, qualifier Q>
		Matrix<4, 2, T, Q> operator+(const Matrix<4, 2, T, Q>& m, T scalar);

		template<typename T, qualifier Q>
		Matrix<4, 2, T, Q> operator+(T scalar, const Matrix<4, 2, T, Q>& m);

		template<typename T, qualifier Q>
		Matrix<4, 2, T, Q> operator+(const Matrix<4, 2, T, Q>& m1, const Matrix<4, 2, T, Q>& m2);

		template<typename T, qualifier Q>
		Matrix<4, 2, T, Q> operator-(const Matrix<4, 2, T, Q>& m, T scalar);

		template<typename T, qualifier Q>
		Matrix<4, 2, T, Q> operator-(T scalar, const Matrix<4, 2, T, Q>& m);

		template<typename T, qualifier Q>
		Matrix<4, 2, T, Q> operator-(const Matrix<4, 2, T, Q>& m1, const Matrix<4, 2, T, Q>& m2);

		template<typename T, qualifier Q>
		Matrix<4, 2, T, Q> operator*(const Matrix<4, 2, T, Q>& m, T scalar);

		template<typename T, qualifier Q>
		Matrix<4, 2, T, Q> operator*(T scalar, const Matrix<4, 2, T, Q>& m);

		template<typename T, qualifier Q>
		typename Matrix<4, 2, T, Q>::col_type operator*(const Matrix<4, 2, T, Q>& m, typename const Matrix<4, 2, T, Q>::row_type& v);

		template<typename T, qualifier Q>
		typename Matrix<4, 2, T, Q>::row_type operator*(typename const Matrix<4, 2, T, Q>::col_type& v, const Matrix<4, 2, T, Q>& m);

		template<typename T, qualifier Q>
		Matrix<4, 2, T, Q> operator*(const Matrix<4, 2, T, Q>& m1, const Matrix<2, 2, T, Q>& m2);

		template<typename T, qualifier Q>
		Matrix<4, 3, T, Q> operator*(const Matrix<4, 2, T, Q>& m1, const Matrix<2, 3, T, Q>& m2);

		template<typename T, qualifier Q>
		Matrix<4, 4, T, Q> operator*(const Matrix<4, 2, T, Q>& m1, const Matrix<2, 4, T, Q>& m2);

		template<typename T, qualifier Q>
		Matrix<4, 2, T, Q> operator/(const Matrix<4, 2, T, Q>& m, T scalar);

		template<typename T, qualifier Q>
		Matrix<4, 2, T, Q> operator/(T scalar, const Matrix<4, 2, T, Q>& m);

		//Boolean operators

		template<typename T, qualifier Q>
		bool operator==(const Matrix<4, 2, T, Q>& m1, const Matrix<4, 2, T, Q>& m2);

		template<typename T, qualifier Q>
		bool operator!=(const Matrix<4, 2, T, Q>& m1, const Matrix<4, 2, T, Q>& m2);


		typedef Matrix<4, 2, float, qualifier::defaultp>	Matrix4x2f;

		typedef Matrix<4, 2, float, qualifier::highp>		Matrix4x2f_highp;
		typedef Matrix<4, 2, float, qualifier::mediump>		Matrix4x2f_mediump;
		typedef Matrix<4, 2, float, qualifier::lowp>		Matrix4x2f_lowp;

		typedef Matrix<4, 2, bool, qualifier::defaultp>		Matrix4x2b;

		typedef Matrix<4, 2, bool, qualifier::highp>		Matrix4x2b_highp;
		typedef Matrix<4, 2, bool, qualifier::mediump>		Matrix4x2b_mediump;
		typedef Matrix<4, 2, bool, qualifier::lowp>			Matrix4x2b_lowp;

		typedef Matrix<4, 2, double, qualifier::defaultp>	Matrix4x2d;

		typedef Matrix<4, 2, double, qualifier::highp>		Matrix4x2d_highp;
		typedef Matrix<4, 2, double, qualifier::mediump>	Matrix4x2d_mediump;
		typedef Matrix<4, 2, double, qualifier::lowp>		Matrix4x2d_lowp;

		typedef Matrix<4, 2, int, qualifier::defaultp>		Matrix4x2i;

		typedef Matrix<4, 2, int, qualifier::highp>			Matrix4x2i_highp;
		typedef Matrix<4, 2, int, qualifier::mediump>		Matrix4x2i_mediump;
		typedef Matrix<4, 2, int, qualifier::lowp>			Matrix4x2i_lowp;

		typedef Matrix<4, 2, unsigned int, qualifier::defaultp> Matrix4x2u;

		typedef Matrix<4, 2, unsigned int, qualifier::highp>	Matrix4x2u_highp;
		typedef Matrix<4, 2, unsigned int, qualifier::mediump>	Matrix4x2u_mediump;
		typedef Matrix<4, 2, unsigned int, qualifier::lowp>		Matrix4x2u_lowp;
	}
}
