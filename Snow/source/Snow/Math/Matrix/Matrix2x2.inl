#pragma once

namespace Snow {
	namespace Math {

		template<typename T, qualifier Q>
		typename Matrix<2, 2, T, Q>::row_type& Matrix<2, 2, T, Q>::operator[](typename Matrix<2, 2, T, Q>::length_type i) {
			//assert this.length>i>0
			return this->values[i];
		}

		template<typename T, qualifier Q>
		constexpr typename const Matrix<2, 2, T, Q>::row_type Matrix<2, 2, T, Q>::operator[](typename Matrix<2, 2, T, Q>::length_type i) const {
			return this->values[i];
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q>::Matrix() :
			values { row_type(1, 0), row_type(0, 1) } {}

		template<typename T, qualifier Q>
		template<qualifier P>
		Matrix<2, 2, T, Q>::Matrix(const Matrix<2, 2, T, P>& mat) :
			values { mat[0], mat[1] } {}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q>::Matrix(T scalar) :
			values { row_type(scalar, 0), row_type(0, scalar) } {}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q>::Matrix(const T& x1, const T& x2,
								   const T& y1, const T& y2) :
			values { row_type(x1, x2), row_type(y1, y2) } {}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q>::Matrix(const row_type& v1, const row_type& v2) :
			values { v1, v2 } {}

		template<typename T, qualifier Q>
		template<typename X1, typename X2, typename Y1, typename Y2>
		Matrix<2, 2, T, Q>::Matrix(const X1& x1, const X2& x2, const Y1& y1, const Y2& y2) :
			values { row_type(static_cast<T>(x1), static_cast<T>(x2)), 
					 row_type(static_cast<T>(y1), static_cast<T>(y2))} {}

		template<typename T, qualifier Q>
		template<typename V1, typename V2>
		Matrix<2, 2, T, Q>::Matrix(const Vector<2, V1, Q>& v1, const Vector<2, V2, Q>& v2) :
			values { row_type(v1), row_type(v2) } {}

		template<typename T, qualifier Q>
		template<typename U, qualifier P>
		Matrix<2, 2, T, Q>::Matrix(const Matrix<2, 2, U, P>& mat) :
			values { row_type(mat[0]), row_type(mat[1]) } {}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q>::Matrix(const Matrix<2, 3, T, Q>& mat) :
			values{ row_type(mat[0]), row_type(mat[1]) } {}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q>::Matrix(const Matrix<2, 4, T, Q>& mat) :
			values{ row_type(mat[0]), row_type(mat[1]) } {}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q>::Matrix(const Matrix<3, 2, T, Q>& mat) :
			values{ row_type(mat[0]), row_type(mat[1]) } {}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q>::Matrix(const Matrix<3, 3, T, Q>& mat) :
			values{ row_type(mat[0]), row_type(mat[1]) } {}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q>::Matrix(const Matrix<3, 4, T, Q>& mat) :
			values{ row_type(mat[0]), row_type(mat[1]) } {}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q>::Matrix(const Matrix<4, 2, T, Q>& mat) :
			values{ row_type(mat[0]), row_type(mat[1]) } {}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q>::Matrix(const Matrix<4, 3, T, Q>& mat) :
			values{ row_type(mat[0]), row_type(mat[1]) } {}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q>::Matrix(const Matrix<4, 4, T, Q>& mat) :
			values{ row_type(mat[0]), row_type(mat[1]) } {}

		template<typename T, qualifier Q>
		template<typename U>
		Matrix<2, 2, T, Q>& Matrix<2, 2, T, Q>::operator=(const Matrix<2, 2, U, Q>& mat) {
			this->values[0] = mat[0];
			this->values[1] = mat[1];
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		Matrix<2, 2, T, Q>& Matrix<2, 2, T, Q>::operator+=(U scalar) {
			this->value[0] += scalar;
			this->value[1] += scalar;
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		Matrix<2, 2, T, Q>& Matrix<2, 2, T, Q>::operator+=(const Matrix<2, 2, U, Q>& mat) {
			this->value[0] += mat[0];
			this->value[1] += mat[1];
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		Matrix<2, 2, T, Q>& Matrix<2, 2, T, Q>::operator-=(U scalar) {
			this->value[0] -= scalar;
			this->value[1] -= scalar;
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		Matrix<2, 2, T, Q>& Matrix<2, 2, T, Q>::operator-=(const Matrix<2, 2, U, Q>& mat) {
			this->value[0] -= mat[0];
			this->value[1] -= mat[1];
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		Matrix<2, 2, T, Q>& Matrix<2, 2, T, Q>::operator*=(U scalar) {
			this->value[0] *= scalar;
			this->value[1] *= scalar;
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		Matrix<2, 2, T, Q>& Matrix<2, 2, T, Q>::operator*=(const Matrix<2, 2, U, Q>& mat) {
			return (*this = *this * mat);
		}

		template<typename T, qualifier Q>
		template<typename U>
		Matrix<2, 2, T, Q>& Matrix<2, 2, T, Q>::operator/=(U scalar) {
			this->value[0] /= scalar;
			this->value[1] /= scalar;
			return *this;
		}

		template<typename T, qualifier Q>
		template<typename U>
		Matrix<2, 2, T, Q>& Matrix<2, 2, T, Q>::operator/=(const Matrix<2, 2, U, Q>& mat) {
			return *this *= inverse(mat);
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q>& Matrix<2, 2, T, Q>::operator++() {
			++this->values[0];
			++this->values[1];
			return *this;
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q>& Matrix<2, 2, T, Q>::operator--() {
			--this->values[0];
			--this->values[1];
			return *this;
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> Matrix<2, 2, T, Q>::operator++(int) {
			Matrix<2, 2, T, Q> result(*this);
			++*this;
			return result;
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> Matrix<2, 2, T, Q>::operator--(int) {
			Matrix<2, 2, T, Q> result(*this);
			--* this;
			return result;
		}

		//unary operators
		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator+(const Matrix<2, 2, T, Q>& mat) {
			return mat;
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator-(const Matrix<2, 2, T, Q>& mat) {
			return Matrix<2, 2, T, Q>(-mat[0], -mat[1]);
		}

		//binary arithmetic operators
		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator+(const Matrix<2, 2, T, Q>& mat, T scalar) {
			return Matrix<2, 2, T, Q>(mat[0] + scalar, mat[1] + scalar);
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator+(T scalar, const Matrix<2, 2, T, Q>& mat) {
			return Matrix<2, 2, T, Q>(mat[0] + scalar, mat[1] + scalar);
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator+(const Matrix<2, 2, T, Q>& m1, const Matrix<2, 2, T, Q>& m2) {
			return Matrix<2, 2, T, Q>(m1[0] + m2[0], m1[1] + m2[1]);
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator-(const Matrix<2, 2, T, Q>& mat, T scalar) {
			return Matrix<2, 2, T, Q>(mat[0] - scalar, mat[1] - scalar);
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator-(T scalar, const Matrix<2, 2, T, Q>& mat) {
			return Matrix<2, 2, T, Q>(scalar - mat[0], scalar - mat[1]);
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator-(const Matrix<2, 2, T, Q>& m1, const Matrix<2, 2, T, Q>& m2) {
			return Matrix<2, 2, T, Q>(m1[0] - m2[0], m1[1] - m2[1]);
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator*(const Matrix<2, 2, T, Q>& mat, T scalar) {
			return Matrix<2, 2, T, Q>(mat[0] * scalar, mat[1] * scalar);
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator*(T scalar, const Matrix<2, 2, T, Q>& mat) {
			return Matrix<2, 2, T, Q>(scalar * mat[0], scalar * mat[1]);
		}

		template<typename T, qualifier Q>
		typename Matrix<2, 2, T, Q>::col_type operator*(const Matrix<2, 2, T, Q>& mat, const typename Matrix<2, 2, T, Q>::row_type& vec) {
			return Vector<2, T, Q>(mat[0][0] * vec.x + mat[0][1] * vec.y, mat[1][0] * vec.x + mat[1][1] * vec.y);
		}

		template<typename T, qualifier Q>
		typename Matrix<2, 2, T, Q>::row_type operator*(const typename Matrix<2, 2, T, Q>::col_type& vec, const Matrix<2, 2, T, Q>& mat) {
			return Vector<2, T, Q>(vec.x * mat[0][0] + vec.y * mat[0][1], vec.x * mat[1][0] + vec.y * mat[1][1]);
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator*(const Matrix<2, 2, T, Q>& m1, const Matrix<2, 2, T, Q>& m2) {
			return Matrix<2, 2, T, Q>(
				(m1[0][0] * m2[0][0] + m1[0][1] * m2[1][0]), (m1[0][0] * m2[0][1] + m1[0][1] * m2[1][1]),
				(m1[1][0] * m2[0][0] + m1[1][1] * m2[1][0]), (m1[1][0] * m2[0][1] + m1[1][1] * m2[1][1])
				);
		}

		template<typename T, qualifier Q>
		Matrix<2, 3, T, Q> operator*(const Matrix<2, 2, T, Q>& m1, const Matrix<2, 3, T, Q>& m2) {
			return Matrix<2, 3, T, Q>(
				(m1[0][0] * m2[0][0] + m1[0][1] * m2[1][0]), (m1[0][0] * m2[0][1] + m1[0][1] * m2[1][1]), (m1[0][0] * m2[0][2] + m1[0][1] * m2[1][2]),
				(m1[1][0] * m2[0][0] + m1[1][1] * m2[1][0]), (m1[1][0] * m2[0][1] + m1[1][1] * m2[1][1]), (m1[1][0] * m2[0][2] + m1[1][1] * m2[1][2])
				);
		}

		template<typename T, qualifier Q>
		Matrix<2, 4, T, Q> operator*(const Matrix<2, 2, T, Q>& m1, const Matrix<2, 4, T, Q>& m2) {
			return Matrix<2, 4, T, Q>(
				(m1[0][0] * m2[0][0] + m1[0][1] * m1[1][0]), (m1[0][0] * m2[0][1] + m1[0][1] * m2[1][1]), (m1[0][0] * m2[0][2] + m1[0][1] * m2[1][2]), (m1[0][0] * m2[0][3] + m1[0][1] * m2[1][3]),
				(m1[1][0] * m2[0][0] + m1[1][1] * m1[1][0]), (m1[1][0] * m2[0][1] + m1[1][1] * m2[1][1]), (m1[1][0] * m2[0][2] + m1[1][1] * m2[1][2]), (m1[1][0] * m2[0][3] + m1[1][1] * m2[1][3])
				);
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator/(const Matrix<2, 2, T, Q>& mat, T scalar) {
			return Matrix<2, 2, T, Q>(mat[0] / scalar, mat[1] / scalar);
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator/(T scalar, const Matrix<2, 2, T, Q>& mat) {
			return Matrix<2, 2, T, Q>(scalar / mat[0], scalar / mat[1]);
		}

		template<typename T, qualifier Q>
		typename Matrix<2, 2, T, Q>::col_type operator/(const Matrix<2, 2, T, Q>& mat, typename const Matrix<2, 2, T, Q>::row_type& vec) {
			return inverse(mat) * vec;
		}

		template<typename T, qualifier Q>
		typename Matrix<2, 2, T, Q>::row_type operator/(typename const Matrix<2, 2, T, Q>::col_type& vec, const Matrix<2, 2, T, Q>& mat) {
			return vec * inverse(mat);
		}

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator/(const Matrix<2, 2, T, Q>& m1, const Matrix<2, 2, T, Q>& m2) {
			Matrix<2, 2, T, Q> result(m1);
			return result /= m2;
		}

		//boolean operators

		template<typename T, qualifier Q>
		bool operator==(const Matrix<2, 2, T, Q>& m1, const Matrix<2, 2, T, Q>& m2) {
			return (m1[0] == m2[0]) && (m1[1] == m2[1]);
		}

		template<typename T, qualifier Q>
		bool operator!=(const Matrix<2, 2, T, Q>& m1, const Matrix<2, 2, T, Q>& m2) {
			return (m1[0] != m2[0]) || (m1[1] != m2[1]);
		}
	}
}