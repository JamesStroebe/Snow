#pragma once

#include "Snow/Math/Vector.h"

#include "Snow/Math/Common/Qualifier.h"

namespace Snow {
	namespace Math {
		template<typename T, qualifier Q>
		struct Matrix<2, 4, T, Q> {
			typedef T value_type;
			typedef Vector<4, T, Q> row_type;
			typedef Vector<2, T, Q> col_type;
			typedef Matrix<2, 4, T, Q> type;
			typedef Matrix<4, 2, T, Q> transpose_type;

			typedef size_t length_type;
			static length_type length() { return 2; }

			row_type& operator[](length_type i);
			constexpr const row_type& operator[](length_type i) const;

			//implicit constructors
			Matrix();
			template<qualifier P>
			Matrix(const Matrix<2, 4, T, P>& m);

			//explicit constructors
			explicit Matrix(T scalar);
			Matrix(const T& x1, const T& x2, const T& x3, const T& x4,
				   const T& y1, const T& y2, const T& y3, const T& y4);
			Matrix(const row_type& v1, const row_type& v2);

			// conversion constructors
			template<typename X1, typename X2, typename X3, typename X4,
				     typename Y1, typename Y2, typename Y3, typename Y4>
			Matrix(const X1& x1, const X2& x2, const X3& x3, const X4& x4, 
				   const Y1& y1, const Y2& y2, const Y3& y3, const Y4& y4);

			template<typename V1, typename V2>
			Matrix(const Vector<4, V1, Q>& v1, const Vector<4, V2, Q>& v2);

			//matrix conversions

			template<typename U, qualifier P>
			Matrix(const Matrix<2, 4, U, P>& m);

			Matrix(const Matrix<2, 2, T, Q>& m);
			Matrix(const Matrix<2, 3, T, Q>& m);
			Matrix(const Matrix<3, 2, T, Q>& m);
			Matrix(const Matrix<3, 3, T, Q>& m);
			Matrix(const Matrix<3, 4, T, Q>& m);
			Matrix(const Matrix<4, 2, T, Q>& m);
			Matrix(const Matrix<4, 3, T, Q>& m);
			Matrix(const Matrix<4, 4, T, Q>& m);


			//unary arithmetic operators

			template<typename U>
			Matrix<2, 4, T, Q>& operator=(const Matrix<2, 4, U, Q>& m);

			template<typename U>
			Matrix<2, 4, T, Q>& operator+=(U scalar);

			template<typename U>
			Matrix<2, 4, T, Q>& operator+=(const Matrix<2, 4, U, Q>& m);

			template<typename U>
			Matrix<2, 4, T, Q>& operator-=(U scalar);

			template<typename U>
			Matrix<2, 4, T, Q>& operator-=(const Matrix<2, 4, U, Q>& m);

			template<typename U>
			Matrix<2, 4, T, Q>& operator*=(U scalar);

			template<typename U>
			Matrix<2, 4, T, Q>& operator*=(const Matrix<2, 4, U, Q>& m);

			template<typename U>
			Matrix<2, 4, T, Q>& operator/=(U scalar);

			template<typename U>
			Matrix<2, 4, T, Q>& operator/=(const Matrix<2, 4, U, Q>& m);

			//Increment and decrement operators
			Matrix<2, 4, T, Q>& operator++();
			Matrix<2, 4, T, Q>& operator--();
			Matrix<2, 4, T, Q> operator++(int);
			Matrix<2, 4, T, Q> operator--(int);

		private:
			row_type values[2];
		};

		//unary operators
		template<typename T, qualifier Q>
		Matrix<2, 4, T, Q> operator+(const Matrix<2, 4, T, Q>& m);

		template<typename T, qualifier Q>
		Matrix<2, 4, T, Q> operator-(const Matrix<2, 4, T, Q>& m);

		//binary operators

		template<typename T, qualifier Q>
		Matrix<2, 4, T, Q> operator+(const Matrix<2, 4, T, Q>& m, T scalar);

		template<typename T, qualifier Q>
		Matrix<2, 4, T, Q> operator+(T scalar, const Matrix<2, 4, T, Q>& m);

		template<typename T, qualifier Q>
		Matrix<2, 4, T, Q> operator+(const Matrix<2, 4, T, Q>& m1, const Matrix<2, 4, T, Q>& m2);

		template<typename T, qualifier Q>
		Matrix<2, 4, T, Q> operator-(const Matrix<2, 4, T, Q>& m, T scalar);

		template<typename T, qualifier Q>
		Matrix<2, 4, T, Q> operator-(T scalar, const Matrix<2, 4, T, Q>& m);

		template<typename T, qualifier Q>
		Matrix<2, 4, T, Q> operator-(const Matrix<2, 4, T, Q>& m1, const Matrix<2, 4, T, Q>& m2);

		template<typename T, qualifier Q>
		Matrix<2, 4, T, Q> operator*(const Matrix<2, 4, T, Q>& m, T scalar);

		template<typename T, qualifier Q>
		Matrix<2, 4, T, Q> operator*(T scalar, const Matrix<2, 4, T, Q>& m);

		template<typename T, qualifier Q>
		typename Matrix<2, 4, T, Q>::col_type operator*(const Matrix<2, 4, T, Q>& m, typename const Matrix<2, 4, T, Q>::row_type& v);

		template<typename T, qualifier Q>
		typename Matrix<2, 4, T, Q>::row_type operator*(typename const Matrix<2, 4, T, Q>::col_type& v, const Matrix<2, 4, T, Q>& m);

		template<typename T, qualifier Q>
		Matrix<2, 2, T, Q> operator*(const Matrix<2, 4, T, Q>& m1, const Matrix<4, 2, T, Q>& m2);

		template<typename T, qualifier Q>
		Matrix<2, 3, T, Q> operator*(const Matrix<2, 4, T, Q>& m1, const Matrix<4, 3, T, Q>& m2);

		template<typename T, qualifier Q>
		Matrix<2, 4, T, Q> operator*(const Matrix<2, 4, T, Q>& m1, const Matrix<4, 4, T, Q>& m2);

		template<typename T, qualifier Q>
		Matrix<2, 4, T, Q> operator/(const Matrix<2, 4, T, Q>& m, T scalar);

		template<typename T, qualifier Q>
		Matrix<2, 4, T, Q> operator/(T scalar, const Matrix<2, 4, T, Q>& m);


		//Boolean operators

		template<typename T, qualifier Q>
		bool operator==(const Matrix<2, 4, T, Q>& m1, const Matrix<2, 4, T, Q>& m2);

		template<typename T, qualifier Q>
		bool operator!=(const Matrix<2, 4, T, Q>& m1, const Matrix<2, 4, T, Q>& m2);


		typedef Matrix<2, 4, float, qualifier::defaultp>	Matrix2x4f;

		typedef Matrix<2, 4, float, qualifier::highp>		Matrix2x4f_highp;
		typedef Matrix<2, 4, float, qualifier::mediump>		Matrix2x4f_mediump;
		typedef Matrix<2, 4, float, qualifier::lowp>		Matrix2x4f_lowp;

		typedef Matrix<2, 4, bool, qualifier::defaultp>		Matrix2x4b;

		typedef Matrix<2, 4, bool, qualifier::highp>		Matrix2x4b_highp;
		typedef Matrix<2, 4, bool, qualifier::mediump>		Matrix2x4b_mediump;
		typedef Matrix<2, 4, bool, qualifier::lowp>			Matrix2x4b_lowp;

		typedef Matrix<2, 4, double, qualifier::defaultp>	Matrix2x4d;

		typedef Matrix<2, 4, double, qualifier::highp>		Matrix2x4d_highp;
		typedef Matrix<2, 4, double, qualifier::mediump>	Matrix2x4d_mediump;
		typedef Matrix<2, 4, double, qualifier::lowp>		Matrix2x4d_lowp;

		typedef Matrix<2, 4, int, qualifier::defaultp>		Matrix2x4i;

		typedef Matrix<2, 4, int, qualifier::highp>			Matrix2x4i_highp;
		typedef Matrix<2, 4, int, qualifier::mediump>		Matrix2x4i_mediump;
		typedef Matrix<2, 4, int, qualifier::lowp>			Matrix2x4i_lowp;

		typedef Matrix<2, 4, unsigned int, qualifier::defaultp> Matrix2x4u;

		typedef Matrix<2, 4, unsigned int, qualifier::highp>	Matrix2x4u_highp;
		typedef Matrix<2, 4, unsigned int, qualifier::mediump>	Matrix2x4u_mediump;
		typedef Matrix<2, 4, unsigned int, qualifier::lowp>		Matrix2x4u_lowp;
	}
}
#include "Snow/Math/Matrix/Matrix2x4.inl"