#pragma once

#include "Setup.h"

#if SNOW_ARCH & SNOW_ARCH_SSE2_BIT
	typedef __m128			snow_f32vec4;
	typedef __m128i			snow_i32vec4;
	typedef __m128i			snow_ui32vec4;
	typedef __m128d			snow_f64vec2;
	typedef __m128i			snow_i64vec2;
	typedef __m128i			snow_ui64vec2;
	
	typedef snow_f64vec2	snow_dvec2;
#endif

#if SNOW_ARCH & SNOW_ARCH_AVX_BIT
	typedef __m256d			snow_f64vec4;
	typedef snow_f64vec4	snow_dvec4;
#endif

#if SNOW_ARCH & SNOW_ARCH_AVX2_BIT
	typedef __m256i			snow_i64vec4;
	typedef __m256i			snow_ui64vec4;
#endif