#pragma once

#include "Snow/Math/Common/Geometric.h"

#include "Setup.h"

#include "Snow/Math/Simd/Platform.h"

#if SNOW_ARCH & SNOW_ARCH_SSE2_BIT

void Snow_matrix4x4_matrixCompMult(const snow_f32vec4 in1[4], const snow_f32vec4 in2[4], snow_f32vec4 out[4]) {
	out[0] = _mm_mul_ps(in1[0], in2[0]);
	out[1] = _mm_mul_ps(in1[1], in2[1]);
	out[2] = _mm_mul_ps(in1[2], in2[2]);
	out[3] = _mm_mul_ps(in1[3], in2[3]);
}

void Snow_matrix4x4_add(const snow_f32vec4 in1[4], const snow_f32vec4 in2[4], snow_f32vec4 out[4]) {
	out[0] = _mm_add_ps(in1[0], in2[0]);
	out[1] = _mm_add_ps(in1[1], in2[1]);
	out[2] = _mm_add_ps(in1[2], in2[2]);
	out[3] = _mm_add_ps(in1[3], in2[3]);
}

void Snow_matrix4x4_sub(const snow_f32vec4 in1[4], const snow_f32vec4 in2[4], snow_f32vec4 out[4]) {
	out[0] = _mm_sub_ps(in1[0], in2[0]);
	out[1] = _mm_sub_ps(in1[1], in2[1]);
	out[2] = _mm_sub_ps(in1[2], in2[2]);
	out[3] = _mm_sub_ps(in1[3], in2[3]);
}

snow_f32vec4 Snow_matrix4x4_mul_vector4(const snow_f32vec4 m[4], snow_f32vec4 v) {
	__m128 v0 = _mm_shuffle_ps(v, v, _MM_SHUFFLE(0, 0, 0, 0));
	__m128 v1 = _mm_shuffle_ps(v, v, _MM_SHUFFLE(1, 1, 1, 1));
	__m128 v2 = _mm_shuffle_ps(v, v, _MM_SHUFFLE(2, 2, 2, 2));
	__m128 v3 = _mm_shuffle_ps(v, v, _MM_SHUFFLE(3, 3, 3, 3));

	__m128 m0 = _mm_mul_ps(m[0], v0);
	__m128 m1 = _mm_mul_ps(m[1], v1);
	__m128 m2 = _mm_mul_ps(m[2], v2);
	__m128 m3 = _mm_mul_ps(m[3], v3);

	__m128 a0 = _mm_add_ps(m0, m1);
	__m128 a1 = _mm_add_ps(m2, m3);
	__m128 a2 = _mm_add_ps(a0, a1);

	return a2;
}

#endif