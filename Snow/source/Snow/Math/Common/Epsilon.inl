#pragma once

#include "Common.h"

namespace Snow {
	namespace Math {

		template<typename T>
		T Epsilon() {
			return std::numeric_limits<T>::epsilon();
		}

		template<typename T>
		bool epsilonEqual(const T& x, const T& y, const T& epsilon) {
			return abs(x - y) < epsilon;
		}

		template<size_t L, typename T, qualifier Q>
		Vector<L, bool, Q> epsilonEqual(const Vector<L, T, Q>& x, const Vector<L, T, Q>& y, const T& epsilon) {
			return lessThan(abs(x - y), Vector<L, T, Q>(epsilon));
		}

		template<size_t L, typename T, qualifier Q>
		Vector<L, bool, Q> epsilonEqual(const Vector<L, T, Q>& x, const Vector<L, T, Q>& y, const Vector<L, T, Q>& epsilon) {
			return lessThan(abs(x - y), epsilon);
		}

		template<typename T>
		bool epsilonNotEqual(const T& x, const T& y, const T& epsilon) {
			return abs(x - y) >= epsilon;
		}

		template<size_t L, typename T, qualifier Q>
		Vector<L, bool, Q> epsilonNotEqual(const Vector<L, T, Q>& x, const Vector<L, T, Q>& y, const T& epsilon) {
			return greaterThanEqual(abs(x - y), Vector<L, T, Q>(epsilon));
		}

		template<size_t L, typename T, qualifier Q>
		Vector<L, bool, Q> epsilonNotEqual(const Vector<L, T, Q>& x, const Vector<L, T, Q>& y, const Vector<L, T, Q>& epsilon) {
			return greaterThanEqual(abs(x - y), epsilon);
		}

		template<typename T, qualifier Q>
		Vector<4, bool, Q> epsilonEqual(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2, const T& epsilon) {
			Vector<4, T, Q> v(q1.x - q2.x, q1.y - q2.y, q1.z - q2.z, q1.w - q2.w);
			return lessThan(abs(v), Vector<4, T, Q>(epsilon));
		}

		template<typename T, qualifier Q>
		Vector<4, bool, Q> epsilonNotEqual(const Quaternion<T, Q>& q1, const Quaternion<T, Q>& q2, const T& epsilon) {
			Vector<4, T, Q> v(q1.x - q2.x, q1.y - q2.y, q1.z - q2.z, q1.w - q2.w);
			return greaterThanEqual(abs(v), Vector<4, T, Q>(epsilon));
		}
	}
}