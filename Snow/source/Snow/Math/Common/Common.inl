#pragma once

namespace Snow {
	namespace Math {
		template<typename T>
		inline constexpr T abs(T x) {
			return (x < 0) ? -x : x;
		}

		template<typename T>
		inline constexpr T ceil(T x) {
			return static_cast<T>(std::ceil(m));
		}

		template<typename T>
		inline constexpr T clamp(T value, T min1, T max1) {
			//assert min<max
			return min(max(value, min1), max1);
		}

		template<typename T>
		inline constexpr T floor(T value) {
			return static_cast<T>(std::floor(value));
		}

		template<typename T>
		inline constexpr T fma(T mul1, T mul2, T add) {
			return mul1 * mul2 + add;
		}

		template<typename T>
		inline constexpr T fract(T value) {
			return value - floor(value);
		}

		template<typename T>
		inline constexpr T frexp(const T& value, int& exp) {
			return std::frexp(value, &exp);
		}

		template<typename T>
		inline constexpr bool isInf(T value) {
			return std::isinf(value);
		}

		template<typename T>
		inline constexpr T ldexp(T value, int& exp) {
			return std::ldexp(value, &exp);
		}

		template<typename T>
		inline constexpr T min(T x, T y) {
			return (y < x) ? y : x;
		}

		template<typename T>
		inline constexpr T max(T x, T y) {
			return (x < y) ? y : x;
		}

		template<typename T, typename U>
		inline constexpr T mix(T x, T y, const U& a) {
			return x * (static_cast<T>(1) - static_cast<T>(a)) + y * static_cast<T>(a);
		}

		template<typename T>
		inline constexpr T mod(T value, T radix) {
			return value - (radix * floor(value / radix));
		}

		template<typename T>
		inline constexpr T modf(T value, int& num) {
			return std::modf(value, &num);
		}

		template<typename T>
		inline constexpr T round(T value) {
			return std::round(value);
		}

		template<typename T>
		inline constexpr T roundEven(T value) {
			int Integer = static_cast<int>(x);
			genType IntegerPart = static_cast<genType>(Integer);
			genType FractionalPart = fract(x);

			if (FractionalPart > static_cast<genType>(0.5) || FractionalPart < static_cast<genType>(0.5)) 
				return round(x);
			else if ((Integer % 2) == 0)
				return IntegerPart;
			else if (x <= static_cast<genType>(0)) // Work around...
				return IntegerPart - static_cast<genType>(1);
			else
				return IntegerPart + static_cast<genType>(1);
		}

		template<typename T>
		inline constexpr T sign(T value) {
			return (value > 0 ? 1 : (value < 0 ? -1 : 0));
		}

		template<typename T>
		inline constexpr T smoothstep(T edge0, T edge1, T value) {
			const T tmp(clamp((value - edge0) / (edge1 - edge0), static_cast<T>(0), static_cast<T>(1)));
			return tmp * tmp * (static_cast<T>(3) - (static_cast<T>(2)* tmp));
		}

		template<typename T>
		inline constexpr T step(T edge, T value) {
			return mix(static_cast<T>(1), static_cast<T>(0), value < edge);
		}

		template<typename T>
		inline constexpr T trunc(T value) {
			return static_cast<T>(std::trunc(value));
		}
	}
}