#pragma once

#include "Common.h"

#include <cmath>

namespace Snow {
	namespace Math {

		template<typename T>
		T sqrt(const T& x) {
			return std::sqrt(x);
		}

		template<typename T>
		T inversesqrt(const T& x) {
			return static_cast<T>(1) / sqrt(x);
		}
	}
}