#pragma once

#include "Qualifier.h"

#include <memory>

namespace Snow {
	namespace Math {
		template<typename T, qualifier Q>
		const T* valuePtr(const Vector<2, T, Q>& v) {
			return &(v.x);
		}

		template<typename T, qualifier Q>
		T* valuePtr(Vector<2, T, Q>& v) {
			return &(v.x);
		}

		template<typename T, qualifier Q>
		const T* valuePtr(const Vector<3, T, Q>& v) {
			return &(v.x);
		}

		template<typename T, qualifier Q>
		T* valuePtr(Vector<3, T, Q>& v) {
			return &(v.x);
		}

		template<typename T, qualifier Q>
		const T* valuePtr(const Vector<4, T, Q>& v) {
			return &(v.x);
		}

		template<typename T, qualifier Q>
		T* valuePtr(Vector<4, T, Q>& v) {
			return &(v.x);
		}

		template<typename T, qualifier Q>
		const T* valuePtr(const Matrix<2, 2, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		T* valuePtr(Matrix<2, 2, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		const T* valuePtr(const Matrix<2, 3, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		T* valuePtr(Matrix<2, 3, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		const T* valuePtr(const Matrix<2, 4, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		T* valuePtr(Matrix<2, 4, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		const T* valuePtr(const Matrix<3, 2, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		T* valuePtr(Matrix<3, 2, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		const T* valuePtr(const Matrix<3, 3, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		T* valuePtr(Matrix<3, 3, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		const T* valuePtr(const Matrix<3, 4, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		T* valuePtr(Matrix<3, 4, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		const T* valuePtr(const Matrix<4, 2, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		T* valuePtr(Matrix<4, 2, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		const T* valuePtr(const Matrix<4, 3, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		T* valuePtr(Matrix<4, 3, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		const T* valuePtr(const Matrix<4, 4, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		T* valuePtr(Matrix<4, 4, T, Q>& m) {
			return &(m[0].x);
		}

		template<typename T, qualifier Q>
		T* valuePtr(Complex<T, Q>& c) {
			return &(c.x);
		}

		template<typename T, qualifier Q>
		T* valuePtr(Quaternion<T, Q>& q) {
			return &(q.x);
		}

		template<typename T, qualifier Q>
		Vector<1, T, Q> makeVector1(const Vector<1, T, Q>& v) {
			return v;
		}

		template<typename T, qualifier Q>
		Vector<1, T, Q> makeVector1(const Vector<2, T, Q>& v) {
			return Vector<1, T, Q>(v);
		}

		template<typename T, qualifier Q>
		Vector<1, T, Q> makeVector1(const Vector<3, T, Q>& v) {
			return Vector<1, T, Q>(v);
		}

		template<typename T, qualifier Q>
		Vector<1, T, Q> makeVector1(const Vector<4, T, Q>& v) {
			return Vector<1, T, Q>(v);
		}

		template<typename T, qualifier Q>
		Vector<2, T, Q> makeVector2(const Vector<1, T, Q>& v) {
			return Vector<2, T, Q>(v);
		}

		template<typename T, qualifier Q>
		Vector<2, T, Q> makeVector2(const Vector<2, T, Q>& v) {
			return v;
		}

		template<typename T, qualifier Q>
		Vector<2, T, Q> makeVector2(const Vector<3, T, Q>& v) {
			return Vector<2, T, Q>(v);
		}

		template<typename T, qualifier Q>
		Vector<2, T, Q> makeVector2(const Vector<4, T, Q>& v) {
			return Vector<2, T, Q>(v);
		}

		template<typename T, qualifier Q>
		Vector<3, T, Q> makeVector3(const Vector<1, T, Q>& v) {
			return Vector<3, T, Q>(v);
		}

		template<typename T, qualifier Q>
		Vector<3, T, Q> makeVector3(const Vector<2, T, Q>& v) {
			return Vector<3, T, Q>(v);
		}

		template<typename T, qualifier Q>
		Vector<3, T, Q> makeVector3(const Vector<3, T, Q>& v) {
			return v;
		}

		template<typename T, qualifier Q>
		Vector<3, T, Q> makeVector3(const Vector<4, T, Q>& v) {
			return Vector<3, T, Q>(v);
		}

		template<typename T, qualifier Q>
		Vector<4, T, Q> makeVector4(const Vector<1, T, Q>& v) {
			return Vector<4, T, Q>(v);
		}

		template<typename T, qualifier Q>
		Vector<4, T, Q> makeVector4(const Vector<2, T, Q>& v) {
			return Vector<4, T, Q>(v);
		}

		template<typename T, qualifier Q>
		Vector<4, T, Q> makeVector4(const Vector<3, T, Q>& v) {
			return Vector<4, T, Q>(v);
		}

		template<typename T, qualifier Q>
		Vector<4, T, Q> makeVector4(const Vector<4, T, Q>& v) {
			return v;
		}

		template<typename T>
		Vector<2, T, defaultp> makeVector2(const T* const ptr) {
			Vector<2, T, defaultp> result;
			memcpy(valuePtr(result), ptr, sizeof(Vector<2, T, defaultp>));
			return result;
		}

		template<typename T>
		Vector<3, T, defaultp> makeVector3(const T* const ptr) {
			Vector<3, T, defaultp> result;
			memcpy(valuePtr(result), ptr, sizeof(Vector<3, T, defaultp>));
			return result;
		}

		template<typename T>
		Vector<4, T, defaultp> makeVector4(const T* const ptr) {
			Vector<4, T, defaultp> result;
			memcpy(valuePtr(result), ptr, sizeof(Vector<4, T, defaultp>));
			return result;
		}

		template<typename T>
		Matrix<2, 2, T, defaultp> makeMatrix2x2(const T* const ptr) {
			Matrix<2, 2, T, defaultp> result;
			memcpy(valuePtr(result), ptr, sizeof(Matrix<2, 2, T, defaultp>));
			return result;
		}

		template<typename T>
		Matrix<2, 3, T, defaultp> makeMatrix2x3(const T* const ptr) {
			Matrix<2, 3, T, defaultp> result;
			memcpy(valuePtr(result), ptr, sizeof(Matrix<2, 3, T, defaultp>));
			return result;
		}

		template<typename T>
		Matrix<2, 4, T, defaultp> makeMatrix2x4(const T* const ptr) {
			Matrix<2, 4, T, defaultp> result;
			memcpy(valuePtr(result), ptr, sizeof(Matrix<2, 4, T, defaultp>));
			return result;
		}

		template<typename T>
		Matrix<3, 2, T, defaultp> makeMatrix3x2(const T* const ptr) {
			Matrix<3, 2, T, defaultp> result;
			memcpy(valuePtr(result), ptr, sizeof(Matrix<3, 2, T, defaultp>));
			return result;
		}

		template<typename T>
		Matrix<3, 3, T, defaultp> makeMatrix3x3(const T* const ptr) {
			Matrix<3, 3, T, defaultp> result;
			memcpy(valuePtr(result), ptr, sizeof(Matrix<3, 3, T, defaultp>));
			return result;
		}

		template<typename T>
		Matrix<3, 4, T, defaultp> makeMatrix3x4(const T* const ptr) {
			Matrix<3, 4, T, defaultp> result;
			memcpy(valuePtr(result), ptr, sizeof(Matrix<3, 4, T, defaultp>));
			return result;
		}

		template<typename T>
		Matrix<4, 2, T, defaultp> makeMatrix4x2(const T* const ptr) {
			Matrix<4, 2, T, defaultp> result;
			memcpy(valuePtr(result), ptr, sizeof(Matrix<4, 2, T, defaultp>));
			return result;
		}

		template<typename T>
		Matrix<4, 3, T, defaultp> makeMatrix4x3(const T* const ptr) {
			Matrix<4, 3, T, defaultp> result;
			memcpy(valuePtr(result), ptr, sizeof(Matrix<4, 3, T, defaultp>));
			return result;
		}
		
		template<typename T>
		Matrix<4, 4, T, defaultp> makeMatrix4x4(const T* const ptr) {
			Matrix<4, 4, T, defaultp> result;
			memcpy(valuePtr(result), ptr, sizeof(Matrix<4, 4, T, defaultp>));
			return result;
		}

		template<typename T>
		Complex<T, defaultp> makeComplex(const T* const ptr) {
			Complex<T, defaultp> result;
			memcpy(valuePtr(result), ptr, sizeof(Complex<T, defaultp>));
			return result;
		}

		template<typename T>
		Quaternion<T, defaultp> makeQuaternion(const T* const ptr) {
			Quaternion<T, defaultp> result;
			memcpy(valuePtr(result), ptr, sizeof(Quaternion<T, defaultp>));
			return result;
		}
	}
}