#pragma once

#include <limits>

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

#ifdef isnan
#undef isnan
#endif

#ifdef isinf
#undef isinf
#endif

namespace Snow {
	namespace Math {
		template<typename T>
		inline constexpr T abs(T value);

		template<typename T>
		inline constexpr T ceil(T value);

		template<typename T>
		inline constexpr T clamp(T value, T min, T max);

		template<typename T>
		inline constexpr T floor(T value);

		template<typename T>
		inline constexpr T fma(T mul1, T mul2, T add);

		template<typename T>
		inline constexpr T fract(T value);

		template<typename T>
		inline constexpr T frexp(const T& value, int& exp);

		template<typename T>
		inline constexpr bool isInf(T value);

		template<typename T>
		inline constexpr T ldexp(T value, int& exp);

		template<typename T>
		inline constexpr T max(T x, T y);

		template<typename T>
		inline constexpr T min(T x, T y);

		template<typename T, typename U>
		inline constexpr T mix(T x, T y, const U& a);

		template<typename T>
		inline constexpr T mod(T value, T radix);

		template<typename T>
		inline constexpr T modf(T value, int& num);

		template<typename T>
		inline constexpr T round(T value);

		template<typename T>
		inline constexpr T roundEven(T value);

		template<typename T>
		inline constexpr T sign(T value);

		template<typename T>
		inline constexpr T smoothstep(T edge0, T edge1, T value);

		template<typename T>
		inline constexpr T step(T edge, T value);

		template<typename T>
		inline constexpr T trunc(T value);

		int floatBitsToInt(const float& x);

		unsigned int floatBitsToUInt(const float& x);

		float intBitsToFloat(const int& x);

		float uintBitsToFloat(const unsigned int& x);
	}
}

#include "Snow/Math/Common/Common.inl"
