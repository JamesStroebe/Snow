#pragma once

namespace Snow {
	namespace Math {
		
		//precision

		enum qualifier {
			packed_highp,
			packed_mediump,
			packed_lowp,

			aligned_highp,
			aligned_mediump,
			aligned_lowp,

			highp = packed_highp,
			mediump = packed_mediump,
			lowp = packed_lowp,

			defaultp = highp
		};

		typedef qualifier precision;

		template<size_t L, typename T, qualifier Q = defaultp> struct Vector;
		template<size_t R, size_t C, typename T, qualifier Q = defaultp> struct Matrix;
		template<typename T, qualifier Q = defaultp> struct Complex;
		template<typename T, qualifier Q = defaultp> struct Quaternion;

		template<typename T, qualifier Q = defaultp> using tvec1 = Vector<1, T, Q>;
		template<typename T, qualifier Q = defaultp> using tvec2 = Vector<2, T, Q>;
		template<typename T, qualifier Q = defaultp> using tvec3 = Vector<3, T, Q>;
		template<typename T, qualifier Q = defaultp> using tvec4 = Vector<4, T, Q>;
		template<typename T, qualifier Q = defaultp> using tmat2x2 = Matrix<2, 2, T, Q>;
		template<typename T, qualifier Q = defaultp> using tmat2x3 = Matrix<2, 3, T, Q>;
		template<typename T, qualifier Q = defaultp> using tmat2x4 = Matrix<2, 4, T, Q>;
		template<typename T, qualifier Q = defaultp> using tmat3x2 = Matrix<3, 2, T, Q>;
		template<typename T, qualifier Q = defaultp> using tmat3x3 = Matrix<3, 3, T, Q>;
		template<typename T, qualifier Q = defaultp> using tmat3x4 = Matrix<3, 4, T, Q>;
		template<typename T, qualifier Q = defaultp> using tmat4x2 = Matrix<4, 2, T, Q>;
		template<typename T, qualifier Q = defaultp> using tmat4x3 = Matrix<4, 3, T, Q>;
		template<typename T, qualifier Q = defaultp> using tmat4x4 = Matrix<4, 4, T, Q>;
		template<typename T, qualifier Q = defaultp> using tquat = Quaternion<T, Q>;

		template<qualifier P>
		struct is_aligned {
			static const bool value = false;
		};

		template<>
		struct is_aligned<aligned_lowp> {
			static const bool value = true;
		};

		template<>
		struct is_aligned<aligned_mediump> {
			static const bool value = true;
		};

		template<>
		struct is_aligned<aligned_highp> {
			static const bool value = true;
		};

		template<size_t L, typename T, bool is_aligned>
		struct storage {
			typedef struct type{
				T data[L];
			} type;
		};

		//if using alignof
		template<size_t L, typename T>
		struct storage<L, T, true> {
			typedef struct alignas(L * sizeof(T)) type {
				T data[L];
			} type;
		};

		template<typename T>
		struct storage<3, T, true> {
			typedef struct alignas(4 * sizeof(T)) type {
				T data[4];
			} type;
		};

		enum genType {
			VECTOR,
			MATRIX,
			COMPLEX,
			QUATERNION
		};

		template<typename gentype>
		struct genTypeTrait {};

		template<size_t R, size_t C, typename T>
		struct genTypeTrait<Matrix<R, C, T>> {
			static const genType gentype = MATRIX;
		};

		template<typename gentype, genType type>
		struct initGenType{};

		template<typename gentype>
		struct initGenType<gentype, QUATERNION> {
			static gentype identity() {
				return gentype(1, 0, 0, 0);
			}
		};

		template<typename gentype>
		struct initGenType<gentype, MATRIX> {
			static gentype identity() {
				return gentype(1);
			}
		};
	}
}
