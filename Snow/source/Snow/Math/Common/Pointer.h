#pragma once


#include "Snow/Math/Matrix/Matrix2x2.h"
#include "Snow/Math/Matrix/Matrix2x3.h"
#include "Snow/Math/Matrix/Matrix2x4.h"
#include "Snow/Math/Matrix/Matrix3x2.h"
#include "Snow/Math/Matrix/Matrix3x3.h"
#include "Snow/Math/Matrix/Matrix3x4.h"
#include "Snow/Math/Matrix/Matrix4x2.h"
#include "Snow/Math/Matrix/Matrix4x3.h"
#include "Snow/Math/Matrix/Matrix4x4.h"

#include "Snow/Math/Vector/Vector2.h"
#include "Snow/Math/Vector/Vector3.h"
#include "Snow/Math/Vector/Vector4.h"

#include "Snow/Math/Complex/Complex.h"
#include "Snow/Math/Complex/Quaternion.h"

namespace Snow {
	namespace Math {
		template<typename T>
		typename const T::value_type* valuePtr(const T& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> makeVector1(const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> makeVector1(const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> makeVector1(const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<1, T, Q> makeVector1(const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<2, T, Q> makeVector2(const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<2, T, Q> makeVector2(const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<2, T, Q> makeVector2(const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<2, T, Q> makeVector2(const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<3, T, Q> makeVector3(const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<3, T, Q> makeVector3(const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<3, T, Q> makeVector3(const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<3, T, Q> makeVector3(const Vector<4, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<4, T, Q> makeVector4(const Vector<1, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<4, T, Q> makeVector4(const Vector<2, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<4, T, Q> makeVector4(const Vector<3, T, Q>& v);

		template<typename T, qualifier Q>
		Vector<4, T, Q> makeVector4(const Vector<4, T, Q>& v);

		template<typename T>
		Vector<2, T, defaultp> makeVector2(const T* const ptr);

		template<typename T>
		Vector<3, T, defaultp> makeVector3(const T* const ptr);

		template<typename T>
		Vector<4, T, defaultp> makeVector4(const T* const ptr);

		template<typename T>
		Matrix<2, 2, T, defaultp> makeMatrix2x2(const T* const ptr);

		template<typename T>
		Matrix<2, 3, T, defaultp> makeMatrix2x3(const T* const ptr);

		template<typename T>
		Matrix<2, 4, T, defaultp> makeMatrix2x4(const T* const ptr);

		template<typename T>
		Matrix<3, 2, T, defaultp> makeMatrix3x2(const T* const ptr);

		template<typename T>
		Matrix<3, 3, T, defaultp> makeMatrix3x3(const T* const ptr);

		template<typename T>
		Matrix<3, 4, T, defaultp> makeMatrix3x4(const T* const ptr);

		template<typename T>
		Matrix<4, 2, T, defaultp> makeMatrix4x2(const T* const ptr);

		template<typename T>
		Matrix<4, 3, T, defaultp> makeMatrix4x3(const T* const ptr);

		template<typename T>
		Matrix<4, 4, T, defaultp> makeMatrix4x4(const T* const ptr);

		template<typename T>
		Complex<T, defaultp> makeComplex(const T* const ptr);

		template<typename T>
		Quaternion<T, defaultp> makeQuaternion(const T* const ptr);
	}
}

#include "Pointer.inl"