#pragma once

#include "Qualifier.h"

#include "Types.h"

namespace Snow {
	namespace Math {

		template<typename T>
		T Epsilon();

		template<typename T>
		bool epsilonEqual(const T& x, const T& y, const T& epsilon);

		template<size_t L, typename T, qualifier Q>
		Vector<L, bool, Q> epsilonEqual(const Vector<L, T, Q>& v1, const Vector<L, T, Q>& v2, const T& epsilon);

		template<typename T>
		bool epsilonNotEqual(const T& x, const T& y, const T& epsilon);

		template<size_t L, typename T, qualifier Q>
		Vector<L, bool, Q> epsilonNotEqual(const Vector<L, T, Q>& v1, const Vector<L, T, Q>& v2, const T& epsilon);

		static union {
			Math::uint32_t bits;
			Math::float32_t ep;
		};
	}
}
#include "Epsilon.inl"