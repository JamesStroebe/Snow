#pragma once

#include "Qualifier.h"

namespace Snow {
	namespace Math {
		template<typename T>
		inline T sqrt(const T& x);

		template<typename T>
		inline T inversesqrt(const T& x);
	}
}
#include "Exponent.inl"