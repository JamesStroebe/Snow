#pragma once

#include "Qualifier.h"

#include <cstdint>

namespace Snow {
	namespace Math {
		typedef signed char			int8;
		typedef signed short		int16;
		typedef signed int			int32;
		typedef signed long long	int64;

		typedef unsigned char		uint8;
		typedef unsigned short		uint16;
		typedef unsigned int		uint32;
		typedef unsigned long long	uint64;

		//typedef std::int8_t			int8;
		//typedef std::int16_t		int16;
		//typedef std::int32_t		int32;
		//typedef std::int64_t		int64;

		//typedef std::uint8_t		uint8;
		//typedef std::uint16_t		uint16;
		//typedef std::uint32_t		uint32;
		//typedef std::uint64_t		uint64;

		typedef int8					i8_lowp;
		typedef int8					i8_mediump;
		typedef int8					i8_highp;
		typedef int8					i8;

		typedef int8					int8_lowp;
		typedef int8					int8_mediump;
		typedef int8					int8_highp;

		typedef int8					int8_t_lowp;
		typedef int8					int8_t_mediump;
		typedef int8					int8_t_highp;
		typedef int8					int8_t;

		typedef int16					i16_lowp;
		typedef int16					i16_mediump;
		typedef int16					i16_highp;
		typedef int16					i16;

		typedef int16					int16_lowp;
		typedef int16					int16_mediump;
		typedef int16					int16_highp;

		typedef int16					int16_t_lowp;
		typedef int16					int16_t_mediump;
		typedef int16					int16_t_highp;
		typedef int16					int16_t;

		typedef int32					i32_lowp;
		typedef int32					i32_mediump;
		typedef int32					i32_highp;
		typedef int32					i32;

		typedef int32					int32_lowp;
		typedef int32					int32_mediump;
		typedef int32					int32_highp;

		typedef int32					int32_t_lowp;
		typedef int32					int32_t_mediump;
		typedef int32					int32_t_highp;
		typedef int32					int32_t;

		typedef int64					i64_lowp;
		typedef int64					i64_mediump;
		typedef int64					i64_highp;
		typedef int64					i64;

		typedef int64					int64_lowp;
		typedef int64					int64_mediump;
		typedef int64					int64_highp;

		typedef int64					int64_t_lowp;
		typedef int64					int64_t_mediump;
		typedef int64					int64_t_highp;
		typedef int64					int64_t;

		typedef uint8					ui8_lowp;
		typedef uint8					ui8_mediump;
		typedef uint8					ui8_highp;
		typedef uint8					ui8;

		typedef uint8					uint8_lowp;
		typedef uint8					uint8_mediump;
		typedef uint8					uint8_highp;

		typedef uint8					uint8_t_lowp;
		typedef uint8					uint8_t_mediump;
		typedef uint8					uint8_t_highp;
		typedef uint8					uint8_t;

		typedef uint16					ui16_lowp;
		typedef uint16					ui16_mediump;
		typedef uint16					ui16_highp;
		typedef uint16					ui16;

		typedef uint16					uint16_lowp;
		typedef uint16					uint16_mediump;
		typedef uint16					uint16_highp;

		typedef uint16					uint16_t_lowp;
		typedef uint16					uint16_t_mediump;
		typedef uint16					uint16_t_highp;
		typedef uint16					uint16_t;

		typedef uint32					ui32_lowp;
		typedef uint32					ui32_mediump;
		typedef uint32					ui32_highp;
		typedef uint32					ui32;

		typedef uint32					uint32_lowp;
		typedef uint32					uint32_mediump;
		typedef uint32					uint32_highp;

		typedef uint32					uint32_t_lowp;
		typedef uint32					uint32_t_mediump;
		typedef uint32					uint32_t_highp;
		typedef uint32					uint32_t;

		typedef uint64					ui64_lowp;
		typedef uint64					ui64_mediump;
		typedef uint64					ui64_highp;
		typedef uint64					ui64;

		typedef uint64					uint64_lowp;
		typedef uint64					uint64_mediump;
		typedef uint64					uint64_highp;

		typedef uint64					uint64_t_lowp;
		typedef uint64					uint64_t_mediump;
		typedef uint64					uint64_t_highp;
		typedef uint64					uint64_t;

		typedef float					f32_lowp;
		typedef float					f32_mediump;
		typedef float					f32_highp;
		typedef float					f32;

		typedef float					float32_lowp;
		typedef float					float32_mediump;
		typedef float					float32_highp;
		typedef float					float32;

		typedef float					float32_t_lowp;
		typedef float					float32_t_mediump;
		typedef float					float32_t_highp;
		typedef float					float32_t;

		typedef double					f64_lowp;
		typedef double					f64_mediump;
		typedef double					f64_highp;
		typedef double					f64;

		typedef double					float64_lowp;
		typedef double					float64_mediump;
		typedef double					float64_highp;
		typedef double					float64;

		typedef double					float64_t_lowp;
		typedef double					float64_t_mediump;
		typedef double					float64_t_highp;
		typedef double					float64_t;

		typedef Vector<1, bool, lowp>	Vector1b_lowp;
		typedef Vector<2, bool, lowp>	Vector2b_lowp;
		typedef Vector<3, bool, lowp>	Vector3b_lowp;
		typedef Vector<4, bool, lowp>	Vector4b_lowp;

		typedef Vector<1, bool, mediump>	Vector1b_mediump;
		typedef Vector<2, bool, mediump>	Vector2b_mediump;
		typedef Vector<3, bool, mediump>	Vector3b_mediump;
		typedef Vector<4, bool, mediump>	Vector4b_mediump;

		typedef Vector<1, bool, highp>	Vector1b_highp;
		typedef Vector<2, bool, highp>	Vector2b_highp;
		typedef Vector<3, bool, highp>	Vector3b_highp;
		typedef Vector<4, bool, highp>	Vector4b_highp;

		typedef Vector<1, bool, defaultp>	Vector1b;
		typedef Vector<2, bool, defaultp>	Vector2b;
		typedef Vector<3, bool, defaultp>	Vector3b;
		typedef Vector<4, bool, defaultp>	Vector4b;

		typedef Vector<1, i8, lowp>			Vector1i8_lowp;
		typedef Vector<2, i8, lowp>			Vector2i8_lowp;
		typedef Vector<3, i8, lowp>			Vector3i8_lowp;
		typedef Vector<4, i8, lowp>			Vector4i8_lowp;

		typedef Vector<1, i8, mediump>		Vector1i8_mediump;
		typedef Vector<2, i8, mediump>		Vector2i8_mediump;
		typedef Vector<3, i8, mediump>		Vector3i8_mediump;
		typedef Vector<4, i8, mediump>		Vector4i8_mediump;

		typedef Vector<1, i8, highp>		Vector1i8_highp;
		typedef Vector<2, i8, highp>		Vector2i8_highp;
		typedef Vector<3, i8, highp>		Vector3i8_highp;
		typedef Vector<4, i8, highp>		Vector4i8_highp;

		typedef Vector<1, i8, defaultp>		Vector1i8;
		typedef Vector<2, i8, defaultp>		Vector2i8;
		typedef Vector<3, i8, defaultp>		Vector3i8;
		typedef Vector<4, i8, defaultp>		Vector4i8;

		typedef Vector<1, i16, lowp>		Vector1i16_lowp;
		typedef Vector<2, i16, lowp>		Vector2i16_lowp;
		typedef Vector<3, i16, lowp>		Vector3i16_lowp;
		typedef Vector<4, i16, lowp>		Vector4i16_lowp;

		typedef Vector<1, i16, mediump>		Vector1i16_mediump;
		typedef Vector<2, i16, mediump>		Vector2i16_mediump;
		typedef Vector<3, i16, mediump>		Vector3i16_mediump;
		typedef Vector<4, i16, mediump>		Vector4i16_mediump;

		typedef Vector<1, i16, highp>		Vector1i16_highp;
		typedef Vector<2, i16, highp>		Vector2i16_highp;
		typedef Vector<3, i16, highp>		Vector3i16_highp;
		typedef Vector<4, i16, highp>		Vector4i16_highp;

		typedef Vector<1, i16, defaultp>	Vector1i16;
		typedef Vector<2, i16, defaultp>	Vector2i16;
		typedef Vector<3, i16, defaultp>	Vector3i16;
		typedef Vector<4, i16, defaultp>	Vector4i16;

		typedef Vector<1, i32, lowp>		Vector1i_lowp;
		typedef Vector<2, i32, lowp>		Vector2i_lowp;
		typedef Vector<3, i32, lowp>		Vector3i_lowp;
		typedef Vector<4, i32, lowp>		Vector4i_lowp;

		typedef Vector<1, i32, mediump>		Vector1i_mediump;
		typedef Vector<2, i32, mediump>		Vector2i_mediump;
		typedef Vector<3, i32, mediump>		Vector3i_mediump;
		typedef Vector<4, i32, mediump>		Vector4i_mediump;

		typedef Vector<1, i32, highp>		Vector1i_highp;
		typedef Vector<2, i32, highp>		Vector2i_highp;
		typedef Vector<3, i32, highp>		Vector3i_highp;
		typedef Vector<4, i32, highp>		Vector4i_highp;

		typedef Vector<1, i32, defaultp>	Vector1i;
		typedef Vector<2, i32, defaultp>	Vector2i;
		typedef Vector<3, i32, defaultp>	Vector3i;
		typedef Vector<4, i32, defaultp>	Vector4i;

		typedef Vector<1, i64, lowp>		Vector1i64_lowp;
		typedef Vector<2, i64, lowp>		Vector2i64_lowp;
		typedef Vector<3, i64, lowp>		Vector3i64_lowp;
		typedef Vector<4, i64, lowp>		Vector4i64_lowp;

		typedef Vector<1, i64, mediump>		Vector1i64_mediump;
		typedef Vector<2, i64, mediump>		Vector2i64_mediump;
		typedef Vector<3, i64, mediump>		Vector3i64_mediump;
		typedef Vector<4, i64, mediump>		Vector4i64_mediump;

		typedef Vector<1, i64, highp>		Vector1i64_highp;
		typedef Vector<2, i64, highp>		Vector2i64_highp;
		typedef Vector<3, i64, highp>		Vector3i64_highp;
		typedef Vector<4, i64, highp>		Vector4i64_highp;

		typedef Vector<1, i64, defaultp>	Vector1i64;
		typedef Vector<2, i64, defaultp>	Vector2i64;
		typedef Vector<3, i64, defaultp>	Vector3i64;
		typedef Vector<4, i64, defaultp>	Vector4i64;

		typedef Vector<1, ui8, lowp>		Vector1ui8_lowp;
		typedef Vector<2, ui8, lowp>		Vector2ui8_lowp;
		typedef Vector<3, ui8, lowp>		Vector3ui8_lowp;
		typedef Vector<4, ui8, lowp>		Vector4ui8_lowp;

		typedef Vector<1, ui8, mediump>		Vector1ui8_mediump;
		typedef Vector<2, ui8, mediump>		Vector2ui8_mediump;
		typedef Vector<3, ui8, mediump>		Vector3ui8_mediump;
		typedef Vector<4, ui8, mediump>		Vector4ui8_mediump;

		typedef Vector<1, ui8, highp>		Vector1ui8_highp;
		typedef Vector<2, ui8, highp>		Vector2ui8_highp;
		typedef Vector<3, ui8, highp>		Vector3ui8_highp;
		typedef Vector<4, ui8, highp>		Vector4ui8_highp;

		typedef Vector<1, ui8, defaultp>	Vector1ui8;
		typedef Vector<2, ui8, defaultp>	Vector2ui8;
		typedef Vector<3, ui8, defaultp>	Vector3ui8;
		typedef Vector<4, ui8, defaultp>	Vector4ui8;

		typedef Vector<1, ui16, lowp>		Vector1ui16_lowp;
		typedef Vector<2, ui16, lowp>		Vector2ui16_lowp;
		typedef Vector<3, ui16, lowp>		Vector3ui16_lowp;
		typedef Vector<4, ui16, lowp>		Vector4ui16_lowp;

		typedef Vector<1, ui16, mediump>	Vector1ui16_mediump;
		typedef Vector<2, ui16, mediump>	Vector2ui16_mediump;
		typedef Vector<3, ui16, mediump>	Vector3ui16_mediump;
		typedef Vector<4, ui16, mediump>	Vector4ui16_mediump;

		typedef Vector<1, ui16, highp>		Vector1ui16_highp;
		typedef Vector<2, ui16, highp>		Vector2ui16_highp;
		typedef Vector<3, ui16, highp>		Vector3ui16_highp;
		typedef Vector<4, ui16, highp>		Vector4ui16_highp;

		typedef Vector<1, ui16, defaultp>	Vector1ui16;
		typedef Vector<2, ui16, defaultp>	Vector2ui16;
		typedef Vector<3, ui16, defaultp>	Vector3ui16;
		typedef Vector<4, ui16, defaultp>	Vector4ui16;

		typedef Vector<1, ui32, lowp>		Vector1ui_lowp;
		typedef Vector<2, ui32, lowp>		Vector2ui_lowp;
		typedef Vector<3, ui32, lowp>		Vector3ui_lowp;
		typedef Vector<4, ui32, lowp>		Vector4ui_lowp;

		typedef Vector<1, ui32, mediump>	Vector1ui_mediump;
		typedef Vector<2, ui32, mediump>	Vector2ui_mediump;
		typedef Vector<3, ui32, mediump>	Vector3ui_mediump;
		typedef Vector<4, ui32, mediump>	Vector4ui_mediump;

		typedef Vector<1, ui32, highp>		Vector1ui_highp;
		typedef Vector<2, ui32, highp>		Vector2ui_highp;
		typedef Vector<3, ui32, highp>		Vector3ui_highp;
		typedef Vector<4, ui32, highp>		Vector4ui_highp;

		typedef Vector<1, ui32, defaultp>	Vector1ui;
		typedef Vector<2, ui32, defaultp>	Vector2ui;
		typedef Vector<3, ui32, defaultp>	Vector3ui;
		typedef Vector<4, ui32, defaultp>	Vector4ui;

		typedef Vector<1, ui64, lowp>		Vector1ui64_lowp;
		typedef Vector<2, ui64, lowp>		Vector2ui64_lowp;
		typedef Vector<3, ui64, lowp>		Vector3ui64_lowp;
		typedef Vector<4, ui64, lowp>		Vector4ui64_lowp;

		typedef Vector<1, ui64, mediump>	Vector1ui64_mediump;
		typedef Vector<2, ui64, mediump>	Vector2ui64_mediump;
		typedef Vector<3, ui64, mediump>	Vector3ui64_mediump;
		typedef Vector<4, ui64, mediump>	Vector4ui64_mediump;

		typedef Vector<1, ui64, highp>		Vector1ui64_highp;
		typedef Vector<2, ui64, highp>		Vector2ui64_highp;
		typedef Vector<3, ui64, highp>		Vector3ui64_highp;
		typedef Vector<4, ui64, highp>		Vector4ui64_highp;

		typedef Vector<1, ui64, defaultp>	Vector1ui64;
		typedef Vector<2, ui64, defaultp>	Vector2ui64;
		typedef Vector<3, ui64, defaultp>	Vector3ui64;
		typedef Vector<4, ui64, defaultp>	Vector4ui64;

		typedef Vector<1, float, lowp>			Vector1f_lowp;
		typedef Vector<2, float, lowp>			Vector2f_lowp;
		typedef Vector<3, float, lowp>			Vector3f_lowp;
		typedef Vector<4, float, lowp>			Vector4f_lowp;

		typedef Vector<1, float, mediump>		Vector1f_mediump;
		typedef Vector<2, float, mediump>		Vector2f_mediump;
		typedef Vector<3, float, mediump>		Vector3f_mediump;
		typedef Vector<4, float, mediump>		Vector4f_mediump;

		typedef Vector<1, float, highp>		Vector1f_highp;
		typedef Vector<2, float, highp>		Vector2f_highp;
		typedef Vector<3, float, highp>		Vector3f_highp;
		typedef Vector<4, float, highp>		Vector4f_highp;

		typedef Vector<1, float, defaultp>		Vector1f;
		typedef Vector<2, float, defaultp>		Vector2f;
		typedef Vector<3, float, defaultp>		Vector3f;
		typedef Vector<4, float, defaultp>		Vector4f;

		typedef Vector<1, f32, lowp>		Vector1f32_lowp;
		typedef Vector<2, f32, lowp>		Vector2f32_lowp;
		typedef Vector<3, f32, lowp>		Vector3f32_lowp;
		typedef Vector<4, f32, lowp>		Vector4f32_lowp;

		typedef Vector<1, f32, mediump>		Vector1f32_mediump;
		typedef Vector<2, f32, mediump>		Vector2f32_mediump;
		typedef Vector<3, f32, mediump>		Vector3f32_mediump;
		typedef Vector<4, f32, mediump>		Vector4f32_mediump;

		typedef Vector<1, f32, highp>		Vector1f32_highp;
		typedef Vector<2, f32, highp>		Vector2f32_highp;
		typedef Vector<3, f32, highp>		Vector3f32_highp;
		typedef Vector<4, f32, highp>		Vector4f32_highp;

		typedef Vector<1, f32, defaultp>	Vector1f32;
		typedef Vector<2, f32, defaultp>	Vector2f32;
		typedef Vector<3, f32, defaultp>	Vector3f32;
		typedef Vector<4, f32, defaultp>	Vector4f32;

		typedef Vector<1, double, lowp>			Vector1d_lowp;
		typedef Vector<2, double, lowp>			Vector2d_lowp;
		typedef Vector<3, double, lowp>			Vector3d_lowp;
		typedef Vector<4, double, lowp>			Vector4d_lowp;

		typedef Vector<1, double, mediump>		Vector1d_mediump;
		typedef Vector<2, double, mediump>		Vector2d_mediump;
		typedef Vector<3, double, mediump>		Vector3d_mediump;
		typedef Vector<4, double, mediump>		Vector4d_mediump;

		typedef Vector<1, double, highp>		Vector1d_highp;
		typedef Vector<2, double, highp>		Vector2d_highp;
		typedef Vector<3, double, highp>		Vector3d_highp;
		typedef Vector<4, double, highp>		Vector4d_highp;

		typedef Vector<1, double, defaultp>		Vector1d;
		typedef Vector<2, double, defaultp>		Vector2d;
		typedef Vector<3, double, defaultp>		Vector3d;
		typedef Vector<4, double, defaultp>		Vector4d;

		typedef Vector<1, f64, lowp>		Vector1f64_lowp;
		typedef Vector<2, f64, lowp>		Vector2f64_lowp;
		typedef Vector<3, f64, lowp>		Vector3f64_lowp;
		typedef Vector<4, f64, lowp>		Vector4f64_lowp;

		typedef Vector<1, f64, mediump>		Vector1f64_mediump;
		typedef Vector<2, f64, mediump>		Vector2f64_mediump;
		typedef Vector<3, f64, mediump>		Vector3f64_mediump;
		typedef Vector<4, f64, mediump>		Vector4f64_mediump;

		typedef Vector<1, f64, highp>		Vector1f64_highp;
		typedef Vector<2, f64, highp>		Vector2f64_highp;
		typedef Vector<3, f64, highp>		Vector3f64_highp;
		typedef Vector<4, f64, highp>		Vector4f64_highp;

		typedef Vector<1, f64, defaultp>	Vector1f64;
		typedef Vector<2, f64, defaultp>	Vector2f64;
		typedef Vector<3, f64, defaultp>	Vector3f64;
		typedef Vector<4, f64, defaultp>	Vector4f64;

		typedef Vector<1, i64, lowp>		Vector1i64_lowp;
		typedef Vector<2, i64, lowp>		Vector2i64_lowp;
		typedef Vector<3, i64, lowp>		Vector3i64_lowp;
		typedef Vector<4, i64, lowp>		Vector4i64_lowp;

		typedef Vector<1, i64, mediump>		Vector1i64_mediump;
		typedef Vector<2, i64, mediump>		Vector2i64_mediump;
		typedef Vector<3, i64, mediump>		Vector3i64_mediump;
		typedef Vector<4, i64, mediump>		Vector4i64_mediump;

		typedef Vector<1, i64, highp>		Vector1i64_highp;
		typedef Vector<2, i64, highp>		Vector2i64_highp;
		typedef Vector<3, i64, highp>		Vector3i64_highp;
		typedef Vector<4, i64, highp>		Vector4i64_highp;

		typedef Vector<1, i64, defaultp>	Vector1i64;
		typedef Vector<2, i64, defaultp>	Vector2i64;
		typedef Vector<3, i64, defaultp>	Vector3i64;
		typedef Vector<4, i64, defaultp>	Vector4i64;

		typedef Matrix<2, 2, i8, lowp>	Matrix2x2i8_lowp;
		typedef Matrix<2, 3, i8, lowp>	Matrix2x3i8_lowp;
		typedef Matrix<2, 4, i8, lowp>	Matrix2x4i8_lowp;
		typedef Matrix<3, 2, i8, lowp>	Matrix3x2i8_lowp;
		typedef Matrix<3, 3, i8, lowp>	Matrix3x3i8_lowp;
		typedef Matrix<3, 4, i8, lowp>	Matrix3x4i8_lowp;
		typedef Matrix<4, 2, i8, lowp>	Matrix4x2i8_lowp;
		typedef Matrix<4, 3, i8, lowp>	Matrix4x3i8_lowp;
		typedef Matrix<4, 4, i8, lowp>	Matrix4x4i8_lowp;

		typedef Matrix<2, 2, i8, mediump>	Matrix2x2i8_mediump;
		typedef Matrix<2, 3, i8, mediump>	Matrix2x3i8_mediump;
		typedef Matrix<2, 4, i8, mediump>	Matrix2x4i8_mediump;
		typedef Matrix<3, 2, i8, mediump>	Matrix3x2i8_mediump;
		typedef Matrix<3, 3, i8, mediump>	Matrix3x3i8_mediump;
		typedef Matrix<3, 4, i8, mediump>	Matrix3x4i8_mediump;
		typedef Matrix<4, 2, i8, mediump>	Matrix4x2i8_mediump;
		typedef Matrix<4, 3, i8, mediump>	Matrix4x3i8_mediump;
		typedef Matrix<4, 4, i8, mediump>	Matrix4x4i8_mediump;

		typedef Matrix<2, 2, i8, highp>	Matrix2x2i8_highp;
		typedef Matrix<2, 3, i8, highp>	Matrix2x3i8_highp;
		typedef Matrix<2, 4, i8, highp>	Matrix2x4i8_highp;
		typedef Matrix<3, 2, i8, highp>	Matrix3x2i8_highp;
		typedef Matrix<3, 3, i8, highp>	Matrix3x3i8_highp;
		typedef Matrix<3, 4, i8, highp>	Matrix3x4i8_highp;
		typedef Matrix<4, 2, i8, highp>	Matrix4x2i8_highp;
		typedef Matrix<4, 3, i8, highp>	Matrix4x3i8_highp;
		typedef Matrix<4, 4, i8, highp>	Matrix4x4i8_highp;

		typedef Matrix<2, 2, i8, defaultp>	Matrix2x2i8;
		typedef Matrix<2, 3, i8, defaultp>	Matrix2x3i8;
		typedef Matrix<2, 4, i8, defaultp>	Matrix2x4i8;
		typedef Matrix<3, 2, i8, defaultp>	Matrix3x2i8;
		typedef Matrix<3, 3, i8, defaultp>	Matrix3x3i8;
		typedef Matrix<3, 4, i8, defaultp>	Matrix3x4i8;
		typedef Matrix<4, 2, i8, defaultp>	Matrix4x2i8;
		typedef Matrix<4, 3, i8, defaultp>	Matrix4x3i8;
		typedef Matrix<4, 4, i8, defaultp>	Matrix4x4i8;

		typedef Matrix<2, 2, i16, lowp>	Matrix2x2i16_lowp;
		typedef Matrix<2, 3, i16, lowp>	Matrix2x3i16_lowp;
		typedef Matrix<2, 4, i16, lowp>	Matrix2x4i16_lowp;
		typedef Matrix<3, 2, i16, lowp>	Matrix3x2i16_lowp;
		typedef Matrix<3, 3, i16, lowp>	Matrix3x3i16_lowp;
		typedef Matrix<3, 4, i16, lowp>	Matrix3x4i16_lowp;
		typedef Matrix<4, 2, i16, lowp>	Matrix4x2i16_lowp;
		typedef Matrix<4, 3, i16, lowp>	Matrix4x3i16_lowp;
		typedef Matrix<4, 4, i16, lowp>	Matrix4x4i16_lowp;

		typedef Matrix<2, 2, i16, mediump>	Matrix2x2i16_mediump;
		typedef Matrix<2, 3, i16, mediump>	Matrix2x3i16_mediump;
		typedef Matrix<2, 4, i16, mediump>	Matrix2x4i16_mediump;
		typedef Matrix<3, 2, i16, mediump>	Matrix3x2i16_mediump;
		typedef Matrix<3, 3, i16, mediump>	Matrix3x3i16_mediump;
		typedef Matrix<3, 4, i16, mediump>	Matrix3x4i16_mediump;
		typedef Matrix<4, 2, i16, mediump>	Matrix4x2i16_mediump;
		typedef Matrix<4, 3, i16, mediump>	Matrix4x3i16_mediump;
		typedef Matrix<4, 4, i16, mediump>	Matrix4x4i16_mediump;

		typedef Matrix<2, 2, i16, highp>	Matrix2x2i16_highp;
		typedef Matrix<2, 3, i16, highp>	Matrix2x3i16_highp;
		typedef Matrix<2, 4, i16, highp>	Matrix2x4i16_highp;
		typedef Matrix<3, 2, i16, highp>	Matrix3x2i16_highp;
		typedef Matrix<3, 3, i16, highp>	Matrix3x3i16_highp;
		typedef Matrix<3, 4, i16, highp>	Matrix3x4i16_highp;
		typedef Matrix<4, 2, i16, highp>	Matrix4x2i16_highp;
		typedef Matrix<4, 3, i16, highp>	Matrix4x3i16_highp;
		typedef Matrix<4, 4, i16, highp>	Matrix4x4i16_highp;

		typedef Matrix<2, 2, i16, defaultp>	Matrix2x2i16;
		typedef Matrix<2, 3, i16, defaultp>	Matrix2x3i16;
		typedef Matrix<2, 4, i16, defaultp>	Matrix2x4i16;
		typedef Matrix<3, 2, i16, defaultp>	Matrix3x2i16;
		typedef Matrix<3, 3, i16, defaultp>	Matrix3x3i16;
		typedef Matrix<3, 4, i16, defaultp>	Matrix3x4i16;
		typedef Matrix<4, 2, i16, defaultp>	Matrix4x2i16;
		typedef Matrix<4, 3, i16, defaultp>	Matrix4x3i16;
		typedef Matrix<4, 4, i16, defaultp>	Matrix4x4i16;

		typedef Matrix<2, 2, i32, lowp>	Matrix2x2i32_lowp;
		typedef Matrix<2, 3, i32, lowp>	Matrix2x3i32_lowp;
		typedef Matrix<2, 4, i32, lowp>	Matrix2x4i32_lowp;
		typedef Matrix<3, 2, i32, lowp>	Matrix3x2i32_lowp;
		typedef Matrix<3, 3, i32, lowp>	Matrix3x3i32_lowp;
		typedef Matrix<3, 4, i32, lowp>	Matrix3x4i32_lowp;
		typedef Matrix<4, 2, i32, lowp>	Matrix4x2i32_lowp;
		typedef Matrix<4, 3, i32, lowp>	Matrix4x3i32_lowp;
		typedef Matrix<4, 4, i32, lowp>	Matrix4x4i32_lowp;

		typedef Matrix<2, 2, i32, mediump>	Matrix2x2i32_mediump;
		typedef Matrix<2, 3, i32, mediump>	Matrix2x3i32_mediump;
		typedef Matrix<2, 4, i32, mediump>	Matrix2x4i32_mediump;
		typedef Matrix<3, 2, i32, mediump>	Matrix3x2i32_mediump;
		typedef Matrix<3, 3, i32, mediump>	Matrix3x3i32_mediump;
		typedef Matrix<3, 4, i32, mediump>	Matrix3x4i32_mediump;
		typedef Matrix<4, 2, i32, mediump>	Matrix4x2i32_mediump;
		typedef Matrix<4, 3, i32, mediump>	Matrix4x3i32_mediump;
		typedef Matrix<4, 4, i32, mediump>	Matrix4x4i32_mediump;

		typedef Matrix<2, 2, i32, highp>	Matrix2x2i32_highp;
		typedef Matrix<2, 3, i32, highp>	Matrix2x3i32_highp;
		typedef Matrix<2, 4, i32, highp>	Matrix2x4i32_highp;
		typedef Matrix<3, 2, i32, highp>	Matrix3x2i32_highp;
		typedef Matrix<3, 3, i32, highp>	Matrix3x3i32_highp;
		typedef Matrix<3, 4, i32, highp>	Matrix3x4i32_highp;
		typedef Matrix<4, 2, i32, highp>	Matrix4x2i32_highp;
		typedef Matrix<4, 3, i32, highp>	Matrix4x3i32_highp;
		typedef Matrix<4, 4, i32, highp>	Matrix4x4i32_highp;

		typedef Matrix<2, 2, i32, defaultp>	Matrix2x2i32;
		typedef Matrix<2, 3, i32, defaultp>	Matrix2x3i32;
		typedef Matrix<2, 4, i32, defaultp>	Matrix2x4i32;
		typedef Matrix<3, 2, i32, defaultp>	Matrix3x2i32;
		typedef Matrix<3, 3, i32, defaultp>	Matrix3x3i32;
		typedef Matrix<3, 4, i32, defaultp>	Matrix3x4i32;
		typedef Matrix<4, 2, i32, defaultp>	Matrix4x2i32;
		typedef Matrix<4, 3, i32, defaultp>	Matrix4x3i32;
		typedef Matrix<4, 4, i32, defaultp>	Matrix4x4i32;

		typedef Matrix<2, 2, i64, lowp>	Matrix2x2i64_lowp;
		typedef Matrix<2, 3, i64, lowp>	Matrix2x3i64_lowp;
		typedef Matrix<2, 4, i64, lowp>	Matrix2x4i64_lowp;
		typedef Matrix<3, 2, i64, lowp>	Matrix3x2i64_lowp;
		typedef Matrix<3, 3, i64, lowp>	Matrix3x3i64_lowp;
		typedef Matrix<3, 4, i64, lowp>	Matrix3x4i64_lowp;
		typedef Matrix<4, 2, i64, lowp>	Matrix4x2i64_lowp;
		typedef Matrix<4, 3, i64, lowp>	Matrix4x3i64_lowp;
		typedef Matrix<4, 4, i64, lowp>	Matrix4x4i64_lowp;

		typedef Matrix<2, 2, i64, mediump>	Matrix2x2i64_mediump;
		typedef Matrix<2, 3, i64, mediump>	Matrix2x3i64_mediump;
		typedef Matrix<2, 4, i64, mediump>	Matrix2x4i64_mediump;
		typedef Matrix<3, 2, i64, mediump>	Matrix3x2i64_mediump;
		typedef Matrix<3, 3, i64, mediump>	Matrix3x3i64_mediump;
		typedef Matrix<3, 4, i64, mediump>	Matrix3x4i64_mediump;
		typedef Matrix<4, 2, i64, mediump>	Matrix4x2i64_mediump;
		typedef Matrix<4, 3, i64, mediump>	Matrix4x3i64_mediump;
		typedef Matrix<4, 4, i64, mediump>	Matrix4x4i64_mediump;

		typedef Matrix<2, 2, i64, highp>	Matrix2x2i64_highp;
		typedef Matrix<2, 3, i64, highp>	Matrix2x3i64_highp;
		typedef Matrix<2, 4, i64, highp>	Matrix2x4i64_highp;
		typedef Matrix<3, 2, i64, highp>	Matrix3x2i64_highp;
		typedef Matrix<3, 3, i64, highp>	Matrix3x3i64_highp;
		typedef Matrix<3, 4, i64, highp>	Matrix3x4i64_highp;
		typedef Matrix<4, 2, i64, highp>	Matrix4x2i64_highp;
		typedef Matrix<4, 3, i64, highp>	Matrix4x3i64_highp;
		typedef Matrix<4, 4, i64, highp>	Matrix4x4i64_highp;

		typedef Matrix<2, 2, i64, defaultp>	Matrix2x2i64;
		typedef Matrix<2, 3, i64, defaultp>	Matrix2x3i64;
		typedef Matrix<2, 4, i64, defaultp>	Matrix2x4i64;
		typedef Matrix<3, 2, i64, defaultp>	Matrix3x2i64;
		typedef Matrix<3, 3, i64, defaultp>	Matrix3x3i64;
		typedef Matrix<3, 4, i64, defaultp>	Matrix3x4i64;
		typedef Matrix<4, 2, i64, defaultp>	Matrix4x2i64;
		typedef Matrix<4, 3, i64, defaultp>	Matrix4x3i64;
		typedef Matrix<4, 4, i64, defaultp>	Matrix4x4i64;

		typedef Matrix<2, 2, ui8, lowp>	Matrix2x2ui8_lowp;
		typedef Matrix<2, 3, ui8, lowp>	Matrix2x3ui8_lowp;
		typedef Matrix<2, 4, ui8, lowp>	Matrix2x4ui8_lowp;
		typedef Matrix<3, 2, ui8, lowp>	Matrix3x2ui8_lowp;
		typedef Matrix<3, 3, ui8, lowp>	Matrix3x3ui8_lowp;
		typedef Matrix<3, 4, ui8, lowp>	Matrix3x4ui8_lowp;
		typedef Matrix<4, 2, ui8, lowp>	Matrix4x2ui8_lowp;
		typedef Matrix<4, 3, ui8, lowp>	Matrix4x3ui8_lowp;
		typedef Matrix<4, 4, ui8, lowp>	Matrix4x4ui8_lowp;

		typedef Matrix<2, 2, ui8, mediump>	Matrix2x2ui8_mediump;
		typedef Matrix<2, 3, ui8, mediump>	Matrix2x3ui8_mediump;
		typedef Matrix<2, 4, ui8, mediump>	Matrix2x4ui8_mediump;
		typedef Matrix<3, 2, ui8, mediump>	Matrix3x2ui8_mediump;
		typedef Matrix<3, 3, ui8, mediump>	Matrix3x3ui8_mediump;
		typedef Matrix<3, 4, ui8, mediump>	Matrix3x4ui8_mediump;
		typedef Matrix<4, 2, ui8, mediump>	Matrix4x2ui8_mediump;
		typedef Matrix<4, 3, ui8, mediump>	Matrix4x3ui8_mediump;
		typedef Matrix<4, 4, ui8, mediump>	Matrix4x4ui8_mediump;

		typedef Matrix<2, 2, ui8, highp>	Matrix2x2ui8_highp;
		typedef Matrix<2, 3, ui8, highp>	Matrix2x3ui8_highp;
		typedef Matrix<2, 4, ui8, highp>	Matrix2x4ui8_highp;
		typedef Matrix<3, 2, ui8, highp>	Matrix3x2ui8_highp;
		typedef Matrix<3, 3, ui8, highp>	Matrix3x3ui8_highp;
		typedef Matrix<3, 4, ui8, highp>	Matrix3x4ui8_highp;
		typedef Matrix<4, 2, ui8, highp>	Matrix4x2ui8_highp;
		typedef Matrix<4, 3, ui8, highp>	Matrix4x3ui8_highp;
		typedef Matrix<4, 4, ui8, highp>	Matrix4x4ui8_highp;

		typedef Matrix<2, 2, ui8, defaultp>	Matrix2x2ui8;
		typedef Matrix<2, 3, ui8, defaultp>	Matrix2x3ui8;
		typedef Matrix<2, 4, ui8, defaultp>	Matrix2x4ui8;
		typedef Matrix<3, 2, ui8, defaultp>	Matrix3x2ui8;
		typedef Matrix<3, 3, ui8, defaultp>	Matrix3x3ui8;
		typedef Matrix<3, 4, ui8, defaultp>	Matrix3x4ui8;
		typedef Matrix<4, 2, ui8, defaultp>	Matrix4x2ui8;
		typedef Matrix<4, 3, ui8, defaultp>	Matrix4x3ui8;
		typedef Matrix<4, 4, ui8, defaultp>	Matrix4x4ui8;

		typedef Matrix<2, 2, ui16, lowp>	Matrix2x2ui16_lowp;
		typedef Matrix<2, 3, ui16, lowp>	Matrix2x3ui16_lowp;
		typedef Matrix<2, 4, ui16, lowp>	Matrix2x4ui16_lowp;
		typedef Matrix<3, 2, ui16, lowp>	Matrix3x2ui16_lowp;
		typedef Matrix<3, 3, ui16, lowp>	Matrix3x3ui16_lowp;
		typedef Matrix<3, 4, ui16, lowp>	Matrix3x4ui16_lowp;
		typedef Matrix<4, 2, ui16, lowp>	Matrix4x2ui16_lowp;
		typedef Matrix<4, 3, ui16, lowp>	Matrix4x3ui16_lowp;
		typedef Matrix<4, 4, ui16, lowp>	Matrix4x4ui16_lowp;

		typedef Matrix<2, 2, ui16, mediump>	Matrix2x2ui16_mediump;
		typedef Matrix<2, 3, ui16, mediump>	Matrix2x3ui16_mediump;
		typedef Matrix<2, 4, ui16, mediump>	Matrix2x4ui16_mediump;
		typedef Matrix<3, 2, ui16, mediump>	Matrix3x2ui16_mediump;
		typedef Matrix<3, 3, ui16, mediump>	Matrix3x3ui16_mediump;
		typedef Matrix<3, 4, ui16, mediump>	Matrix3x4ui16_mediump;
		typedef Matrix<4, 2, ui16, mediump>	Matrix4x2ui16_mediump;
		typedef Matrix<4, 3, ui16, mediump>	Matrix4x3ui16_mediump;
		typedef Matrix<4, 4, ui16, mediump>	Matrix4x4ui16_mediump;

		typedef Matrix<2, 2, ui16, highp>	Matrix2x2ui16_highp;
		typedef Matrix<2, 3, ui16, highp>	Matrix2x3ui16_highp;
		typedef Matrix<2, 4, ui16, highp>	Matrix2x4ui16_highp;
		typedef Matrix<3, 2, ui16, highp>	Matrix3x2ui16_highp;
		typedef Matrix<3, 3, ui16, highp>	Matrix3x3ui16_highp;
		typedef Matrix<3, 4, ui16, highp>	Matrix3x4ui16_highp;
		typedef Matrix<4, 2, ui16, highp>	Matrix4x2ui16_highp;
		typedef Matrix<4, 3, ui16, highp>	Matrix4x3ui16_highp;
		typedef Matrix<4, 4, ui16, highp>	Matrix4x4ui16_highp;

		typedef Matrix<2, 2, ui16, defaultp>	Matrix2x2ui16;
		typedef Matrix<2, 3, ui16, defaultp>	Matrix2x3ui16;
		typedef Matrix<2, 4, ui16, defaultp>	Matrix2x4ui16;
		typedef Matrix<3, 2, ui16, defaultp>	Matrix3x2ui16;
		typedef Matrix<3, 3, ui16, defaultp>	Matrix3x3ui16;
		typedef Matrix<3, 4, ui16, defaultp>	Matrix3x4ui16;
		typedef Matrix<4, 2, ui16, defaultp>	Matrix4x2ui16;
		typedef Matrix<4, 3, ui16, defaultp>	Matrix4x3ui16;
		typedef Matrix<4, 4, ui16, defaultp>	Matrix4x4ui16;

		typedef Matrix<2, 2, ui32, lowp>	Matrix2x2ui32_lowp;
		typedef Matrix<2, 3, ui32, lowp>	Matrix2x3ui32_lowp;
		typedef Matrix<2, 4, ui32, lowp>	Matrix2x4ui32_lowp;
		typedef Matrix<3, 2, ui32, lowp>	Matrix3x2ui32_lowp;
		typedef Matrix<3, 3, ui32, lowp>	Matrix3x3ui32_lowp;
		typedef Matrix<3, 4, ui32, lowp>	Matrix3x4ui32_lowp;
		typedef Matrix<4, 2, ui32, lowp>	Matrix4x2ui32_lowp;
		typedef Matrix<4, 3, ui32, lowp>	Matrix4x3ui32_lowp;
		typedef Matrix<4, 4, ui32, lowp>	Matrix4x4ui32_lowp;

		typedef Matrix<2, 2, ui32, mediump>	Matrix2x2ui32_mediump;
		typedef Matrix<2, 3, ui32, mediump>	Matrix2x3ui32_mediump;
		typedef Matrix<2, 4, ui32, mediump>	Matrix2x4ui32_mediump;
		typedef Matrix<3, 2, ui32, mediump>	Matrix3x2ui32_mediump;
		typedef Matrix<3, 3, ui32, mediump>	Matrix3x3ui32_mediump;
		typedef Matrix<3, 4, ui32, mediump>	Matrix3x4ui32_mediump;
		typedef Matrix<4, 2, ui32, mediump>	Matrix4x2ui32_mediump;
		typedef Matrix<4, 3, ui32, mediump>	Matrix4x3ui32_mediump;
		typedef Matrix<4, 4, ui32, mediump>	Matrix4x4ui32_mediump;

		typedef Matrix<2, 2, ui32, highp>	Matrix2x2ui32_highp;
		typedef Matrix<2, 3, ui32, highp>	Matrix2x3ui32_highp;
		typedef Matrix<2, 4, ui32, highp>	Matrix2x4ui32_highp;
		typedef Matrix<3, 2, ui32, highp>	Matrix3x2ui32_highp;
		typedef Matrix<3, 3, ui32, highp>	Matrix3x3ui32_highp;
		typedef Matrix<3, 4, ui32, highp>	Matrix3x4ui32_highp;
		typedef Matrix<4, 2, ui32, highp>	Matrix4x2ui32_highp;
		typedef Matrix<4, 3, ui32, highp>	Matrix4x3ui32_highp;
		typedef Matrix<4, 4, ui32, highp>	Matrix4x4ui32_highp;

		typedef Matrix<2, 2, ui32, defaultp>	Matrix2x2ui32;
		typedef Matrix<2, 3, ui32, defaultp>	Matrix2x3ui32;
		typedef Matrix<2, 4, ui32, defaultp>	Matrix2x4ui32;
		typedef Matrix<3, 2, ui32, defaultp>	Matrix3x2ui32;
		typedef Matrix<3, 3, ui32, defaultp>	Matrix3x3ui32;
		typedef Matrix<3, 4, ui32, defaultp>	Matrix3x4ui32;
		typedef Matrix<4, 2, ui32, defaultp>	Matrix4x2ui32;
		typedef Matrix<4, 3, ui32, defaultp>	Matrix4x3ui32;
		typedef Matrix<4, 4, ui32, defaultp>	Matrix4x4ui32;

		typedef Matrix<2, 2, ui64, lowp>	Matrix2x2ui64_lowp;
		typedef Matrix<2, 3, ui64, lowp>	Matrix2x3ui64_lowp;
		typedef Matrix<2, 4, ui64, lowp>	Matrix2x4ui64_lowp;
		typedef Matrix<3, 2, ui64, lowp>	Matrix3x2ui64_lowp;
		typedef Matrix<3, 3, ui64, lowp>	Matrix3x3ui64_lowp;
		typedef Matrix<3, 4, ui64, lowp>	Matrix3x4ui64_lowp;
		typedef Matrix<4, 2, ui64, lowp>	Matrix4x2ui64_lowp;
		typedef Matrix<4, 3, ui64, lowp>	Matrix4x3ui64_lowp;
		typedef Matrix<4, 4, ui64, lowp>	Matrix4x4ui64_lowp;

		typedef Matrix<2, 2, ui64, mediump>	Matrix2x2ui64_mediump;
		typedef Matrix<2, 3, ui64, mediump>	Matrix2x3ui64_mediump;
		typedef Matrix<2, 4, ui64, mediump>	Matrix2x4ui64_mediump;
		typedef Matrix<3, 2, ui64, mediump>	Matrix3x2ui64_mediump;
		typedef Matrix<3, 3, ui64, mediump>	Matrix3x3ui64_mediump;
		typedef Matrix<3, 4, ui64, mediump>	Matrix3x4ui64_mediump;
		typedef Matrix<4, 2, ui64, mediump>	Matrix4x2ui64_mediump;
		typedef Matrix<4, 3, ui64, mediump>	Matrix4x3ui64_mediump;
		typedef Matrix<4, 4, ui64, mediump>	Matrix4x4ui64_mediump;

		typedef Matrix<2, 2, ui64, highp>	Matrix2x2ui64_highp;
		typedef Matrix<2, 3, ui64, highp>	Matrix2x3ui64_highp;
		typedef Matrix<2, 4, ui64, highp>	Matrix2x4ui64_highp;
		typedef Matrix<3, 2, ui64, highp>	Matrix3x2ui64_highp;
		typedef Matrix<3, 3, ui64, highp>	Matrix3x3ui64_highp;
		typedef Matrix<3, 4, ui64, highp>	Matrix3x4ui64_highp;
		typedef Matrix<4, 2, ui64, highp>	Matrix4x2ui64_highp;
		typedef Matrix<4, 3, ui64, highp>	Matrix4x3ui64_highp;
		typedef Matrix<4, 4, ui64, highp>	Matrix4x4ui64_highp;

		typedef Matrix<2, 2, ui64, defaultp>	Matrix2x2ui64;
		typedef Matrix<2, 3, ui64, defaultp>	Matrix2x3ui64;
		typedef Matrix<2, 4, ui64, defaultp>	Matrix2x4ui64;
		typedef Matrix<3, 2, ui64, defaultp>	Matrix3x2ui64;
		typedef Matrix<3, 3, ui64, defaultp>	Matrix3x3ui64;
		typedef Matrix<3, 4, ui64, defaultp>	Matrix3x4ui64;
		typedef Matrix<4, 2, ui64, defaultp>	Matrix4x2ui64;
		typedef Matrix<4, 3, ui64, defaultp>	Matrix4x3ui64;
		typedef Matrix<4, 4, ui64, defaultp>	Matrix4x4ui64;

		typedef Matrix<2, 2, float, lowp>	Matrix2x2f_lowp;
		typedef Matrix<2, 3, float, lowp>	Matrix2x3f_lowp;
		typedef Matrix<2, 4, float, lowp>	Matrix2x4f_lowp;
		typedef Matrix<3, 2, float, lowp>	Matrix3x2f_lowp;
		typedef Matrix<3, 3, float, lowp>	Matrix3x3f_lowp;
		typedef Matrix<3, 4, float, lowp>	Matrix3x4f_lowp;
		typedef Matrix<4, 2, float, lowp>	Matrix4x2f_lowp;
		typedef Matrix<4, 3, float, lowp>	Matrix4x3f_lowp;
		typedef Matrix<4, 4, float, lowp>	Matrix4x4f_lowp;

		typedef Matrix<2, 2, float, mediump>	Matrix2x2f_mediump;
		typedef Matrix<2, 3, float, mediump>	Matrix2x3f_mediump;
		typedef Matrix<2, 4, float, mediump>	Matrix2x4f_mediump;
		typedef Matrix<3, 2, float, mediump>	Matrix3x2f_mediump;
		typedef Matrix<3, 3, float, mediump>	Matrix3x3f_mediump;
		typedef Matrix<3, 4, float, mediump>	Matrix3x4f_mediump;
		typedef Matrix<4, 2, float, mediump>	Matrix4x2f_mediump;
		typedef Matrix<4, 3, float, mediump>	Matrix4x3f_mediump;
		typedef Matrix<4, 4, float, mediump>	Matrix4x4f_mediump;

		typedef Matrix<2, 2, float, highp>	Matrix2x2f_highp;
		typedef Matrix<2, 3, float, highp>	Matrix2x3f_highp;
		typedef Matrix<2, 4, float, highp>	Matrix2x4f_highp;
		typedef Matrix<3, 2, float, highp>	Matrix3x2f_highp;
		typedef Matrix<3, 3, float, highp>	Matrix3x3f_highp;
		typedef Matrix<3, 4, float, highp>	Matrix3x4f_highp;
		typedef Matrix<4, 2, float, highp>	Matrix4x2f_highp;
		typedef Matrix<4, 3, float, highp>	Matrix4x3f_highp;
		typedef Matrix<4, 4, float, highp>	Matrix4x4f_highp;

		typedef Matrix<2, 2, float, defaultp>	Matrix2x2f;
		typedef Matrix<2, 3, float, defaultp>	Matrix2x3f;
		typedef Matrix<2, 4, float, defaultp>	Matrix2x4f;
		typedef Matrix<3, 2, float, defaultp>	Matrix3x2f;
		typedef Matrix<3, 3, float, defaultp>	Matrix3x3f;
		typedef Matrix<3, 4, float, defaultp>	Matrix3x4f;
		typedef Matrix<4, 2, float, defaultp>	Matrix4x2f;
		typedef Matrix<4, 3, float, defaultp>	Matrix4x3f;
		typedef Matrix<4, 4, float, defaultp>	Matrix4x4f;

		typedef Matrix<2, 2, f32, lowp>	Matrix2x2f32_lowp;
		typedef Matrix<2, 3, f32, lowp>	Matrix2x3f32_lowp;
		typedef Matrix<2, 4, f32, lowp>	Matrix2x4f32_lowp;
		typedef Matrix<3, 2, f32, lowp>	Matrix3x2f32_lowp;
		typedef Matrix<3, 3, f32, lowp>	Matrix3x3f32_lowp;
		typedef Matrix<3, 4, f32, lowp>	Matrix3x4f32_lowp;
		typedef Matrix<4, 2, f32, lowp>	Matrix4x2f32_lowp;
		typedef Matrix<4, 3, f32, lowp>	Matrix4x3f32_lowp;
		typedef Matrix<4, 4, f32, lowp>	Matrix4x4f32_lowp;

		typedef Matrix<2, 2, f32, mediump>	Matrix2x2f32_mediump;
		typedef Matrix<2, 3, f32, mediump>	Matrix2x3f32_mediump;
		typedef Matrix<2, 4, f32, mediump>	Matrix2x4f32_mediump;
		typedef Matrix<3, 2, f32, mediump>	Matrix3x2f32_mediump;
		typedef Matrix<3, 3, f32, mediump>	Matrix3x3f32_mediump;
		typedef Matrix<3, 4, f32, mediump>	Matrix3x4f32_mediump;
		typedef Matrix<4, 2, f32, mediump>	Matrix4x2f32_mediump;
		typedef Matrix<4, 3, f32, mediump>	Matrix4x3f32_mediump;
		typedef Matrix<4, 4, f32, mediump>	Matrix4x4f32_mediump;

		typedef Matrix<2, 2, f32, highp>	Matrix2x2f32_highp;
		typedef Matrix<2, 3, f32, highp>	Matrix2x3f32_highp;
		typedef Matrix<2, 4, f32, highp>	Matrix2x4f32_highp;
		typedef Matrix<3, 2, f32, highp>	Matrix3x2f32_highp;
		typedef Matrix<3, 3, f32, highp>	Matrix3x3f32_highp;
		typedef Matrix<3, 4, f32, highp>	Matrix3x4f32_highp;
		typedef Matrix<4, 2, f32, highp>	Matrix4x2f32_highp;
		typedef Matrix<4, 3, f32, highp>	Matrix4x3f32_highp;
		typedef Matrix<4, 4, f32, highp>	Matrix4x4f32_highp;

		typedef Matrix<2, 2, f32, defaultp>	Matrix2x2f32;
		typedef Matrix<2, 3, f32, defaultp>	Matrix2x3f32;
		typedef Matrix<2, 4, f32, defaultp>	Matrix2x4f32;
		typedef Matrix<3, 2, f32, defaultp>	Matrix3x2f32;
		typedef Matrix<3, 3, f32, defaultp>	Matrix3x3f32;
		typedef Matrix<3, 4, f32, defaultp>	Matrix3x4f32;
		typedef Matrix<4, 2, f32, defaultp>	Matrix4x2f32;
		typedef Matrix<4, 3, f32, defaultp>	Matrix4x3f32;
		typedef Matrix<4, 4, f32, defaultp>	Matrix4x4f32;

		typedef Matrix<2, 2, double, lowp>	Matrix2x2d_lowp;
		typedef Matrix<2, 3, double, lowp>	Matrix2x3d_lowp;
		typedef Matrix<2, 4, double, lowp>	Matrix2x4d_lowp;
		typedef Matrix<3, 2, double, lowp>	Matrix3x2d_lowp;
		typedef Matrix<3, 3, double, lowp>	Matrix3x3d_lowp;
		typedef Matrix<3, 4, double, lowp>	Matrix3x4d_lowp;
		typedef Matrix<4, 2, double, lowp>	Matrix4x2d_lowp;
		typedef Matrix<4, 3, double, lowp>	Matrix4x3d_lowp;
		typedef Matrix<4, 4, double, lowp>	Matrix4x4d_lowp;

		typedef Matrix<2, 2, double, mediump>	Matrix2x2d_mediump;
		typedef Matrix<2, 3, double, mediump>	Matrix2x3d_mediump;
		typedef Matrix<2, 4, double, mediump>	Matrix2x4d_mediump;
		typedef Matrix<3, 2, double, mediump>	Matrix3x2d_mediump;
		typedef Matrix<3, 3, double, mediump>	Matrix3x3d_mediump;
		typedef Matrix<3, 4, double, mediump>	Matrix3x4d_mediump;
		typedef Matrix<4, 2, double, mediump>	Matrix4x2d_mediump;
		typedef Matrix<4, 3, double, mediump>	Matrix4x3d_mediump;
		typedef Matrix<4, 4, double, mediump>	Matrix4x4d_mediump;

		typedef Matrix<2, 2, double, highp>	Matrix2x2d_highp;
		typedef Matrix<2, 3, double, highp>	Matrix2x3d_highp;
		typedef Matrix<2, 4, double, highp>	Matrix2x4d_highp;
		typedef Matrix<3, 2, double, highp>	Matrix3x2d_highp;
		typedef Matrix<3, 3, double, highp>	Matrix3x3d_highp;
		typedef Matrix<3, 4, double, highp>	Matrix3x4d_highp;
		typedef Matrix<4, 2, double, highp>	Matrix4x2d_highp;
		typedef Matrix<4, 3, double, highp>	Matrix4x3d_highp;
		typedef Matrix<4, 4, double, highp>	Matrix4x4d_highp;

		typedef Matrix<2, 2, double, defaultp>	Matrix2x2d;
		typedef Matrix<2, 3, double, defaultp>	Matrix2x3d;
		typedef Matrix<2, 4, double, defaultp>	Matrix2x4d;
		typedef Matrix<3, 2, double, defaultp>	Matrix3x2d;
		typedef Matrix<3, 3, double, defaultp>	Matrix3x3d;
		typedef Matrix<3, 4, double, defaultp>	Matrix3x4d;
		typedef Matrix<4, 2, double, defaultp>	Matrix4x2d;
		typedef Matrix<4, 3, double, defaultp>	Matrix4x3d;
		typedef Matrix<4, 4, double, defaultp>	Matrix4x4d;

		typedef Matrix<2, 2, f64, lowp>	Matrix2x2f64_lowp;
		typedef Matrix<2, 3, f64, lowp>	Matrix2x3f64_lowp;
		typedef Matrix<2, 4, f64, lowp>	Matrix2x4f64_lowp;
		typedef Matrix<3, 2, f64, lowp>	Matrix3x2f64_lowp;
		typedef Matrix<3, 3, f64, lowp>	Matrix3x3f64_lowp;
		typedef Matrix<3, 4, f64, lowp>	Matrix3x4f64_lowp;
		typedef Matrix<4, 2, f64, lowp>	Matrix4x2f64_lowp;
		typedef Matrix<4, 3, f64, lowp>	Matrix4x3f64_lowp;
		typedef Matrix<4, 4, f64, lowp>	Matrix4x4f64_lowp;

		typedef Matrix<2, 2, f64, mediump>	Matrix2x2f64_mediump;
		typedef Matrix<2, 3, f64, mediump>	Matrix2x3f64_mediump;
		typedef Matrix<2, 4, f64, mediump>	Matrix2x4f64_mediump;
		typedef Matrix<3, 2, f64, mediump>	Matrix3x2f64_mediump;
		typedef Matrix<3, 3, f64, mediump>	Matrix3x3f64_mediump;
		typedef Matrix<3, 4, f64, mediump>	Matrix3x4f64_mediump;
		typedef Matrix<4, 2, f64, mediump>	Matrix4x2f64_mediump;
		typedef Matrix<4, 3, f64, mediump>	Matrix4x3f64_mediump;
		typedef Matrix<4, 4, f64, mediump>	Matrix4x4f64_mediump;

		typedef Matrix<2, 2, f64, highp>	Matrix2x2f64_highp;
		typedef Matrix<2, 3, f64, highp>	Matrix2x3f64_highp;
		typedef Matrix<2, 4, f64, highp>	Matrix2x4f64_highp;
		typedef Matrix<3, 2, f64, highp>	Matrix3x2f64_highp;
		typedef Matrix<3, 3, f64, highp>	Matrix3x3f64_highp;
		typedef Matrix<3, 4, f64, highp>	Matrix3x4f64_highp;
		typedef Matrix<4, 2, f64, highp>	Matrix4x2f64_highp;
		typedef Matrix<4, 3, f64, highp>	Matrix4x3f64_highp;
		typedef Matrix<4, 4, f64, highp>	Matrix4x4f64_highp;

		typedef Matrix<2, 2, f64, defaultp>	Matrix2x2f64;
		typedef Matrix<2, 3, f64, defaultp>	Matrix2x3f64;
		typedef Matrix<2, 4, f64, defaultp>	Matrix2x4f64;
		typedef Matrix<3, 2, f64, defaultp>	Matrix3x2f64;
		typedef Matrix<3, 3, f64, defaultp>	Matrix3x3f64;
		typedef Matrix<3, 4, f64, defaultp>	Matrix3x4f64;
		typedef Matrix<4, 2, f64, defaultp>	Matrix4x2f64;
		typedef Matrix<4, 3, f64, defaultp>	Matrix4x3f64;
		typedef Matrix<4, 4, f64, defaultp>	Matrix4x4f64;

		typedef Quaternion<i8, lowp>		Quaternioni8_lowp;
		typedef Quaternion<i8, mediump>		Quaternioni8_mediump;
		typedef Quaternion<i8, highp>		Quaternioni8_highp;
		typedef Quaternion<i8, defaultp>	Quaternioni8;

		typedef Quaternion<i16, lowp>		Quaternioni16_lowp;
		typedef Quaternion<i16, mediump>	Quaternioni16_mediump;
		typedef Quaternion<i16, highp>		Quaternioni16_highp;
		typedef Quaternion<i16, defaultp>	Quaternioni16;

		typedef Quaternion<i32, lowp>		Quaternioni32_lowp;
		typedef Quaternion<i32, mediump>	Quaternioni32_mediump;
		typedef Quaternion<i32, highp>		Quaternioni32_highp;
		typedef Quaternion<i32, defaultp>	Quaternioni32;

		typedef Quaternion<i64, lowp>		Quaternioni64_lowp;
		typedef Quaternion<i64, mediump>	Quaternioni64_mediump;
		typedef Quaternion<i64, highp>		Quaternioni64_highp;
		typedef Quaternion<i64, defaultp>	Quaternioni64;

		typedef Quaternion<ui8, lowp>		Quaternionui8_lowp;
		typedef Quaternion<ui8, mediump>	Quaternionui8_mediump;
		typedef Quaternion<ui8, highp>		Quaternionui8_highp;
		typedef Quaternion<ui8, defaultp>	Quaternionui8;

		typedef Quaternion<ui16, lowp>		Quaternionui16_lowp;
		typedef Quaternion<ui16, mediump>	Quaternionui16_mediump;
		typedef Quaternion<ui16, highp>		Quaternionui16_highp;
		typedef Quaternion<ui16, defaultp>	Quaternionui16;

		typedef Quaternion<ui32, lowp>		Quaternionui32_lowp;
		typedef Quaternion<ui32, mediump>	Quaternionui32_mediump;
		typedef Quaternion<ui32, highp>		Quaternionui32_highp;
		typedef Quaternion<ui32, defaultp>	Quaternionui32;

		typedef Quaternion<ui64, lowp>		Quaternionui64_lowp;
		typedef Quaternion<ui64, mediump>	Quaternionui64_mediump;
		typedef Quaternion<ui64, highp>		Quaternionui64_highp;
		typedef Quaternion<ui64, defaultp>	Quaternionui64;

		typedef Quaternion<float, lowp>		Quaternionf_lowp;
		typedef Quaternion<float, mediump>	Quaternionf_mediump;
		typedef Quaternion<float, highp>	Quaternionf_highp;
		typedef Quaternion<float, defaultp>	Quaternionf;

		typedef Quaternion<f32, lowp>		Quaternionf32_lowp;
		typedef Quaternion<f32, mediump>	Quaternionf32_mediump;
		typedef Quaternion<f32, highp>		Quaternionf32_highp;
		typedef Quaternion<f32, defaultp>	Quaternionf32;

		typedef Quaternion<double, lowp>		Quaterniond_lowp;
		typedef Quaternion<double, mediump>		Quaterniond_mediump;
		typedef Quaternion<double, highp>		Quaterniond_highp;
		typedef Quaternion<double, defaultp>	Quaterniond;

		typedef Quaternion<f64, lowp>		Quaternionf64_lowp;
		typedef Quaternion<f64, mediump>	Quaternionf64_mediump;
		typedef Quaternion<f64, highp>		Quaternionf64_highp;
		typedef Quaternion<f64, defaultp>	Quaternionf64;
	}
}
