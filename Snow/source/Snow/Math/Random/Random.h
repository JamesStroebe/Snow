#pragma once

#include <random>

namespace Snow {
	namespace Math {
		class Random {
		public:
			static void Init() {
				s_Mersenne.seed(std::random_device()());
			}

			static int RandInt(int lower, int upper) { return (int)(RandFloat() * (upper - lower) + lower); }

			static float RandFloat() { return (float)s_Distribution(s_Mersenne)/(float)std::numeric_limits<uint32_t>::max(); }
			static float RandFloat(float lower, float upper) { return RandFloat() * (upper - lower) + lower; }
			

		private:
			static std::mt19937 s_Mersenne;
			static std::uniform_int_distribution<std::mt19937::result_type> s_Distribution;
		};
	}
}