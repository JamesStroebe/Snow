#include "spch.h"
#include "Snow/Math/Random/Random.h"

namespace Snow {
	namespace Math {
		std::mt19937 Random::s_Mersenne;
		std::uniform_int_distribution<std::mt19937::result_type> Random::s_Distribution;
	}
}
