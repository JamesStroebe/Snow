///////////////////////////////////////////
// File: StringUtils.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once

#include <string>
#include <vector>

#include "Snow/Core/Buffer.h"

namespace Snow {
	const char* FindToken(const char* str, const std::string& token);
	const char* FindToken(const std::string& string, const std::string& token);

	std::vector<std::string> SplitString(const std::string& string, const std::string& delimiters);
	std::vector<std::string> SplitString(const std::string& string, const char delimiter);
	std::vector<std::string> Tokenize(const std::string& string);
	std::vector<std::string> GetLines(const std::string& string);

	std::string GetBlock(const char* str, const char** outPosition);

	std::string GetStatement(const char* str, const char** outPosition);
	bool StartsWith(const std::string& string, const std::string& start);

	void RemoveToken(std::string& string, const std::string& token);

	int32_t NextInt(const std::string& string);

	std::string FindFileName(const std::string& string);

	Core::Buffer ReadFileToBuffer(const std::string& path);
	std::string ReadFileToString(const std::string& path);
}
