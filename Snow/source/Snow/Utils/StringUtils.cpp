#include "spch.h"
#include "StringUtils.h"

namespace Snow {
	const char* FindToken(const char* str, const std::string& token) {
		const char* t = str;
		while (t = strstr(t, token.c_str())) {
			bool left = str == t || isspace(t[-1]);
			bool right = !t[token.size()] || isspace(t[token.size()]);
			if (left && right)
				return t;
			t += token.size();
		}
		return nullptr;
	}

	const char* FindToken(const std::string& string, const std::string& token) {
		return FindToken(string.c_str(), token);
	}

	std::vector<std::string> SplitString(const std::string& string, const std::string& delimiters) {
		size_t start = 0;
		size_t end = string.find_first_of(delimiters);
		std::vector<std::string> result;

		while (end <= std::string::npos) {
			std::string token = string.substr(start, end - start);
			if (!token.empty())
				result.push_back(token);

			if (end == std::string::npos)
				break;

			start = end + 1;
			end = string.find_first_of(delimiters, start);
		}
		return result;
	}

	std::vector<std::string> SplitString(const std::string& string, const char delimiter) {
		return SplitString(string, std::string(1, delimiter));
	}

	std::vector<std::string> Tokenize(const std::string& string) {
		return SplitString(string, " \t\n\r");
	}

	std::vector<std::string> GetLines(const std::string& string) {
		return SplitString(string, "\n\r");
	}

	std::string GetBlock(const char* str, const char** outPosition) {
		const char* end = strstr(str, "}");
		if (!end)
			return str;

		if (outPosition)
			*outPosition = end;

		uint32_t length = end - str + 1;
		return std::string(str, length);
	}

	std::string GetStatement(const char* str, const char** outPosition) {
		const char* end = strstr(str, ";");
		if (!end)
			return str;

		if (outPosition)
			*outPosition = end;

		uint32_t length = end - str + 1;
		return std::string(str, length);
	}

	bool StartsWith(const std::string& string, const std::string& start) {
		return string.find(start) == 0;
	}

	void RemoveToken(std::string& string, const std::string& token) {
		string.erase(string.find(token), token.length() + 1);
	}

	int32_t NextInt(const std::string& string) {
		const char* str = string.c_str();
		for (unsigned int i = 0; i < string.size(); i++) {
			if (isdigit(string[i]))
				return atoi(&string[i]);
		}
		return -1;
	}

	std::string FindFileName(const std::string& string) {
		auto lastSlash = string.find_last_of("/\\");
		lastSlash = lastSlash == std::string::npos ? 0 : lastSlash + 1;
		auto lastDot = string.rfind('.');
		auto count = lastDot == std::string::npos ? string.size() - lastSlash : lastDot - lastSlash;
		return string.substr(lastSlash, count);
	}

	Core::Buffer ReadFileToBuffer(const std::string& path) {
		Core::Buffer result;
		std::ifstream in(path, std::ios::in | std::ios::binary);
		if (in) {
			in.seekg(0, std::ios::end);
			result.Allocate(in.tellg());
			in.seekg(0, std::ios::beg);
			in.read((char*)result.Data, result.Size);
			in.close();
		}
		else
			SNOW_CORE_WARN("Could not read file '{0}'", path);
		return result;
	}

	std::string ReadFileToString(const std::string& path) {
		std::string result;
		std::ifstream in(path, std::ios::in | std::ios::binary);
		if (in) {
			in.seekg(0, std::ios::end);
			result.resize(in.tellg());
			in.seekg(0, std::ios::beg);
			in.read(&result[0], result.size());
			in.close();
		}
		else
			SNOW_CORE_WARN("Could not read file '{0}'", path);
		return result;
	}
}