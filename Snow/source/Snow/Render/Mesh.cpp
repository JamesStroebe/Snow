#include "spch.h"
#include "Snow/Render/Mesh.h"


#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/DefaultIOStream.h> 

#include "Snow/Render/Renderer3D.h"


namespace Snow {
	namespace Render {
		static Math::Matrix4x4f aiMatToSnow(const aiMatrix4x4& mat) {
			Math::Matrix4x4f m;
			
			m[0][0] = mat.a1; m[0][1] = mat.a2; m[0][2] = mat.a3; m[0][3] = mat.a4;
			m[1][0] = mat.b1; m[1][1] = mat.b2; m[1][2] = mat.b3; m[1][3] = mat.b4;
			m[2][0] = mat.c1; m[2][1] = mat.c2; m[2][2] = mat.c3; m[2][3] = mat.c4;
			m[3][0] = mat.d1; m[3][1] = mat.d2; m[3][2] = mat.d3; m[3][3] = mat.d4;
			return m;
		}

		static const uint32_t aiFlags =
			aiProcess_CalcTangentSpace |
			aiProcess_Triangulate |
			aiProcess_SortByPType |
			aiProcess_GenNormals |
			aiProcess_GenUVCoords |
			aiProcess_OptimizeMeshes |
			aiProcess_ValidateDataStructure;


		Mesh::Mesh(const std::string& path) {
			m_Path = path;
			LoadMesh(path);
		}

		Mesh::~Mesh() {

		}

		void Mesh::LoadMesh(const std::string& path) {
			const aiScene* scene = Renderer3D::s_AImporter->ReadFile(path, aiFlags);

			if (!scene || !scene->HasMeshes())
				SNOW_CORE_ERROR("Failed to load mesh file: {0}", path);

			API::PipelineSpecification pipelineSpec;
			pipelineSpec.Shaders = { Shader::Shader::Create("assets/shaders/glsl/PhongVert.glsl", Shader::ShaderType::VertexShader), Shader::Shader::Create("assets/shaders/glsl/PhongFrag.glsl", Shader::ShaderType::PixelShader) };
			pipelineSpec.VertexBufferLayout = {
				{API::VertexAttribType::Float3, "POSITION"},
				{API::VertexAttribType::Float3, "NORMAL"},
				{API::VertexAttribType::Float3, "BITANGENT"},
				{API::VertexAttribType::Float3, "TANGENT"},
				{API::VertexAttribType::Float2, "TEXCOORD"},
			};
			m_Pipeline = API::Pipeline::Create(pipelineSpec);

			m_Material = Material::Create(pipelineSpec.Shaders);
			m_InverseTransform = Math::inverse(aiMatToSnow(scene->mRootNode->mTransformation));

			if (scene) {
				uint32_t vertexCount = 0;
				uint32_t indexCount = 0;

				m_Submeshes.reserve(scene->mNumMeshes);
				for (uint32_t m = 0; m < scene->mNumMeshes; m++) {
					aiMesh* mesh = scene->mMeshes[m];

					Submesh submesh;
					submesh.BaseVertex = vertexCount;
					submesh.BaseIndex = indexCount;
					submesh.MaterialIndex = mesh->mMaterialIndex;
					submesh.IndexCount = mesh->mNumFaces * 3;
					m_Submeshes.push_back(submesh);

					vertexCount += mesh->mNumVertices;
					indexCount += submesh.IndexCount;

					for (uint32_t i = 0; i < mesh->mNumVertices; i++) {
						Vertex vertex;
						vertex.Position = { mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z };
						if (mesh->HasNormals())
							vertex.Normal = { mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z };

						if (mesh->HasTangentsAndBitangents()) {
							vertex.Tangent = { mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z };
							vertex.Binormal = { mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z };

						}

						if (mesh->HasTextureCoords(0))
							vertex.TexCoord = { mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y };

						m_Vertices.push_back(vertex);
					}

					for (uint32_t i = 0; i < mesh->mNumFaces; i++) {
						m_Indices.push_back({ mesh->mFaces[i].mIndices[0], mesh->mFaces[i].mIndices[1], mesh->mFaces[i].mIndices[2] });
					}
				}

				SNOW_CORE_TRACE("Nodes:");
				SNOW_CORE_TRACE("------------");
				TraverseNodes(scene->mRootNode, 0);
				SNOW_CORE_TRACE("------------");

			}

			m_VertexBuffer = API::VertexBuffer::Create();
			m_VertexBuffer->SetData(m_Vertices.data(), m_Vertices.size() * sizeof(Vertex));

			m_IndexBuffer = API::IndexBuffer::Create();
			m_IndexBuffer->SetData(m_Indices.data(), m_Indices.size() * sizeof(Index));
		}

		void Mesh::TraverseNodes(aiNode* node, int level) {
			std::string levelText;
			for (int i = 0; i < level; i++)
				levelText += "-";
			SNOW_CORE_TRACE("{0}NodeName: {1}", levelText, node->mName.data);

			for (uint32_t i = 0; i < node->mNumMeshes; i++) {
				uint32_t mesh = node->mMeshes[i];
				m_Submeshes[mesh].Transform = aiMatToSnow(node->mTransformation);
			}

			for (uint32_t i = 0; i < node->mNumChildren; i++) {
				aiNode* child = node->mChildren[i];
				TraverseNodes(child, level + 1);
			}
		}


	}
}