///////////////////////////////////////////
// File: Mesh.h
//
// Original Author: James Stroebe
//
// Creation Date: 24/01/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Render/API/Buffer.h"
#include "Snow/Render/API/Texture.h"
#include "Snow/Render/API/Pipeline.h"

#include "Snow/Render/Material.h"

#include "Snow/Math/Matrix.h"
#include "Snow/Math/Vector.h"

#include <string>


struct aiNode;

namespace Snow {
	namespace Render {
		struct Vertex {
			Math::Vector3f Position;
			Math::Vector3f Normal;
			Math::Vector3f Tangent;
			Math::Vector3f Binormal;
			Math::Vector2f TexCoord;
		};

		struct Index {
			uint32_t V1, V2, V3;
		};

		class Submesh {
		public:
			uint32_t BaseVertex;
			uint32_t BaseIndex;
			uint32_t MaterialIndex;
			uint32_t IndexCount;

			Math::Matrix4x4f Transform;
		};

		class Mesh {
		public:
			Mesh(const std::string& path);
			~Mesh();

			const std::vector<Vertex> GetVertices() { return m_Vertices; }
			const std::vector<Index> GetIndices() { return m_Indices; }

			const Core::Ref<API::VertexBuffer>& GetVertexBuffer() { return m_VertexBuffer; }
			const Core::Ref<API::IndexBuffer>& GetIndexBuffer() { return m_IndexBuffer; }

			const Core::Ref<Material> GetMaterial() { return m_Material; }
			const Core::Ref<API::Pipeline> GetPipeline() { return m_Pipeline; }

			const void SetMaterial(const Core::Ref<Material>& material) { m_Material = material; }

			const std::string& GetPath() const { return m_Path; }


		private:
			void LoadMesh(const std::string& path);

			void TraverseNodes(aiNode* node, int level = 0);

			std::vector<Submesh> m_Submeshes;

			Math::Matrix4x4f m_InverseTransform;

			std::string m_Path, m_Name;
			std::vector<Vertex> m_Vertices;
			std::vector<Index> m_Indices;

			Core::Ref<API::Pipeline> m_Pipeline;
			Core::Ref<Material> m_Material;

			Core::Ref<API::VertexBuffer> m_VertexBuffer;
			Core::Ref<API::IndexBuffer> m_IndexBuffer;
		};
	}
}