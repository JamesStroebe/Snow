///////////////////////////////////////////
// File: PerspectiveCamera.h
//
// Original Author: James Stroebe
//
// Creation Date: 15/03/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Core/Time/Timestep.h"
#include "Snow/Core/Events/Events.h"

#include "Snow/Math/Matrix.h"
#include "Snow/Math/Vector.h"
#include "Snow/Math/Complex/Quaternion.h"
#include "Snow/Math/Complex/QuaternionFunction.h"

#include "Snow/Render/Camera/Camera.h"

namespace Snow {
	namespace Render {
		namespace Camera {
			class PerspectiveCamera : public Camera {
			public:
				PerspectiveCamera(Math::Vector3f pos, float width, float height);

				void OnUpdate(Core::Time::Timestep ts);

				void OnEvent(Core::Event::Event& e);

				virtual inline const Math::Matrix4x4f& GetViewMatrix() const override { return m_ViewMatrix; }
				virtual inline const Math::Matrix4x4f& GetProjectionMatrix() const override { return m_ProjectionMatrix; }
				virtual inline const Math::Matrix4x4f& GetViewProjectionMatrix() const override { return m_ViewProjectionMatrix; }

				virtual inline const Math::Vector3f& GetPosition() const override { return m_Position; }
				virtual inline void SetPosition(const Math::Vector3f& position) override { m_Position = position; }

				inline void SetProjectionMatrix(const Math::Matrix4x4f& projectionMatrix) { m_ProjectionMatrix = projectionMatrix; }

				inline float GetDistance() const { return m_Distance; }
				inline void SetDistance(float distance) { m_Distance = distance; }

				inline void SetViewportSize(uint32_t width, uint32_t height) { m_ViewportWidth = width; m_ViewportHeight = height; }

				Math::Vector3f GetUpDirection();
				Math::Vector3f GetRightDirection();
				Math::Vector3f GetForwardDirection();
			private:
				bool OnWindowResize(Core::Event::WindowResizeEvent& e);
				bool OnWindowMoved(Core::Event::WindowMovedEvent& e);

				void MousePan(const Math::Vector2f& delta);
				void MouseRotate(const Math::Vector2f& delta);
				void MouseZoom(float delta);

				Math::Vector3f CalculatePosition();
				Math::Quaternionf GetOrientation();

				std::pair<float, float> PanSpeed() const;
				float RotationSpeed() const;
				float ZoomSpeed() const;

				Math::Matrix4x4f m_ViewMatrix, m_ProjectionMatrix, m_ViewProjectionMatrix;
				Math::Vector3f m_Position, m_Rotation, m_FocalPoint;
				
				bool m_Panning, m_Rotating;
				Math::Vector2f m_InitialMousePosition;
				Math::Vector3f m_InitialFocalPoint, m_InitialRotation;

				float m_Distance;

				Math::Vector2f m_ApplicationCenter;
				float m_Speed = 1.0f;
				float m_Pitch, m_Yaw;
				uint32_t m_ViewportWidth = 1280, m_ViewportHeight = 720;
			};
		}
	}
}