#include "spch.h"
#include "PerspectiveCamera.h"

#include "Snow/Core/Application.h"
#include "Snow/Core/Input.h"

#include "Snow/Math/Common/Constants.h"
#include "Snow/Math/Trig/Trigonomic.h"

namespace Snow {
	namespace Render {
		namespace Camera {
			
			PerspectiveCamera::PerspectiveCamera(Math::Vector3f position, float width, float height) :
				m_Position(position) {

				m_ProjectionMatrix = Math::PerspectiveFov(Math::radians(45.0f), width, height, 0.1f, 10000.0f);
				m_Rotation = Math::Vector3f(90.0f, 0.0f, 0.0f);

				m_FocalPoint = Math::Vector3f(0.0f);
				m_Distance = Math::distance(m_Position, m_FocalPoint);

				m_Yaw = 3.0f * Math::pi<float>() / 4.0f;
				m_Pitch = Math::pi<float>() / 4.0f;

				//SNOW_CORE_TRACE("Pitch, Yaw: {0}, {1}", Math::degrees(m_Pitch), Math::degrees(m_Yaw));
				m_ApplicationCenter = Math::Vector2f(Core::Application::GetWindow()->GetXPos() + Core::Application::GetWindow()->GetWidth() / 2, Core::Application::GetWindow()->GetYPos() + Core::Application::GetWindow()->GetHeight() / 2);
			}

			std::pair<float, float> PerspectiveCamera::PanSpeed() const {
				float x = std::min(m_ViewportWidth / 1000.0f, 2.4f); // max = 2.4f
				float xFactor = 0.0366f * (x * x) - 0.1778f * x + 0.3021f;

				float y = std::min(m_ViewportHeight / 1000.0f, 2.4f); // max = 2.4f
				float yFactor = 0.0366f * (y * y) - 0.1778f * y + 0.3021f;

				return { xFactor, yFactor };
			}

			float PerspectiveCamera::RotationSpeed() const {
				return 0.8f;
			}

			float PerspectiveCamera::ZoomSpeed() const {
				float distance = m_Distance * 0.2f;
				distance = std::max(distance, 0.0f);
				float speed = distance * distance;
				speed = std::min(speed, 100.0f); // max speed = 100
				return speed;
			}

			void PerspectiveCamera::OnUpdate(Core::Time::Timestep ts) {
				if (Core::Input::IsKeyPressed(SNOW_KEY_Z)) {
					const Math::Vector2f& mouse{ Core::Input::GetMousePosition().x, Core::Input::GetMousePosition().y };
					Math::Vector2f delta = mouse - m_InitialMousePosition;
					m_InitialMousePosition = mouse;

					delta *= ts.GetSeconds();

					if (Core::Input::IsMouseButtonPressed(SNOW_MOUSE_MIDDLE))
						MousePan(delta);
					else if (Core::Input::IsMouseButtonPressed(SNOW_MOUSE_LEFT))
						MouseRotate(delta);
					else if (Core::Input::IsMouseButtonPressed(SNOW_MOUSE_RIGHT))
						MouseZoom(delta.y);
				}

				m_Position = CalculatePosition();

				Math::Quaternionf orientation = GetOrientation();
				m_Rotation = Math::eulerAngles(orientation) * (180.0f / Math::pi<float>());
				m_ViewMatrix = Math::translate(Math::Matrix4x4f(1.0f), Math::Vector3f(0.0f, 0.0f, 1.0f)) * Math::ToMatrix4x4(Math::conjugate(orientation)) * Math::translate(Math::Matrix4x4f(1.0f), -m_Position);
				m_ViewMatrix = Math::translate(Math::Matrix4x4f(1.0f), m_Position) * Math::ToMatrix4x4(orientation);
				m_ViewMatrix = Math::inverse(m_ViewMatrix);
				m_ViewProjectionMatrix = m_ProjectionMatrix * m_ViewMatrix;
			}



			void PerspectiveCamera::OnEvent(Core::Event::Event& e) {
				Core::Event::EventDispatcher dispatcher(e);
				dispatcher.Dispatch<Core::Event::WindowResizeEvent>(SNOW_BIND_EVENT_FN(PerspectiveCamera::OnWindowResize));
				dispatcher.Dispatch<Core::Event::WindowMovedEvent>(SNOW_BIND_EVENT_FN(PerspectiveCamera::OnWindowMoved));
			}

			bool PerspectiveCamera::OnWindowResize(Core::Event::WindowResizeEvent& e) {
				m_ApplicationCenter = Math::Vector2f(Core::Application::GetWindow()->GetXPos() + Core::Application::GetWindow()->GetWidth() / 2, Core::Application::GetWindow()->GetYPos() + Core::Application::GetWindow()->GetHeight() / 2);
				m_ProjectionMatrix = Math::Perspective(Math::radians(45.0f), (float)e.GetWidth() / (float)e.GetHeight(), 0.05f, 1000.0f);
				SNOW_CORE_TRACE("{0}, {1}", m_ApplicationCenter.x, m_ApplicationCenter.y);
				return false;
			}

			bool PerspectiveCamera::OnWindowMoved(Core::Event::WindowMovedEvent& e) {
				m_ApplicationCenter = Math::Vector2f(Core::Application::GetWindow()->GetXPos() + Core::Application::GetWindow()->GetWidth() / 2, Core::Application::GetWindow()->GetYPos() + Core::Application::GetWindow()->GetHeight() / 2);
				SNOW_CORE_TRACE("{0}, {1}", m_ApplicationCenter.x, m_ApplicationCenter.y);
				return false;
			}

			void PerspectiveCamera::MousePan(const Math::Vector2f& delta)
			{
				auto [xSpeed, ySpeed] = PanSpeed();
				m_FocalPoint += -GetRightDirection() * delta.x * xSpeed * m_Distance;
				m_FocalPoint += GetUpDirection() * delta.y * ySpeed * m_Distance;
			}

			void PerspectiveCamera::MouseRotate(const Math::Vector2f& delta)
			{
				float yawSign = GetUpDirection().y < 0 ? -1.0f : 1.0f;
				m_Yaw += yawSign * delta.x * RotationSpeed();
				m_Pitch += delta.y * RotationSpeed();
			}

			void PerspectiveCamera::MouseZoom(float delta)
			{
				m_Distance -= delta * ZoomSpeed();
				if (m_Distance < 1.0f)
				{
					m_FocalPoint += GetForwardDirection();
					m_Distance = 1.0f;
				}
			}

			Math::Vector3f PerspectiveCamera::GetUpDirection() {
				return Math::rotate(GetOrientation(), Math::Vector3f(0.0f, 1.0f, 0.0f));
			}

			Math::Vector3f PerspectiveCamera::GetRightDirection()
			{
				return Math::rotate(GetOrientation(), Math::Vector3f(1.0f, 0.0f, 0.0f));
			}

			Math::Vector3f PerspectiveCamera::GetForwardDirection()
			{
				return Math::rotate(GetOrientation(), Math::Vector3f(0.0f, 0.0f, -1.0f));
			}

			Math::Vector3f PerspectiveCamera::CalculatePosition()
			{
				return m_FocalPoint - GetForwardDirection() * m_Distance;
			}

			Math::Quaternionf PerspectiveCamera::GetOrientation()
			{
				return Math::Quaternionf(Math::Vector3f(-m_Pitch, -m_Yaw, 0.0f));
			}
		}
	}
}