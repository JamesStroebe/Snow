#include "spch.h"
#include "OrthographicCamera.h"

#include "Snow/Math/Trig/Trigonomic.h"

#include "Snow/Core/Input.h"

#include "Snow/Render/Renderer.h"

namespace Snow {
	namespace Render {
		namespace Camera {
			OrthographicCamera::OrthographicCamera(float left, float right, float bottom, float top) :
				m_ViewMatrix(1.0f) {
				m_ZoomLevel = 10.0f;
				m_AspectRatio = left / bottom;
				SetProjectionMatrix(-m_AspectRatio * m_ZoomLevel, m_AspectRatio * m_ZoomLevel, -m_ZoomLevel, m_ZoomLevel);

				//if (Renderer::GetRenderAPI() == RenderAPI::DIRECTX)
				//	m_ProjectionMatrix = Math::transpose(m_ProjectionMatrix);

				RecalculateViewMatrix();
				//SNOW_CORE_TRACE("{0}", m_ZoomLevel);
			}

			void OrthographicCamera::SetProjectionMatrix(float left, float right, float bottom, float top) {
				m_ProjectionMatrix = Math::Ortho(left, right, bottom, top, -1.0f, 1.0f);

				//if (Renderer::GetRenderAPI() == RenderAPI::DIRECTX)
				//	m_ProjectionMatrix = Math::transpose(m_ProjectionMatrix);

				m_ViewProjectionMatrix = m_ProjectionMatrix * m_ViewMatrix;
			}

			void OrthographicCamera::OnUpdate(Core::Time::Timestep ts) {
				if (Core::Input::IsKeyPressed(SNOW_KEY_W)) {
					m_Position.x += -Math::sin(Math::radians(m_Rotation)) * m_TranslationSpeed * ts.GetSeconds();
					m_Position.y += Math::cos(Math::radians(m_Rotation)) * m_TranslationSpeed * ts.GetSeconds();
				}
				else if (Core::Input::IsKeyPressed(SNOW_KEY_S)) {
					m_Position.x -= -Math::sin(Math::radians(m_Rotation)) * m_TranslationSpeed * ts.GetSeconds();
					m_Position.y -= Math::cos(Math::radians(m_Rotation)) * m_TranslationSpeed * ts.GetSeconds();
				}

				if (Core::Input::IsKeyPressed(SNOW_KEY_A)) {
					m_Position.x -= Math::cos(Math::radians(m_Rotation)) * m_TranslationSpeed * ts.GetSeconds();
					m_Position.y -= Math::sin(Math::radians(m_Rotation)) * m_TranslationSpeed * ts.GetSeconds();
				}
				else if (Core::Input::IsKeyPressed(SNOW_KEY_D)) {
					m_Position.x += Math::cos(Math::radians(m_Rotation)) * m_TranslationSpeed * ts.GetSeconds();
					m_Position.y += Math::sin(Math::radians(m_Rotation)) * m_TranslationSpeed * ts.GetSeconds();
				}

				if (Core::Input::IsKeyPressed(SNOW_KEY_Q))
					m_Rotation -= m_RotationSpeed * ts.GetSeconds();
				else if (Core::Input::IsKeyPressed(SNOW_KEY_E))
					m_Rotation += m_RotationSpeed * ts.GetSeconds();

				if (m_Rotation > 180.0f)
					m_Rotation -= 360.0f;
				else if (m_Rotation <= -180.0f)
					m_Rotation += 360.0f;

				RecalculateViewMatrix();
				m_TranslationSpeed = m_ZoomLevel;
			}

			void OrthographicCamera::RecalculateViewMatrix() {
				Math::Matrix4x4f transform = Math::translate(Math::Matrix4x4f(1.0f), m_Position);//*
					/*Math::rotate(Math::Matrix4x4f(1.0f), Math::radians(m_Rotation), Math::Vector3f(0.0f, 0.0f, 1.0f));*/

				m_ViewMatrix = Math::inverse(transform);
				m_ViewProjectionMatrix = m_ViewMatrix * m_ProjectionMatrix;
			}

			void OrthographicCamera::OnEvent(Core::Event::Event& e) {
				Core::Event::EventDispatcher dispatcher(e);
				dispatcher.Dispatch<Core::Event::MouseScrolledEvent>(SNOW_BIND_EVENT_FN(OrthographicCamera::OnMouseScrolled));
				dispatcher.Dispatch<Core::Event::WindowResizeEvent>(SNOW_BIND_EVENT_FN(OrthographicCamera::OnWindowResized));
			}

			bool OrthographicCamera::OnMouseScrolled(Core::Event::MouseScrolledEvent& e) {
				m_ZoomLevel -= e.GetYOffset() / 480.0f;
				m_ZoomLevel = Math::max(m_ZoomLevel, 0.25f);
				SetProjectionMatrix(-m_AspectRatio * m_ZoomLevel, m_AspectRatio * m_ZoomLevel, -m_ZoomLevel, m_ZoomLevel);
				return false;
			}

			bool OrthographicCamera::OnWindowResized(Core::Event::WindowResizeEvent& e) {
				m_AspectRatio = (float)e.GetWidth() / (float)e.GetHeight();
				SetProjectionMatrix(-m_AspectRatio * m_ZoomLevel, m_AspectRatio * m_ZoomLevel, -m_ZoomLevel, m_ZoomLevel);
				return false;
			}
		}
	}
}