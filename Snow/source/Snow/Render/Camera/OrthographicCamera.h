///////////////////////////////////////////
// File: OrthographicCamera.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Math/Matrix.h"
#include "Snow/Math/Vector.h"

#include "Snow/Core/Time/Timestep.h"

#include "Snow/Core/Events/Events.h"

#include "Snow/Render/Camera/Camera.h"

namespace Snow {
	namespace Render {
		namespace Camera {
			class OrthographicCamera : public Camera {
			public:
				OrthographicCamera(float left, float right, float bottom, float top);

				void OnUpdate(Core::Time::Timestep ts);
				void OnEvent(Core::Event::Event& e);
				void SetProjectionMatrix(float left, float right, float bottom, float top);

				virtual inline const Math::Vector3f& GetPosition() const override { return m_Position; }
				void SetPosition(const Math::Vector3f& position) override { m_Position = position; RecalculateViewMatrix(); }
				float GetRotation() const { return m_Rotation; }
				void SetRotation(float rotation) { m_Rotation = rotation; RecalculateViewMatrix(); }

				
				inline const Math::Matrix4x4f& GetViewMatrix() const override { return m_ViewMatrix; }
				inline const Math::Matrix4x4f& GetProjectionMatrix() const override { return m_ProjectionMatrix; }
				inline const Math::Matrix4x4f& GetViewProjectionMatrix() const override { return m_ViewProjectionMatrix; }

				void RecalculateViewMatrix();
			private:

				bool OnMouseScrolled(Core::Event::MouseScrolledEvent& e);
				bool OnWindowResized(Core::Event::WindowResizeEvent& e);

				Math::Matrix4x4f m_ViewMatrix, m_ProjectionMatrix;
				Math::Matrix4x4f m_ViewProjectionMatrix;

				Math::Vector3f m_Position = { 0.0f, 0.0f, 0.0f };
				float m_Rotation = 0.0f, m_RotationSpeed = 180.0f, m_TranslationSpeed = 2.0f;
				float m_AspectRatio, m_ZoomLevel = 1.0f;
			};
		}
	}
}