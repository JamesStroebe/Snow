///////////////////////////////////////////
// File: Camera.h
//
// Original Author: James Stroebe
//
// Creation Date: 06/04/2020
//
/////////////////////////////////////////// 

#pragma once


#include "Snow/Math/Matrix.h"

namespace Snow {
	namespace Render {
		namespace Camera {
			class Camera {
			public:
				virtual ~Camera() = default;

				virtual inline const Math::Matrix4x4f& GetViewMatrix() const = 0;
				virtual inline const Math::Matrix4x4f& GetProjectionMatrix() const = 0;
				virtual inline const Math::Matrix4x4f& GetViewProjectionMatrix() const = 0;

				virtual inline const Math::Vector3f& GetPosition() const = 0;
				virtual void SetPosition(const Math::Vector3f& position) = 0;

			};
		}
	}
}