///////////////////////////////////////////
// File: RenderCommandBuffer.h
//
// Original Author: James Stroebe
//
// Creation Date: 11/01/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Render/API/Buffer.h"
#include "Snow/Render/API/RenderPass.h"
#include "Snow/Render/API/Pipeline.h"


namespace Snow {
	namespace Render {
		class RenderCommandBuffer {
		public:
			~RenderCommandBuffer() = default;

			typedef void(*RenderCommandFn)(void*);

			virtual void* Allocate(RenderCommandFn func, uint32_t size) = 0;

			virtual void Execute() = 0;

			inline Math::uint32_t GetCommandCount() { return m_CommandCount; }
		

			virtual void BeginRenderPass(const Core::Ref<API::RenderPass>& renderPass) = 0;
			virtual void EndRenderPass() = 0;

			virtual void SubmitElements(const Core::Ref<API::Pipeline>& pipeline, const Core::Ref<API::VertexBuffer>& vertexBuffer, const Core::Ref<API::IndexBuffer>& indexBuffer, uint32_t indexCount = 0) = 0;
			virtual void SubmitInstanced(const Core::Ref<API::Pipeline>& pipeline, const Core::Ref<API::VertexBuffer>& vertexBuffer, const Core::Ref<API::IndexBuffer>& indexBuffer, uint32_t instanceCount = 0) = 0;

			virtual void Flush() = 0;

			virtual void Present() = 0;

			static RenderCommandBuffer* Create();
		protected:
			uint8_t* m_CommandBuffer;
			uint8_t* m_CommandBufferPtr;
			uint32_t m_CommandCount = 0;
		};
	}
}