///////////////////////////////////////////
// File: Renderer.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Core/Base.h"

#include "Snow/Render/API/Buffer.h"
#include "Snow/Render/API/Context.h"
#include "Snow/Render/API/RenderPass.h"
#include "Snow/Render/API/Pipeline.h"
#include "Snow/Render/API/SwapChain.h"

#include "Snow/Render/RenderAPI.h"
#include "Snow/Render/RenderCommandBuffer.h"
#include "Snow/Render/Shaders/ShaderLibrary.h"
//#include "Snow/Render/RenderCommandQueue.h"

#include "Snow/Core/Window.h"


namespace Snow {
	namespace Render {
		//replace with an actual AssetManager
		//class ShaderLibrary;



		enum class RenderBlendFunction {
			NONE,
			ZERO,
			ONE,
			SOURCE_ALPHA,
			DESTINATION_ALPHA,
			ONE_MINUS_SOURCE_ALPHA
		};

		enum class RenderBlendEquation {
			NONE,
			ADD,
			SUBTRACT
		};

		class Renderer {
		protected:
			virtual void InitInternal() = 0;
			virtual void SetDepthTestingInternal(bool enabled) = 0;
			virtual void SetBlendInternal(bool enabled) = 0;
			virtual void SetViewportInternal(int x, int y, unsigned int width, unsigned int height) = 0;
		public:
			Renderer() = default;
			Renderer(const Renderer&) = delete;
			~Renderer() = default;

			typedef void(*RenderCommandFn)(void*);

			static void Init(const Core::Ref<Core::Window>& window);
			static void Shutdown();
			
			static void SetDepthTesting(bool enabled);
			static void SetBlending(bool enabled);
			static void SetViewport(int x, int y, unsigned int width, unsigned int height);
			

			template<typename FuncT>
			static void Submit(FuncT&& func) {
				auto renderCmd = [](void* ptr) {
					auto pFunc = (FuncT*)ptr;
					(*pFunc)();
					pFunc->~FuncT();
				};
				auto storageBuffer = GetCommandBuffer()->Allocate(renderCmd, sizeof(func));
				new (storageBuffer) FuncT(std::forward<FuncT>(func));
			}

			static void WaitAndRender();
			
			/*
			inline static void SetBlendFunction(RenderBlendFunction source, RenderBlendFunction dest) { s_Instance->SetBlendFunctionInternal(source, dest); }
			inline static void SetBlendEquation(RenderBlendEquation blendEquation) { s_Instance->SetBlendEquationInternal(blendEquation); }
			*/

			inline static RenderAPI GetRenderAPI() { return s_RenderAPI; }
			inline static void SetRenderAPI(RenderAPI api) { s_RenderAPI = api; }

			inline static API::Context* GetContext() { return s_Context; }
			inline static RenderCommandBuffer* GetCommandBuffer() { return s_RenderCommandBuffer; }
			inline static Shader::ShaderLibrary* GetShaderLibrary() { return s_ShaderLibrary; }

			inline static Renderer& Get() { return *s_Instance; }

			static RenderAPICapabilities& GetCapabilities() {
				return s_Capabilities;
			}

		private:
			static Renderer* s_Instance;
			static RenderAPI s_RenderAPI;
			static RenderAPICapabilities s_Capabilities;
			static RenderCommandBuffer* s_RenderCommandBuffer;

			static Shader::ShaderLibrary* s_ShaderLibrary;

			static API::Context* s_Context;
		};
	}
}