///////////////////////////////////////////
// File: Renderer2D.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Scene/Scene.h"
#include "Snow/Render/Camera/OrthographicCamera.h"

#include "Snow/Render/API/Pipeline.h"
#include "Snow/Render/API/Texture.h"
#include "Snow/Render/API/Buffer.h"

#include "Snow/Render/Sprite/Sprite.h"

#include "Snow/Math/Matrix.h"
#include "Snow/Math/Vector.h"

//#include <ft2build.h>
//#include <freetype/freetype.h>


namespace Snow {
	namespace Render {
		class Renderer2D {

			struct Vertex {
				Math::Vector3f Position;
				Math::Vector2f TexCoord;
				float TexID;
				Math::Vector4f Color;
			};


		public:
			struct Stats {
				Math::uint32_t QuadCount;
				Math::uint32_t DrawCalls;
				Math::uint32_t VertexCount;
				Math::uint32_t IndexCount;
			};

			struct Renderer2DStorage {
				static const Math::uint32_t MaxQuadsPerDrawCall = 25000;
				static const Math::uint32_t MaxVertices = MaxQuadsPerDrawCall * 4;
				static const Math::uint32_t MaxIndices = MaxQuadsPerDrawCall * 6;

				Core::Ref<API::Pipeline> Pipeline2D;
				Core::Ref<API::Pipeline> BackgroundPipeline;

				Core::Ref<API::VertexBuffer> VertexBuffer2D;
				Core::Ref<API::IndexBuffer> IndexBuffer2D;
				Core::Ref<API::VertexBuffer> BackgroundVBO;
				Core::Ref<API::IndexBuffer> BackgroundIBO;

				Core::Ref<API::UniformBuffer> CameraBuffer;

				Vertex* VertexDataBase;
				Vertex* VertexDataPtr;

				const Scene::Scene2D* BackgroundScene;

				

				std::vector<Core::Ref<API::Texture2D>> m_Textures;
				Math::uint32_t TextureIndex = 1;

				Math::Vector4f QuadVertexPositions[4];

				
				Stats Statistics;

				//FT_Library FTLib;
				//FT_Error FTResult;
			};

			static void Init();
			static void Shutdown();

			static void BeginScene(const Scene::Scene2D& scene);
			static void EndScene();

			static void Present();

			static Renderer2DStorage* GetData() { return s_Data; }
			static Stats GetStats() { return s_Data->Statistics; }
			static void ResetStats();

			static float SubmitTexture(const Core::Ref<API::Texture2D>& texture);

			//make a lot of overloaded functions or have something like a renderable storage that you populate?

			static void SubmitLine(const Math::Vector2f& startPosition, const Math::Vector2f& endPosition, const Math::float32_t& thickness, const Math::Vector4f& color);
			static void SubmitLine(const Math::Vector3f& startPosition, const Math::Vector3f& endPosition, const Math::float32_t& thickness, const Math::Vector4f& color);

			static void SubmitColoredQuad(const Math::Vector2f& position, const Math::float32_t& size, const Math::Vector4f& color);
			static void SubmitColoredQuad(const Math::Vector2f& position, const Math::Vector2f& size, const Math::Vector4f& color);
			static void SubmitColoredQuad(const Math::Vector3f& position, const Math::float32_t& size, const Math::Vector4f& color);
			static void SubmitColoredQuad(const Math::Vector3f& position, const Math::Vector2f& size, const Math::Vector4f& color);

			static void SubmitTexturedQuad(const Math::Vector2f& position, const Math::float32_t& size, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint = Math::Vector4f(1.0f));
			static void SubmitTexturedQuad(const Math::Vector2f& position, const Math::Vector2f& size, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint = Math::Vector4f(1.0f));
			static void SubmitTexturedQuad(const Math::Vector3f& position, const Math::float32_t& size, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint = Math::Vector4f(1.0f));
			static void SubmitTexturedQuad(const Math::Vector3f& position, const Math::Vector2f& size, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint = Math::Vector4f(1.0f));

			static void SubmitRotatedColoredQuad(const Math::Vector2f& position, const Math::float32_t& size, const Math::float32_t angle, const Math::Vector4f& color);
			static void SubmitRotatedColoredQuad(const Math::Vector2f& position, const Math::Vector2f& size, const Math::float32_t angle, const Math::Vector4f& color);
			static void SubmitRotatedColoredQuad(const Math::Vector3f& position, const Math::float32_t& size, const Math::float32_t angle, const Math::Vector4f& color);
			static void SubmitRotatedColoredQuad(const Math::Vector3f& position, const Math::Vector2f& size, const Math::float32_t angle, const Math::Vector4f& color);

			static void SubmitRotatedTexturedQuad(const Math::Vector2f& position, const Math::float32_t& size, const Math::float32_t angle, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint = Math::Vector4f(1.0f));
			static void SubmitRotatedTexturedQuad(const Math::Vector2f& position, const Math::Vector2f& size, const Math::float32_t angle, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint = Math::Vector4f(1.0f));
			static void SubmitRotatedTexturedQuad(const Math::Vector3f& position, const Math::float32_t& size, const Math::float32_t angle, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint = Math::Vector4f(1.0f));
			static void SubmitRotatedTexturedQuad(const Math::Vector3f& position, const Math::Vector2f& size, const Math::float32_t angle, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint = Math::Vector4f(1.0f));

			static void SubmitSprite(const Math::Vector2f& position, const Math::float32_t& size, Sprite::Sprite& Sprite);
			static void SubmitSprite(const Math::Vector2f& position, const Math::Vector2f& size, Sprite::Sprite& Sprite);
			static void SubmitSprite(const Math::Vector3f& position, const Math::float32_t& size, Sprite::Sprite& Sprite);
			static void SubmitSprite(const Math::Vector3f& position, const Math::Vector2f& size, Sprite::Sprite& Sprite);

			static Renderer2DStorage* s_Data;

		private:
			//static void InitFreeType();
		};
	}
}