///////////////////////////////////////////
// File: Renderer3D.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Render/API/Pipeline.h"
#include "Snow/Render/API/RenderPass.h"

#include "Snow/Render/API/Texture.h"
#include "Snow/Render/API/Buffer.h"
#include "Snow/Render/API/Framebuffer.h"
#include "Snow/Render/API/RenderPass.h"

#include "Snow/Render/Material.h"
#include "Snow/Render/Mesh.h"
#include "Snow/Scene/Scene.h"

#include "Snow/Math/Matrix.h"
#include "Snow/Math/Vector.h"

#include <assimp/Importer.hpp>
#include <assimp/DefaultLogger.hpp>

#include "Snow/Core/Base.h"

#include "Snow/Render/Renderer.h"

namespace Snow {
	namespace Render {
		class AssimpImporter;

		class Renderer3D {
		public:
			struct Renderer3DStorage {
				const Scene::Scene3D* mainScene;

				Core::Ref<API::UniformBuffer> CameraBuffer;
				Core::Ref<API::UniformBuffer> ModelBuffer;
			};

			struct AILogStream : public Assimp::LogStream {
				static void Initialize() {
					if (Assimp::DefaultLogger::isNullLogger()) {
						Assimp::DefaultLogger::create("", Assimp::Logger::VERBOSE);
						Assimp::DefaultLogger::get()->attachStream(new AILogStream, Assimp::Logger::Err | Assimp::Logger::Warn);
					}
				}

				virtual void write(const char* message) {
					SNOW_CORE_ERROR("Assimp error : {0}", message);
				}
			};

			static void Init();
			static void Shutdown();

			static void BeginScene(const Scene::Scene3D& scene);
			static void EndScene();

			static void Present();

			static void SubmitMesh(const Core::Ref<Mesh>& mesh, const Math::Matrix4x4f& transform = Math::Matrix4x4f(1.0f), const Core::Ref<MaterialInstance>& materialInstance = Core::Ref<MaterialInstance>());

			static Renderer3DStorage* s_Data;

			static std::unique_ptr<Assimp::Importer> s_AImporter;
		};


	}
}
