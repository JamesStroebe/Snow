#include "spch.h"
#include "Snow/Render/Font/Font.h"

#include "Snow/Render/Renderer2D.h"

namespace Snow {
	namespace Render {
		namespace Font {
			Font::Font(const std::string& path) {
				FT_Face face;
				if (Renderer2D::GetData()->FTResult = FT_New_Face(Renderer2D::GetData()->FTLib, path.c_str(), 0, &face))
					SNOW_CORE_TRACE("FreeType error :  Failed to load font {0}, {1}", path, Renderer2D::GetData()->FTResult);
				FT_Set_Pixel_Sizes(face, 0, 48);

				//if (Renderer2D::GetData()->FTResult = FT_Load_Char(face, 'X', FT_LOAD_RENDER))
				//	SNOW_CORE_TRACE("FreeType error : Failed to load char 'x' {0}", Renderer2D::GetData()->FTResult);

				for (Math::uint32_t i = 0; i < 128; i++) {
					if(Renderer2D::GetData()->FTResult = FT_Load_Char(face, i, FT_LOAD_RENDER))
						SNOW_CORE_TRACE("FreeType error : Failed to load glyph number {0} {1}", i, Renderer2D::GetData()->FTResult);
					Character ch = {
						Core::Buffer(face->glyph->bitmap.buffer, face->glyph->bitmap.rows * face->glyph->bitmap.width),
						Math::Vector2i(face->glyph->bitmap.width, face->glyph->bitmap.rows),
						Math::Vector2i(face->glyph->bitmap_left, face->glyph->bitmap_top),
						face->glyph->advance.x,
						Math::Vector2f(0.0f),
						Math::Vector2f(0.0f)
					};
					m_Characters.push_back(ch);
				}

				BitmapPackCharacterBitmaps();
			}


			Core::Buffer Font::BitmapPackCharacterBitmaps() {

				uint32_t MaxBufferSize = 0;
				for (Math::uint32_t i = 0; i < m_Characters.size(); i++) {
					if (m_Characters[i].buffer.Size > MaxBufferSize)
						MaxBufferSize = m_Characters[i].buffer.Size;


				}
				SNOW_CORE_TRACE(MaxBufferSize);

				API::TextureSpecification textureAtlasSpec;
				textureAtlasSpec.Format = API::TextureFormat::R;
				m_TextureAtlas = API::Texture2D::Create(m_Characters[69].Size.x, m_Characters[69].Size.y, textureAtlasSpec);
				m_TextureAtlas->SetData(m_Characters[69].buffer.Data, m_Characters[69].buffer.Size);

				return m_Characters[69].buffer;
			}
		}
	}
}