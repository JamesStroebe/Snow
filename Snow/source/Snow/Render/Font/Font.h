///////////////////////////////////////////
// File: Font.h
//
// Original Author: James Stroebe
//
// Creation Date: 07/04/2020
//
///////////////////////////////////////////

#pragma once

#include <string>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "Snow/Render/API/Texture.h"

namespace Snow {
	namespace Render {
		namespace Font {

			struct Character {
				Core::Buffer buffer;

				Math::Vector2i Size;
				Math::Vector2i Bearing;
				Math::uint32_t Advance;

				Math::Vector2f StartUV;
				Math::Vector2f EndUV;
			};

			class Font {
			public:
				Font(const std::string& path);

				Core::Ref<API::Texture2D>& GetTextureAtlas() { return m_TextureAtlas; }
				
				std::vector<Character> GetCharacters() { return m_Characters; }

			private:

				Core::Buffer BitmapPackCharacterBitmaps();

				Core::Ref<API::Texture2D> m_TextureAtlas;

				std::vector<Character> m_Characters;
			};

		}
	}
}