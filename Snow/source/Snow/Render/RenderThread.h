///////////////////////////////////////////
// File: RenderThread.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Core/Thread.h"

namespace Snow {
	namespace Render {
		enum class RenderThreadState {
			Wait, Render, Stop
		};

		class RenderThread : public Core::Thread {
		public:
			static void Start();
			static void Stop();

			static void Wait();
			static void WaitForUpdate();
			static void WaitAndRender();

			static void Run();


		private:
			Core::Thread m_Thread;
			RenderThreadState m_State = RenderThreadState::Stop;
			static RenderThread s_Instance;
		};
	}
}