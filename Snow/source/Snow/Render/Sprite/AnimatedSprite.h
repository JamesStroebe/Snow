///////////////////////////////////////////
// File: AnimatedSprite.h
//
// Original Author: James Stroebe
//
// Creation Date: 16/01/2020
//
///////////////////////////////////////////

#pragma once
#include "Snow/Render/Sprite/Sprite.h"
#include <vector>

#include "Snow/Core/Time/Timestep.h"

namespace Snow {
	namespace Render {
		namespace Sprite {
			class AnimatedSprite {
			public:
				AnimatedSprite(uint32_t numFrames, float animationTime, std::vector<Sprite> sprites);
				~AnimatedSprite();

				Sprite GetFrame(Core::Time::Timestep ts);

			private:
				uint32_t m_FrameCount;
				float m_AnimTime;
				std::vector<Sprite> m_Sprites;
			};
		}
	}
}