///////////////////////////////////////////
// File: SpriteSheet.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Render/API/Texture.h"

namespace Snow {
	namespace Render {
		namespace Sprite {
			class SpriteSheet {
			public:
				SpriteSheet() = default;
				SpriteSheet(const std::string& path, const API::TextureSpecification& spec = API::TextureSpecification());

				inline Core::Ref<API::Texture2D> GetTexture() const { return m_Texture; }

				inline float GetWidth() { return m_Width; }
				inline float GetHeight() { return m_Height; }

			private:
				Core::Ref<API::Texture2D> m_Texture;

				float m_Width, m_Height;

			};
		}
	}
}
