#include "spch.h"
#include "Snow/Render/Sprite/AnimatedSprite.h"

namespace Snow {
	namespace Render {
		namespace Sprite {
			AnimatedSprite::AnimatedSprite(uint32_t numFrames, float animationTime, std::vector<Sprite> sprites) :
				m_FrameCount(numFrames), m_AnimTime(animationTime), m_Sprites(sprites) {

			}

			AnimatedSprite::~AnimatedSprite() {

			}

			Sprite AnimatedSprite::GetFrame(Core::Time::Timestep ts) {
				return m_Sprites[0];
			}
		}
	}
}