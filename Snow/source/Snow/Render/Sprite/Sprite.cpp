#include "spch.h"
#include "Snow/Render/Sprite/Sprite.h"

#include "Snow/Math/Common/Epsilon.h"

namespace Snow {
	namespace Render {
		namespace Sprite {
			//TEXTURES START AT BOTTOM LEFT
			Sprite::Sprite(float xOffset, float yOffset, float width, float height, SpriteSheet sheet) :
				m_X(xOffset), m_Y(yOffset), m_Width(width), m_Height(height), m_Sheet(sheet) {

				
				
				m_StartUV.x = (float)xOffset / sheet.GetWidth() + Math::Epsilon<float>();
				m_StartUV.y = (float)yOffset / sheet.GetHeight() + Math::Epsilon<float>();
				m_EndUV.x = (xOffset + (float)width) / sheet.GetWidth() - Math::Epsilon<float>();
				m_EndUV.y = (yOffset + (float)height) / sheet.GetHeight() - Math::Epsilon<float>();
			}
		}
	}
}