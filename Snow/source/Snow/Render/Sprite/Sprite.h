///////////////////////////////////////////
// File: Sprite.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Render/API/Texture.h"
#include "Snow/Render/Sprite/SpriteSheet.h"

#include "Snow/Math/Vector.h"

namespace Snow {
	namespace Render {
		namespace Sprite {
			class Sprite {
			public:
				Sprite() = default;
				Sprite(float x, float y, float width, float height, SpriteSheet sheet);

				inline Core::Ref<API::Texture2D> GetTexture() const { return m_Sheet.GetTexture(); }

				inline SpriteSheet GetSheet() const { return m_Sheet; }

				inline uint32_t GetWidth() const { return m_Width; }
				inline uint32_t GetHeight() const { return m_Height; }

				inline Math::Vector2f GetStartUV() { return m_StartUV; }
				inline Math::Vector2f GetEndUV() { return m_EndUV; }
			private:
				Core::Ref<API::Texture2D> m_Texture;
				SpriteSheet m_Sheet;
				int m_X, m_Y;
				uint32_t m_Width, m_Height;

				Math::Vector2f m_StartUV;
				Math::Vector2f m_EndUV;
			};

		}
	}
}
