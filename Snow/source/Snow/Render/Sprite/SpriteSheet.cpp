#include "spch.h"
#include "Snow/Render/Sprite/SpriteSheet.h"

namespace Snow {
	namespace Render {
		namespace Sprite {
			SpriteSheet::SpriteSheet(const std::string& path, const API::TextureSpecification& spec) {
				m_Texture = API::Texture2D::Create(path, spec);

				m_Width = m_Texture->GetWidth();
				m_Height = m_Texture->GetHeight();

			}
		}
	}
}