///////////////////////////////////////////
// File: Material.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Core/Base.h"

#include "Snow/Render/API/Buffer.h"
#include "Snow/Render/API/Texture.h"
#include "Snow/Render/API/Pipeline.h"

#include <unordered_set>

namespace Snow {
	namespace Render {
		class Material {
			friend class MaterialInstance;
		public:
			Material(const std::vector<Core::Ref<Shader::Shader>>& shaders);
			virtual ~Material();

			void Bind(const Core::Ref<API::Pipeline>& pipeline) const;

			template <typename T>
			void Set(const std::string& name, const T& value) {
				auto decl = FindFieldDeclaration(name);
				//SNOW_CORE_ASSERT(decl, "Could not find uniform with name 'x'");
				auto& buffer = GetUniformBufferTarget(decl);


				buffer.Write((Core::byte*)&value, decl->GetSize(), decl->GetOffset());

				for (auto mi : m_MaterialInstances)
					mi->OnMaterialValueUpdated(decl);
			}

			void Set(const std::string& name, const Core::Ref<API::Texture>& texture) {
				auto decl = FindResourceDeclaration(name);
				uint32_t slot = decl->GetSlot();
				if (m_Textures.size() <= slot)
					m_Textures.resize((size_t)slot + 1);
				m_Textures[slot] = texture;
			}

			void Set(const std::string& name, const Core::Ref<API::Texture2D>& texture) {
				Set(name, (const Core::Ref<API::Texture>&)texture);
			}

			void Set(const std::string& name, const Core::Ref<API::TextureCube>& texture) {
				Set(name, (const Core::Ref<API::Texture>&)texture);
			}

		public:
			static Core::Ref<Material> Create(const std::vector<Core::Ref<Shader::Shader>>& shaders);
		private:
			void AllocateStorage();
			void OnShaderReloaded();
			void BindTextures() const;

			Shader::ShaderField* FindFieldDeclaration(const std::string& name);
			Shader::ShaderResource* FindResourceDeclaration(const std::string& name);
			Core::Buffer& GetUniformBufferTarget(Shader::ShaderField* field);

			std::vector<Core::Ref<Shader::Shader>> m_Shaders;
			std::unordered_set<MaterialInstance*> m_MaterialInstances;

			Shader::ShaderBuffer m_VSUniformStorageBuffer = Shader::ShaderBuffer();
			Shader::ShaderBuffer m_PSUniformStorageBuffer = Shader::ShaderBuffer();

			Core::Buffer m_VSBuffer;
			Core::Buffer m_PSBuffer;

			Core::Ref<API::UniformBuffer> m_VSMaterialBuffer;
			Core::Ref<API::UniformBuffer> m_PSMaterialBuffer;
			std::vector<Core::Ref<API::Texture>> m_Textures;

			int32_t m_RenderFlags = 0;
		};

		class MaterialInstance {
			friend class Material;
		public:
			MaterialInstance(const Core::Ref<Material>& material);
			virtual ~MaterialInstance();

			template <typename T>
			void Set(const std::string& name, const T& value) {
				auto decl = m_Material->FindFieldDeclaration(name);
				if (!decl)
					return;
				SNOW_CORE_ASSERT(decl, "Could not find uniform with name 'x'");
				auto& buffer = GetUniformBufferTarget(decl);
				buffer.Write((Core::byte*)&value, decl->GetSize(), decl->GetOffset());
				m_OverriddenValues.insert(name);
			}

			void Set(const std::string& name, const Core::Ref<API::Texture>& texture) {
				auto decl = m_Material->FindResourceDeclaration(name);
				uint32_t slot = decl->GetSlot();
				if (m_Textures.size() <= slot)
					m_Textures.resize((size_t)slot + 1);
				m_Textures[slot] = texture;
			}

			void Set(const std::string& name, const Core::Ref<API::Texture2D>& texture) {
				Set(name, (const Core::Ref<API::Texture>&)texture);
			}

			void Set(const std::string& name, const Core::Ref<API::TextureCube>& texture) {
				Set(name, (const Core::Ref<API::Texture>&)texture);
			}

			void Bind(const Core::Ref<API::Pipeline>& pipeline) const;

			static Core::Ref<MaterialInstance> Create(const Core::Ref<Material>& material);
		private:
			void AllocateStorage();
			void OnShaderReloaded();
			Core::Buffer& GetUniformBufferTarget(Shader::ShaderField* field);
			void OnMaterialValueUpdated(Shader::ShaderField* field);

			Core::Ref<Material> m_Material;

			Shader::ShaderBuffer m_VSUniformStorageBuffer;
			Shader::ShaderBuffer m_PSUniformStorageBuffer;

			Core::Buffer m_VSBuffer;
			Core::Buffer m_PSBuffer;

			Core::Ref<API::UniformBuffer> m_VSMaterialBuffer;
			Core::Ref<API::UniformBuffer> m_PSMaterialBuffer;
			std::vector<Core::Ref<API::Texture>> m_Textures;

			std::unordered_set<std::string> m_OverriddenValues;
		};
	}
}