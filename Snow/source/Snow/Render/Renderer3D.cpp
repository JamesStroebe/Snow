#include "spch.h"
#include "Renderer3D.h"



namespace Snow {
	namespace Render {
		Renderer3D::Renderer3DStorage* Renderer3D::s_Data = {};

		std::unique_ptr<Assimp::Importer> Renderer3D::s_AImporter = nullptr;

		void Renderer3D::Init() {
			s_Data = new Renderer3DStorage();
			AILogStream::Initialize();

			s_AImporter = std::make_unique<Assimp::Importer>();

			s_Data->CameraBuffer = API::UniformBuffer::Create("Camera", Shader::ShaderType::VertexShader);
		}

		void Renderer3D::Shutdown() {

		}

		void Renderer3D::BeginScene(const Scene::Scene3D& scene) {
			s_Data->mainScene = &scene;

			

			API::ShaderBufferDeclaration<sizeof(Math::Matrix4x4f) * 1, 1> cameraBuffer;
			cameraBuffer.Push("r_MVP", Math::transpose(scene.GetCamera()->GetViewProjectionMatrix()));


			
			s_Data->CameraBuffer->SetData(cameraBuffer.buffer->Data, cameraBuffer.buffer->Size);

		}

		void Renderer3D::EndScene() {
			auto renderCommandBuffer = Renderer::GetCommandBuffer();
			renderCommandBuffer->EndRenderPass();
		}

		void Renderer3D::SubmitMesh(const Core::Ref<Mesh>& mesh, const Math::Matrix4x4f& transform, const Core::Ref<MaterialInstance>& materialInstance) {
			auto renderCommandBuffer = Renderer::GetCommandBuffer();


			API::ShaderBufferDeclaration<sizeof(Math::Matrix4x4f) * 1, 1> modelBuffer;
			Math::Matrix4x4f mvp = s_Data->mainScene->GetCamera()->GetViewProjectionMatrix() * transform;
			modelBuffer.Push("r_MVP", Math::transpose(mvp));
			s_Data->CameraBuffer->SetData(modelBuffer.buffer->Data, modelBuffer.buffer->Size);
			
			//mesh->GetPipeline()->Bind();
			mesh->GetPipeline()->UploadBuffer(s_Data->CameraBuffer);


			if (materialInstance)
				materialInstance->Bind(mesh->GetPipeline());

			renderCommandBuffer->SubmitElements(mesh->GetPipeline(), mesh->GetVertexBuffer(), mesh->GetIndexBuffer());
			//renderCommandBuffer->Flush();
		}

		void Renderer3D::Present() {

		}

	}
}