#include "spch.h"
#include "RenderThread.h"

#include "Renderer.h"

#include <chrono>

namespace Snow {
	namespace Render {
		RenderThread RenderThread::s_Instance;

		void RenderThread::Start() {

			s_Instance.m_Thread.Start("Render", RenderThread::Run);
			s_Instance.m_State = RenderThreadState::Wait;
		}

		void RenderThread::Stop() {

			s_Instance.m_State = RenderThreadState::Stop;
			s_Instance.m_Thread.Join();
		}

		void RenderThread::Run() {

			while (s_Instance.m_State != RenderThreadState::Stop) {
				if (s_Instance.m_State == RenderThreadState::Render) {
					//Renderer::Get().WaitAndRender();
					s_Instance.m_State = RenderThreadState::Wait;
				}
				while (s_Instance.m_State == RenderThreadState::Wait)
					Wait();
			}
		}

		void RenderThread::Wait() {

			using namespace std::chrono_literals;
			std::this_thread::sleep_for(1ms);
		}

		void RenderThread::WaitForUpdate() {

			s_Instance.m_State = RenderThreadState::Wait;
			while (s_Instance.m_State == RenderThreadState::Wait)
				Wait();
		}

		void RenderThread::WaitAndRender() {

			while (s_Instance.m_State == RenderThreadState::Render)
				Wait();
			s_Instance.m_State = RenderThreadState::Render;
		}
	}
}