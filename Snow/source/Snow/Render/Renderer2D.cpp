#include "spch.h"
#include "Snow/Render/Renderer2D.h"
#include "Snow/Render/Renderer.h"


namespace Snow {
	namespace Render {
		Renderer2D::Renderer2DStorage* Renderer2D::s_Data = nullptr;

		void Renderer2D::Init() {
			s_Data = new Renderer2DStorage();
			SNOW_CORE_TRACE("Renderer2DInit");


			API::PipelineSpecification backgroundPipelineSpec;
			switch (Renderer::GetRenderAPI()) {
			case RenderAPI::OPENGL: backgroundPipelineSpec.Shaders = { Shader::Shader::Create("assets/shaders/glsl/Render2DBackgroundVert.glsl", Shader::ShaderType::VertexShader), Shader::Shader::Create("assets/shaders/glsl/Render2DBackgroundFrag.glsl", Shader::ShaderType::PixelShader) }; break;
			case RenderAPI::DIRECTX:backgroundPipelineSpec.Shaders = { Shader::Shader::Create("assets/shaders/hlsl/Render2DBackgroundVert.hlsl", Shader::ShaderType::VertexShader), Shader::Shader::Create("assets/shaders/hlsl/Render2DBackgroundPixl.hlsl", Shader::ShaderType::PixelShader) };
			}
			backgroundPipelineSpec.VertexBufferLayout = {
				{ API::VertexAttribType::Float3, "POSITION" },
				{ API::VertexAttribType::Float2, "TEXCOORD" }
			};
			SNOW_CORE_TRACE("\tCreating Background Pipeline...");
			s_Data->BackgroundPipeline = API::Pipeline::Create(backgroundPipelineSpec);
			//s_Data->BackgroundPipeline->SetPrimitiveType(API::PrimitiveType::Triangle);

			float backgroundVBOData[20] = {
				-1.0f, -1.0f, 0.0f, 0.0, 0.0,
				 1.0f, -1.0f, 0.0f, 1.0, 0.0,
				 1.0f,  1.0f, 0.0f, 1.0, 1.0,
				-1.0f,  1.0f, 0.0f, 0.0, 1.0
			};

			Math::uint32_t backgroundIBOData[6] = {
				0, 1, 2, 2, 3, 0
			};

			s_Data->BackgroundVBO = API::VertexBuffer::Create(backgroundVBOData, 20 * sizeof(float));
			s_Data->BackgroundIBO = API::IndexBuffer::Create(backgroundIBOData, 6 * sizeof(Math::uint32_t));


			API::PipelineSpecification pipelineSpec;


			switch (Renderer::GetRenderAPI()) {
			case RenderAPI::OPENGL:		pipelineSpec.Shaders = { Shader::Shader::Create("assets/shaders/glsl/BatchRenderVert.glsl", Shader::ShaderType::VertexShader), Shader::Shader::Create("assets/shaders/glsl/BatchRenderFrag.glsl", Shader::ShaderType::PixelShader) }; break;
			case RenderAPI::DIRECTX:	pipelineSpec.Shaders = { Shader::Shader::Create("assets/shaders/hlsl/BatchRenderVert.hlsl", Shader::ShaderType::VertexShader), Shader::Shader::Create("assets/shaders/hlsl/BatchRenderPixl.hlsl", Shader::ShaderType::PixelShader) }; break;
			//case RenderAPI::VULKAN:		pipelineSpec.Shader = Shader::Shader::Create("assets/shaders/basic.vert.spv", "assets/shaders/basic.frag.spv"); break;
			}


			pipelineSpec.VertexBufferLayout = {
				{ API::VertexAttribType::Float3, "POSITION" },
				{ API::VertexAttribType::Float2, "TEXCOORD" },
				{ API::VertexAttribType::Float, "ID" },
				{ API::VertexAttribType::Float4, "COLOR" },

			};
			SNOW_CORE_TRACE("\tCreating Batch Pipeline...");
			s_Data->Pipeline2D = API::Pipeline::Create(pipelineSpec);

			s_Data->VertexDataBase = new Vertex[s_Data->MaxVertices];
            s_Data->VertexBuffer2D = API::VertexBuffer::Create(sizeof(Vertex) * s_Data->MaxVertices);
			
			uint32_t* quadIndices = new uint32_t[s_Data->MaxIndices];

			uint32_t offset = 0;
			for (uint32_t i = 0; i < s_Data->MaxIndices; i += 6) {
				quadIndices[i + 0] = offset + 0;
				quadIndices[i + 1] = offset + 1;
				quadIndices[i + 2] = offset + 2;

				quadIndices[i + 3] = offset + 2;
				quadIndices[i + 4] = offset + 3;
				quadIndices[i + 5] = offset + 0;

				offset += 4;
			}

			s_Data->IndexBuffer2D = API::IndexBuffer::Create(quadIndices, sizeof(uint32_t) * s_Data->MaxIndices);
			delete[] quadIndices;

			s_Data->CameraBuffer = API::UniformBuffer::Create("RCamera", Shader::ShaderType::VertexShader);

			SNOW_CORE_TRACE("\tCreating Colored Texture...");
			Math::uint32_t whiteTextureData = 0xffffffff;

			Core::Ref<API::Texture2D> whiteTexture = API::Texture2D::Create(1, 1);
			whiteTexture->SetData(&whiteTextureData, sizeof(Math::uint32_t));

			s_Data->m_Textures.resize(Renderer::GetCapabilities().MaxSamplerCount);
			s_Data->m_Textures[0] = whiteTexture;

			s_Data->QuadVertexPositions[0] = Math::Vector4f(-0.5f, -0.5f, 0.0f, 1.0f);
			s_Data->QuadVertexPositions[1] = Math::Vector4f( 0.5f, -0.5f, 0.0f, 1.0f);
			s_Data->QuadVertexPositions[2] = Math::Vector4f( 0.5f,  0.5f, 0.0f, 1.0f);
			s_Data->QuadVertexPositions[3] = Math::Vector4f(-0.5f,  0.5f, 0.0f, 1.0f);

			//Render::Renderer::GetCommandBuffer()->Execute();

			//s_Data->CameraBuffer->SetDomain(Shader::ShaderDomain::Vertex);
			//InitFreeType();
		}

		void Renderer2D::Shutdown() {
			delete s_Data;
		}

		//void Renderer2D::InitFreeType() {
		//	
		//	if (s_Data->FTResult = FT_Init_FreeType(&s_Data->FTLib))
		//		SNOW_CORE_TRACE("FreeType error :  Failed to init FreeType {0}", s_Data->FTResult);
		//}

		void Renderer2D::BeginScene(const Scene::Scene2D& scene) {
			s_Data->BackgroundScene = &scene;

			if (s_Data->BackgroundScene->GetBackground() != nullptr) {
				s_Data->BackgroundPipeline->Bind();
				s_Data->BackgroundScene->GetBackground()->Bind(0);
				Renderer::GetCommandBuffer()->SubmitElements(s_Data->BackgroundPipeline, s_Data->BackgroundVBO, s_Data->BackgroundIBO);
			}

			s_Data->VertexDataPtr = s_Data->VertexDataBase;
			
			API::ShaderBufferDeclaration<sizeof(Math::Matrix4x4f) * 1, 1> cameraBuffer;
			
			cameraBuffer.Push("r_MVP", Math::transpose(s_Data->BackgroundScene->GetCamera().GetViewProjectionMatrix()));

			s_Data->CameraBuffer->SetData(cameraBuffer.GetBuffer().Data, cameraBuffer.GetBuffer().Size);
			

			s_Data->Pipeline2D->Bind();
			s_Data->Pipeline2D->UploadBuffer(s_Data->CameraBuffer);

			
			s_Data->Statistics.IndexCount = 0;
			s_Data->TextureIndex = 1;
			s_Data->VertexDataPtr = s_Data->VertexDataBase;

		}

		void Renderer2D::EndScene() {
			Math::uint32_t dataSize = (Math::uint32_t)((Math::uint8_t*)s_Data->VertexDataPtr - (Math::uint8_t*)s_Data->VertexDataBase);
			s_Data->VertexBuffer2D->SetData(s_Data->VertexDataBase, dataSize);

			Present();
		}

		void Renderer2D::Present() {
			//SNOW_CORE_TRACE(s_Data->m_Textures.size());

			

			for (uint32_t i = 0; i < s_Data->TextureIndex; i++) 
				s_Data->m_Textures[i]->Bind(i);

			s_Data->Statistics.DrawCalls++;
			Renderer::GetCommandBuffer()->SubmitElements(s_Data->Pipeline2D, s_Data->VertexBuffer2D, s_Data->IndexBuffer2D, s_Data->Statistics.IndexCount);
			
			
		}

		void Renderer2D::ResetStats() {
			s_Data->Statistics.DrawCalls = 0;
			s_Data->Statistics.QuadCount = 0;
			s_Data->Statistics.VertexCount = 0;
		}

		float Renderer2D::SubmitTexture(const Core::Ref<API::Texture2D>& texture) {
			auto renderCommandBuffer = Renderer::GetCommandBuffer();

			uint32_t maxSlots = Renderer::GetCapabilities().MaxSamplerCount - 1;
			float result = 0.0;
			bool found = false;
			

			for (uint32_t i = 1; i < s_Data->TextureIndex; i++) {
				if (s_Data->m_Textures[i] == texture) {
					result = (float)(i);
					found = true;
					break;
				}
			}

			if (!found) {
				if (s_Data->TextureIndex >= maxSlots) {
					EndScene();
					Present();
					//renderCommandBuffer->Flush();
					s_Data->VertexDataPtr = s_Data->VertexDataBase;
				}
				result = (float)s_Data->TextureIndex;
				s_Data->m_Textures[s_Data->TextureIndex] = texture;
				s_Data->TextureIndex++;
			}
			return result;

		}

		void Renderer2D::SubmitLine(const Math::Vector2f& startPosition, const Math::Vector2f& endPosition, const Math::float32_t& thickness, const Math::Vector4f& color) {
			SubmitLine({ startPosition.x, startPosition.y, 0.0f }, { endPosition.x, endPosition.y, 0.0f }, thickness, color);
		}

		void Renderer2D::SubmitLine(const Math::Vector3f& startPosition, const Math::Vector3f& endPosition, const Math::float32_t& thickness, const Math::Vector4f& color) {
			auto renderCommandBuffer = Renderer::GetCommandBuffer();

			if (s_Data->Statistics.IndexCount >= Renderer2DStorage::MaxIndices)
				EndScene();

			Math::Vector2f slope = Math::Vector2f(endPosition.y - startPosition.y, -(endPosition.x - startPosition.x));
			Math::Vector2f normal = Math::normalize(slope) * thickness;
			
			s_Data->VertexDataPtr->Position = Math::Vector3f(startPosition.x + normal.x, startPosition.y + normal.y, startPosition.z);
			s_Data->VertexDataPtr->TexCoord = Math::Vector2f(0.0f, 0.0f);
			s_Data->VertexDataPtr->TexID = 0.0f;
			s_Data->VertexDataPtr->Color = color;
			s_Data->VertexDataPtr++;

			s_Data->VertexDataPtr->Position = Math::Vector3f(endPosition.x + normal.x, endPosition.y + normal.y, startPosition.z);
			s_Data->VertexDataPtr->TexCoord = Math::Vector2f(1.0f, 0.0f);
			s_Data->VertexDataPtr->TexID = 0.0f;
			s_Data->VertexDataPtr->Color = color;
			s_Data->VertexDataPtr++;

			s_Data->VertexDataPtr->Position = Math::Vector3f(endPosition.x - normal.x, endPosition.y - normal.y, startPosition.z);
			s_Data->VertexDataPtr->TexCoord = Math::Vector2f(1.0f, 1.0f);
			s_Data->VertexDataPtr->TexID = 0.0f;
			s_Data->VertexDataPtr->Color = color;
			s_Data->VertexDataPtr++;

			s_Data->VertexDataPtr->Position = Math::Vector3f(startPosition.x - normal.x, startPosition.y - normal.y, startPosition.z);;
			s_Data->VertexDataPtr->TexCoord = Math::Vector2f(0.0f, 1.0f);
			s_Data->VertexDataPtr->TexID = 0.0f;
			s_Data->VertexDataPtr->Color = color;
			s_Data->VertexDataPtr++;

			s_Data->Statistics.VertexCount += 4;
			s_Data->Statistics.IndexCount += 6;
			s_Data->Statistics.QuadCount++;
		}

		void Renderer2D::SubmitColoredQuad(const Math::Vector2f& position, const Math::float32_t& size, const Math::Vector4f& color) {
			SubmitColoredQuad(position, { size, size }, color);
		}

		void Renderer2D::SubmitColoredQuad(const Math::Vector2f& position, const Math::Vector2f& size, const Math::Vector4f& color) {
			SubmitColoredQuad({ position.x, position.y, 0.0f }, size, color);
		}

		void Renderer2D::SubmitColoredQuad(const Math::Vector3f& position, const Math::float32_t& size, const Math::Vector4f& color) {
			SubmitColoredQuad(position, { size, size }, color);
		}

		void Renderer2D::SubmitColoredQuad(const Math::Vector3f& position, const Math::Vector2f& size, const Math::Vector4f& color) {
			auto renderCommandBuffer = Renderer::GetCommandBuffer();

			if (s_Data->Statistics.IndexCount >= Renderer2DStorage::MaxIndices)
				EndScene();

			
			const Math::float32_t textureID = 0.0f;
			constexpr Math::Vector2f texcoords[] = { {0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f} };
			Math::Vector3f scale[] = { {0.0f, 0.0f, 0.0f}, {size.x, 0.0f, 0.0f}, {size.x, size.y, 0.0f}, {0.0f, size.y, 0.0f} };

			for (size_t i = 0; i < 4; i++) {
				s_Data->VertexDataPtr->Position = position + scale[i];
				s_Data->VertexDataPtr->TexCoord = texcoords[i];
				s_Data->VertexDataPtr->TexID = textureID;
				s_Data->VertexDataPtr->Color = color;
				s_Data->VertexDataPtr++;
			}

			s_Data->Statistics.VertexCount += 4;
			s_Data->Statistics.IndexCount += 6;
			s_Data->Statistics.QuadCount++;
		}

		void Renderer2D::SubmitTexturedQuad(const Math::Vector2f& position, const Math::float32_t& size, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint) {
			SubmitTexturedQuad(position, { size, size }, texture, tint);
		}

		void Renderer2D::SubmitTexturedQuad(const Math::Vector2f& position, const Math::Vector2f& size, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint) {
			SubmitTexturedQuad({ position.x, position.y, 0.0f }, size, texture, tint);
		}

		void Renderer2D::SubmitTexturedQuad(const Math::Vector3f& position, const Math::float32_t& size, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint) {
			SubmitTexturedQuad(position, { size, size }, texture, tint);
		}

		void Renderer2D::SubmitTexturedQuad(const Math::Vector3f& position, const Math::Vector2f& size, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint) {
			auto renderCommandBuffer = Renderer::GetCommandBuffer();

			if (s_Data->Statistics.IndexCount >= Renderer2DStorage::MaxIndices)
				EndScene();
			//move camera culling into a different stage in the pipeline

			const Math::Vector4f& color{ tint };
			const Math::float32_t textureID = SubmitTexture(texture);
			constexpr Math::Vector2f texcoords[] = { {0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f} };
			Math::Vector3f scale[] = { {0.0f, 0.0f, 0.0f}, {size.x, 0.0f, 0.0f}, {size.x, size.y, 0.0f}, {0.0f, size.y, 0.0f} };

			for (size_t i = 0; i < 4; i++) {
				s_Data->VertexDataPtr->Position = position + scale[i];
				s_Data->VertexDataPtr->TexCoord = texcoords[i];
				s_Data->VertexDataPtr->TexID = textureID;
				s_Data->VertexDataPtr->Color = color;
				s_Data->VertexDataPtr++;
			}

			s_Data->Statistics.VertexCount += 4;
			s_Data->Statistics.IndexCount += 6;
			s_Data->Statistics.QuadCount++;
		}

		void Renderer2D::SubmitRotatedColoredQuad(const Math::Vector2f& position, const Math::float32_t& size, const Math::float32_t angle, const Math::Vector4f& color) {
			SubmitRotatedColoredQuad(position, { size, size }, angle, color);
		}

		void Renderer2D::SubmitRotatedColoredQuad(const Math::Vector2f& position, const Math::Vector2f& size, const Math::float32_t angle, const Math::Vector4f& color) {
			SubmitRotatedColoredQuad({ position.x, position.y, 0.0f }, size, angle, color);
		}

		void Renderer2D::SubmitRotatedColoredQuad(const Math::Vector3f& position, const Math::float32_t& size, const Math::float32_t angle, const Math::Vector4f& color) {
			SubmitRotatedColoredQuad(position, { size, size }, angle, color);
		}

		void Renderer2D::SubmitRotatedColoredQuad(const Math::Vector3f& position, const Math::Vector2f& size, const Math::float32_t angle, const Math::Vector4f& color) {
			auto renderCommandBuffer = Renderer::GetCommandBuffer();
			
			if (s_Data->Statistics.IndexCount >= Renderer2DStorage::MaxIndices)
				EndScene();
			
			const Math::float32_t textureID = 0.0f;

			Math::Complexf rotate(Math::cos(angle), Math::sin(angle));
			Math::Complexf translation(position.x, position.y);

			constexpr Math::Vector2f texcoords[] = { {0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f} };

			for (size_t i = 0; i < 4; i++) {
				auto scale = Math::scaleXY(Math::Complexf(s_Data->QuadVertexPositions[i].x, s_Data->QuadVertexPositions[i].y), { size.x, size.y });
				s_Data->VertexDataPtr->Position = Math::Vector3f(Math::Vector2Cast(scale * rotate + translation), position.z);
				s_Data->VertexDataPtr->TexCoord = texcoords[i];
				s_Data->VertexDataPtr->TexID = textureID;
				s_Data->VertexDataPtr->Color = color;
				s_Data->VertexDataPtr++;
			}

			s_Data->Statistics.VertexCount += 4;
			s_Data->Statistics.IndexCount += 6;
			s_Data->Statistics.QuadCount++;
		}

		void Renderer2D::SubmitRotatedTexturedQuad(const Math::Vector2f& position, const Math::float32_t& size, const Math::float32_t angle, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint) {
			SubmitRotatedTexturedQuad(position, { size, size }, angle, texture, tint);
		}

		void Renderer2D::SubmitRotatedTexturedQuad(const Math::Vector2f& position, const Math::Vector2f& size, const Math::float32_t angle, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint) {
			SubmitRotatedTexturedQuad({ position.x, position.y, 0.0f }, size, angle, texture, tint);
		}

		void Renderer2D::SubmitRotatedTexturedQuad(const Math::Vector3f& position, const Math::float32_t& size, const Math::float32_t angle, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint) {
			SubmitRotatedTexturedQuad(position, { size, size }, angle, texture, tint);
		}

		void Renderer2D::SubmitRotatedTexturedQuad(const Math::Vector3f& position, const Math::Vector2f& size, const Math::float32_t angle, const Core::Ref<API::Texture2D>& texture, const Math::Vector4f& tint) {
			auto renderCommandBuffer = Renderer::GetCommandBuffer();

			if (s_Data->Statistics.IndexCount >= Renderer2DStorage::MaxIndices)
				EndScene();
			
			const Math::Vector4f& color{ tint };
			const Math::float32_t textureID = SubmitTexture(texture);

			Math::Complexf rotate(Math::cos(angle), Math::sin(angle));
			Math::Complexf translation(position.x, position.y);

			constexpr Math::Vector2f texcoords[] = { {0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f} };

			for (size_t i = 0; i < 4; i++) {
				auto scale = Math::scaleXY(Math::Complexf(s_Data->QuadVertexPositions[i].x, s_Data->QuadVertexPositions[i].y), { size.x, size.y });
				s_Data->VertexDataPtr->Position = Math::Vector3f(Math::Vector2Cast(scale * rotate + translation), position.z);
				s_Data->VertexDataPtr->TexCoord = texcoords[i];
				s_Data->VertexDataPtr->TexID = textureID;
				s_Data->VertexDataPtr->Color = color;
				s_Data->VertexDataPtr++;
			}

			s_Data->Statistics.VertexCount += 4;
			s_Data->Statistics.IndexCount += 6;
			s_Data->Statistics.QuadCount++;
		}

		void Renderer2D::SubmitSprite(const Math::Vector2f& position, const Math::float32_t& size, Sprite::Sprite& sprite) {
			SubmitSprite(position, { size, size }, sprite);
		}

		void Renderer2D::SubmitSprite(const Math::Vector2f& position, const Math::Vector2f& size, Sprite::Sprite& sprite) {
			SubmitSprite({ position.x, position.y, 0.0 }, size, sprite);
		}

		void Renderer2D::SubmitSprite(const Math::Vector3f& position, const Math::float32_t& size, Sprite::Sprite& sprite) {
			SubmitSprite(position, { size, size }, sprite);
		}
		
		void Renderer2D::SubmitSprite(const Math::Vector3f& position, const Math::Vector2f& size, Sprite::Sprite& sprite) {
			auto renderCommandBuffer = Renderer::GetCommandBuffer();
		
			if (s_Data->Statistics.IndexCount >= Renderer2DStorage::MaxIndices)
				EndScene();
		
			const Math::Vector4f color{ 1.0f, 1.0f, 1.0f, 1.0f };
			const Math::float32_t textureID = SubmitTexture(sprite.GetTexture());

			Math::Matrix4x4f transform = Math::translate(Math::Matrix4x4f(1.0f), position) *
				Math::scale(Math::Matrix4x4f(1.0f), Math::Vector3f(size.x, size.y, 1.0f));
		
			s_Data->VertexDataPtr->Position = Math::Vector3f(position.x, position.y, position.z);
			s_Data->VertexDataPtr->TexCoord = sprite.GetStartUV();
			s_Data->VertexDataPtr->TexID = textureID;
			s_Data->VertexDataPtr->Color = color;
			s_Data->VertexDataPtr++;
		
			s_Data->VertexDataPtr->Position = Math::Vector3f(position.x + size.x, position.y, position.z);
			s_Data->VertexDataPtr->TexCoord = Math::Vector2f(sprite.GetEndUV().x, sprite.GetStartUV().y);
			s_Data->VertexDataPtr->TexID = textureID;
			s_Data->VertexDataPtr->Color = color;
			s_Data->VertexDataPtr++;
		
			s_Data->VertexDataPtr->Position = Math::Vector3f(position.x + size.x, position.y + size.y, position.z);
			s_Data->VertexDataPtr->TexCoord = sprite.GetEndUV();
			s_Data->VertexDataPtr->TexID = textureID;
			s_Data->VertexDataPtr->Color = color;
			s_Data->VertexDataPtr++;
		
			s_Data->VertexDataPtr->Position = Math::Vector3f(position.x, position.y + size.y, position.z);
			s_Data->VertexDataPtr->TexCoord = Math::Vector2f(sprite.GetStartUV().x, sprite.GetEndUV().y);
			s_Data->VertexDataPtr->TexID = textureID;
			s_Data->VertexDataPtr->Color = color;
			s_Data->VertexDataPtr++;
		
			s_Data->Statistics.VertexCount += 4;
			s_Data->Statistics.IndexCount += 6;
			s_Data->Statistics.QuadCount++;
		}
	}
}