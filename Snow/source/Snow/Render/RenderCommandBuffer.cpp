#include "spch.h"

#include "Snow/Render/RenderCommandBuffer.h"

#include "Snow/Render/Renderer.h"

#include "Snow/Platform/OpenGL/OpenGLRenderCommandBuffer.h"
#include "Snow/Platform/DirectX/DirectXRenderCommandBuffer.h"
//#include "Snow/Platform/Vulkan/VulkanRenderCommandBuffer.h"

namespace Snow {
	namespace Render {
		RenderCommandBuffer* RenderCommandBuffer::Create() {
			switch (Renderer::GetRenderAPI()) {
			case RenderAPI::OPENGL:	return new OpenGLRenderCommandBuffer();
			case RenderAPI::DIRECTX:return new DirectXRenderCommandBuffer();
			//case RenderAPI::VULKAN:	return new VulkanRenderCommandBuffer();
			}
		}
	}
}