///////////////////////////////////////////
// File: ShaderLibrary.h
//
// Original Author: James Stroebe
//
// Creation Date: 27/01/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Render/Shaders/Shader.h"

#include <iostream>
#include <unordered_map>

namespace Snow {
	namespace Render {
		namespace Shader {
			class ShaderLibrary {
			public:
				ShaderLibrary();
				~ShaderLibrary();

				static void Add(const Core::Ref<Shader>& shader);
				static void Remove(const Core::Ref<Shader>& shader);
				static Core::Ref<Shader>& Get(const std::string& shaderName);


			private:
				static std::unordered_map<std::string, Core::Ref<Shader>> s_Shaders;
			};
		}
	}
}