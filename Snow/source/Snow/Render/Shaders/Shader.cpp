#include "spch.h"
#include "Snow/Render/Shaders/Shader.h"

#include "Snow/Render/Renderer.h"

#include "Snow/Platform/OpenGL/OpenGLShader.h"
#include "Snow/Platform/DirectX/DirectXShader.h"
#include "Snow/Platform/Vulkan/VulkanShader.h"

namespace Snow {
	namespace Render {
		namespace Shader {
			Core::Ref<Shader> Shader::Create(const std::string& path, ShaderType shaderType) {
				Core::Ref<Shader> shader = nullptr;
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::NONE:	return nullptr;
				case RenderAPI::OPENGL:	shader = Core::CreateRef<OpenGLShader>(path, shaderType); break;
				case RenderAPI::DIRECTX:shader = Core::CreateRef<DirectXShader>(path, shaderType); break;
					//case RenderAPI::VULKAN:	return CreateRef<VulkanShader>(name, shaderType); break;
				}
				Renderer::GetShaderLibrary()->Add(shader);
				return shader;
			}

			//Core::Ref<Shader> Shader::Create(const std::string& vertPath, const std::string& fragPath) {
			//	Core::Ref<Shader> shader = nullptr;
			//	switch (Renderer::GetRenderAPI()) {
			//	case RenderAPI::NONE:	return nullptr;
			//	case RenderAPI::OPENGL:	shader = Core::CreateRef<OpenGLShader>(vertPath, fragPath); break;
			//	case RenderAPI::DIRECTX:shader = Core::CreateRef<DirectXShader>(vertPath, fragPath); break;
			//	case RenderAPI::VULKAN:	shader = Core::CreateRef<VulkanShader>(vertPath, fragPath); break;
			//	}
			//	Renderer::GetShaderLibrary()->Add(shader);
			//	return shader;
			//}

		}
	}
}