#include "spch.h"

#include "Snow/Render/Shaders/ShaderUniform.h"

namespace Snow {
	namespace Render {
		namespace Shader {
			static Math::uint32_t GetFieldTypeSize(FieldType type) {
				switch (type) {
				case FieldType::Float:	return sizeof(Math::f32);
				case FieldType::Float2:	return sizeof(Math::f32) * 2;
				case FieldType::Float3:	return sizeof(Math::f32) * 3;
				case FieldType::Float4:	return sizeof(Math::f32) * 4;
				case FieldType::Mat3:	return sizeof(Math::f32) * 3 * 3;
				case FieldType::Mat4:	return sizeof(Math::f32) * 4 * 4;
				case FieldType::Int:	return sizeof(Math::i32);
				case FieldType::UInt:	return sizeof(Math::ui32);
				}
			}

			ShaderField::ShaderField(FieldType type, const std::string& name, uint32_t count) :
				Type(type), Struct(nullptr), Name(name), Count(count), Location(-1), Offset(0) {
				Size = GetFieldTypeSize(type) * count;
			}

			ShaderField::ShaderField(ShaderStruct* shaderStruct, const std::string& name, uint32_t count) :
				Type(FieldType::Struct), Struct(shaderStruct), Name(name), Count(count), Location(-1), Offset(0) {
				Size = shaderStruct->GetSize() * count;
			}

			Math::uint32_t ShaderField::GetAbsoluteOffset() const {
				return Struct ? Struct->GetOffset() : Offset;
			}

			void ShaderField::SetOffset(uint32_t offset) {
				if (Type == FieldType::Struct)
					Struct->SetOffset(offset);
				Offset = offset;
			}

			ShaderBuffer::ShaderBuffer(const std::string& name, uint32_t slot) :
				Name(name), Size(0), Slot(slot) {}

			void ShaderBuffer::PushField(ShaderField* field) {
				uint32_t offset = 0;
				if (Fields.size()) {
					ShaderField* previous = Fields.back();
					offset = previous->GetOffset() + previous->GetSize();
				}
				field->SetOffset(offset);
				Size += field->GetSize();
				Fields.push_back(field);
			}

			ShaderField* ShaderBuffer::FindField(const std::string& name) {
				for (ShaderField* field : Fields)
					if (field->GetName() == name)
						return field;
				return nullptr;
			}

			ShaderResource::ShaderResource(ResourceType type, const std::string& name, uint32_t count) :
				Type(type), Name(name), Count(count), Slot(-1) {}
		}
	}
}