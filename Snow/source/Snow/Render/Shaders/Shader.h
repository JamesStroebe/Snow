///////////////////////////////////////////
// File: Shader.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Core/Base.h"
#include "Snow/Core/Buffer.h"

#include "Snow/Render/Shaders/ShaderUniform.h"


namespace Snow {
	namespace Render {
		namespace Shader {

			enum class ShaderType {
				None = 0,
				VertexShader, PixelShader, GeometryShader, ComputeShader
			};

			class Shader {
			public:
				virtual ~Shader() = default;

				virtual void Reload() = 0;

				virtual const ShaderBufferList& GetRendererBuffers() const = 0;
				virtual const ShaderBuffer& GetMaterialBuffer() const = 0;
				virtual const ShaderFieldList& GetGlobalUniforms() const = 0;
				virtual const ShaderResourceList& GetResources() const = 0;

				virtual const std::string& GetName() const = 0;
				virtual const ShaderType GetType() const = 0;

				static Core::Ref<Shader> Create(const std::string& path, ShaderType shaderType);
			};
		}
	}
}