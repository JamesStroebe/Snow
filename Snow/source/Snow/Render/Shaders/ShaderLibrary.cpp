#include "spch.h"
#include "Snow/Render/Shaders/ShaderLibrary.h"

namespace Snow {
	namespace Render {
		namespace Shader {
			std::unordered_map<std::string, Core::Ref<Shader>> ShaderLibrary::s_Shaders = {};

			ShaderLibrary::ShaderLibrary() {

			}

			ShaderLibrary::~ShaderLibrary() {

			}

			void ShaderLibrary::Add(const Core::Ref<Shader>& shader) {
				auto& name = shader->GetName();
				SNOW_CORE_ASSERT(s_Shaders.find(name) == s_Shaders.end());
				s_Shaders[name] = shader;
			}

			void ShaderLibrary::Remove(const Core::Ref<Shader>& shader) {
				auto& name = shader->GetName();
				SNOW_CORE_ASSERT(s_Shaders.find(name) != s_Shaders.end());
				s_Shaders.erase(shader->GetName());
			}

			Core::Ref<Shader>& ShaderLibrary::Get(const std::string& name) {
				SNOW_CORE_ASSERT(s_Shaders.find(name) != s_Shaders.end());
				return s_Shaders[name];
			}
		}
	}
}