///////////////////////////////////////////
// File:ShaderUniform.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once

#include <vector>

#include "Snow/Math/Math.h"

namespace Snow {
	namespace Render {
		namespace Shader {
			//CPU parsing

			enum class FieldType {
				None = -1,
				Float, Float2, Float3, Float4,
				Mat3, Mat4,
				Int, UInt, Struct
			};


			class ShaderField {
			private:
				friend class ShaderStruct;
				friend class ShaderBuffer;

				std::string Name;
				Math::uint32_t Size;
				Math::uint32_t Count;
				Math::uint32_t Offset;
				FieldType Type;

				ShaderStruct* Struct;
				mutable Math::int32_t Location;
			public:
				ShaderField(FieldType type, const std::string& name, Math::uint32_t count = 1);
				ShaderField(ShaderStruct* shaderStruct, const std::string& name, Math::uint32_t count = 1);

				inline const std::string& GetName() const { return Name; }
				inline Math::uint32_t GetSize() const { return Size; }
				inline Math::uint32_t GetCount() const { return Count; }
				inline Math::uint32_t GetOffset() const { return Offset; }
				inline Math::uint32_t GetAbsoluteOffset() const;
				inline FieldType GetType() const { return Type; }

				inline const ShaderStruct& GetStruct() { return *Struct; }

				inline void SetLocation(Math::uint32_t location) { Location = location; }
			protected:
				void SetOffset(Math::uint32_t offset);
			};

			typedef std::vector<ShaderField*> ShaderFieldList;

			class ShaderBuffer {
			private:
				std::string Name;
				ShaderFieldList Fields;
				Math::uint32_t Size;
				Math::uint32_t Slot;
			public:
				ShaderBuffer() = default;
				ShaderBuffer(const std::string& name, Math::uint32_t slot);

				void PushField(ShaderField* field);

				inline const std::string& GetName() const { return Name; }
				inline Math::uint32_t GetSize() const { return Size; }
				inline Math::uint32_t GetSlot() const { return Slot; }
				inline void SetSlot(Math::uint32_t slot) { Slot = slot; }
				inline const ShaderFieldList& GetFieldList() const { return Fields; }

				ShaderField* FindField(const std::string& name);

			};

			typedef std::vector<ShaderBuffer*> ShaderBufferList;

			enum class ResourceType {
				None = -1,
				Sampler, Texture2D, TextureCube,
			};

			class ShaderResource {
			private:
				std::string Name;
				Math::uint32_t Count;
				Math::uint32_t Slot;
				ResourceType Type;
			public:
				ShaderResource(ResourceType type, const std::string& name, Math::uint32_t count = 1);

				inline const std::string& GetName() const { return Name; }
				inline Math::uint32_t GetSlot() const { return Slot; }
				inline Math::uint32_t GetCount() const { return Count; }

				inline void SetSlot(Math::uint32_t slot) { Slot = slot; }

				inline ResourceType GetType() const { return Type; }
			};

			typedef std::vector<ShaderResource*> ShaderResourceList;

			class ShaderStruct {
			private:
				friend class Shader;

				std::string Name;
				ShaderFieldList Fields;
				Math::uint32_t Size;
				Math::uint32_t Offset;
			public:
				ShaderStruct(const std::string& name) :
					Name(name), Size(0), Offset(0) {}

				void AddField(ShaderField* field) {
					Size += field->GetSize();
					Math::uint32_t offset = 0;
					if (Fields.size()) {
						ShaderField* previous = Fields.back();
						offset = previous->GetOffset() + previous->GetSize();
					}
					field->SetOffset(offset);
					Fields.push_back(field);
				}

				inline void SetOffset(Math::uint32_t offset) { Offset = offset; }

				inline const std::string& GetName() const { return Name; }
				inline Math::uint32_t GetSize() const { return Size; }
				inline Math::uint32_t GetOffset() const { return Offset; }
				inline const ShaderFieldList& GetFields() const { return Fields; }
			};

			typedef std::vector<ShaderStruct*> ShaderStructList;
		}
	}
}