#include "spch.h"

#include "Material.h"

namespace Snow {
	namespace Render {

		Core::Ref<Material> Material::Create(const std::vector<Core::Ref<Shader::Shader>>& shaders) {
			return Core::CreateRef<Material>(shaders);
		}

		Material::Material(const std::vector<Core::Ref<Shader::Shader>>& shaders) :
			m_Shaders(shaders) {

			for (uint32_t i = 0; i < m_Shaders.size(); i++) {
				if (m_Shaders[i]->GetType() == Shader::ShaderType::VertexShader)
					m_VSUniformStorageBuffer = m_Shaders[i]->GetMaterialBuffer();
				else if (m_Shaders[i]->GetType() == Shader::ShaderType::PixelShader)
					m_PSUniformStorageBuffer = m_Shaders[i]->GetMaterialBuffer();
			}
			

			m_VSMaterialBuffer = Render::API::UniformBuffer::Create(m_VSUniformStorageBuffer.GetName(), Shader::ShaderType::VertexShader);
			m_PSMaterialBuffer = Render::API::UniformBuffer::Create(m_PSUniformStorageBuffer.GetName(), Shader::ShaderType::PixelShader);

			//m_Shader->AddShaderReloadedCallback(std::bind(&Material::OnShaderReloaded, this));
			AllocateStorage();
		}

		Material::~Material() {}

		void Material::AllocateStorage() {
			m_VSBuffer.Allocate(m_VSUniformStorageBuffer.GetSize());
			m_VSBuffer.ZeroInitialize();
			
			m_PSBuffer.Allocate(m_VSUniformStorageBuffer.GetSize());
			m_PSBuffer.ZeroInitialize();
		}

		void Material::OnShaderReloaded() {
			AllocateStorage();

			for (auto mi : m_MaterialInstances)
				mi->OnShaderReloaded();
		}

		Shader::ShaderField* Material::FindFieldDeclaration(const std::string& name) {
			if (m_VSUniformStorageBuffer.GetSize()) {
				auto& declarations = m_VSUniformStorageBuffer.GetFieldList();
				for (Shader::ShaderField* field : declarations) {
					if (field->GetName() == name)
						return field;
				}
			}

			if (m_PSUniformStorageBuffer.GetSize()) {
				auto& declarations = m_PSUniformStorageBuffer.GetFieldList();
				for (Shader::ShaderField* field : declarations) {
					if (field->GetName() == name)
						return field;
				}
			}
			return nullptr;
		}

		Shader::ShaderResource* Material::FindResourceDeclaration(const std::string& name) {
			for (uint32_t i = 0; i < m_Shaders.size(); i++) {
				auto& resources = m_Shaders[i]->GetResources();
				for (Shader::ShaderResource* resource : resources) {
					if (resource->GetName() == name)
						return resource;
				}
			}
			return nullptr;
		}

		Core::Buffer& Material::GetUniformBufferTarget(Shader::ShaderField* field) {
			//switch (field->GetDomain()) {
			//case Shader::ShaderDomain::Vertex:    return m_VSBuffer;
			//case Shader::ShaderDomain::Pixel:     return m_PSBuffer;
			//}

			SNOW_CORE_ASSERT(false, "Invalid uniform declaration domain! Material does not support this shader type.");
			return m_VSBuffer;
		}

		void Material::Bind(const Core::Ref<API::Pipeline>& pipeline) const {

			for (uint32_t i = 0; i < m_Shaders.size(); i++) {
				if (m_Shaders[i]->GetType() == Shader::ShaderType::VertexShader) {
					m_VSMaterialBuffer->SetData(m_VSBuffer.Data, m_VSBuffer.Size);
					pipeline->UploadBuffer(m_VSMaterialBuffer);
				}
				else if (m_Shaders[i]->GetType() == Shader::ShaderType::PixelShader) {
					m_PSMaterialBuffer->SetData(m_PSBuffer.Data, m_PSBuffer.Size);
					pipeline->UploadBuffer(m_PSMaterialBuffer);
				}
			}

			BindTextures();
		}

		void Material::BindTextures() const {
			for (size_t i = 0; i < m_Textures.size(); i++) {
				auto& texture = m_Textures[i];
				if (texture)
					texture->Bind(i);
			}
		}

		Core::Ref<MaterialInstance> MaterialInstance::Create(const Core::Ref<Material>& material) {
			return Core::CreateRef<MaterialInstance>(material);
		}

		MaterialInstance::MaterialInstance(const Core::Ref<Material>& material) :
			m_Material(material) {
			m_Material->m_MaterialInstances.insert(this);
			AllocateStorage();
		}

		MaterialInstance::~MaterialInstance() {
			m_Material->m_MaterialInstances.erase(this);
		}

		void MaterialInstance::OnShaderReloaded() {
			AllocateStorage();
			m_OverriddenValues.clear();
		}

		void MaterialInstance::AllocateStorage() {

			for (Math::uint32_t i = 0; i < m_Material->m_Shaders.size(); i++) {
				if (m_Material->m_Shaders[i]->GetType() == Shader::ShaderType::VertexShader) {
					m_VSBuffer.Allocate(m_Material->m_Shaders[i]->GetMaterialBuffer().GetSize());
					m_VSBuffer.ZeroInitialize();
					m_VSMaterialBuffer = m_Material->m_VSMaterialBuffer;
				}
				else if (m_Material->m_Shaders[i]->GetType() == Shader::ShaderType::VertexShader) {
					m_PSBuffer.Allocate(m_Material->m_Shaders[i]->GetMaterialBuffer().GetSize());
					m_PSBuffer.ZeroInitialize();
					m_PSMaterialBuffer = m_Material->m_PSMaterialBuffer;
				}
			}
		}

		void MaterialInstance::OnMaterialValueUpdated(Shader::ShaderField* decl) {
			if (m_OverriddenValues.find(decl->GetName()) == m_OverriddenValues.end()) {
				auto& buffer = GetUniformBufferTarget(decl);
				auto& materialBuffer = m_Material->GetUniformBufferTarget(decl);
				buffer.Write(materialBuffer.Data + decl->GetOffset(), decl->GetSize(), decl->GetOffset());
			}
		}

		Core::Buffer& MaterialInstance::GetUniformBufferTarget(Shader::ShaderField* field) {
			//switch (field->GetDomain()) {
			//case Shader::ShaderDomain::Vertex:    return m_VSBuffer;
			//case Shader::ShaderDomain::Pixel:     return m_PSBuffer;
			//}

			SNOW_CORE_ASSERT(false, "Invalid uniform declaration domain! Material does not support this shader type.");
			return m_VSBuffer;
		}

		void MaterialInstance::Bind(const Core::Ref<API::Pipeline>& pipeline) const {
			if (m_VSBuffer) {
				m_Material->m_VSMaterialBuffer->SetData(m_VSBuffer.Data, m_VSBuffer.Size);
				pipeline->UploadBuffer(m_Material->m_VSMaterialBuffer);
			}

			if (m_PSBuffer) {
				m_PSMaterialBuffer->SetData(m_PSBuffer.Data, m_PSBuffer.Size);
				pipeline->UploadBuffer(m_PSMaterialBuffer);
			}

			m_Material->BindTextures();
			for (size_t i = 0; i < m_Textures.size(); i++) {
				auto& texture = m_Textures[i];
				if (texture)
					texture->Bind(i);
			}
		}
	}
}