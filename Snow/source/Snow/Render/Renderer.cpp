#include "spch.h"
#include "SNow/Render/Renderer.h"

#include "Snow/Platform/OpenGL/OpenGLRenderer.h"
#include "Snow/Platform/DirectX/DirectXRenderer.h"
#include "Snow/Platform/Vulkan/VulkanRenderer.h"

#include "Snow/Render/Renderer2D.h"
#include "Snow/Render/Renderer3D.h"


namespace Snow {
	namespace Render {
		Renderer* Renderer::s_Instance = nullptr;
		RenderAPI Renderer::s_RenderAPI = RenderAPI::NONE;
		RenderCommandBuffer* Renderer::s_RenderCommandBuffer = nullptr;
		RenderAPICapabilities Renderer::s_Capabilities = RenderAPICapabilities();
		Shader::ShaderLibrary* Renderer::s_ShaderLibrary = new Shader::ShaderLibrary();
		API::Context* Renderer::s_Context = nullptr;

		void Renderer::Init(const Core::Ref<Core::Window>& window) {

			switch (GetRenderAPI()) {
			case RenderAPI::NONE:		s_Instance = nullptr; break;
			case RenderAPI::OPENGL:		s_Instance = new OpenGLRenderer(); break;
			case RenderAPI::DIRECTX:	s_Instance = new DirectXRenderer(); break;
			case RenderAPI::VULKAN:		s_Instance = new VulkanRenderer(); break;
			}

			API::ContextSpecification ContextSpec;
			ContextSpec.Window = window;

			s_Context = API::Context::Create(ContextSpec);

			s_RenderCommandBuffer = RenderCommandBuffer::Create();

			s_Instance->InitInternal();


			SNOW_CORE_TRACE("Renderer2D Initialization");
			Renderer2D::Init();
			SNOW_CORE_TRACE("");
			Renderer3D::Init();
		}

		void Renderer::Shutdown() {

		}


		void Renderer::SetDepthTesting(bool enabled) {
			s_Instance->SetDepthTestingInternal(enabled);
		}

		void Renderer::SetBlending(bool enabled) {
			s_Instance->SetBlendInternal(enabled);
		}

		void Renderer::SetViewport(int x, int y, unsigned int width, unsigned int height) {
			s_Instance->SetViewportInternal(x, y, width, height);
		}

		void Renderer::WaitAndRender() {
			Submit([]() {
				s_RenderCommandBuffer->Present();
			});
			s_RenderCommandBuffer->Execute();
		}

		//void Renderer::WaitAndRender() {
		//	s_Instance->m_CommandQueue.Execute();
		//}



	}
}