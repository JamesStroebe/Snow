#include "spch.h"
#include "Snow/Render/API/Pipeline.h"

#include "Snow/Render/Renderer.h"

#include "Snow/Platform/OpenGL/OpenGLPipeline.h"
#include "Snow/Platform/DirectX/DirectXPipeline.h"
//#include "Snow/Platform/Vulkan/VulkanPipeline.h"

namespace Snow {
	namespace Render {
		namespace API {
			Core::Ref<Pipeline> Pipeline::Create(const PipelineSpecification& spec) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::NONE:	return nullptr;
				case RenderAPI::OPENGL:	return Core::CreateRef<OpenGLPipeline>(spec);
				case RenderAPI::DIRECTX:return Core::CreateRef<DirectXPipeline>(spec);
				//case RenderAPI::VULKAN:	return Core::CreateRef<VulkanPipeline>(spec);
				}
				SNOW_CORE_ASSERT(false, "Unknown RenderAPI, Pipeline not created.");
			}
		}
	}
}