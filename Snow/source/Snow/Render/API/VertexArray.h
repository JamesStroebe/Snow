#pragma once

#include "Snow/Core/Base.h"

namespace Snow {
	namespace Render {
		namespace API {
			class VertexArray {
			public:
				virtual ~VertexArray() = default;

				virtual void Bind() const = 0;
				virtual void Unbind() const = 0;

				static Core::Ref<VertexArray> Create();
			};
		}
	}
}