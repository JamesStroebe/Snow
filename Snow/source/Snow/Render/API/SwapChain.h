///////////////////////////////////////////
// File: SwapChain.h
//
// Original Author: James Stroebe
//
// Creation Date: 11/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Base.h"

#include "Snow/Render/API/Context.h"

namespace Snow {
	namespace Render {
		namespace API {

			struct SwapChainSpecification {
				Context* Context;
			};

			class SwapChain {
			public:
				virtual ~SwapChain() = default;

				virtual const SwapChainSpecification& GetSpecification() const = 0;

				static SwapChain* Create(const SwapChainSpecification& spec);
			};
		}
	}
}
