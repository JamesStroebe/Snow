#include "spch.h"
#include "Snow/Render/API/Framebuffer.h"

#include "Snow/Render/Renderer.h"

#include "Snow/Platform/OpenGL/OpenGLFramebuffer.h"
#include "Snow/Platform/DirectX/DirectXFramebuffer.h"
//#include "Snow/Platform/Vulkan/VulkanFramebuffer.h"

namespace Snow {
	namespace Render {
		namespace API {
			Core::Ref<Framebuffer> Framebuffer::Create(const FramebufferSpecification& spec) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::NONE:	return nullptr;
				case RenderAPI::OPENGL: return Core::CreateRef<OpenGLFramebuffer>(spec);
				case RenderAPI::DIRECTX:return Core::CreateRef<DirectXFramebuffer>(spec);
				//case RenderAPI::VULKAN:	return Core::CreateRef<VulkanFramebuffer>(spec);
				}
				SNOW_CORE_ASSERT(false, "Unknown RenderAPI, Framebuffer not created.");
			}
		}
	}
}