#include "spch.h"
#include "Texture.h"

#include "Snow/Render/Renderer.h"
#include "Snow/Platform/OpenGL/OpenGLTexture.h"
#include "Snow/Platform/DirectX/DirectXTexture.h"

namespace Snow {
	namespace Render {
		namespace API {
			Core::Ref<Texture2D> Texture2D::Create(Math::uint32_t width, Math::uint32_t height, const TextureSpecification& spec) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::OPENGL:		return Core::CreateRef<OpenGLTexture2D>(width, height, spec);
				case RenderAPI::DIRECTX:	return Core::CreateRef<DirectXTexture2D>(width, height, spec);
				}
				return nullptr;
			}

			Core::Ref<Texture2D> Texture2D::Create(const std::string& path, const TextureSpecification& spec, bool srgb) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::OPENGL:		return Core::CreateRef<OpenGLTexture2D>(path, spec, srgb);
				case RenderAPI::DIRECTX:	return Core::CreateRef<DirectXTexture2D>(path, spec, srgb);
				}
				return nullptr;
			}

			Core::Ref<TextureCube> TextureCube::Create(const std::string& path, const TextureSpecification& spec) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::OPENGL:	return Core::CreateRef<OpenGLTextureCube>(path, spec);
				case RenderAPI::DIRECTX:return Core::CreateRef<DirectXTextureCube>(path, spec);
				}
				return nullptr;
			}

			Math::uint32_t Texture::GetBPP(TextureFormat format) {
				switch (format) {
				case TextureFormat::R8G8B8:	return 3;
				case TextureFormat::R8G8B8A8:	return 4;
				}
				return 0;
			}
		}
	}
}