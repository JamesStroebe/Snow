#include "spch.h"
#include "Snow/Render/API/Device.h"

#include "Snow/Render/Renderer.h"

#include "Snow/Platform/OpenGL/OpenGLDevice.h"
#include "Snow/Platform/DirectX/DirectXDevice.h"
#include "Snow/Platform/Vulkan/VulkanDevice.h"

namespace Snow {
	namespace Render {
		namespace API {
			Device* Device::Create(const DeviceSpecification& spec) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::NONE:	return nullptr;
				case RenderAPI::OPENGL:	return new OpenGLDevice(spec);
				case RenderAPI::DIRECTX:return new DirectXDevice(spec);
				case RenderAPI::VULKAN:	return new VulkanDevice(spec);
				}
				SNOW_CORE_ASSERT(false, "Unknown RenderAPI, Device not created.");
			}
		}
	}
}