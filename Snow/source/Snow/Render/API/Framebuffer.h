///////////////////////////////////////////
// File: Framebuffer.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Base.h"

#include "Snow/Math/Vector.h"


namespace Snow {
	namespace Render {
		namespace API {

			enum FramebufferType {
				RENDER_BUFFER_NONE = 0,
				RENDER_BUFFER_COLOR = BIT(0),
				RENDER_BUFFER_DEPTH = BIT(1),
				RENDER_BUFFER_STENCIL = BIT(2)
			};

			enum class FramebufferFormat {
				RGBA16F, RGBA8
			};

			struct FramebufferSpecification {
                FramebufferType AttachmentTypes;
				FramebufferFormat Format;
                Math::Vector2f Size;
                Math::Vector4f Color;
				Math::uint32_t Samples;
            };

			class Framebuffer {
			public:
				virtual ~Framebuffer() = default;

				virtual void Bind() const = 0; // binds it to the current render target, the screen
                virtual void Unbind() const = 0;
                virtual void BindToRead() const = 0; // binds the framebuffer to be read from
                virtual void BindColorAttachment(uint32_t slot = 0) const = 0;

				virtual void SetColor(Math::Vector4f color) = 0;

                virtual const FramebufferSpecification& GetSpecification() const = 0;

				static Core::Ref<Framebuffer> Create(const FramebufferSpecification& spec);
			};
		}
	}
}
