///////////////////////////////////////////
// File: RenderPass.h
//
// Original Author: James Stroebe
//
// Creation Date: 11/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Base.h"
#include "Snow/Render/API/Framebuffer.h"

namespace Snow {
	namespace Render {
		namespace API {
			

			struct RenderPassSpecification {
                Core::Ref<Framebuffer> TargetFramebuffer;
            };

			class RenderPass {
			public:
				virtual ~RenderPass() = default;

				virtual const RenderPassSpecification& GetSpecification() const = 0;

				static Core::Ref<RenderPass> Create(const RenderPassSpecification& spec);
			};
		}
	}
}
