#include "spch.h"
#include "Snow/Render/API/VertexArray.h"

#include "Snow/Render/Renderer.h"

#include "Snow/Platform/OpenGL/OpenGLVertexArray.h"

namespace Snow {
	namespace Render {
		namespace API {
			Core::Ref<VertexArray> VertexArray::Create() {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::OPENGL:	return Core::CreateRef<OpenGLVertexArray>();
				}
				return nullptr;
			}
		}
	}
}