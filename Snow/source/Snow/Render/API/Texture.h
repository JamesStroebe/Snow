///////////////////////////////////////////
// File: Texture.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Base.h"
#include "Snow/Core/Buffer.h"

#include "Snow/Render/RenderAPI.h"

#include "Snow/Math/Math.h"

#include <string>

namespace Snow {
	namespace Render {
		namespace API {
			enum class TextureFormat {
				None = 0,
				R8 = 1,
				R8G8 = 2,
				R8G8B8 = 3,
				R8G8B8A8 = 4,
				
			};

			enum class TextureFilter {
				None = 0,
				Linear = 1,
				Nearest = 2
			};

			enum class TextureWrap {
				None = 0,
				Clamp = 1,
				Repeat = 2,
				MirroredRepeat = 3,
				ClampEdge = 4,
				ClampBorder = 5
			};

			struct TextureSpecification {
				TextureSpecification() :
					Format(TextureFormat::R8G8B8A8), Filter(TextureFilter::Nearest), Wrap(TextureWrap::ClampEdge) {}

				TextureFormat Format;
				TextureFilter Filter;
				TextureWrap Wrap;
			};

			class Texture {
			public:
				virtual ~Texture() = default;

				virtual Math::uint32_t GetWidth() const = 0;
				virtual Math::uint32_t GetHeight() const = 0;

				virtual void Bind(Math::uint32_t slot = 0) = 0;
				virtual Math::uint32_t GetSlot() const = 0;

				static Math::uint32_t GetBPP(TextureFormat format);

				virtual void SetData(void* data, Math::uint32_t size) = 0;

				virtual const TextureSpecification& GetSpecification() const = 0;
			};

			class Texture2D : public Texture {
			public:
				static Core::Ref<Texture2D> Create(Math::uint32_t width, Math::uint32_t height, const TextureSpecification& spec = TextureSpecification());
				static Core::Ref<Texture2D> Create(const std::string& path, const TextureSpecification& spec = TextureSpecification(), bool srgb = false);

				virtual void Lock() = 0;
				virtual void Unlock() = 0;

				virtual void Resize(Math::uint32_t width, Math::uint32_t height) = 0;
				virtual Core::Buffer GetWriteableBuffer() = 0;

				virtual const std::string& GetPath() const = 0;
			};

			class TextureCube : public Texture {
			public:
				static Core::Ref<TextureCube> Create(const std::string& path, const TextureSpecification& spec = TextureSpecification());

				virtual const std::string& GetPath() const = 0;
			};
		}
	}
}
