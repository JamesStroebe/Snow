#include "spch.h"
#include "Snow/Render/API/Context.h"

#include "Snow/Render/Renderer.h"

#include "Snow/Platform/OpenGL/OpenGLContext.h"
#include "Snow/Platform/DirectX/DirectXContext.h"
#include "Snow/Platform/Vulkan/VulkanContext.h"

namespace Snow {
	namespace Render {
		namespace API {
			Context* Context::Create(const ContextSpecification& spec) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::NONE:	return nullptr;
				case RenderAPI::OPENGL:	return new OpenGLContext(spec);
				case RenderAPI::DIRECTX:return new DirectXContext(spec);
				case RenderAPI::VULKAN:	return new VulkanContext(spec);
				}
				SNOW_CORE_ASSERT(false, "Context must not be null");
			}
		}
	}
}