#include "spch.h"
#include "Snow/Render/API/RenderPass.h"

#include "Snow/Render/Renderer.h"

#include "Snow/Platform/OpenGL/OpenGLRenderPass.h"
#include "Snow/Platform/DirectX/DirectXRenderPass.h"
#include "Snow/Platform/Vulkan/VulkanRenderPass.h"

namespace Snow {
	namespace Render {
		namespace API {
			Core::Ref<RenderPass> RenderPass::Create(const RenderPassSpecification& spec) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::NONE:	return nullptr;
				case RenderAPI::OPENGL: return Core::CreateRef<OpenGLRenderPass>(spec);
				case RenderAPI::DIRECTX:return Core::CreateRef<DirectXRenderPass>(spec);
				case RenderAPI::VULKAN:	return Core::CreateRef<VulkanRenderPass>(spec);
				}
			}
		}
	}
}