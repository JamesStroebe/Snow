///////////////////////////////////////////
// File: Context.h
//
// Original Author: James Stroebe
//
// Creation Date: 11/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Window.h"
#include "Snow/Core/Base.h"

namespace Snow {
	namespace Render {
		namespace API {
			struct ContextSpecification {
				Core::Ref<Core::Window> Window;
			};

			class Context {
			public:
				~Context() = default;

				virtual const ContextSpecification& GetSpecification() const = 0;

				static Context* Create(const ContextSpecification& spec);
			};
		}
	}
}