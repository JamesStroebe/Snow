#include "spch.h"
#include "Snow/Render/API/Buffer.h"

#include "Snow/Render/Renderer.h"

#include "Snow/Platform/OpenGL/OpenGLBuffer.h"
#include "Snow/Platform/DirectX/DirectXBuffer.h"

namespace Snow {
	namespace Render {
		namespace API {
			Core::Ref<VertexBuffer> VertexBuffer::Create(BufferSpecification spec) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::OPENGL:	return Core::CreateRef<OpenGLVertexBuffer>(spec);
				case RenderAPI::DIRECTX: return Core::CreateRef<DirectXVertexBuffer>(spec);
				}
				return nullptr;
			}

			Core::Ref<VertexBuffer> VertexBuffer::Create(uint32_t size, BufferSpecification spec) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::OPENGL:	return Core::CreateRef<OpenGLVertexBuffer>(size, spec);
				case RenderAPI::DIRECTX: return Core::CreateRef<DirectXVertexBuffer>(size, spec);
				}
				return nullptr;
			}

			Core::Ref<VertexBuffer> VertexBuffer::Create(void* data, uint32_t size, BufferSpecification spec) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::OPENGL:	return Core::CreateRef<OpenGLVertexBuffer>(data, size, spec);
				case RenderAPI::DIRECTX: return Core::CreateRef<DirectXVertexBuffer>(data, size, spec);
				}
				return nullptr;
			}

			Core::Ref<IndexBuffer> IndexBuffer::Create(BufferSpecification spec) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::OPENGL:	return Core::CreateRef<OpenGLIndexBuffer>(spec);
				case RenderAPI::DIRECTX: return Core::CreateRef<DirectXIndexBuffer>(spec);
				}
				return nullptr;
			}

			Core::Ref<IndexBuffer> IndexBuffer::Create(uint32_t size, BufferSpecification spec) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::OPENGL:	return Core::CreateRef<OpenGLIndexBuffer>(size, spec);
				case RenderAPI::DIRECTX: return Core::CreateRef<DirectXIndexBuffer>(size, spec);
				}
				return nullptr;
			}

			Core::Ref<IndexBuffer> IndexBuffer::Create(void* data, uint32_t size, BufferSpecification spec) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::OPENGL:	return Core::CreateRef<OpenGLIndexBuffer>(data, size, spec);
				case RenderAPI::DIRECTX: return Core::CreateRef<DirectXIndexBuffer>(data, size, spec);
				}
				return nullptr;
			}

			Core::Ref<UniformBuffer> UniformBuffer::Create(const std::string& name, Shader::ShaderType type) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::OPENGL:	return Core::CreateRef<OpenGLUniformBuffer>(name, type);
				//case RenderAPI::DIRECTX: return Core::CreateRef<DirectXConstantBuffer>(name);
				}
				return nullptr;
			}
		}
	}
}