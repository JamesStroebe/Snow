///////////////////////////////////////////
// File: Device.h
//
// Original Author: James Stroebe
//
// Creation Date: 11/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Base.h"

#include "Snow/Render/API/Context.h"
namespace Snow {
	namespace Render {
		namespace API {
			struct DeviceSpecification {
				Context* Context;
			};

			class Device {
			public:
				~Device() = default;

				virtual const DeviceSpecification& GetSpecification() const = 0;

				static Device* Create(const DeviceSpecification& spec);
			};
		}
	}
}
