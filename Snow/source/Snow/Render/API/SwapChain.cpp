#include "spch.h"
#include "Snow/Render/API/SwapChain.h"

#include "Snow/Render/Renderer.h"

#include "Snow/Platform/OpenGL/OpenGLSwapChain.h"
#include "Snow/Platform/DirectX/DirectXSwapchain.h"
#include "Snow/Platform/Vulkan/VulkanSwapChain.h"

namespace Snow {
	namespace Render {
		namespace API {

			SwapChain* SwapChain::Create(const SwapChainSpecification& spec) {
				switch (Renderer::GetRenderAPI()) {
				case RenderAPI::NONE:	return nullptr;
				case RenderAPI::OPENGL:	return new OpenGLSwapChain(spec);
				case RenderAPI::DIRECTX:return new DirectXSwapChain(spec);
				case RenderAPI::VULKAN:	return new VulkanSwapChain(spec);

				}
				SNOW_CORE_ASSERT(false, "Context must not be null");
			}
		}
	}
}