///////////////////////////////////////////
// File: Buffer.h
//
// Original Author: James Stroebe
//
// Creation Date: 23/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Base.h"

#include "Snow/Render/Shaders/Shader.h"
#include "Snow/Render/Shaders/ShaderUniform.h"

#include <string>
#include <vector>

namespace Snow {
	namespace Render {
		namespace API {

			enum class BufferMap {
				Write = BIT(0),
				Read = BIT(1),
				Both = Write | Read
			};

			enum class BufferStorage {
				Static,
				Dynamic,
				Staging
			};

			enum class BufferUsage {
				View,
				Draw
			};

			struct BufferSpecification {
				BufferSpecification(BufferStorage storage = BufferStorage::Dynamic, BufferUsage usage = BufferUsage::Draw, BufferMap map = BufferMap::Write) :
					Storage(storage), Usage(usage), Map(map) {}

				BufferStorage GetStorage() { return Storage; }
				BufferUsage GetUsage() { return Usage; }
				BufferMap GetMap() { return Map; }

			private:
				BufferStorage Storage;
				BufferUsage Usage;
				BufferMap Map;
			};

			class VertexBuffer {
			public:
				virtual ~VertexBuffer() = default;

				virtual void Bind() const = 0;
				virtual void Unbind() const = 0;

				virtual void SetData(void* data, uint32_t size) = 0;
				virtual void SetSubData(void* data, uint32_t size, uint32_t offset = 0) = 0;

				virtual void* Map() = 0;
				virtual void* MapRange(uint32_t size, uint32_t offset = 0) = 0;
				virtual void Unmap() = 0;

				virtual void Resize(uint32_t size) = 0;

				static Core::Ref<VertexBuffer> Create(BufferSpecification spec = BufferSpecification());
				static Core::Ref<VertexBuffer> Create(uint32_t size, BufferSpecification spec = BufferSpecification());
				static Core::Ref<VertexBuffer> Create(void* data, uint32_t size, BufferSpecification spec = BufferSpecification());
			};

			class IndexBuffer {
			public:
				virtual ~IndexBuffer() = default;

				virtual void Bind() const = 0;
				virtual void Unbind() const = 0;

				virtual void SetData(void* data, uint32_t size) = 0;
				virtual void SetSubData(void* data, uint32_t size, uint32_t offset = 0) = 0;

				virtual void* Map() = 0;
				virtual void* MapRange(uint32_t size, uint32_t offset = 0) = 0;
				virtual void Unmap() = 0;

				virtual void Resize(uint32_t size) = 0;

				virtual uint32_t GetCount() = 0;

				static Core::Ref<IndexBuffer> Create(BufferSpecification spec = BufferSpecification());
				static Core::Ref<IndexBuffer> Create(uint32_t size, BufferSpecification spec = BufferSpecification());
				static Core::Ref<IndexBuffer> Create(void* data, uint32_t size, BufferSpecification spec = BufferSpecification());
			};

			class UniformBuffer {
			public:
				virtual ~UniformBuffer() = default;

				virtual void* Map() = 0;
				virtual void* MapRange() = 0;
				virtual void Unmap() = 0;

				virtual void Bind(uint32_t slot = 0) const = 0;
				virtual void Unbind() const = 0;

				virtual const std::string& GetName() = 0;

				virtual void SetData(void* data, uint32_t size) = 0;

				virtual void SetDomain(Shader::ShaderType domain) = 0;
                virtual Shader::ShaderType GetDomain() const = 0;

                static Core::Ref<UniformBuffer> Create(const std::string& name, Shader::ShaderType domain);
            };
		}
	}
}