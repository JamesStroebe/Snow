///////////////////////////////////////////
// File: Pipeline.h
//
// Original Author: James Stroebe
//
// Creation Date: 11/01/2020
//
/////////////////////////////////////////// 

#pragma once

#include "Snow/Core/Base.h"
#include "Snow/Render/API/Buffer.h"
#include "Snow/Render/Shaders/Shader.h"
#include "Snow/Render/Shaders/ShaderUniform.h"

#include "Snow/Math/Common/Pointer.h"

namespace Snow {
	namespace Render {
		namespace API {

			enum class VertexAttribType {
				None = 0, 
				Float, Float2, Float3, Float4, 
				Mat3, Mat4, 
				Int, Int2, Int3, Int4, 
				Bool
			};

			struct AttribElement {
				std::string Name;
				VertexAttribType Type;
				uint32_t Size;
				uint32_t Offset;
				uint32_t Index;
				bool Normalized;

				static Math::uint32_t ShaderDataTypeSize(VertexAttribType type) {
					switch (type) {
					case VertexAttribType::Float:		return sizeof(Math::f32);
					case VertexAttribType::Float2:		return sizeof(Math::f32) * 2;
					case VertexAttribType::Float3:		return sizeof(Math::f32) * 3;
					case VertexAttribType::Float4:		return sizeof(Math::f32) * 4;
					case VertexAttribType::Mat3:		return sizeof(Math::f32) * 3 * 3;
					case VertexAttribType::Mat4:		return sizeof(Math::f32) * 4 * 4;
					case VertexAttribType::Int:			return sizeof(Math::i32);
					case VertexAttribType::Int2:		return sizeof(Math::i32) * 2;
					case VertexAttribType::Int3:		return sizeof(Math::i32) * 3;
					case VertexAttribType::Int4:		return sizeof(Math::i32) * 4;
					case VertexAttribType::Bool:		return sizeof(bool);
					}
					SNOW_ASSERT(false, "Unknown ShaderDataType");
					return 0;
				}

				AttribElement() {}

				AttribElement(VertexAttribType type, const std::string& name, Math::uint32_t index = 0, bool normalized = false) :
					Name(name), Type(type), Size(ShaderDataTypeSize(type)), Offset(0), Normalized(normalized), Index(index) {}

				uint32_t GetComponentCount() const {
					switch (Type) {
					case VertexAttribType::Float:   return 1;
					case VertexAttribType::Float2:  return 2;
					case VertexAttribType::Float3:  return 3;
					case VertexAttribType::Float4:  return 4;
					case VertexAttribType::Mat3:    return 3 * 3;
					case VertexAttribType::Mat4:    return 4 * 4;
					case VertexAttribType::Int:     return 1;
					case VertexAttribType::Int2:    return 2;
					case VertexAttribType::Int3:    return 3;
					case VertexAttribType::Int4:    return 4;
					case VertexAttribType::Bool:    return 1;
					}

					SNOW_CORE_ASSERT(false, "Unknown ShaderDataType!");
					return 0;
				}
			};


			class VertexBufferLayout {
			public:
				VertexBufferLayout() {}

				VertexBufferLayout(const std::initializer_list<AttribElement>& elements) :
					m_Elements(elements) {
					CalculateStrideOffset();
				}

				inline Math::uint32_t GetStride() const { return m_Stride; }
				inline const std::vector<AttribElement>& GetElements() const { return m_Elements; }

				std::vector<AttribElement>::iterator begin() { return m_Elements.begin(); }
				std::vector<AttribElement>::iterator end() { return m_Elements.end(); }
				std::vector<AttribElement>::const_iterator begin() const { return m_Elements.begin(); }
				std::vector<AttribElement>::const_iterator end() const { return m_Elements.end(); }

			private:
				void CalculateStrideOffset() {
					Math::uint32_t offset = 0;
					m_Stride = 0;
					for (auto& element : m_Elements) {
						element.Offset = offset;
						offset += element.Size;
						m_Stride += element.Size;
					}
				}

				std::vector<AttribElement> m_Elements;
				Math::uint32_t m_Stride;
			};


			//-----Input for api (that is not resources or from a vbo)-----
			//DirectX uses constantbuffers, packed into a single buffer of data, you do not push fields on their own, but as a buffer.
			//OpenGL has the use of Uniforms, being able to push uniforms at different times
			//Vulkan uses uniform buffers in the same way as directx, no allowing of pushing singular values, but there are push constants, allowing small buffers of data to be pushed rather then a whole buffer at a time, for more control 

			//-----BUFFER ALIGNMENT-----
			//IMPORTANT, hlsl does not like constant buffer data to cross 16byte alignment, so a float3 followed by float2 is not really good, so for stuff like that, there needs to be padding
			// vulkan wants alignment in special ways, scalar is N, vec2 is 2N, vec3|vec4 is 4N, nested is aligned to closted multiple of 16, mat4 is same as vec4, mat3 is same as vec3(maybe)

			//-----general info about buffers-----
			//buffers are bound with a location/binding point, dx11 Each shader stage allows up to 15 constant buffers, and each buffer can hold up to 4,096 constant variables. , opengl is GL_MAX_UNIFORM_BUFFERS_BINDINGS (there is also maximum uniform size in opengl GL_MAX_UNIFORM_SIZE)

			enum class FieldType {
				None = -1,
				Float, Float2, Float3, Float4,
				Mat3, Mat4,
				Int, UInt, Struct
			};

			struct FieldDecl {
				FieldType Type;
				std::ptrdiff_t Offset;
				std::string Name;
			};

			struct ShaderBufferDecl {
				Core::Buffer buffer;
				std::vector<FieldDecl> Fields;

				virtual const Core::Buffer GetBuffer() const = 0;
				virtual const FieldDecl* GetFields() const = 0;
				virtual uint32_t GetFieldCount() const = 0;
			};

			template<uint32_t Size, uint32_t Count>
			struct ShaderBufferDeclaration : public ShaderBufferDecl {
				ShaderBufferDeclaration() {
					buffer = new Core::Buffer(Size);
					FieldBase = new FieldDecl[Count];
					Fields = FieldBase;
				}

				~ShaderBufferDeclaration() {

				}


				Core::Buffer* buffer;
				FieldDecl* FieldBase;
				FieldDecl* Fields;

				std::ptrdiff_t Cursor = 0;
				int Index = 0;

				//virtual Core::Buffer* GetData() const { return FieldBase; }

				virtual const Core::Buffer GetBuffer() const override { return *buffer; }
				virtual const FieldDecl* GetFields() const override { return Fields; }
				virtual uint32_t GetFieldCount() const override { return Count; }

				

				template<typename T>
				void Push(const std::string& name, const T& data) {}

				template<>
				void Push(const std::string& name, const float& data) {
					*Fields = { FieldType::Float, Cursor, name };
					Fields++;
					buffer->Write((Core::byte*) & data, sizeof(float), Cursor);
					Cursor += sizeof(float);
				}

				void Push(const std::string& name, const Math::Vector2f& data) {
					*Fields = { FieldType::Vec2, Cursor, name };
					Fields++;
					buffer->Write(Math::valuePtr(data), sizeof(Math::Vector2f), Cursor);
					Cursor += sizeof(Math::Vector2f);
				}

				void Push(const std::string& name, const Math::Vector3f& data) {
					*Fields = { FieldType::Vec3, Cursor, name };
					Fields++;
					buffer->Write((Core::byte*)Math::valuePtr(data), sizeof(Math::Vector3f), Cursor);
					Cursor += sizeof(Math::Vector3f);
				}

				void Push(const std::string& name, const Math::Vector4f& data) {
					*Fields = { FieldType::Vec4, Cursor, name };
					Fields++;
					buffer->Write(Math::valuePtr(data), sizeof(Math::Vector4f), Cursor);
					Cursor += sizeof(Math::Vector4f);
				}

				void Push(const std::string& name, const Math::Matrix3x3f& data) {
					*Fields = { FieldType::Mat3, Cursor, name };
					Fields++;
					buffer->Write(Math::valuePtr(data), sizeof(Math::Matrix3x3f), Cursor);
					Cursor += sizeof(Math::Matrix3x3f);
				}

				void Push(const std::string& name, const Math::Matrix4x4f& data) {
					*Fields = { FieldType::Mat4, Cursor, name };
					Fields++;
					buffer->Write(const_cast<float*>(Math::valuePtr(data)), sizeof(Math::Matrix4x4f), Cursor);
					Cursor += sizeof(Math::Matrix4x4f);
				}

				void Push(const std::string& name, const int& data) {
					*Field = { FieldType::Int, Cursor, name };
					Fields++;
					buffer->Write(&data, sizeof(int), Cursor);
					Cursor += sizeof(int);
				}

				void Push(const std::string& name, const uint32_t& data) {
					*Fields = { FieldType::UInt, Cursor, name };
					Fields++;
					buffer->Write(&data, sizeof(uint32_t), Cursor);
					Cursor += sizeof(uint32_t);
				}
			};

            enum class PrimitiveType {
				None = -1,
                Point, Triangle, TriangleStrip
            };

            struct PipelineSpecification {
				std::vector<Core::Ref<Shader::Shader>> Shaders;
				VertexBufferLayout VertexBufferLayout;
			};

			class Pipeline {
			public:
				virtual ~Pipeline() = default;

				virtual const PipelineSpecification& GetSpecification() const = 0;

				virtual void Bind() const = 0;

                virtual PrimitiveType GetPrimitiveType() const = 0;
                virtual void SetPrimitiveType(PrimitiveType type) = 0;

                virtual void UploadBuffer(const Core::Ref<UniformBuffer>& buffer) = 0;

				static Core::Ref<Pipeline> Create(const PipelineSpecification& spec);
			};
		}
	}
}
