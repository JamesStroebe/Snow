///////////////////////////////////////////
// File: RenderAPI.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once


#include "Snow/Core/Base.h"

namespace Snow {
	namespace Render {
		enum class RenderAPI {
			NONE, OPENGL, DIRECTX, VULKAN, METAL
		};

		struct RenderAPICapabilities {
			std::string Vendor;
			std::string Renderer;
			std::string Version;
			int ExtensionCount;
			std::vector<std::string> Extensions;
			std::string ShadingLanguageVersion;
			int VersionMajor;
			int VersionMinor;
			int VersionPatch;


			int MaxSamplerCount;
			int MinFramebuffers;
			int MaxFramebuffers;
			int MaxFramebufferWidth;
			int MaxFramebufferHeight;


			int MaxViewports;
			int MaxViewportWidth;
			int MaxViewportHeight;

			int MaxVertexAttribCount;
			int MaxVertexAttribOffset;

			int MaxUniformBlockSize;
			int MaxUniformBlockCount;
			int MaxVertexStageUniformBlockCount;
			int MaxComputeStageUniformBlockCount;
			int MaxGeometryStageUniformBlockCount;
			int MaxPixelStageUniformBlockCount;

			int MaxUniformbufferBindingPoints;
			int UniformbufferOffsetAlignment;
			

			bool DoubleBufferSupport;
			bool TripleBufferSupport;

			int MaxVertexStageGlobalUniformCount;
			int MaxComputeStageGlobalUniformCount;
			int MaxGeometryStageGlobalUniformCount;
			int MaxPixelStageGlobalUniformCount;
			int MaxGlobalUniformLocations;


			float MaxAnisotropy;
		};
	}
}