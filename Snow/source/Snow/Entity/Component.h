#pragma once

#include "Snow/Math/Math.h"

namespace Snow {
	namespace Entity {
		struct BaseComponent;
		typedef void* EntityHandle;
		typedef Math::uint32_t (*ComponentCreateFunction)(std::vector<Math::uint8_t>& memory, EntityHandle entity, BaseComponent* comp);
		typedef void (*ComponentFreeFunction)(BaseComponent* comp);

		struct BaseComponent {
			static Math::uint32_t RegisterComponentType(ComponentCreateFunction createFn, ComponentFreeFunction freeFn, size_t size);
			EntityHandle Entity = nullptr;

			inline static ComponentCreateFunction GetTypeCreateFunction(Math::uint32_t ID) { return std::get<0>((*m_ComponentTypes)[ID]); }
			inline static ComponentFreeFunction GetTypeFreeFunction(Math::uint32_t ID) { return std::get<1>((*m_ComponentTypes)[ID]); }
			inline static size_t GetTypeSize(Math::uint32_t ID) { return std::get<2>((*m_ComponentTypes)[ID]); }

			inline static bool IsTypeValid(Math::uint32_t ID) { return ID < m_ComponentTypes->size(); }
		private:
			static std::vector<std::tuple<ComponentCreateFunction, ComponentFreeFunction, size_t>>* m_ComponentTypes;
		};

		template<typename T>
		struct Component : public BaseComponent {
		protected:
			static const ComponentCreateFunction Create_Function;
			static const ComponentFreeFunction Free_Function;
			
		public:
			static const Math::uint32_t ID;
			static const size_t Size;
		};

		template<typename C>
		Math::uint32_t ComponentCreate(std::vector<Math::uint8_t>& memory, EntityHandle entity, BaseComponent* comp) {
			Math::uint32_t index = memory.size();
			memory.resize(index + C::Size);
			C* component = new(&memory[index])C(*(C*)comp);
			component->Entity = entity;
			return index;
		}

		template<typename C>
		void ComponentFree(BaseComponent* comp) {
			C* component = (C*)comp;
			component->~C();
		}

		template<typename T>
		const Math::uint32_t Component<T>::ID(BaseComponent::RegisterComponentType(ComponentCreate<T>, ComponentFree<T>, sizeof(T)));

		template<typename T>
		const size_t Component<T>::Size(sizeof(T));

		template<typename T>
		const ComponentCreateFunction Component<T>::Create_Function(ComponentCreate<T>);

		template<typename T>
		const ComponentFreeFunction Component<T>::Free_Function(ComponentFree<T>);
	}
}