#include "spch.h"
#include "Snow/Entity/Component.h"

namespace Snow {
	namespace Entity {

		std::vector<std::tuple<ComponentCreateFunction, ComponentFreeFunction, size_t>>* BaseComponent::m_ComponentTypes;

		uint32_t BaseComponent::RegisterComponentType(ComponentCreateFunction createFn, ComponentFreeFunction freeFn, size_t size) {
			if (m_ComponentTypes == nullptr)
				m_ComponentTypes = new std::vector<std::tuple<ComponentCreateFunction, ComponentFreeFunction, size_t>>();
			uint32_t componentID = m_ComponentTypes->size();
			m_ComponentTypes->push_back(std::tuple<ComponentCreateFunction, ComponentFreeFunction, size_t>(createFn, freeFn, size));
			return componentID;
		}
	}
}