#include "spch.h"
#include "Snow/Entity/EntityComponentSystem.h"

namespace Snow {
	namespace Entity {

		std::map<ComponentID, std::vector<uint8_t>> EntityComponentSystem::s_Components = {};
		std::vector<EntityPtr> EntityComponentSystem::s_Entities = {};

		void EntityComponentSystem::Init() {}

		void EntityComponentSystem::Shutdown() {
			for (std::map<ComponentID, std::vector<uint8_t>>::iterator it = s_Components.begin(); it != s_Components.end(); it++) {
				size_t typeSize = BaseComponent::GetTypeSize(it->first);
				ComponentFreeFunction freeFn = BaseComponent::GetTypeFreeFunction(it->first);
				for (Math::uint32_t i = 0; i < it->second.size(); i += typeSize) {
					freeFn((BaseComponent*)&it->second[i]);
				}
			}

			for (Math::uint32_t i = 0; i < s_Entities.size(); i++) {
				delete s_Entities[i];
			}
		}

		EntityHandle EntityComponentSystem::CreateEntity(BaseComponent** components, const Math::uint32_t* componentIDs, size_t numComponents) {
			auto* entity = new std::pair<EntityIndex, std::vector<std::pair<ComponentID, ComponentIndex>>>();
			EntityHandle handle = (EntityHandle)entity;
			for (Math::uint32_t i = 0; i < numComponents; i++) {
				if (!BaseComponent::IsTypeValid(componentIDs[i])) {
					SNOW_CORE_WARN("Component id {0} not a valid component type", componentIDs[i]);
					delete entity;
					return nullptr;
				}
				AddComponentInternal(handle, entity->second, componentIDs[i], components[i]);
			}

			entity->first = s_Entities.size();
			s_Entities.push_back(entity);
			return handle;
		}

		void EntityComponentSystem::RemoveEntity(EntityHandle handle) {
			EntityComponentList& compList = GetEntityComponentList(handle);
			// Deletes the Component attachments to the entity
			for (Math::uint32_t i = 0; i < compList.size(); i++) {
				DeleteComponent(compList[i].first, compList[i].second);
			}

			// deletes the entity from the ECS
			Math::uint32_t destIndex = GetEntityIndex(handle);
			Math::uint32_t sourceIndex = s_Entities.size() - 1;
			delete s_Entities[destIndex];
			s_Entities[destIndex] = s_Entities[sourceIndex];
			s_Entities[destIndex]->first = destIndex;
			s_Entities.pop_back();
		}

		

		void EntityComponentSystem::UpdateSystems(SystemList& systemList, Core::Time::Timestep ts) {
			std::vector<BaseComponent*> componentParam;
			std::vector<std::vector<Math::uint8_t>*> componentArrays;
			for (Math::uint32_t i = 0; i < systemList.GetSize(); i++) {
				const std::vector<Math::uint32_t>& componentTypes = systemList[i]->GetComponentTypes();
				if (componentTypes.size() == 1) {
					size_t typeSize = BaseComponent::GetTypeSize(componentTypes[0]);
					std::vector<uint8_t> array = s_Components[componentTypes[0]];
					for (Math::uint32_t j = 0; j < array.size(); j += typeSize) {
						BaseComponent* comp = (BaseComponent*)&array[j];
						systemList[i]->UpdateComponents(ts, &comp);
					}
				}
				else {
					UpdateSystemMultipleComponents(i, systemList, ts, componentTypes, componentParam, componentArrays);
				}
			}
		}

		void EntityComponentSystem::AddComponentInternal(EntityHandle handle, EntityComponentList& entityComponentList, ComponentID componentID, BaseComponent* component) {
			ComponentCreateFunction createFn = BaseComponent::GetTypeCreateFunction(componentID);
			std::pair<ComponentID, ComponentIndex> newComponent;
			newComponent.first = componentID;
			newComponent.second = createFn(s_Components[componentID], handle, component);
			entityComponentList.push_back(newComponent);
		}

		void EntityComponentSystem::DeleteComponent(ComponentID componentID, ComponentIndex componentIndex) {
			std::vector<Math::uint8_t>& array = s_Components[componentID];
			ComponentFreeFunction freeFn = BaseComponent::GetTypeFreeFunction(componentID);
			size_t typeSize = BaseComponent::GetTypeSize(componentID);
			Math::uint32_t sourceIndex = array.size() - typeSize;

			BaseComponent* sourceComponent = (BaseComponent*)&array[sourceIndex];
			BaseComponent* destComponent = (BaseComponent*)&array[componentIndex];
			freeFn(destComponent);

			if (componentIndex == sourceIndex) {
				array.resize(sourceIndex);
				return;
			}

			memcpy(destComponent, sourceComponent, typeSize);

			EntityComponentList& sourceComponents = GetEntityComponentList(sourceComponent->Entity);
			for (uint32_t i = 0; i < sourceComponents.size(); i++) {
				if (componentID == sourceComponents[i].first && sourceIndex == sourceComponents[i].second) {
					sourceComponents[i].second = componentIndex;
					break;
				}
			}
			array.resize(sourceIndex);
		}

		bool EntityComponentSystem::RemoveComponentInternal(EntityHandle handle, ComponentID componentID) {
			EntityComponentList& entityComponentList = GetEntityComponentList(handle);
			for (Math::uint32_t i = 0; i < entityComponentList.size(); i++) {
				if (componentID == entityComponentList[i].first) {
					DeleteComponent(entityComponentList[i].first, entityComponentList[i].second);
					Math::uint32_t sourceIndex = entityComponentList.size() - 1;
					Math::uint32_t destIndex = i;
					entityComponentList[destIndex] = entityComponentList[sourceIndex];
					entityComponentList.pop_back();
					return true;
				}
			}
			return false;
		}

		BaseComponent* EntityComponentSystem::GetComponentInternal(EntityComponentList& entityComponents, std::vector<Math::uint8_t>& array, ComponentID componentID) {
			for (Math::uint32_t i = 0; i < entityComponents.size(); i++) {
				if (componentID == entityComponents[i].first) {
					return (BaseComponent*)&array[entityComponents[i].second];
				}
			}
			return nullptr;
		}

		void EntityComponentSystem::UpdateSystemMultipleComponents(ComponentIndex componentIndex, SystemList& systemList, Core::Time::Timestep ts, const std::vector<Math::uint32_t>& componentTypes, std::vector<BaseComponent*>& componentParam, std::vector<std::vector<Math::uint8_t>*>& componentArrays) {
			auto& componentFlags = systemList[componentIndex]->GetComponentFlags();
			
			componentParam.resize(Math::max(componentParam.size(), componentTypes.size()));
			componentArrays.resize(Math::max(componentArrays.size(), componentTypes.size()));
			for (Math::uint32_t i = 0; i < componentTypes.size(); i++) {
				componentArrays[i] = &s_Components[componentTypes[i]];
			}

			Math::uint32_t minSizeIndex = FindLeastCommonComponent(componentTypes, componentFlags);

			size_t typeSize = BaseComponent::GetTypeSize(componentTypes[minSizeIndex]);
			std::vector<Math::uint8_t>& array = *componentArrays[minSizeIndex];
			for (Math::uint32_t i = 0; i < array.size(); i += typeSize) {
				componentParam[minSizeIndex] = (BaseComponent*)&array[i];
				EntityComponentList& entityComponents = GetEntityComponentList(componentParam[minSizeIndex]->Entity);

				bool isValid = true;
				for (Math::uint32_t j = 0; j < componentTypes.size(); j++) {
					if (j == minSizeIndex)
						continue;
					componentParam[j] = GetComponentInternal(entityComponents, *componentArrays[j], componentTypes[j]);
					if (componentParam[j] == nullptr && (componentFlags[j] & ComponentFlags::Optional) == 0) {
						isValid = false;
						break;
					}
				}

				if (isValid) {
					systemList[componentIndex]->UpdateComponents(ts, &componentParam[0]);
				}
			}
		}

		Math::uint32_t EntityComponentSystem::FindLeastCommonComponent(const std::vector<Math::uint32_t>& componentTypes, const std::vector<Math::uint32_t>& componentFlags) {
			Math::uint32_t minSize = (uint32_t)-1;
			Math::uint32_t minIndex = (uint32_t)-1;
			for (Math::uint32_t i = 0; i < componentTypes.size(); i++) {
				if ((componentFlags[i] & ComponentFlags::Optional) != 0) {
					continue;
				}
				size_t typeSize = BaseComponent::GetTypeSize(componentTypes[i]);
				Math::uint32_t size = s_Components[componentTypes[i]].size() / typeSize;
				if (size <= minSize) {
					minSize = size;
					minIndex = i;
				}
			}
			return minIndex;
		}
	}
}