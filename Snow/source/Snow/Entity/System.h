#pragma once

#include "Snow/Entity/Component.h"

#include "Snow/Core/Base.h"
#include "Snow/Core/Time/Timestep.h"

#include "Snow/Math/Math.h"

namespace Snow {
	namespace Entity {

		enum ComponentFlags {
			Optional = BIT(0)
		};

		class BaseSystem {
		public:
			

			BaseSystem() {}
			virtual void UpdateComponents(Core::Time::Timestep ts, BaseComponent** comps) {}
			inline const std::vector<Math::uint32_t>& GetComponentTypes() { return m_ComponentTypes; }
			inline const std::vector<Math::uint32_t>& GetComponentFlags() { return m_ComponentFlags; }
			inline bool IsValid();
		protected:
			void AddComponentType(Math::uint32_t componentType, uint32_t componentFlag = 0) { 
				m_ComponentTypes.push_back(componentType); 
				m_ComponentFlags.push_back(componentFlag); 
			}
		private:
			std::vector<Math::uint32_t> m_ComponentTypes;
			std::vector<Math::uint32_t> m_ComponentFlags;
		};

		class SystemList {
		public:
			bool AddSystem(BaseSystem& system);
			bool RemoveSystem(BaseSystem& system);

			inline size_t GetSize() { return m_Systems.size(); }
			inline BaseSystem* operator[](Math::uint32_t index) { return m_Systems[index]; }
		private:
			std::vector<BaseSystem*> m_Systems;
		};
	}
}
