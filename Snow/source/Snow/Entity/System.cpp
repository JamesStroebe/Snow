#include "spch.h"
#include "Snow/Entity/System.h"

namespace Snow {
	namespace Entity {
		bool BaseSystem::IsValid() {
			for (Math::uint32_t i = 0; i < m_ComponentFlags.size(); i++) {
				if ((m_ComponentFlags[i] & Optional) == 0) {
					return true;
				}
			}
			return false;
		}

		bool SystemList::AddSystem(BaseSystem& system) {
			if (!system.IsValid())
				return false;
			m_Systems.push_back(&system);
			return true;
		}

		bool SystemList::RemoveSystem(BaseSystem& system) {
			for (Math::uint32_t i = 0; i < m_Systems.size(); i++) {
				if (&system == m_Systems[i]) {
					m_Systems.erase(m_Systems.begin() + i);
					return true;
				}
			}
			return false;
		}
	}
}