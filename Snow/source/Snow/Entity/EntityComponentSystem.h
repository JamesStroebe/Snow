#pragma once

#include "Snow/Entity/Component.h"
#include "Snow/Entity/System.h"

#include "Snow/Math/Math.h"

#include <vector>
#include <map>
#include <utility>

namespace Snow {
	namespace Entity {

		typedef Math::uint32_t ComponentID;
		typedef Math::uint32_t ComponentIndex;
		typedef Math::uint32_t EntityID;
		typedef Math::uint32_t EntityIndex;
		typedef std::vector<std::pair<ComponentID, ComponentIndex>> EntityComponentList;
		typedef std::pair<EntityID, EntityComponentList>* EntityPtr;
		

		class EntityComponentSystem {
		public:
			static void Init();
			static void Shutdown();
			EntityComponentSystem() = default;
			~EntityComponentSystem() = default;

			// Entity Methods
			static EntityHandle CreateEntity(BaseComponent** components, const uint32_t* componentIDs, size_t numComponents);
			static void RemoveEntity(EntityHandle handle);

			template<class... Ts>
			static EntityHandle CreateEntity(Ts&... ts) {
				BaseComponent* components[] = { &ts... };
				Math::uint32_t componentIDs[] = { Ts::ID... };
				return CreateEntity(components, componentIDs, sizeof(componentIDs)/sizeof(componentIDs[0]));
			}

			// Component Methods
			template<class Comp>
			inline static void AddComponent(EntityHandle entity, Comp* component) { AddComponentInternal(entity, GetEntityHandle(entity), Comp::ID, component); }

			template<class Comp>
			inline static bool RemoveComponent(EntityHandle entity) { return RemoveComponentInternal(entity, Comp::ID); }

			template<class Comp>
			inline static Comp* GetComponent(EntityHandle entity) { 
				return (Comp*)GetComponentInternal(GetEntityComponentList(entity), s_Components[Comp::ID], Comp::ID); 
			}

			// System Methods

			static void UpdateSystems(SystemList& systemList, Core::Time::Timestep ts);
		private:
			static std::map<ComponentID, std::vector<uint8_t>> s_Components;
			static std::vector<EntityPtr> s_Entities;

			static inline EntityPtr RawTypeHandle(EntityHandle handle) {
				return (EntityPtr)handle;
			}

			static inline EntityIndex GetEntityIndex(EntityHandle handle) {
				return RawTypeHandle(handle)->first;
			}

			static inline EntityComponentList& GetEntityComponentList(EntityHandle handle) {
				return RawTypeHandle(handle)->second;
			}

			static void AddComponentInternal(EntityHandle handle, EntityComponentList& entity, ComponentID componentID, BaseComponent* component);
			static void DeleteComponent(ComponentID compID, ComponentIndex compIndex);
			static bool RemoveComponentInternal(EntityHandle handle, ComponentID componentID);
			static BaseComponent* GetComponentInternal(EntityComponentList& entityComponents, std::vector<Math::uint8_t>& array, ComponentID componentID);
			static void UpdateSystemMultipleComponents(ComponentIndex componentIndex, SystemList& systemList, Core::Time::Timestep ts, const std::vector<Math::uint32_t>& componentTypes, std::vector<BaseComponent*>& componentParam, std::vector<std::vector<Math::uint8_t>*>& componentArrays);
			static Math::uint32_t FindLeastCommonComponent(const std::vector<Math::uint32_t>& componentTypes, const std::vector<Math::uint32_t>& componentFlags);
		};
	}
}