#include "spch.h"
#include "Snow/ImGui/ImGuiLayer.h"

#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include <imgui.h>
#include <examples/imgui_impl_opengl3.h>
#include <examples/imgui_impl_dx11.h>
#include <examples/imgui_impl_win32.h>
#include <examples/imgui_impl_vulkan.h>

#include "Snow/Core/Application.h"
#include <Snow/Platform/Windows/WindowsWindow.h>

#include "Snow/Render/Renderer.h"
#include "Snow/Platform/DirectX/DirectXContext.h"
#include "Snow/Platform/OpenGL/OpenGLContext.h"


namespace Snow {
	namespace Imgui {
		ImGuiLayer::ImGuiLayer() :
			Layer("ImGuiLayer") {}

		void ImGuiLayer::OnAttach() {
			IMGUI_CHECKVERSION();
			ImGui::CreateContext();
			ImGuiIO& io = ImGui::GetIO(); (void)io;
			io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
			io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
			io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;

			ImFont* pFont = io.Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\segoeui.ttf", 18.0f);
			io.FontDefault = io.Fonts->Fonts.back();


			ImGui::StyleColorsClassic();

			ImGuiStyle& style = ImGui::GetStyle();
			if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
				style.WindowRounding = 0.0f;
				style.Colors[ImGuiCol_WindowBg].w = 1.0;
			}

#if SNOW_PLATFORM & SNOW_PLATFORM_WINDOWS64_BIT
			WindowsWindow* window = static_cast<WindowsWindow*>(Core::Application::GetWindow().get());
			HWND hwnd = window->GetNativeWindowHandle();
			
			ImGui_ImplWin32_Init(hwnd);
#endif

			switch (Render::Renderer::GetRenderAPI()) {
			case Render::RenderAPI::OPENGL: ImGui_ImplOpenGL3_Init("#version 410"); break;
			case Render::RenderAPI::DIRECTX: {
				DirectXContext* dxContext = static_cast<DirectXContext*>(Render::Renderer::GetContext());
				DirectXDevice* dxDevice = dxContext->GetDevice();
				ImGui_ImplDX11_Init(dxDevice->GetLogicalDevice(), dxContext->GetDeviceContextHandle());
				break;
			}
			case Render::RenderAPI::VULKAN: {
				SNOW_CORE_WARN("Vulkan Machine broke, try again"); break;
			}
			}
		}

		void ImGuiLayer::OnDetach() {
#if SNOW_PLATFORM & SNOW_PLATFORM_WINDOWS64_BIT
			ImGui_ImplWin32_Shutdown();
#endif
			switch (Render::Renderer::GetRenderAPI()) {
			case Render::RenderAPI::OPENGL: ImGui_ImplOpenGL3_Shutdown(); break;
			case Render::RenderAPI::DIRECTX:	ImGui_ImplDX11_Shutdown(); break;
			}
			ImGui::DestroyContext();
		}

		void ImGuiLayer::Begin() {
			switch (Render::Renderer::GetRenderAPI()) {
			case Render::RenderAPI::OPENGL: ImGui_ImplOpenGL3_NewFrame(); break;
			case Render::RenderAPI::DIRECTX:	ImGui_ImplDX11_NewFrame(); break;
			}
			
#if SNOW_PLATFORM & SNOW_PLATFORM_WINDOWS64_BIT
			ImGui_ImplWin32_NewFrame();
#endif

			ImGui::NewFrame();
		}

		void ImGuiLayer::End() {
			ImGuiIO& io = ImGui::GetIO();
			auto& app = Core::Application::Get();
			io.DisplaySize = ImVec2(app.GetWindow()->GetWidth(), app.GetWindow()->GetHeight());

			ImGui::Render();
			switch (Render::Renderer::GetRenderAPI()) {
			case Render::RenderAPI::OPENGL: ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData()); break;
			case Render::RenderAPI::DIRECTX:	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData()); break;
			}

			if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
#if SNOW_PLATFORM & SNOW_PLATFORM_WINDOWS64_BIT
				auto window = static_cast<WindowsWindow*>(Core::Application::Get().GetWindow().get());
				
				ImGui::UpdatePlatformWindows();
				ImGui::RenderPlatformWindowsDefault();
				switch (Render::Renderer::GetRenderAPI()) {
				case Render::RenderAPI::OPENGL: {
					OpenGLContext* glContext = static_cast<OpenGLContext*>(Render::Renderer::GetContext());
					wglMakeCurrent(window->GetDeviceContextHandle(), glContext->GetHGLRC());
					break;
				}
				}
#endif
			}
		}

		void ImGuiLayer::OnEvent(Core::Event::Event& event) {
			Core::Event::EventDispatcher dispatcher(event);
			dispatcher.Dispatch<Core::Event::MouseButtonPressedEvent>(SNOW_BIND_EVENT_FN(ImGuiLayer::OnMouseButtonPressedEvent));
			dispatcher.Dispatch<Core::Event::MouseButtonReleasedEvent>(SNOW_BIND_EVENT_FN(ImGuiLayer::OnMouseButtonReleasedEvent));
			dispatcher.Dispatch<Core::Event::MouseMovedEvent>(SNOW_BIND_EVENT_FN(ImGuiLayer::OnMouseMovedEvent));
			dispatcher.Dispatch<Core::Event::MouseScrolledEvent>(SNOW_BIND_EVENT_FN(ImGuiLayer::OnMouseScrolledEvent));
			dispatcher.Dispatch<Core::Event::KeyPressedEvent>(SNOW_BIND_EVENT_FN(ImGuiLayer::OnKeyPressedEvent));
			dispatcher.Dispatch<Core::Event::KeyReleasedEvent>(SNOW_BIND_EVENT_FN(ImGuiLayer::OnKeyReleasedEvent));
			dispatcher.Dispatch<Core::Event::KeyTypedEvent>(SNOW_BIND_EVENT_FN(ImGuiLayer::OnKeyTypedEvent));
			dispatcher.Dispatch<Core::Event::WindowResizeEvent>(SNOW_BIND_EVENT_FN(ImGuiLayer::OnWindowResizeEvent));
		}

		bool ImGuiLayer::OnMouseButtonPressedEvent(Core::Event::MouseButtonPressedEvent& e) {
			ImGuiIO& io = ImGui::GetIO();
			io.MouseDown[e.GetMouseButton()] = true;

			return false;
		}

		bool ImGuiLayer::OnMouseButtonReleasedEvent(Core::Event::MouseButtonReleasedEvent& e) {
			ImGuiIO& io = ImGui::GetIO();
			io.MouseDown[e.GetMouseButton()] = false;

			return false;
		}

		bool ImGuiLayer::OnMouseMovedEvent(Core::Event::MouseMovedEvent& e) {
			ImGuiIO& io = ImGui::GetIO();
			io.MousePos = ImVec2(e.GetX(), e.GetY());

			return false;
		}

		bool ImGuiLayer::OnMouseScrolledEvent(Core::Event::MouseScrolledEvent& e) {
			ImGuiIO& io = ImGui::GetIO();
			io.MouseWheelH += e.GetXOffset();
			io.MouseWheel += e.GetYOffset();

			return false;
		}

		bool ImGuiLayer::OnKeyPressedEvent(Core::Event::KeyPressedEvent& e) {
			ImGuiIO& io = ImGui::GetIO();
			io.KeysDown[e.GetKeyCode()] = true;

			io.KeyCtrl = io.KeysDown[SNOW_KEY_LCONTROL] || io.KeysDown[SNOW_KEY_RCONTROL];
			io.KeyAlt = io.KeysDown[SNOW_KEY_LALT] || io.KeysDown[SNOW_KEY_RALT];
			io.KeyShift = io.KeysDown[SNOW_KEY_LSHIFT] || io.KeysDown[SNOW_KEY_RSHIFT];

			return false;
		}

		bool ImGuiLayer::OnKeyReleasedEvent(Core::Event::KeyReleasedEvent& e) {
			ImGuiIO& io = ImGui::GetIO();
			io.KeysDown[e.GetKeyCode()] = false;

			return false;
		}

		bool ImGuiLayer::OnKeyTypedEvent(Core::Event::KeyTypedEvent& e) {
			ImGuiIO& io = ImGui::GetIO();
			Math::int32_t keycode = e.GetKeyCode();
			if (keycode > 0 && keycode < 0x10000)
				io.AddInputCharacter((Math::uint32_t)keycode);

			return false;
		}

		bool ImGuiLayer::OnWindowResizeEvent(Core::Event::WindowResizeEvent& e) {
			return false;
		}
	}
}