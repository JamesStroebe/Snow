#pragma once

#include "Snow/Core/Layer.h"

#include "Snow/Render/API/Context.h"

namespace Snow {
	namespace Imgui {
		class ImGuiLayer : public Core::Layer {
		public:
			ImGuiLayer();
			virtual ~ImGuiLayer() = default;

			void Begin();
			void End();

			virtual void OnAttach() override;
			virtual void OnDetach();
			virtual void OnUpdate(Core::Time::Timestep ts) {}
			virtual void OnRender() {}
			virtual void OnImGuiRender() {}
			virtual void OnEvent(Core::Event::Event& event);
		private:
			bool OnMouseButtonPressedEvent(Core::Event::MouseButtonPressedEvent& e);
			bool OnMouseButtonReleasedEvent(Core::Event::MouseButtonReleasedEvent& e);
			bool OnMouseMovedEvent(Core::Event::MouseMovedEvent& e);
			bool OnMouseScrolledEvent(Core::Event::MouseScrolledEvent& e);
			bool OnKeyPressedEvent(Core::Event::KeyPressedEvent& e);
			bool OnKeyReleasedEvent(Core::Event::KeyReleasedEvent& e);
			bool OnKeyTypedEvent(Core::Event::KeyTypedEvent& e);
			bool OnWindowResizeEvent(Core::Event::WindowResizeEvent& e);
		};
	}
}