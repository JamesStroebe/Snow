///////////////////////////////////////////
// File: EntryPoint.h
//
// Original Author: James Stroebe
//
// Creation Date: 09/01/2020
//
///////////////////////////////////////////

#pragma once

#include "Snow/Core/Base.h"
#include "Setup.h"

extern Snow::Core::Application* Snow::Core::CreateApplication();

#if SNOW_PLATFORM & SNOW_PLATFORM_WINDOWS32_BIT
int main(int* argc, char** argv) {
	Snow::Core::InitializeCore();
	Snow::Core::Application* app = Snow::Core::CreateApplication();
	app->Run();
	Snow::Core::ShutdownCore();
	delete app;
}
#endif
