#pragma once

#include "Allocator.h"

#pragma warning(disable : 4595)

void* operator new(size_t size) {
	return Snow::System::Allocator::Allocate(size);
}

inline void* operator new[](size_t size) {
	return Snow::System::Allocator::Allocate(size);
}

inline void operator delete(void* memory) {
	return Snow::System::Allocator::Free(memory);
}

inline void operator delete[](void* memory) {
	return Snow::System::Allocator::Free(memory);
}

#pragma warning(default : 4595)
