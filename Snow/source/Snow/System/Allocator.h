#pragma once

//#include "Snow/Core/Log.h"

#include <malloc.h>
#include <memory>

namespace Snow {
	namespace System {
		class Allocator {
		public:
			static void* Allocate(size_t size);
			static void* AllocateDebug(size_t size, const char* file, uint32_t line);

			static void Free(void* block);
		};
	}
}

