#include "spch.h"
#include "Snow/System/Allocator.h"

#include <iostream>

namespace Snow {
	namespace System {
		void* Allocator::Allocate(size_t size) {
			std::cout << (int)size;
			//SNOW_CORE_TRACE("Allocating {0} bytes", size);
			void* mem = malloc(size);
			return mem;

		}

		void* Allocator::AllocateDebug(size_t size, const char* file, uint32_t line) {
			return Allocate(size);
		}

		void Allocator::Free(void* block) {
			free(block);
		}
	}
}