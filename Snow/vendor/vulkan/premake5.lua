project "Vulkan"
    kind "StaticLib"
    language "C++"
    staticruntime "On"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

    files {
    }

    includedirs {
        "Include",
        --"shaderc"
    }

    links {
        "Lib/vulkan-1.lib",
        --"Lib/shaderc_combined.lib"
	}
   